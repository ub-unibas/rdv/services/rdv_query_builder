# rdv-query-builder

This middle layer is called by the rdv client (the viewer). It translates queries and interact with elastic.

This could be installed locally or ran via docker-compose which installs the whole stack

## Local Installation instructions

 * Tunnel to Elastic
   * ssh -f -N -L 9209:sb-ues5:8080 USERNAME@sb-ues5.swissbib.unibas.ch
 * If needed to inspect data, tunnel to Kibana
   * ssh -f -N -L 5609:sb-ues5:5601 USERNAME@sb-ues5.swissbib.unibas.ch
 * you need the following environment variables / k8s-secrets:
   * MONGO_HOST
   * MONGO_TEST_HOST
   * MONGO_SWAPDF_TEST_HOST
   * MONGO_ITB_LOC_HOST
   * GOOGLE_DOC_API_KEY

### Python setup
  * setup virtual environment: python3 -m venv ./venv
  * install requirements (make sure to use UBIT pypi on gitlab): ./venv/bin/python3 -m pip install -r requirements.txt

### Run service
  * export RDV_DEBUG=1
  * start service: ./venv/bin/python3 uwsgi_rdv.py
  * check: http://127.0.0.1:5000/v1/doc/
  * Example for Afrika Portal http://127.0.0.1:5000/v1/rdv_object/object_view/afrikaportal-loc/ubsalma_9913924500105504/de
  * or http://127.0.0.1:5000/v1/rdv_object/object_view/afrikaportal-loc/babarchive2022_ce1f2401-4db4-5e86-95fa-aefbc9e54d12/de
  * or http://127.0.0.1:5000/v1/rdv_object/object_view/afrikaportal-compose/bablibrary2022_8f1b9ae5-c4c9-5d2f-9173-a95d7f225649/de



## Run the full stack locally with docker-compose

With some minimal preparations, you can run the whole stack locally (mongo, redis, elasticsearch and the viewer, as well as a kibana instance to inspect the data)

### Set the GOOGLE_DOC_API_KEY

Set the environment variable (replace XXXXX with the correct values) 

```
export GOOGLE_DOC_API_KEY='{
        "type": "service_account",
        "project_id": "rdviewer",
        "private_key_id": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
        "private_key": "-----BEGIN PRIVATE KEY-----\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n-----END PRIVATE KEY-----\n",
        "client_email": "viewmapping@rdviewer.iam.gserviceaccount.com",
        "client_id": "101115577980920887427",
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/viewmapping%40rdviewer.iam.gserviceaccount.com"
      }'
```

### Local preparations to run elastic on docker

The elastic data will be stored on the host computer and mounted in the container. `esdata01` and `esdata02` are the directories where the elasticsearch data is stored. They are mounted in docker-compose.
You need to do the following, from the home directory of the rdv_query_builder
```
mkdir esdata01
mkdir esdata02
chmod g+rwx esdata01
chmod g+rwx esdata02
sudo chgrp 0 esdata01
sudo chgrp 0 esdata02
```
[Learn more on this](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html#_configuration_files_must_be_readable_by_the_elasticsearch_user)

On top of that it is necessary to increase the `vm.max_map_count`

If you want to increase temporarily : 
```
sudo sysctl -w vm.max_map_count=262144
```

Or to permanently change the value of the `vm.max_map_count` setting, update the value in /etc/sysctl.conf.

[Learn more on this](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html#_set_vm_max_map_count_to_at_least_262144)

### Run the service

Then do 
```
make up
```
Then the rdv_query_builder is available on http://127.0.0.1:5000

The project which is working on docker-compose is `afrikaportal-compose`. You can load some data when running the tests.

You can also access : 
 * [elastic](http://localhost:9209)
 * [kibana](http://localhost:5610)
 * [rdv viewer](http://127.0.0.1:4200)


In intellij/pycharm, you can also click the small green arrows in the 2nd line of the file docker-compose.yml and you can see all containers in docker window.

In the swagger documentation, you need to use the project value `afrikaportal-compose` to get results.

At first, the elastic index is empty. If you run once the tests (cf. below), then the elastic index is filled with some sample data.


### Run the tests

```
make test
```

For the moment, you should see

```
test_rdv/test_lio.py::test_rdv_requests
test_rdv/test_lio.py::test_rdv_requests
  /rdv_query_builder/base/rdv_esquery.py:975: DeprecationWarning: The 'body' parameter is deprecated for the 'search' API and will be removed in a future version. Instead use API parameters directly. See https://github.com/elastic/elasticsearch-py/issues/1698 for more information
    result = self.es.search(index=self.index, body=self.body)

============= 2 passed, 20 warnings in 68.70s (0:01:08) =============
```
And then there is an error because of the missing connection to the IIIF service.

## Debug and run in PyCharm with docker-compose remote interpreter

[PyCharm Documentation](https://www.jetbrains.com/help/pycharm/using-docker-compose-as-a-remote-interpreter.html)

PyCharm is a bit confusing when configuring the Docker Compose intepreter. Here are some screenshots.

Add interpreter based on Docker Compose

![Capture d’écran du 2022-12-20 16-16-41.png](doc%2Fscreenshots%2FCapture%20d%E2%80%99%C3%A9cran%20du%202022-12-20%2016-16-41.png)


Define the service and add the GOOGLE_DOC_API_KEY as environment variable
![Capture d’écran du 2022-12-20 16-17-45.png](doc%2Fscreenshots%2FCapture%20d%E2%80%99%C3%A9cran%20du%202022-12-20%2016-17-45.png)

Choose a system interpreter and use the python version which is installed in the container. You need to add a path, enter `/usr/local/bin/python` 
![Capture d’écran du 2022-12-20 16-18-32.png](doc%2Fscreenshots%2FCapture%20d%E2%80%99%C3%A9cran%20du%202022-12-20%2016-18-32.png)


Select the new path just entered (which is not automatically selected), even if it is marked invalid
![Capture d’écran du 2022-12-20 16-19-13.png](doc%2Fscreenshots%2FCapture%20d%E2%80%99%C3%A9cran%20du%202022-12-20%2016-19-13.png)

The the python interpreter for this project will be updated.

On top of that, so that debugging is working, you need to define the path mappings (which files from the container correspond to files in your local system)

This is done in the pycharm docker configuration

![Capture d’écran du 2022-12-20 16-27-34.png](doc%2Fscreenshots%2FCapture%20d%E2%80%99%C3%A9cran%20du%202022-12-20%2016-27-34.png)

You can also define that in PyCharm run configuration (but you need to define it for each run configuration)

To run the application, you can click on the small green arrow (run arrow) in the file uwsgi_rdv.py. Or use a run configuration as this one

![Capture d’écran du 2022-12-20 16-34-48.png](doc%2Fscreenshots%2FCapture%20d%E2%80%99%C3%A9cran%20du%202022-12-20%2016-34-48.png)

Please note that docker-compose uses the development Dockerfile `dev.Dockerfile` to use different entrypoints and commands. This allows debugging in PyCharm, which doesn't work with gunicorn entrypoint.

## Examples on kubernetes

* https://test.rdv-query-builder.ub-digitale-dienste.k8s-001.unibas.ch/v1/rdv_object/object_view/afrikaportal-test/ubsalma_9913924500105504/de

## Example POST request (search)

```shell
curl --header "Content-type: application/json" --request POST 'http://127.0.0.1:5000/v1/rdv_query/es_proxy/afrikaportal-loc/' --data '
{
  "query_params": {
    "size": 10,
    "sort": [
      {
        "field": "_score",
        "order": "desc"
      }
    ],
    "start": 0,
    "source": []
  },
  "query": {
    "bool": {
      "must": [
        {
          "query_string": {
            "query": [
              "afrika"
            ],
            "op": "AND"
          }
        }
      ]
    }
  },
  "facets": {
    "fct_countryname": {
      "field": "fct_countryname.keyword",
      "values": [],
      "operator": "OR",
      "order": "count",
      "facet_type": "basic",
      "size": 10
    },
    "fct_institution": {
      "field": "fct_institution.keyword",
      "values": [],
      "operator": "OR",
      "order": "count",
      "facet_type": "basic",
      "size": 10
    },
    "fct_location": {
      "field": "fct_location.keyword",
      "values": [],
      "operator": "OR",
      "order": "count",
      "facet_type": "basic",
      "size": 10
    },
    "hidden_digitized": {
      "field": "hidden_digitized.keyword",
      "values": [],
      "operator": "OR",
      "order": "count",
      "facet_type": "basic",
      "size": 10
    },
    "fct_mediatype": {
      "field": "fct_mediatype.keyword",
      "values": [],
      "operator": "OR",
      "order": "count",
      "facet_type": "basic",
      "size": 10
    },
    "fct_person_organisation": {
      "field": "fct_person_organisation.keyword",
      "values": [],
      "operator": "OR",
      "order": "count",
      "facet_type": "basic",
      "size": 10
    },
    "fct_topic": {
      "field": "fct_topic.keyword",
      "values": [],
      "operator": "OR",
      "order": "count",
      "facet_type": "basic",
      "size": 10
    },
    "fct_date": {
      "field": "fct_date",
      "operator": "AND",
      "values": [],
      "show_aggs": true,
      "size": 31,
      "facet_type": "date"
    },
    "hierarchy_filter.keyword": {
      "field": "hierarchy_filter.keyword",
      "values": [],
      "operator": "AND",
      "facet_type": "hierarchy",
      "size": 100
    }
  },
  "lang": "fr",
  "open_facets": {},
  "viewer": "List"
}
'
```


## Some documentation
  * https://ub-basel.atlassian.net/wiki/spaces/RDV/pages/1851260951/Beispielprojekt+Fortbildungsdatenbank+Personaldatenbank
  * https://ub-basel.atlassian.net/wiki/spaces/RDV/pages/1855455264/RDV+Middlelayer

## Restart

The middleware runs on ub-rdv.ub.unibas.ch. To restart the dev service :  

```sudo service rdv2es_dev restart```

## Deployment on ub-rdv

Go to ub-rdv.ub.unibas.ch

Use rdv user and do

```
rdv@ub-rdv:/users/staff/ub/reisache$ ~/dev_rdv_proxy_ubit/bin/python3 -m pip install --upgrade rdv_query_builder
```

## Generate test data automatically

It is possible to generate test data automatically. Once you have a running instance locally (either with all services local or via docker-compose), you can do the following : 

in `test_builder.py`, set TESTS = TRUE (line 6)

Then click on various places in the interface (rdv_viewer). This will record the queries sent to the query_builder (`input_test.json`) and the response returned (`output_test.json`) in the folder defined in the variable `test_data_path` of `test_builder.py`

You can then use this data to write tests or run tests based on this data using `test_rdv/utilities/test_api.py`

## SWA-PDFs

* GD_SERVICE_FILE: Service File to access Google Spreadsheets Config
* PDF_UPLOAD_FOLDER: Folder from which pdfs can be uploaded only by their name
* PDF_STORE_FOLDER: Folder where uploaded pdfs are stored, for testing "/opt/rdv/test_upload_swa"
* ALMA_API_KEY: read only API Key to get fast NZ-ID <-> IZ-ID concordance
* UPLOAD_PREFIX: Protocol / Domain through which uploaded pdfs can be accessed
* SWA_MARC_GDSHEET: Marc Spreadsheet that contains the transformation rules
* MONGO_HOST: Mongo Host containing password and username mongodb://***:***@dd-db5.ub.unibas.ch:29017


