# rdv-query-builder

This middle layer is called by the rdv client. It translates queries and interact with elastic.

## Installation

 * Setup from pypi to use internal ub repos : https://wiki.biozentrum.unibas.ch/pages/viewpage.action?spaceKey=UBIT&title=ub-dataservice#ubdataservice-ub-pypi.ub.unibas.ch 
 * Tunnel to Elastic
   * ssh -f -N -L 9209:sb-ues5:8080 USERNAME@sb-ues5.swissbib.unibas.ch
 * If needed to inspect data, tunnel to Kibana
   * ssh -f -N -L 5609:sb-ues5:5601 USERNAME@sb-ues5.swissbib.unibas.ch
 * you need the following environment variables / k8s-secrets:
   * MONGO_HOST
   * MONGO_TEST_HOST
   * MONGO_SWAPDF_TEST_HOST
   * MONGO_ITB_LOC_HOST
   * GOOGLE_DOC_API_KEY

## Python setup
  * setup virtual environment: python3 -m venv ./venv
  * install requirements (make sure to use UBIT pypi on gitlab): ./venv/bin/python3 -m pip install -r requirements.txt

## Run service
  * set RDV_DEBUG=1
  * start service: ./venv/bin/python3 uwsgi_rdv.py
  * check: http://127.0.0.1:5000/v1/doc/
  * Example for Afrika Portal http://127.0.0.1:5000/v1/rdv_object/object_view/afrikaportal-loc/ubsalma_9913924500105504/de

## Examples on kubernetes

* https://test.rdv-query-builder.ub-digitale-dienste.k8s-001.unibas.ch/v1/rdv_object/object_view/afrikaportal-test/ubsalma_9913924500105504/de

## Example POST request (search)

```shell
curl --header "Content-type: application/json" --request POST 'http://127.0.0.1:5000/v1/rdv_query/es_proxy/afrikaportal-loc/' --data '{"query_params":{"size":200,"sort":[{"field":"_score","order":"desc"}],"start":0,"source":[]},"query":{"bool":{"must":[{"query_string":{"query":["afrika"],"op":"AND"}}]}},"facets":{"fct_countryname":{"field":"fct_countryname.keyword","values":[],"operator":"OR","order":"count","facet_type":"basic","size":10},"fct_institution":{"field":"fct_institution.keyword","values":[],"operator":"OR","order":"count","facet_type":"basic","size":10},"fct_location":{"field":"fct_location.keyword","values":[],"operator":"OR","order":"count","facet_type":"basic","size":10},"hidden_digitized":{"field":"hidden_digitized.keyword","values":[],"operator":"OR","order":"count","facet_type":"basic","size":10},"fct_mediatype":{"field":"fct_mediatype.keyword","values":[],"operator":"OR","order":"count","facet_type":"basic","size":10},"fct_person_organisation":{"field":"fct_person_organisation.keyword","values":[],"operator":"OR","order":"count","facet_type":"basic","size":10},"fct_topic":{"field":"fct_topic.keyword","values":[],"operator":"OR","order":"count","facet_type":"basic","size":10},"fct_date":{"field":"fct_date","operator":"AND","values":[],"show_aggs":true,"size":31,"facet_type":"date"},"hierarchy_filter.keyword":{"field":"hierarchy_filter.keyword","values":[],"operator":"AND","facet_type":"hierarchy","size":100}},"lang":"fr","open_facets":{},"viewer":"List"}'
```

## Some documentation
  * https://ub-basel.atlassian.net/wiki/spaces/RDV/pages/1851260951/Beispielprojekt+Fortbildungsdatenbank+Personaldatenbank
  * https://ub-basel.atlassian.net/wiki/spaces/RDV/pages/1855455264/RDV+Middlelayer

## Restart

The middleware runs on ub-rdv.ub.unibas.ch. To restart the dev service :  

```sudo service rdv2es_dev restart```


## Deployment on ub-rdv

Go to ub-rdv.ub.unibas.ch

Use rdv user and do

```
rdv@ub-rdv:/users/staff/ub/reisache$ ~/dev_rdv_proxy_ubit/bin/python3 -m pip install --upgrade rdv_query_builder
```



## SWA-PDFs

* GD_SERVICE_FILE: Service File to access Google Spreadsheets Config
* PDF_UPLOAD_FOLDER: Folder from which pdfs can be uploaded only by their name
* PDF_STORE_FOLDER: Folder where uploaded pdfs are stored, for testing "/opt/rdv/test_upload_swa"
* ALMA_API_KEY: read only API Key to get fast NZ-ID <-> IZ-ID concordance
* UPLOAD_PREFIX: Protocol / Domain through which uploaded pdfs can be accessed
* SWA_MARC_GDSHEET: Marc Spreadsheet that contains the transformation rules
* MONGO_HOST: Mongo Host containing password and username mongodb://***:***@dd-db5.ub.unibas.ch:29017

## How to generate test data and run tests

