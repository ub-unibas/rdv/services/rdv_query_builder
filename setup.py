#!/usr/bin/python3

import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()


setuptools.setup(
    name="rdv_query_builder",
    setup_requires=['setuptools-git-versioning'],
    setuptools_git_versioning={
        "enabled": True,
    },
    author="Martin Reisacher",
    author_email="martin.reisacher@unibas.ch",
    description="prepare ES Data for rdv",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/ub-unibas/rdv/services/rdv_query_builder",
    # pycryptodome wegen Upload von https://www.electrosuisse.ch/wp-content/uploads/2021/06/Broschuere_E-Mobilitaet-%E2%80%93-Der-Antrieb-der-Zukunft.pdf
    install_requires=['elasticsearch<8', 'openpyxl',
                      'flask<2.2', 'flask_cors', 'flask_compress', 'flask-restx',  # remove flask version fixing as soon as python3.8 is minimum on all servers running rdv
                      'markupsafe', 'text-unidecode', 'pymongo',  'pytest',
                      'requests',  'feedparser',  'jsonschema', 'uwsgi',
                      'python-dateutil', 'PyPDF2', 'pycryptodome', 'pathvalidate', 'pyyaml', 'cryptography',
                      'pygsheets', 'rdv_marc_ubit', 'cache_decorator_redis_ubit', 'gdspreadsheets_ubit',
                      'rdv_data_helpers_ubit', 'rdv_config_store_ubit', 'cryptography',
                      'rdv_querybuilder_afrikaportal_ubit', 'rdv_config_store_ubit',
                      'rdv_querybuilder_zas_ubit', 'rdv_es_dsl_ubit'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    #package_dir={"": "logging-decorator"},
    packages=setuptools.find_packages(),
    python_requires=">=3.6",
    include_package_data=True,
    zip_safe=False
)
