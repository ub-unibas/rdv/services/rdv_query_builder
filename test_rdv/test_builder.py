from flask import request
import os
import json
import hashlib

TESTS = False

def build_test_case(func):

    import functools

    @functools.wraps(func)
    def decorated_function(self_obj, *args, build_test=True, **kwargs):
        if build_test and TESTS:

            input_test = "input_test.json"
            output_test = "output_test.json"

            test_data_path = "/home/martin/PycharmProjects/rdv_query_builder/test/data"
            # todo class name für classmethods anpassen
            class_name = type(self_obj).__name__
            url_path_parts = request.url.split("/")[3:]
            url_path = "/".join(url_path_parts)
            fn_name = func.__name__
            test_class_path = os.path.join(test_data_path, class_name)
            #build_test=False,
            results = func(self_obj, *args, **kwargs)
            if fn_name == "get":
                url_concat = "-".join(url_path_parts)
                test_url_path = os.path.join(test_class_path, fn_name, url_concat)
                if not os.path.exists(test_url_path):
                    os.makedirs(test_url_path)
                    with open(os.path.join(test_url_path, input_test), "w") as input_file:
                        input_file.write("/" + url_path)
                    with open(os.path.join(test_url_path, output_test), "w") as output_file:
                        json.dump(results, output_file)
            elif fn_name == "post":
                url_path = "/".join(url_path_parts)
                payload = json.loads(request.get_data().decode("utf-8"))
                check_sum = hashlib.md5(json.dumps(payload, sort_keys=True).encode('utf-8')).hexdigest()
                test_url_path = os.path.join(test_class_path, fn_name, url_path, check_sum)
                if not os.path.exists(test_url_path):
                    os.makedirs(test_url_path)
                    with open(os.path.join(test_url_path, input_test), "w") as input_file:
                        json.dump(payload, input_file)
                    with open(os.path.join(test_url_path, output_test), "w") as output_file:
                        result = results.json if hasattr(results, "json") else results
                        json.dump(result, output_file)
        # , build_test=False
        return func(self_obj, *args, **kwargs)
    return decorated_function