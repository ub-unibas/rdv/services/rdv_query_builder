import pytest
import json
import os
import traceback
from elasticsearch import Elasticsearch, helpers

@pytest.fixture
def client():
    from uwsgi_rdv import rdv_query_app
    rdv_query_app.debug = True
    rdv_query_app.config['TESTING'] = True
    with rdv_query_app.test_client() as client:
        yield client

@pytest.fixture
def load_sample_documents():
    print("Load test data")
    all_documents = json.load(open("/app/test_rdv/data/sample_data.json"))
    esclient = Elasticsearch("es01:9200")
    for document in all_documents:
        esclient.index(index="afrikaportal", document=document, id=document["identifier"])

@pytest.fixture
def load_sample_thesaurus():
    print("Load test thesaurus data")
    all_thesaurus_entries = json.load(open("/app/test_rdv/data/sample_data_thesaurus.json"))
    esclient = Elasticsearch("es01:9200")
    for document in all_thesaurus_entries:
        # The index name must be the same as in sample_data_thesaurus.json, fields identifier and hierarchy_filter
        esclient.index(index="babthesaurus2022", document=document, id=document["identifier"])


def test_one_simple_rdv_request(load_sample_documents, load_sample_thesaurus):

    from uwsgi_rdv import rdv_query_app

    # necessary to propagate exceptions
    rdv_query_app.testing = True

    result = rdv_query_app.test_client().post("/v1/rdv_query/es_proxy/afrikaportal-compose/", json=json.load(open("/app/test_rdv/data/query.json")))
    result_dict = json.loads(result.data)

    expected = json.load(open("/app/test_rdv/data/result.json"))
    assert result_dict["hits"] == expected["hits"]
    assert result_dict["facets"] == expected["facets"]
    # failing for the moment
    # assert result_dict == expected

def test_multiple_rdv_requests(load_sample_documents, load_sample_thesaurus):
    test_data_path = "/app/test_rdv/data/generated_test_data"
    from uwsgi_rdv import rdv_query_app

    input_test = "input_test.json"
    output_test = "output_test.json"

    for class_dir in os.listdir(test_data_path):
        class_path = os.path.join(test_data_path, class_dir)
        for http_method in os.listdir(class_path):
            http_method_path = os.path.join(class_path, http_method)
            if http_method == "get":
                for test_dir in os.listdir(http_method_path):
                    input_path = os.path.join(http_method_path, test_dir, input_test)
                    print("testing " + input_path)
                    output_path = os.path.join(http_method_path, test_dir, output_test)
                    url_path = open(input_path).read()
                    rv = rdv_query_app.test_client().get(url_path)
                    assert json.loads(rv.data) == json.load(open(output_path))
            elif http_method == "post":
                for root, dirs, files in os.walk(http_method_path):
                    if input_test in files and output_test in files:
                        input_path = os.path.join(root, input_test)
                        output_path = os.path.join(root, output_test)
                        root_without_id = "/".join(root.split("/")[:-1])
                        url_path = root_without_id.replace(http_method_path, "") + "/"
                        rv = rdv_query_app.test_client().post(url_path, json=json.load(open(input_path)))

                        assert json.loads(rv.data) == json.load(open(output_path))

