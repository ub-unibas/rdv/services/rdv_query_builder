import pytest
import json
import os
import traceback


@pytest.fixture
def client():
    from uwsgi_rdv import rdv_query_app
    rdv_query_app.debug = False
    rdv_query_app.config['TESTING'] = True
    with rdv_query_app.test_client() as client:
        yield client



def test_rdv_requests():
    """Start with a blank database."""
    test_data_path = "/home/martin/PycharmProjects/rdv_query_builder/test/data"
    from uwsgi_rdv import rdv_query_app

    input_test = "input_test.json"
    output_test = "output_test.json"

    for class_dir in os.listdir(test_data_path):
        class_path = os.path.join(test_data_path, class_dir)
        for http_method in os.listdir(class_path):
            http_method_path = os.path.join(class_path, http_method)
            if http_method == "get":
                for test_dir in os.listdir(http_method_path):
                    input_path = os.path.join(http_method_path, test_dir, input_test)
                    output_path = os.path.join(http_method_path, test_dir, output_test)
                    url_path = open(input_path).read()
                    rv = rdv_query_app.test_client().get(url_path)
                    json.load(open(output_path))
                    try:
                        assert json.loads(rv.data) == json.load(open(output_path))
                        print(f"passed: {output_path}")
                    except AssertionError:
                        print(f"Test-Error: {output_path}")
                        old_data = json.load(open(output_path))
                        for key, value in json.loads(rv.data).items():
                            if value != old_data.get(key):
                                pass
                                #print(key, value, old_data)
                        # raise AssertionError
                        # traceback.print_exc()
            elif http_method == "post":
                for root, dirs, files in os.walk(http_method_path):
                    if input_test in files and output_test in files:
                        input_path = os.path.join(root, input_test)
                        output_path = os.path.join(root, output_test)
                        root_without_id = "/".join(root.split("/")[:-1])
                        url_path = root_without_id.replace(http_method_path, "") + "/"
                        rv = rdv_query_app.test_client().post(url_path, json=json.load(open(input_path)))
                        try:
                            assert json.loads(rv.data) == json.load(open(output_path))
                            print(f"passed: {output_path}")
                        except AssertionError:
                            print(f"Test-Error: {output_path}")
                            # raise AssertionError
                            #traceback.print_exc()
                            #import sys
                            #sys.exit(1)
                            #print(rv.data)
                            #print(open(output_path).read())


test_rdv_requests()