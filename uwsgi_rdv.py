import os
import socket
import logging
from flask import Flask
from flask.logging import default_handler
from flask_cors import CORS
from flask_compress import Compress

from rdv_apis import api_v1, rdv_api_v1, home_page, api_v2, rdv_api_v2

rdv_query_app = Flask(__name__)
rdv_query_app.register_blueprint(api_v1)
rdv_query_app.register_blueprint(api_v2)
rdv_query_app.register_blueprint(home_page)

# for changes here, copy uwsgi_rdv to destination (simple deployment not working) - mre workflow
origins = [
    "https?://127.0.0.1.*",
    "https?://localhost.*",
    "https?://ub-.*.unibas.ch",
    "https?://192.168.128.*",
    "https?://.*.ub-digitale-dienste.k8s-001.unibas.ch",
    "https?://.*.ub-dd-test.k8s-001.unibas.ch",
    "https?://.*.ub-dd-prod.k8s-001.unibas.ch"
]
CORS(rdv_query_app, origins=origins, supports_credentials=True)
Compress(rdv_query_app)

debug = os.environ.get('RDV_DEBUG')

# If app is started via gunicorn
if __name__ != '__main__':
    from uwsgi_rdv import rdv_query_app
    gunicorn_logger = logging.getLogger('gunicorn.logger')
    rdv_query_app.logger.handlers = gunicorn_logger.handlers
    rdv_query_app.logger.setLevel(gunicorn_logger.level)
    rdv_query_app.logger.removeHandler(default_handler)
    rdv_query_app.logger.info('Starting production server')

# to run it locally
if __name__ == "__main__":
    # Flask App
    api_server = "local"
    if os.environ.get('RDV_DEBUG') and api_server:
        api_settings = {
            "local": "127.0.0.1:5000",
            "test": "ub-rdv-test-proxy.ub.unibas.ch:443",
            "stable": "ub-rdv-proxy.ub.unibas.ch:443"
        }
        rdv_query_app.config['SERVER_NAME'] = api_settings.get(api_server)

        rdv_api_v1.title = "{} {}".format(api_server, rdv_api_v1.title)
        rdv_api_v2.title = "{} {}".format(api_server, rdv_api_v2.title)

        def see_json():
            from flask import json
            urlvars = False  # Build query strings in URLs
            swagger = True  # Export Swagger specifications
            for api in [rdv_api_v1, rdv_api_v2]:
                data = api.as_postman(urlvars=urlvars, swagger=swagger)
                with open("postman_{}_{}.json".format(api_server,api.app.name),"w") as postman:
                    json.dumps(data)
                    json.dump(data, postman)
        with rdv_query_app.app_context():
            see_json()
        rdv_query_app.run(debug=debug, host='0.0.0.0')
    else:
        rdv_query_app.run(debug=debug)
