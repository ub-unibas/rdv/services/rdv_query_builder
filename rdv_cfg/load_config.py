import os
import yaml
from pkg_resources import Requirement, resource_filename

#from flask import current_app
#mode = current_app.get("ENV_MODE")

def get_api_datalayer_connection(mode: str):
    config_file = resource_filename(Requirement.parse("rdv_query_builder"), "rdv_cfg/{}.yaml".format(mode))
    if os.path.exists(config_file):
        api_datalayer_access = yaml.load(open(config_file))
    else:
        raise Exception("Config File {} not found".format(config_file))
    return api_datalayer_access

def get_project_config(proj_file_path: str="", project=""):
    if proj_file_path:
        proj_config_file = proj_file_path
    else:
        proj_config_file = resource_filename(Requirement.parse("rdv_query_builder"), "rdv_query_builder/projects/refactured/{0}/cfg/{0}.yaml".format(project))
    if os.path.exists(proj_config_file):
        proj_config = yaml.load(open(proj_config_file))
    else:
        return None
        raise Exception("Proj Config File {} not found".format(proj_config_file))
    return proj_config

def build_config(proj_file_path: str="", project="", mode: str = ""):
    config = {}
    config.update(get_api_datalayer_connection(mode=mode))
    config.update(get_project_config(proj_file_path=proj_file_path, project=project))
    return config