from flask import request
import json
from flask_restx import Namespace, Resource
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import RQ_SORTAFTER_VALUES

rdv_object_version_api = Namespace('rdv_object_version', description='RDV Object versions operations, this is not used yet, but might come later https://gitlab.switch.ch/ub-unibas/rdv/services/rdv_viewer/-/issues/156')


@rdv_object_version_api.route('/object_versions/<project>/<object_type>/<object_id>/<lang>', methods=['GET'])
@rdv_object_version_api.route('/object_versions/<project>/', methods=['POST'])
class RDVObjVersions(Resource):

    @classmethod
    @rdv_object_version_api.response(200, "Success")
    @rdv_object_version_api.doc(params={"object_id": "id to identify object"})
    def get(self, object_id, object_type, lang, project=None):
        from .rdv_query import map_host2def, get_db_class
        rdv_class, project_config_store = map_host2def(project)
        db_class = get_db_class(project_config_store)
        db = db_class(request_data={"object_id": object_id, "object_type": {"value": object_type}},
                      project_config_store=project_config_store)
        objs = db.get_versions(object_id, object_type)
        return objs



@rdv_object_version_api.route('/object_version/<project>/<object_type>/<store_id>/<lang>/', methods=['GET'])
@rdv_object_version_api.route('/object_version/<project>/<lang>', methods=['POST'])
class RDVObjVersion(Resource):
    @classmethod
    @rdv_object_version_api.response(200, "Success")
    @rdv_object_version_api.doc(params={"store_id": "id to identify version of object in mongodb"})
    def get(self, store_id, object_type, lang, project=None):
        from .rdv_query import map_host2def, get_view_class, get_db_class
        rdv_class, project_config_store = map_host2def(project)
        view_class = get_view_class(project_config_store)
        db_class = get_db_class(project_config_store)

        db = db_class(request_data={"object_id": None, "object_type": object_type},
                      project_config_store=project_config_store)
        page = db.load_version(store_id, object_type)

        zas = view_class(page_id="get only for test reasons", page=page, lang=lang,
                         project_config_store=project_config_store)
        version_obj = zas.return_rdv_view(edit=True)

        return version_obj


    @classmethod
    @rdv_object_version_api.response(200, "Success")
    @rdv_object_version_api.doc(params={"store_id": "id to identify version of object in mongodb"})
    def post(self, lang, project=None):
        from .rdv_query import map_host2def, get_view_class, get_db_class
        rdv_class, project_config_store = map_host2def(project)
        view_class = get_view_class(project_config_store)
        db_class = get_db_class(project_config_store)

        request_data = json.loads(request.get_data().decode("utf-8"))
        object_id = request_data["object_id"]
        store_id = request_data["version_id"]
        object_type = request_data["object_type"]["value"]
        object_sort = request_data[RQ_SORTAFTER_VALUES]

        db = db_class(request_data=request_data,
                      project_config_store=project_config_store)
        page = db.load_version(store_id, object_type)

        zas = view_class(page_id=object_id, page=page, lang=lang,
                         project_config_store=project_config_store)
        version_obj = zas.return_rdv_view(edit=True)
        if zas.show_versions:
            version_obj["func_metadata"]["old_versions"] = RDVObjVersions.get(object_id=object_id, lang=lang,
                                                                              project=project, object_type=view_object.new_obj_type)

        # for s -es int problem
        version_obj["func_metadata"][RQ_SORTAFTER_VALUES] = json.dumps(object_sort)
        return version_obj
