import feedparser
from flask_restx import Namespace, Resource, fields

rdv_rss_api = Namespace('rdv_rss', description='RDV RSS operations')

rss_object = rdv_rss_api.model("highlight_object", {
    "title": fields.String(example="Title1", description="Title of Page/Highlight"),
    "description": fields.String(example="HTML Description1 .............",
                                 description="Description of Page/Highlight"),
    "content": fields.String(example="HTML Content1 ..............", description="page content"),
    "background_image": fields.Url(
        example="https://www.e-manuscripta.ch/bau/i3f/v20/1875698/pct:42.89049,34.83571,5.47538,7.00785/full/0/gray.jpg",
        description="Url to background image for highlight"),
    "linking_url": fields.Url(example="https://ub.unibas.ch", description="Url for linking")
})


@rdv_rss_api.route('/highlights/<rss_feed_id>', defaults={"rss_feed_id": "news-3"}, methods=['GET'])
class RDVCarusselContent(Resource):
    @rdv_rss_api.response(200, "Success", [rss_object])
    @rdv_rss_api.doc(params={"rss_feed_id": "id to identify rss feed"})
    def get(self, rss_feed_id):
        """
        get all highlight-objects from a rss feed
        """
        carussel_dict = self.rss2json(rss_feed_id)
        return list(carussel_dict.values())

    def rss2json(self, rss_feed_id):
        # todo: anpassen
        url = "https://ub-easyweb.ub.unibas.ch/de/home/feed/{}/feed.rss".format(rss_feed_id)
        url = "https://ub-easyweb.ub.unibas.ch/de/rdv-zas-highlights/feed/zas-highlights/feed.rss"
        rss_feed = feedparser.parse(url)
        carussel_dict = {}
        for e in rss_feed.entries:
            links = {l.type: l.href for l in e.links}
            o = {"title": e.title,
                 "description": e.summary,
                 "content": e.content[0].value,
                 "background_image": links.get("text/html", ""),
                 "linking_url": links.get("image/jpeg", "") or links.get("image/png", "")}
            carussel_dict[e.guid] = o
        return carussel_dict


@rdv_rss_api.route('/static_page/<rss_feed_id>/<page_id>', defaults={"rss_feed_id": "news-3", "page_id": "news-5"},
                     methods=['GET'])
class RDVStaticContent(RDVCarusselContent, Resource):
    @rdv_rss_api.response(200, "Success", rss_object)
    @rdv_rss_api.doc(params={"rss_feed_id": "id to identify rss feed", "page_id": "id to identify page"})
    def get(self, rss_feed_id, page_id):
        """
        get content for static page
        """
        carussel_dict = self.rss2json(rss_feed_id)
        return carussel_dict.get(page_id, {})