from flask import request
import re
import uuid
import json
from copy import deepcopy
from flask_restx import Namespace, Resource, fields
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import RQ_SORTAFTER_VALUES

class RDVSearch():
    @classmethod
    def get_request_data(cls):
        request_data = json.loads(request.get_data().decode("utf-8"))
        return request_data

from test_rdv.test_builder import build_test_case

rdv_object_api = Namespace('rdv_object', description='RDV Object operations')

network = rdv_object_api.model("network", {
    "label": fields.String(example="label", description="Object Title"),
    "id": fields.String,
    "connection_type": fields.String
})

defined_section = rdv_object_api.model("definded_section", {
    "title": fields.String(example="title", description="Object Title"),
    "preview_image": fields.Url(example="http://example.com/info.tif", description="IIIF Link to preview Image"),
    "iiif_manifest": fields.Url(example="https://ub-iiiifprestention/dizas/test/manifest", description="URL to hande over to IIIF-Viewer to present images"),
    "geo_coordinates": fields.Arbitrary(example="?", description="coordinates to show object on a map"),
    "network": fields.List(fields.Nested(network), example="?", description="entries to show network"),
    RQ_SORTAFTER_VALUES: fields.List(fields.Arbitrary(example="date", description="name of fields to sort on")),
    "id": fields.String
})

descriptive_section = rdv_object_api.model("descriptive_section", {
  "order": fields.Integer,
  "label": fields.String,
  "value": fields.Arbitrary,
  "section": fields.String(description="if metadata fields shall be combined in a section"),
  "link": fields.Url(description="Link to be used for entry")
})

view_object = rdv_object_api.model("view_object", {
    "desc_metadata": fields.List(fields.Nested(descriptive_section, example=[{"label":"label1", "value":"value1", "link":"http://...", "section":"section1"}], description="label - value pairs to describe object")),
    "func_metadata": fields.Nested(defined_section, description="fields that have a specific view function for representing an object (e.g. image, coordinates, ...)")
})


@rdv_object_api.route('/object_versions/<project>/<object_type>/<object_id>/<lang>', methods=['GET'])
@rdv_object_api.route('/object_versions/<project>/', methods=['POST'])
class RDVObjVersions(Resource):

    @classmethod
    @rdv_object_api.response(200, "Success")
    @rdv_object_api.doc(params={"object_id": "id to identify object"})
    def get(self, object_id, object_type, lang, project=None):
        from .rdv_query import map_host2def, get_db_class
        rdv_class, project_config_store = map_host2def(project)
        db_class = get_db_class(project_config_store)

        db_class.initialize(object_type, db_host)
        objs = db_class.get_versions(object_id, object_type)
        return objs



@rdv_object_api.route('/object_version/<project>/<object_type>/<store_id>/<lang>/', methods=['GET'])
@rdv_object_api.route('/object_version/<project>', methods=['POST'], defaults={"lang": "de"})
@rdv_object_api.route('/object_version/<project>/<lang>', methods=['POST'])
class RDVObjVersion(Resource):
    @classmethod
    @rdv_object_api.response(200, "Success")
    @rdv_object_api.doc(params={"store_id": "id to identify version of object in mongodb"})
    def get(self, store_id, object_type, lang, project=None):
        from .rdv_query import map_host2def, get_view_class, get_db_class
        rdv_class, project_config_store = map_host2def(project)
        view_class = get_view_class(project_config_store)
        db_class = get_db_class(project_config_store)

        db_class.initialize(object_type, db_host)
        page = db_class.load_version(store_id, object_type)

        zas = view_class(page_id="get only for test reasons", page=page, lang=lang,
                         project_config_store=project_config_store)
        version_obj = zas.return_rdv_view(edit=True)

        return version_obj


    @classmethod
    @rdv_object_api.response(200, "Success")
    @rdv_object_api.doc(params={"store_id": "id to identify version of object in mongodb"})
    def post(self, lang, project=None):
        from .rdv_query import map_host2def, get_view_class, get_db_class
        rdv_class, project_config_store = map_host2def(project)
        view_class = get_view_class(project_config_store)
        db_class = get_db_class(project_config_store)

        request_data = json.loads(request.get_data().decode("utf-8"))
        object_id = request_data["object_id"]
        store_id = request_data["version_id"]
        object_type = request_data["object_type"]["value"]
        object_sort = request_data[RQ_SORTAFTER_VALUES]

        db_class.initialize(object_type, db_host)
        page = db_class.load_version(store_id, object_type)

        zas = view_class(page_id=object_id, page=page, lang=lang,
                         project_config_store=project_config_store)
        version_obj = zas.return_rdv_view(edit=True)
        if zas.show_versions:
            version_obj["func_metadata"]["old_versions"] = RDVObjVersions.get(object_id=object_id, lang=lang,
                                                                              project=project, object_type=view_object.new_obj_type)

        # for s -es int problem
        version_obj["func_metadata"][RQ_SORTAFTER_VALUES] = json.dumps(object_sort)
        return version_obj

@rdv_object_api.route('/object_view/<object_id>', methods=['GET', 'POST'], defaults={'lang': 'de', "project": None})
@rdv_object_api.route('/object_view/<object_id>/<lang>', methods=['GET', 'POST'], defaults={"project": None})
@rdv_object_api.route('/object_view/<project>/<object_id>/<lang>', methods=['GET', 'POST'])
class RDVJSONView(Resource):
    @classmethod
    @rdv_object_api.response(200, "Success", view_object)
    @rdv_object_api.doc(params={"object_id": "id to identify object"})
    @build_test_case
    def get(self, object_id, lang, object_sort=[], build_test=False, project=None):
        """
        get json data to build object view
        """
        from .rdv_query import map_host2def, get_view_class
        rdv_class, project_config_store = map_host2def(project)
        view_class = get_view_class(project_config_store)

        lang = lang or "de"

        zas = view_class.get_page(page_id=object_id, lang=lang, project_config_store=project_config_store)
        obj_json = zas.return_rdv_view(edit=False)
        # for s -es int problem
        obj_json["func_metadata"][RQ_SORTAFTER_VALUES] = json.dumps(object_sort)
        return obj_json

@rdv_object_api.route('/object_edit/', methods=['POST'], defaults={'lang': 'de', "project": None})
@rdv_object_api.route('/object_edit/<project>/', methods=['POST'], defaults={'lang': 'de'})
@rdv_object_api.route('/object_edit/<object_id>', methods=['GET'], defaults={'lang': 'de', "project": None})
@rdv_object_api.route('/object_edit/<object_id>/<lang>', methods=['GET'], defaults={"project": None})
@rdv_object_api.route('/object_edit/<project>/<object_id>/<lang>', methods=['GET'])
class RDVEDitView(Resource):
    @classmethod
    @rdv_object_api.response(200, "Success", view_object, project=None)
    @rdv_object_api.doc(params={"object_id": "id to identify object"})
    def get(self, object_id, lang, object_sort=[], project=None):
        """
        get json data to build object view
        """

        from .rdv_query import map_host2def, get_view_class
        rdv_class, project_config_store = map_host2def(project)
        view_class = get_view_class(project_config_store)

        lang = lang or "de"

        view_object = view_class.get_page(page_id=object_id, lang=lang,
                                                project_config_store=project_config_store)

        obj_json = view_object.return_rdv_view(edit=True)
        # for s -es int problem
        obj_json["func_metadata"][RQ_SORTAFTER_VALUES] = json.dumps(object_sort)
        # ausgeschalten da für swa kleinschrifen keine Versionen
        if view_object.show_versions:
            obj_json["func_metadata"]["old_versions"] = RDVObjVersions.get(object_id=object_id, lang=lang,
                                                                           project=project, object_type=view_object.new_obj_type)
        return obj_json

    def post(self, lang="de", project=None):
        # 9951604850105504 zum Testen
        from .rdv_query import map_host2def, get_db_class
        rdv_class, project_config_store = map_host2def(project)
        db_class = get_db_class(project_config_store)
        request_data = json.loads(request.get_data().decode("utf-8"))
        db = db_class(request_data=request_data,
                      project_config_store=project_config_store)

        # if record hast object_id -> update
        if request_data["object_id"]:
            # Todo Update prozess umsetzen: Dateien werden auch verschoben
            response = db.update_obj_data()
            return response
        # otherwise new object
        else:
            response = db.create_new_record()
            return response

@rdv_object_api.route('/object_new/<object_type>', methods=['GET'], defaults={'lang': 'de', "project": None})
@rdv_object_api.route('/object_new/<object_type>/<lang>', methods=['GET'], defaults={"project": None})
@rdv_object_api.route('/object_new/<project>/<object_type>/<lang>', methods=['GET'])
class RDVNewObj(Resource):
    @classmethod
    @rdv_object_api.response(200, "Success", view_object, project=None)
    @rdv_object_api.doc(params={"object_type": "type of object to be created"})
    def get(self, object_type, lang, object_sort=[], project=None):
        """
        get json data to build object view
        """

        from .rdv_query import map_host2def, get_view_class
        rdv_class, project_config_store = map_host2def(project)
        view_class = get_view_class(project_config_store)

        lang = lang or "de"

        empty = view_class.get_empty_page(lang=lang, obj_type=object_type,
                                          project_config_store=project_config_store)
        groups = view_class.get_groups()
        return {"desc_metadata": empty.get_edit_metadata(object_type, dict_type="edit"), "def_metadata": groups}
