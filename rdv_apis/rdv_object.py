from flask import request
import json
from flask_restx import Namespace, Resource, fields
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import RQ_SORTAFTER_VALUES

from rdv_apis.rdv_object_version import RDVObjVersions
from test_rdv.utilities.test_builder import build_test_case

rdv_object_api = Namespace('rdv_object', description='Get, create or edit RDV Objects')

network = rdv_object_api.model("network", {
    "label": fields.String(example="label", description="Object Title"),
    "id": fields.String,
    "connection_type": fields.String
})

defined_section = rdv_object_api.model("defined_section", {
    "title": fields.String(example="title", description="Object Title"),
    "preview_image": fields.Url(example="http://example.com/info.tif", description="IIIF Link to preview Image"),
    "iiif_manifest": fields.Url(example="https://ub-iiiifprestention/dizas/test/manifest", description="URL to hande over to IIIF-Viewer to present images"),
    "geo_coordinates": fields.Arbitrary(example="?", description="coordinates to show object on a map"),
    "network": fields.List(fields.Nested(network), example="?", description="entries to show network"),
    RQ_SORTAFTER_VALUES: fields.List(fields.Arbitrary(example="date", description="name of fields to sort on")),
    "id": fields.String
})

descriptive_section = rdv_object_api.model("descriptive_section", {
  "order": fields.Integer,
  "label": fields.String,
  "value": fields.Arbitrary,
  "section": fields.String(description="if metadata fields shall be combined in a section"),
  "link": fields.Url(description="Link to be used for entry")
})

view_object = rdv_object_api.model("view_object", {
    "desc_metadata": fields.List(fields.Nested(descriptive_section, example=[{"label":"label1", "value":"value1", "link":"http://...", "section":"section1"}], description="label - value pairs to describe object")),
    "func_metadata": fields.Nested(defined_section, description="fields that have a specific view function for representing an object (e.g. image, coordinates, ...)")
})

project_documentation = {"project": "the abbreviation of the project, for example `afrikaportal-test` or `zas-loc`. It has the form of a [project](https://gitlab.switch.ch/ub-unibas/rdv/modules/rdv_config_store_ubit/-/tree/master/rdv_config_store_ubit/configs), a `-`and then an environment (like dev, prod, test or loc)"}

@rdv_object_api.route('/object_view/<project>/<object_id>/<lang>', methods=['GET'])
class RDVJSONView(Resource):
    @classmethod
    @rdv_object_api.response(200, "Success", view_object)
    @rdv_object_api.doc(params={"object_id": "id to identify an object, for example `babarchive2022_ce1f2401-4db4-5e86-95fa-aefbc9e54d12`"})
    @rdv_object_api.doc(params=project_documentation)
    @rdv_object_api.doc(params={"lang": "the abbreviation of the language, for example `de`"})
    @build_test_case
    def get(self, object_id, lang, object_sort=[], build_test=False, project=None):
        """
        Get the object data to display it
        """
        from .rdv_query import map_host2def, get_view_class
        rdv_class, project_config_store = map_host2def(project)
        view_class = get_view_class(project_config_store)

        lang = lang or "de"

        view_object = view_class.get_page(page_id=object_id, lang=lang, project_config_store=project_config_store)
        obj_json = view_object.return_rdv_view(edit=False)
        # for s -es int problem
        obj_json["func_metadata"][RQ_SORTAFTER_VALUES] = json.dumps(object_sort)
        return obj_json


@rdv_object_api.route('/object_edit/<project>/', methods=['POST'], defaults={'lang': 'de'})
@rdv_object_api.route('/object_edit/<project>/<lang>', methods=['POST'])
@rdv_object_api.route('/object_edit/<project>/<object_id>/<lang>', methods=['GET'])
class RDVEDitView(Resource):
    @classmethod
    @rdv_object_api.response(200, "Success", view_object, project=None)
    @rdv_object_api.doc(params={"object_id": "id to identify object"})
    @rdv_object_api.doc(params=project_documentation)
    def get(self, object_id, lang, object_sort=[], project=None):
        """
        Get the object data to edit it
        """

        from .rdv_query import map_host2def, get_view_class
        rdv_class, project_config_store = map_host2def(project)
        view_class = get_view_class(project_config_store)

        lang = lang or "de"

        view_object = view_class.get_page(page_id=object_id, lang=lang,
                                                project_config_store=project_config_store)

        obj_json = view_object.return_rdv_view(edit=True)
        # for s -es int problem
        obj_json["func_metadata"][RQ_SORTAFTER_VALUES] = json.dumps(object_sort)
        # ausgeschalten da für swa kleinschrifen keine Versionen
        if view_object.show_versions:
            obj_json["func_metadata"]["old_versions"] = RDVObjVersions.get(object_id=object_id, lang=lang,
                                                                           project=project, object_type=view_object.new_obj_type)
        return obj_json

    def post(self, lang="de", project=None):
        """
        Post an updated object
        """
        # 9951604850105504 zum Testen
        from .rdv_query import map_host2def, get_db_class
        rdv_class, project_config_store = map_host2def(project)
        db_class = get_db_class(project_config_store)
        request_data = json.loads(request.get_data().decode("utf-8"))
        db = db_class(request_data=request_data,
                      project_config_store=project_config_store)

        # if record hast object_id -> update
        if request_data["object_id"]:
            # Todo Update prozess umsetzen: Dateien werden auch verschoben
            response = db.update_obj_data()
            return response
        # otherwise new object
        else:
            response = db.create_new_record()
            return response


@rdv_object_api.route('/object_new/<project>/<object_type>/<lang>', methods=['GET'])
class RDVNewObj(Resource):
    @classmethod
    @rdv_object_api.response(200, "Success", view_object, project=None)
    @rdv_object_api.doc(params={"object_type": "type of object to be created"})
    @rdv_object_api.doc(params=project_documentation)
    def get(self, object_type, lang, object_sort=[], project=None):
        """
        Get an empty json structure for an object
        """

        from .rdv_query import map_host2def, get_view_class
        rdv_class, project_config_store = map_host2def(project)
        view_class = get_view_class(project_config_store)

        lang = lang or "de"

        empty = view_class.get_empty_page(lang=lang, obj_type=object_type,
                                          project_config_store=project_config_store)
        groups = view_class.get_groups()
        return {"desc_metadata": empty.get_edit_metadata(object_type, dict_type="edit"), "def_metadata": groups}
