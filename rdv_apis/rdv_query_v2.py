import os
import json
import time
import importlib
import rdv_es_dsl_ubit
import requests
from threading import Thread
from copy import deepcopy
from rdv_config_store_ubit import ConfigStore
from flask import jsonify, request
from flask_restx import Namespace, Resource
from rdv_marc_ubit import ZAS_Enricher_SLSP
from rdv_apis.rdv_object import view_object
from test_rdv.utilities.test_builder import build_test_case
from flask_restx import fields
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import RQ_QUERY_PARAMS, RQ_FACETS, RQ_SORTAFTER_VALUES, RQ_SORTAFTER_ORDER, \
    RDV_FACETPAIRS, RDV_SUBVALUES, RDV_SNIPPETS, RDV_NO_ENTRY, RDV_HIT_COUNT, RDV_VISUAL_AGGS, \
    SNIPPET_SEARCH_AFTER, SNIPPET_TITLE, SNIPPET_OBJ_TYPE, SNIPPET_LINE1, SNIPPET_FULLTEXT, SNIPPET_IIIF_PREVIEW, \
    SNIPPET_ID, \
    RQ_GTE, RQ_LTE, RQ_POS, RQ_SHOW_AGGS, RQ_ORDER, RQ_FIELD, RDV_FIELD, RQ_OPERATOR, RQ_VALUES, RDV_VALUE, RDV_LABEL, \
    RDV_COUNT, RQ_FACET_TYPE
from rdv_es_dsl_ubit import RDVESDslQuery, RDVESFacetSearch, NextSnippets, NextObject, RDVESAutocomplete, RDVESCountrySearch

rdv_query_api_v2 = Namespace('rdv_query', description='RDV Query operations')
DEBUG = os.environ.get('RDV_DEBUG')

config_store = ConfigStore()
config_store.load_project_config('baseconfig')

PROFILE = config_store.get_value('base.PROFILE')
BUILD_IIIF_CACHE = config_store.get_value('base.BUILD_IIIF_CACHE')
if PROFILE:
    import cProfile
    import pstats

def get_view_class(config_store_project):
    view_class_name = config_store_project.get_value('classes.view')
    module = config_store_project.get_value('module') + ".rdv"

    try:
        view_class = getattr(
            importlib.import_module(module),
            view_class_name
        )
    except Exception as ex:
        import traceback
        traceback.print_exc()
        print("Exception while getting biew_class {} from module {}: {}".format(view_class_name, module, ex))
    return view_class

def get_db_class(config_store_project):
    db_class_name = config_store_project.get_value('classes.db')
    module = config_store_project.get_value('module') + ".rdv"

    try:
        db_class = getattr(
            importlib.import_module(module),
            db_class_name
        )
    except Exception as ex:
        import traceback
        traceback.print_exc()
        print("Exception while getting db_class {} from module {}: {}".format(db_class_name, module, ex))
    return db_class

def get_form_class(config_store_project):
    form_class_name = config_store_project.get_value('classes.form')
    module = config_store_project.get_value('module') + ".rdv"

    try:
        form_class = getattr(
            importlib.import_module(module),
            form_class_name
        )
    except Exception as ex:
        import traceback
        traceback.print_exc()
        print("Exception while getting form_class {} from module {}: {}".format(form_class_name, module, ex))
    return form_class

def map_host2def(project=None):
    """ get rdv_class, es_client and index name for each project / host"""
    # todo: host auslesen absichern

    from flask import current_app
    debug = current_app.debug

    config_store_project = ConfigStore()
    config_store_project.load_project_config(project)


    if config_store_project.get_value('classes.query'):
        rdv_class_name = config_store_project.get_value('classes.query')

        module = config_store_project.get_value('module') + ".rdv"

        try:
            rdv_class = getattr(
                                importlib.import_module(module),
                                rdv_class_name
                                )
        except Exception as ex:
            import traceback
            traceback.print_exc()
            print(f"Exception while getting rdv_class {rdv_class_name} from module {module}: {ex} for {project} ")
        if DEBUG:
            print(f"RDV Config Store for {project} loaded {rdv_class_name}")
    else:
        # TODO fix Invalid Usage not working
            raise InvalidUsage('No project definied for host {}'.format(project), status_code=410)

    return rdv_class, config_store_project


#############
# EXCEPTION #
#############

class InvalidUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv


@rdv_query_api_v2.errorhandler(InvalidUsage)
class RDVInvalidUsage(Resource):
    def get(self, error):
        response = jsonify(error.to_dict())
        response.status_code = error.status_code
        return response


##########
# MODELS #
##########

project_documentation = {"project": "the abbreviation of the project, for example `afrikaportal-test` or `zas-loc`. It has the form of a [project](https://gitlab.switch.ch/ub-unibas/rdv/modules/rdv_config_store_ubit/-/tree/master/rdv_config_store_ubit/configs), a `-`and then an environment (like dev, prod, test or loc)"}


##################
# request models #
##################

sort_params = rdv_query_api_v2.model("sort_params", {
    RQ_FIELD: fields.String(example="_score", description="field to sort on"),
    RQ_ORDER: fields.String(enum=["asc", "desc"], default="desc")
})

query_params = rdv_query_api_v2.model("query_params", {
    "start": fields.Integer(min=0, default=0),
    "size": fields.Integer(min=1, default=10, example=10),
    "sort": fields.List(fields.Nested(sort_params)),
    "source": fields.List(fields.String),
})

hier_entry = rdv_query_api_v2.model('hier_value', {
    "id": fields.Arbitrary(example="stw_http://zbw.eu/stw/thsys/70904", description="hierarchy entry id"),
    RQ_POS: fields.Arbitrary(example="1.10.34", description="hierarchy entry positionion"),
})

gte_lte_params = rdv_query_api_v2.model('get_lte_params', {
    RQ_GTE: fields.Arbitrary(example="1985-11-04"),
    RQ_LTE: fields.Arbitrary(example="1985-12-04"),
})

facet_params = rdv_query_api_v2.model('facet_params', {
    RQ_OPERATOR: fields.String(enum=["OR", "MUST"], description="How filters within one facet shall be combined"),
    RQ_FIELD: fields.String(example="fct_institution.keyword", description="field to query for facet values"),
    "size": fields.Integer(example=10, description="number of aggregatins to be returned"),
    RQ_FACET_TYPE: fields.String(example="basic", enum=["date", "int", "geo", "basic", "subcat"],
                                 description="which type of range"),
    "order": fields.String(example="count", enum=["count", "label"])
})

facet_params_with_values = rdv_query_api_v2.inherit('facet_params_with_values', facet_params, {
    RQ_VALUES: fields.List(fields.String, example=[], description="facet values for query")
})

hierarchy_params = rdv_query_api_v2.inherit('hierarchy_params', facet_params, {
    RQ_VALUES: fields.List(fields.Nested(hier_entry))
})

rangefilter_params = rdv_query_api_v2.model('rangefilter_params', facet_params, {
    RQ_VALUES: fields.List(fields.Nested(gte_lte_params, description="date, number or coord range for query")),
    RQ_SHOW_AGGS: fields.Boolean(example=True, description="whether aggregations key: count pairs shall be shown"),
})

histogram_params = rdv_query_api_v2.inherit('histogram_params', rangefilter_params, {
    "min": fields.Integer(example=1900, description="start point for time slider"),
    "max": fields.Integer(example=2019, description="end point for time slider"),
    "show_missing_values": fields.Boolean(description="if objects without field entry shall be shown")
})

# todo: attribute='private_name' for field aenderung von sebastian
rdv_complex_query = rdv_query_api_v2.model("rdv_complex_query", {
    RQ_QUERY_PARAMS: fields.Nested(query_params),
    "query": fields.Raw(example={"bool": {"must": [{"query_string": {"query": ["afrika"], "op": "AND"}}]}},
                              description="ES query_string query, xor with match_all param"),
    #"match_all": fields.Boolean(example=True, description="If no string query is performed, xor with query param"),
    #"ids": fields.List(fields.String, example=["id1", "id2"], description="ids to be queried"),
    # RQ_FILTER: fields.Raw(example={"bool": {"must": [{
    #     "bool": {"should": [{"term": {"hidden_digitized": "ja"}}, {"term": {"hidden_digitized": "nein"}}]}}]}},
    #     description="ES query filter, not used? for performance?"),
    #RQ_VISUAL_AGGS: fields.Wildcard(fields.Nested(histogram_params), description="range for histogramm"),
    # TODO: auch facet_params_with_values, rangefilter_params, hierarchy_params
    RQ_FACETS: fields.Wildcard(fields.Nested(facet_params_with_values)),
    "lang": fields.String(example="de"),
    "viewer": fields.String(example="List", enum=["List", "documentViewer"], description="the way the results are displayed")
})

rdv_facet_query = rdv_query_api_v2.model("rdv_facet_query", {
    RQ_QUERY_PARAMS: fields.Nested(query_params),
    "facets": fields.Raw(example={
        "fct_person_organisation": {
          "field": "fct_person_organisation.keyword",
          "values": [],
          "operator": "OR",
          "order": "count",
          "facet_type": "basic",
          "size": 10
        }
    }
    ),
    "match_all": fields.Boolean(default=True),
    "open_facets": fields.Raw(example={
        "fct_person_organisation.keyword": "wei"
    }),
    "lang": fields.String(default="de"),
    #"query": fields.Raw(example={"bool": {"must": [{"query_string": {"query": ["afrika"], "op": "AND"}}]}},
    #                    description="ES query_string query, xor with match_all param")
    #RQ_FACET_QUERY: fields.String(example="val..",
    #                                 description="values to query within one facet, if left empty, first 100 aggs are loaded for one aggregation"),
    #RQ_FACET_FIELD: fields.String(example="field1", description="field to build aggregations on")
})

# todo: tiebraker Indexierung
rdv_search_after = rdv_query_api_v2.inherit("search_after", rdv_complex_query, {
    RQ_SORTAFTER_VALUES: fields.String(example="[0.57377094, 0.57377094, \"babarchive2022_a9f2bbdc-d4e5-5429-9aaf-1ac84307acff\"]", description="the search after values"),
    RQ_SORTAFTER_ORDER: fields.String(enum=["asc", "desc"], description="to get values before and after entries")
})

rdv_geo_query = rdv_query_api_v2.inherit('geo_query', facet_params, {
        'coordinates': fields.Nested(rdv_query_api_v2.model('Coordinates', {
            'top_left': fields.Nested(rdv_query_api_v2.model('TopLeft', {
                'lon': fields.Float(example=3.1),
                'lat': fields.Float(example=6.3)
            })),
            'bottom_right': fields.Nested(rdv_query_api_v2.model('BottomRight', {
                'lon': fields.Float(example=8.02),
                'lat': fields.Float(example=4.13)
            }))
        })),
        'precision': fields.Integer(min=1, max=12, example=7)
})

###################
# response models #
###################

rdv_hist_entry = rdv_query_api_v2.model('rdv_hist_entry', {
    RDV_LABEL: fields.Integer(example=1983, description="histogram field label"),
    RDV_COUNT: fields.Integer(example=10, description="number of counts")
})

# TODO: hier_entry, no value, gte_lte
rdv_fct_entry = rdv_query_api_v2.model('rdv_fct_entry', {
    RDV_VALUE: fields.List(fields.Nested(hier_entry)),
    RDV_LABEL: fields.String(example="Hierarchy Label", description="hierarchy entry label"),
    RDV_COUNT: fields.Integer(example=10, description="number of counts"),
    RDV_FIELD: fields.String(example="field1", description="field for which results are returned"),
    RDV_SUBVALUES: fields.List(fields.Nested(rdv_hist_entry, description="subcategories for a field")),
})

rdv_facet_data = fields.Wildcard(
    fields.List(fields.Nested(rdv_fct_entry, example=
    [{
        "label": "label1",
        "count": 53,
        "value": {
            "id": "id1"
        }
    }],
        description="Label and count pairs for facets, format ")
                )
)

rdv_histogram_data = fields.Wildcard(
    fields.List(fields.Nested(rdv_hist_entry)))

rdv_docs = rdv_query_api_v2.model("rdv_docs", {
    SNIPPET_ID: fields.String(example="id1", description="ID of the object, for example to retrieve JSON-View"),
    SNIPPET_TITLE: fields.String(example="Title", description="Object title"),
    SNIPPET_OBJ_TYPE: fields.String(example="Record", description="Object type"),
    SNIPPET_LINE1: fields.String(example="Zeitung: NZZ, Datum: 10.10.2019",
                                 description="First line for scnippet view "),
    # momentan nicht nötig: "line_2": fields.String(example="weiterführende Informationen", description="Second line for scnippet view, should be shown if no fulltext snippet available "),
    SNIPPET_FULLTEXT: fields.List(fields.String,
                                  example=["Sind mit Colombo Markt-\n<em>tests</em> durchgeführt worden?"],
                                  description="Fulltext snippet"),
    SNIPPET_IIIF_PREVIEW: fields.Url(example="http://example.com/info.tif", description="IIIF Link to preview Image"),
    SNIPPET_SEARCH_AFTER: fields.List(fields.Arbitrary, example=[1556841600000, "zas_dossiers_test_Zentralbank"],
                                      description="Values to used for search_after query to request next entries")
})

rdv_data = rdv_query_api_v2.model("rdv_facet_data", {
    RDV_HIT_COUNT: fields.Integer(example=1, description="Number of results found"),
    RDV_SNIPPETS: fields.List(fields.Nested(rdv_docs)),
    RDV_FACETPAIRS: rdv_facet_data,
    RDV_VISUAL_AGGS: rdv_histogram_data,
    RDV_NO_ENTRY: fields.Wildcard(
        fields.Integer(example={"{{!ex=fct_year}fct_year:0": 18298}, description="no idea")),
})


##########
#  API   #
##########

class RDVSearch():
    @classmethod
    def get_request_data(cls):
        request_data = json.loads(request.get_data().decode("utf-8"))
        return request_data

@rdv_query_api_v2.route('/es_proxy/<project>/', methods=['POST'], defaults={'hits_subcat': True}, doc={
    "description": "perform a search for the given project",
}, )
@rdv_query_api_v2.route('/es_proxy/<project>/open_facets', methods=['POST'], defaults={'hits_subcat': True, "only_open_facets": True}, doc={
    "description": "perform a search for the given project but only build aggregations for open facets, ",
}, )
@rdv_query_api_v2.route('/es_proxy/<project>/no-hierarchy/', methods=['POST'], defaults={'hits_subcat': False}, doc={
    "description": "do not get hits for hierarchical facets (performance)",
}, )
@rdv_query_api_v2.route('/es_proxy/<project>/aggregation-only/', methods=['POST'], defaults={'hits_subcat': True, "only_aggs": True}, doc={
    "description": "return only aggregation results"
})
@rdv_query_api_v2.route('/es_proxy/<project>/countries/', methods=['POST'], defaults={'hits_subcat': True, "countries": True}, doc={
    "description": "return only aggregation results"
})
class RDVComplexSearch(RDVSearch, Resource):

    @rdv_query_api_v2.expect(rdv_complex_query)
    @rdv_query_api_v2.response(200, "Success", rdv_data)
    @rdv_query_api_v2.doc(params=project_documentation)
    @build_test_case
    def post(self, hits_subcat, build_test=True, only_aggs=False, project=None, countries=False, only_open_facets=False):
        """
        query endpoint for search page, get updated facet values and snippets for list view,
        """

        start = time.time()
        request_data = self.get_request_data()

        try:
            rdv_class, project_config_store = map_host2def(project)
        except InvalidUsage:
            raise InvalidUsage('No project defined for host', status_code=410)
        deepcopy_request_data = deepcopy(request_data)
        rdv_object = rdv_class(request_data=deepcopy_request_data,
                                project_config_store=project_config_store)

        if countries:
            # remove coordinates fields
            dsl_query_builder = RDVESCountrySearch(config_store=project_config_store, request_data=request_data, only_open_facets=only_open_facets)
        else:
            dsl_query_builder = RDVESDslQuery(config_store=project_config_store, request_data=request_data, only_open_facets=only_open_facets)
        # searchquery is same as v1 but additinoally contains the block(s): filter": [ { "bool": { "must": [ { "query_string": { "query": "afrika", "rewrite": "top_terms_1000" } } ] } } ] }
        search = dsl_query_builder.build_complex_facet_search_body()
        rdv_object.body = search.to_dict()

        if PROFILE:
            cProfile.runctx('rdv_object.build_rdvformat(hits_subcat=hits_subcat, dsl=True)', globals(), locals(), "restats")
            p = pstats.Stats('restats')
            p.strip_dirs().sort_stats('cumtime').print_stats("es2rdv")
            p.print_callees("pre_build_rdvformat(hits_subcat=hits_subcat, dsl=True)")

        r = rdv_object.build_rdvformat(hits_subcat=hits_subcat, only_aggs=only_aggs, dsl=True)

        if BUILD_IIIF_CACHE and rdv_object.iiif:
            Thread(target=RDVFlexIIIF.build_iiif_cache,
                   kwargs={"rdv_class": rdv_class, "project_config_store": project_config_store,
                           "request_data": request_data,
                           "next": False}).start()

        end = time.time()
        if DEBUG:
            print("ES-Proxy request Time:", end - start, r)
        return r


@rdv_query_api_v2.route('/facet_search/<project>/', methods=['POST'])
# for backwards compatibility
@rdv_query_api_v2.route('/simple_facet_search/<project>', methods=['POST'])
class RDVFacetSearch(RDVSearch, Resource):
    @rdv_query_api_v2.expect(rdv_facet_query)
    @rdv_query_api_v2.response(200, "Success", rdv_facet_data)
    @rdv_query_api_v2.doc(params=project_documentation)
    @build_test_case
    def post(self, project=None):
        """
        search within facets
        """
        request_data = self.get_request_data()
        aggs_size = request_data.get("aggs_size", 0)
        rdv_class, project_config_store = map_host2def(project)
        deepcopy_request_data = deepcopy(request_data)
        rdv_object = rdv_class(request_data=deepcopy_request_data,
                                project_config_store=project_config_store)
        dsl_query_builder = RDVESFacetSearch(config_store=project_config_store, request_data=request_data)
        # searchquery is same as v1 but additinoally contains the block(s): filter": [ { "bool": { "must": [ { "query_string": { "query": "afrika", "rewrite": "top_terms_1000" } } ] } } ] }
        search = dsl_query_builder.build_complex_facet_search_body()
        rdv_object.body = search.to_dict()

        return rdv_object.build_facetsearch_format()


@rdv_query_api_v2.route('/further_snippets/<project>/', methods=['POST'])
class RDVNextSnippets(RDVSearch, Resource):
    @rdv_query_api_v2.expect(rdv_search_after)
    @rdv_query_api_v2.response(200, "Success", fields.List(fields.Nested(rdv_docs)))
    @rdv_query_api_v2.doc(params=project_documentation)
    def post(self, project=None):
        """
        get the result snippets for the next page of results
        """
        request_data = self.get_request_data()
        rdv_class, project_config_store = map_host2def(project)
        deepcopy_request_data = deepcopy(request_data)
        # searchquery is same as v1 but additinoally contains the block(s): filter": [ { "bool": { "must": [ { "query_string": { "query": "afrika", "rewrite": "top_terms_1000" } } ] } } ] }
        next_snippets = NextSnippets(config_store=project_config_store, request_data=deepcopy_request_data)
        search = next_snippets.get_next_snippets_query()
        deepcopy_request_data = deepcopy(request_data)
        rdv_object = rdv_class(request_data=deepcopy_request_data,
                                project_config_store=project_config_store)
        rdv_object.body = search.to_dict()
        return rdv_object.next_snippets()


@rdv_query_api_v2.route('/next_objectview/<project>/', methods=['POST'])
class RDVNextObject(RDVSearch, Resource):
    @rdv_query_api_v2.expect(rdv_search_after)
    @rdv_query_api_v2.response(200, "Success", view_object)
    @rdv_query_api_v2.doc(params=project_documentation)
    def post(self, project=None):
        """
        get json view for next object
        """
        request_data = self.get_request_data()
        rdv_class, project_config_store = map_host2def(project)
        deepcopy_request_data = deepcopy(request_data)
        rdv_object = rdv_class(request_data=deepcopy_request_data,
                                project_config_store=project_config_store)

        next_object = NextObject(config_store=project_config_store, request_data=request_data)
        # searchquery is same as v1 but additinoally contains the block(s): filter": [ { "bool": { "must": [ { "query_string": { "query": "afrika", "rewrite": "top_terms_1000" } } ] } } ] }
        search = next_object.get_next_object()
        rdv_object.body = search.to_dict()

        try:
            obj_id, obj_sort = rdv_object.get_nextobj_id()
        except IndexError:
            return []
        from rdv_apis.rdv_object import RDVJSONView
        lang = request_data.get('lang', 'en')
        r = RDVJSONView.get(object_id=obj_id, lang=lang, object_sort=obj_sort, project=project)
        return r


@rdv_query_api_v2.route('/popup_query/<project>/', methods=['POST'])
class RDFCatalogQuery(RDVSearch, Resource):
    # todo: @rdv_query_api_v2.expect(sys_id)
    @rdv_query_api_v2.response(200, "Success", rdv_data)
    @rdv_query_api_v2.doc(params=project_documentation)
    def post(self, hits_subcat=True, project=None):
        """
        This is used to enable deep links from other catalogs, for example in Alma as on https://basel.swisscovery.org/discovery/sourceRecord?vid=41SLSP_UBS:live&docId=alma9961503680105504&recordOwner=41SLSP_NETWORK. Such links use then a "popup" query to display the results.
        """

        request_data = self.get_request_data()
        sys_id = request_data.get("popup_query").get("id")

        try:
            rdv_class, project_config_store = map_host2def(project)
        except InvalidUsage:
            raise InvalidUsage('No project definied for host', status_code=410)
        index = project_config_store.get_value("databases.index_name")
        es_host = project_config_store.get_value("hosts.index")
        zas = ZAS_Enricher_SLSP(es_host=es_host, index=index)
        request_data, selected_facets, text = zas.build_rdv_query(sys_id=sys_id, query_template=request_data)
        deepcopy_request_data = deepcopy(request_data)
        rdv_object = rdv_class(request_data=deepcopy_request_data,
                                project_config_store=project_config_store)

        dsl_query_builder = RDVESDslQuery(config_store=project_config_store, request_data=request_data)
        # searchquery is same as v1 but additinoally contains the block(s): filter": [ { "bool": { "must": [ { "query_string": { "query": "afrika", "rewrite": "top_terms_1000" } } ] } } ] }
        search = dsl_query_builder.build_complex_facet_search_body()

        rdv_object.body = search.to_dict()
        json_response = rdv_object.pre_build_rdvformat(hits_subcat=hits_subcat, dsl=True)
        json_response["popup"] = {"i18nKey": "catalog-link.auto-filter-popup-info", "values": {"key": text}}
        json_response.update(selected_facets)
        r = rdv_object._return_response(json_response)
        return r


@rdv_query_api_v2.route('/get_dataset_id/<project>/', methods=['POST'])
class RDVDatasetID(RDVSearch, Resource):

    @rdv_query_api_v2.expect(rdv_complex_query)
    # TODO: response format definieren
    @rdv_query_api_v2.response(200, "Success")
    @rdv_query_api_v2.doc(params=project_documentation)
    def post(self, project=None):
        print("TEST")
        request_data = self.get_request_data()
        rdv_class, project_config_store = map_host2def(project)
        deepcopy_request_data = deepcopy(request_data)
        rdv_object = rdv_class(request_data=deepcopy_request_data,
                               project_config_store=project_config_store)
        # todo: signature für query-bauen, um für gleiche queries nicht zweimal build_id_query loszuschicken

        dsl_query_builder = RDVESDslQuery(config_store=project_config_store, request_data=request_data)
        rdv_object = rdv_class(request_data=deepcopy_request_data,
                               project_config_store=project_config_store)

        search = dsl_query_builder.build_complex_facet_search_body()
        rdv_object.body = search.to_dict()
        json_response = rdv_object.pre_build_rdvformat(hits_subcat=True, dsl=True)

        id_query = dsl_query_builder.rewrite_query2ids()
        id_query_json = id_query.to_dict()
        from cache_decorator_redis_ubit import CacheDecorator
        import hashlib
        cache = CacheDecorator()

        check_sum = hashlib.md5(json.dumps(id_query_json).encode('utf-8')).hexdigest()
        # necessary to only include correct entries, but only for iii
        #_canvas_id_field = s.mapping_manifest["page_id"]
        #id_query_json["query"]["bool"]["must"].append({"exists": {"field": _canvas_id_field}})
        cache_key = cache.get_cache_key(unique_id=check_sum)
        cache.set_keyvalue_cache(key=cache_key, value=id_query_json)
        print("Dataset store query", id_query_json)
        return {"dataset_id": cache_key}

@rdv_query_api_v2.route('/iiif_flex_pres/<project>/', methods=['POST'])
class RDVFlexIIIF(RDVSearch, Resource):

    @rdv_query_api_v2.expect(rdv_complex_query)
    @rdv_query_api_v2.response(200, "Success", rdv_data)
    @rdv_query_api_v2.doc(params=project_documentation)
    def post(self, project=None):
        request_data = self.get_request_data()
        rdv_class, project_config_store = map_host2def(project)
        if PROFILE:
            cProfile.runctx(
                'self.get_iiif_flexurl(request_data, rdv_class, es_host, iiif_host, index, db_host, dyn_manif)',
                globals(), locals(), "restats")
            p = pstats.Stats('restats')
            p.sort_stats('cumtime').print_stats(0.02)
            # build_id_query pre_build_rdvformat
            p.print_callees('pre_build_rdvformat(hits_subcat=hits_subcat, dsl=True)')
            # _get_manifest_pages
            # _build_manifest _build_content
            p.print_callers("profile_test")
        iiif_view = project_config_store.get_value("query.iiif")
        if iiif_view:
            r = self.get_iiif_flexurl(request_data, rdv_class, project_config_store)
        if BUILD_IIIF_CACHE and iiif_view:
            # RDVFlexIIIF.build_iiif_cache(request_data=request_data, rdv_class=rdv_class, project_config_store=project_config_store, next=True)
            Thread(target=RDVFlexIIIF.build_iiif_cache,
                   kwargs={"rdv_class": rdv_class, "project_config_store": project_config_store,
                           "request_data": request_data,
                           "next": True}).start()
        return r

    @classmethod
    def build_iiif_cache(cls, request_data, rdv_class, project_config_store, next=True):
        # todo: Umlaute einheitlich normalisieren für gleiche Cache-ID, z.B. Zürich

        if next:
            # cache fürs weiterblättern
            max_ids_dyn_manif = config_store.get_value("query.max_ids_dyn_manif")
            next_request_data = deepcopy(request_data)
            start = request_data["query_params"].get("start", 0)
            size = request_data["query_params"].get("size", 0)
            next_size = size if size and size < max_ids_dyn_manif else max_ids_dyn_manif
            next_from = start + next_size if start + next_size < 9900 else 0
            next_request_data["query_params"]["start"] = next_from
            next_request_data["query_params"]["size"] = next_size
            next_request_data["persist"] = False
            next_resp = cls.get_iiif_flexurl(next_request_data, rdv_class, project_config_store)
            next_iiif_cache_url = next_resp.json["iiif_flex_url"]
            requests.get(next_iiif_cache_url, verify=False)
            if DEBUG:
                print("NEXT IIIF CACHE URL", next_iiif_cache_url)
        else:
            # zum wechseln von listen auf iiif ansicht
            resp = cls.get_iiif_flexurl(request_data, rdv_class, project_config_store)
            if resp:
                iiif_cache_url = resp.json["iiif_flex_url"]
                requests.get(iiif_cache_url, verify=False)
                if DEBUG:
                    print("Listenansicht IIIF CACHE URL", iiif_cache_url)


    @classmethod
    def get_iiif_flexurl(cls, request_data, rdv_class, project_config_store):

        # todo: signature für query-bauen, um für gleiche queries nicht zweimal build_id_query loszuschicken
        deepcopy_request_data = deepcopy(request_data)
        dsl_query_builder = RDVESDslQuery(config_store=project_config_store, request_data=request_data)
        rdv_object = rdv_class(request_data=deepcopy_request_data,
                               project_config_store=project_config_store)

        # searchquery is same as v1 but additinoally contains the block(s): filter": [ { "bool": { "must": [ { "query_string": { "query": "afrika", "rewrite": "top_terms_1000" } } ] } } ] }
        search = dsl_query_builder.build_complex_facet_search_body()
        rdv_object.body = search.to_dict()
        print(json.dumps(rdv_object.body))
        json_response = rdv_object.pre_build_rdvformat(hits_subcat=True, dsl=True)

        id_query = dsl_query_builder.rewrite_query2ids()
        id_query_json = id_query.to_dict()

        # store es-query and build iiif uri
        persist = request_data.get("persist", False)
        # todo für Darstellung im UV Scroll
        dyn_manif =project_config_store.get_value("query.dyn_manif")
        iiif_host =project_config_store.get_value("hosts.iiif_presentation")

        if not dyn_manif:
            return None
        if request_data.get("viewer") and "Scroll" in request_data.get("viewer") and "UV" in request_data.get("viewer") and dyn_manif:
            dyn_manif = dyn_manif + "2"

        if dyn_manif:
            store_url = f"{iiif_host}/{dyn_manif}/store_id/"
            store_id_response = requests.post(store_url, json=id_query_json, verify=False)
            if DEBUG:
                print(store_url)
                print(store_id_response)
            if store_id_response:
                store_id = store_id_response.json()["dyn_manif_id"]
                dyn_manif_url = f"{iiif_host}/{dyn_manif}/{store_id.strip()}/flex_manifest/"
                if DEBUG:
                    print("ID QUERY", id_query_json)
                    print(store_url)
                    print(dyn_manif_url)
                if persist:
                    # todo: Datenbank einbauen
                    print("ID will be stored in local database: {}".format(dyn_manif_url))
                # manipulate json response add iiif flex manifest url and remove Dossier hit from facets for IIIF View
                json_response["iiif_flex_url"] = dyn_manif_url
                object_facet_values = json_response.get("facets", {}).get("object_type.keyword", [])
                for n, k in enumerate(object_facet_values):
                    if k["label"] == "Dossier":
                        object_facet_values.pop(n)

            r = rdv_object._return_response(json_response)
            return r


#todo: ablöse GET
@rdv_query_api_v2.route('/form_query/<project>/', methods=['POST'])
@rdv_query_api_v2.route('/form_query/<field>/', methods=['GET'], defaults={'value': "", "project": None})
@rdv_query_api_v2.route('/form_query/<field>/<value>', methods=['GET'], defaults={"project": None})
@rdv_query_api_v2.route('/form_query/<project>/<field>/<value>', methods=['GET'])
class RDVForm(RDVSearch, Resource):

    # todo: @rdv_query_api_v2.expect(rdv_complex_query)
    # todo: @rdv_query_api_v2.response(200, "Success", rdv_data)
    @rdv_query_api_v2.doc(params=project_documentation)
    def get(self, field, value, project=None):

        rdv_class, project_config_store = map_host2def(project)
        form_class = get_form_class(project_config_store)

        form_obj = form_class(project_config_store=project_config_store,
                              host=request.url_root, project=project)
        if PROFILE:
            cProfile.runctx('form_obj(field=field, query=value)', globals(), locals(), "restats")
            p = pstats.Stats('restats')
            p.strip_dirs().sort_stats('cumtime').print_stats("*")
            p.print_callees("pre_build_rdvformat(hits_subcat=hits_subcat, dsl=True)")
        return form_obj(field=field, query=value) #object_id="ezas2_1a7c4")


    def post(self, project=None):
        request_data = self.get_request_data()
        if DEBUG:
            print(request_data)
        field = request_data.get("field")
        query = request_data.get("query", "")
        selected_entries = request_data.get("added_entries", "")
        object_id = request_data.get("object_id", "")

        rdv_class, project_config_store = map_host2def(project)
        form_class = get_form_class(project_config_store)

        form_obj = form_class(project_config_store=project_config_store,
                              host=request.url_root, project=project)
        if PROFILE:
            cProfile.runctx('form_obj(field=field, query=value)', globals(), locals(), "restats")
            p = pstats.Stats('restats')
            p.strip_dirs().sort_stats('cumtime').print_stats("*")
            p.print_callees("pre_build_rdvformat(hits_subcat=hits_subcat, dsl=True)")
        return form_obj(field=field, query=query, object_id = object_id, selected_entries=selected_entries)


@rdv_query_api_v2.route('/autocomplete/<project>/', methods=['POST'])
class RDVAutocomplete(RDVSearch, Resource):

    @rdv_query_api_v2.expect(rdv_complex_query)
    @rdv_query_api_v2.response(200, "Success", rdv_data)
    @rdv_query_api_v2.doc(params=project_documentation)
    def post(self, project=None):
        request_data = self.get_request_data()
        del request_data["query_params"]["sort"]
        request_data["query_params"]["sort"] = []

        try:
            rdv_class, project_config_store = map_host2def(project)
        except InvalidUsage:
            raise InvalidUsage('No project defined for host', status_code=410)
        deepcopy_request_data = deepcopy(request_data)
        rdv_object = rdv_class(request_data=deepcopy_request_data,
                                project_config_store=project_config_store, autocomplete=True)
        autocomplete = RDVESAutocomplete(config_store=project_config_store, request_data=request_data)
        # searchquery is same as v1 but additinoally contains the block(s): filter": [ { "bool": { "must": [ { "query_string": { "query": "afrika", "rewrite": "top_terms_1000" } } ] } } ] }
        search = autocomplete.build_complex_facet_search_body()
        rdv_object.body = search.to_dict()

        r = rdv_object.build_facet_autocomplete()

        return r
