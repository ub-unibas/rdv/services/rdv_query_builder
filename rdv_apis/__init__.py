

import socket

from flask import Blueprint
from flask_restx import Api

from .rdv_query import rdv_query_api as rdv_query_api
from .rdv_rss import rdv_rss_api as rdv_rss_api
from .rdv_iiif import rdv_iiif_api as rdv_iiif_api
from .rdv_object import rdv_object_api as rdv_object_api

# v1 APIs
api_v1 = Blueprint('api_v1', __name__, url_prefix='/v1')
rdv_api_v1 = Api(api_v1, doc='/doc/', version="unstable", title='RDV API',
                    description='APIs for RDV (ES Query, View, RSS Highlights, IIIF...)')
rdv_api_v1.add_namespace(rdv_query_api)
rdv_api_v1.add_namespace(rdv_object_api)
rdv_api_v1.add_namespace(rdv_iiif_api)
rdv_api_v1.add_namespace(rdv_rss_api)




