from flask import Blueprint
from flask_restx import Api
from .rdv_object_version import rdv_object_version_api
from .rdv_query import rdv_query_api as rdv_query_api
from .rdv_query_v2 import rdv_query_api_v2 as rdv_query_api_v2
from .rdv_object import rdv_object_api as rdv_object_api
#from rdv_feature_extraction_ubit.blueprint_feature_detection import ns_iiif_features_extraction
from rdv_exhibit_ubit import ns_exhibit

url_prefix = "/v1"
documentation_endpoint = "/doc/"

# v1 APIs
api_v1 = Blueprint('api_v1', __name__, url_prefix=url_prefix)
home_page = Blueprint('home-page', __name__)

@home_page.route("/")
def index_page():
    return 'See <a href="{}{}">Swagger Documentation</a> of this API'.format(url_prefix, documentation_endpoint)

rdv_api_v1 = Api(api_v1, doc=documentation_endpoint, version="unstable", title='RDV API',
                    description='This is the documentation of the [rdv_query_builder](https://gitlab.switch.ch/ub-unibas/rdv/services/rdv_query_builder) API. The rdv_query_builder API is the backend to which the angular front-end application [rdv_viewer](https://gitlab.switch.ch/ub-unibas/rdv/services/rdv_viewer) speaks. Its main duty is to transform queries sent by the front-end to elasticsearch and vice versa.')
rdv_api_v1.add_namespace(rdv_query_api)
rdv_api_v1.add_namespace(rdv_object_api)
rdv_api_v1.add_namespace(rdv_object_version_api)
#rdv_api_v1.add_namespace(ns_iiif_features_extraction)
rdv_api_v1.add_namespace(ns_exhibit)

## API v2
api_v2 = Blueprint('api_v2', __name__, url_prefix='/v2')
rdv_api_v2 = Api(api_v2,
                 doc=documentation_endpoint,
                 version="unstable",
                 title='RDV API',
                 description='This is the documentation of the [rdv_query_builder](https://gitlab.switch.ch/ub-unibas/rdv/services/rdv_query_builder) API. The rdv_query_builder API is the backend to which the angular front-end application [rdv_viewer](https://gitlab.switch.ch/ub-unibas/rdv/services/rdv_viewer) speaks. Its main duty is to transform queries sent by the front-end to elasticsearch and vice versa.')
rdv_api_v2.add_namespace(rdv_query_api_v2)
rdv_api_v2.add_namespace(rdv_object_api)
rdv_api_v2.add_namespace(rdv_object_version_api)
rdv_api_v2.add_namespace(ns_exhibit)
