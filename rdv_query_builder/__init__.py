from rdv_query_builder.base.rdv_esquery import RDV2ES
from rdv_query_builder.base.rdv_view import RDVView
from rdv_query_builder.base.rdv_hier import RDVViewHier
from rdv_query_builder.base.rdv_form import RDVForm, RDVDatabase

"""
from rdv_query_builder.projects.refactured.zas import RDV2ES4ZAS, RDV2ES4TESTZAS, ZasView, RDV2ES4DevZAS
from rdv_query_builder.projects.refactured.zas_int import RDV2ES4ZASINT, RDV2ES4TESTZASINT, ZASForm, ZasDatabase, ZasTestDatabase, \
    ZasLocalTestDatabase, RDV2ES4DEVZASINT, ZasIntDevView, ZasLocalDevDatabase
from rdv_query_builder.projects.refactured.zas_auth import RDV2ES4ZASAUTH

from rdv_query_builder.projects.discarded.digi2rdv import RDV2ES4Digi
# from rdv_query_builder.projects.bla import RDV2ES4Bla
from rdv_query_builder.projects.edoc import RDV2ES4Edoc
from rdv_query_builder.projects.discarded.itb import RDV2ES4ITB
from rdv_query_builder.projects.hieronymus import RDV2ES4Hieronymus, HieronymusView

# refactured projects
#from rdv_query_builder.projects.refactured.afrikaportal import RDV2ES4Afrika, AfricaView
from rdv_query_builder.projects.refactured.autographen import AutographenView, RDV2ES4Autographen
from rdv_query_builder.projects.refactured.bernoulli import RDV2ES4Bernoulli, BernoulliView
from rdv_query_builder.projects.refactured.best_db import RDV2ES4Bestand, BestView
from rdv_query_builder.projects.refactured.best_db.tinti import RDV2ES4Tinti, TintiView
from rdv_query_builder.projects.refactured.bla import RDV2ES4Bla, BlaView
from rdv_query_builder.projects.refactured.burckhardt import RDV2ES4Burckhardt, BurckhardtView
from rdv_query_builder.projects.refactured.digispace import RDV2ES4Digispace, DigispaceView
from rdv_query_builder.projects.refactured.fortbild import RDV2ES4Fortbild, FortbildView
from rdv_query_builder.projects.refactured.faesch import RDV2ES4Faesch, FaeschView
from rdv_query_builder.projects.refactured.googlebooks import RDV2ES4GBooks, GBooksView
from rdv_query_builder.projects.refactured.itb import RDV2ES4ITB, ITBView, ITBDatabase, RDV2ES4TESTITB, ITBTestView
from rdv_query_builder.projects.refactured.persdb import RDV2ES4PersDB, PersDBView
from rdv_query_builder.projects.refactured.mf226 import RDV2ES4MF226, MF226View
from rdv_query_builder.projects.refactured.nl_351 import RDV2ES4NL351, NL351View
from rdv_query_builder.projects.refactured.portraets import RDV2ES4Port, PortraetView
from rdv_query_builder.projects.refactured.swa_pdf import RDV2ES4SWAPDF, SWAPDFView, RDV2ES4TESTSWAPDF, RDV2ES4LOCALSWAPDF, \
    SWAPDFView, SWAPDFTestView, SWAPDFLocalView
from rdv_query_builder.projects.refactured.swa_sachdok import RDV2ES4SWASachdok, SWASachdokView, SWASachdokTestView
from rdv_query_builder.projects.refactured.swa_search import RDV2ES4SWA, SWAView, SWATestView, RDV2ES4TestSWA
from rdv_query_builder.projects.refactured.vise import RDV2ES4Vise, ViseView
from rdv_query_builder.projects.refactured.newspapers import RDV2ES4Newspaper, NewspapersView

# not relevant projects, tipp tipp, tipp2
from rdv_query_builder.projects.discarded.basbibl import RDV2ES4BasBibl
from rdv_query_builder.projects.discarded.az_db import RDV2ES4AZDB
from rdv_query_builder.projects.discarded.bibnetz import RDV2ES4Bibnetz
from rdv_query_builder.projects.discarded.lesesaal import RDV2ES4Lesesaal
from rdv_query_builder.projects.discarded.old_news2rdv import RDV2ES4OldNews
"""