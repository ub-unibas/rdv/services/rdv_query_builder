import time
import json
import calendar
import unicodedata
import re
import os
from copy import deepcopy
from collections import defaultdict, Counter

from flask import Response
from text_unidecode import unidecode

from datetime import datetime
from elasticsearch import Elasticsearch

from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import RQ_FACETS, RQ_VISUAL_AGGS, RQ_QUERY_PARAMS, \
    RQ_FILTER, RQ_SORTAFTER_ORDER, RQ_SORTAFTER_VALUES, RDV_HIT_COUNT, RDV_NO_ENTRY, RQ_SIZE, RDV_FACETPAIRS, RDV_SNIPPETS, RDV_SUBVALUES, \
    RDV_VISUAL_AGGS, \
    SNIPPET_SEARCH_AFTER, SNIPPET_FULLTEXT, SNIPPET_ID, \
    RQ_GTE, RQ_LTE, RQ_POS, RQ_SHOW_AGGS, RQ_ORDER, RQ_FIELD, RDV_FIELD, RQ_OPERATOR, RQ_VALUES, RDV_VALUE, RDV_LABEL, \
    RDV_COUNT, RQ_FACET_TYPE, HIER_ES_BROADER, HIER_ES_LABEL, HIER_ES_POSITION, RQ_LANG, RQ_SELECTION, RQ_FACET_VISIBLE, \
    HIER_ES_SORT, RDV_GEOPOINT, RDV_CENTROID, RDV_GEO_SNIPPETS, RDV_GEOHASH

nested_dict = lambda: defaultdict(nested_dict)
# TODO: aggregations und filters entschlacken



class RDV2ES:

    def __init__(self, request_data, project_config_store, autocomplete=False, load_aggs_async=False):
        """

        :param request_data:
        :param es_client:
        :param index: is ignored, used from class var
        :param debug:
        :param iiif_host:
        :param autocomplete:
        :param es_host:
        """

        self.debug = project_config_store.get_value('base.DEBUG')
        self.load_aggs_async = load_aggs_async
        self.autocomplete = autocomplete
        self.hierarchy_field = ""
        hier_pos_field = HIER_ES_POSITION
        self.get_buckets = self._get_filtered_buckets
        if self.debug:
            print(json.dumps(request_data))

        self.request_data = request_data
        self.pj_conf = project_config_store
        view_class_name = self.pj_conf.get_value('classes.view')
        module = self.pj_conf.get_value('module') + ".rdv"

        import importlib
        try:
            self.view_class = getattr(
                importlib.import_module(module),
                view_class_name
            )
        except Exception as ex:
            import traceback
            traceback.print_exc()
            print("Exception while getting view_class {} from module {}: {}".format(view_class_name, module, ex))

        self.es_host = self.pj_conf.get_value('hosts.index')
        self.iiif_host = self.pj_conf.get_value('hosts.iiif_presentation')

        self.index = self.pj_conf.get_value('databases.index_name')

        self.iiif = self.pj_conf.get_value('query.iiif')
        self.dyn_manif = self.pj_conf.get_value('query.dyn_manif')
        self.max_ids_dyn_manif = self.pj_conf.get_value('query.max_ids_dyn_manif')

        self.max_facet_values = self.pj_conf.get_value('query.max_facet_values')

        self.highlight_field = self.pj_conf.get_value('query.highlight_field')
        self.highlight_included_fieldname = self.pj_conf.get_value('query.highlight_included_fieldname')
        # to be overwritten

        self.hierarchy_index = self.pj_conf.get_value('query.hierarchy_index')

        self.top_hier_query = self.pj_conf.get_value('query.top_hier_query')
        self.hier_top_id = self.pj_conf.get_value('query.hier_top_id') or ""
        self.default_sort = self.pj_conf.get_value('query.default_sort')
        self.search_after_query = self.pj_conf.get_value('query.search_after_query')

        # just for digidata tryout
        self.hierarchy_index_host = self.pj_conf.get_value('query.hierarchy_index_host')

        self.default_date_range = self.pj_conf.get_value('query.default_date_range')
        self.default_int_range = self.pj_conf.get_value('query.default_int_range')
        self.simple_query_fields = self.pj_conf.get_value('query.simple_query_fields')
        self.source_fields = self.pj_conf.get_value('query.source_fields')

        self.hierarchy_facet_sort = lambda x: str(x.get("label",""))
        # Elasticsearch
        self.es = Elasticsearch(self.es_host)

        # extract data from request
        # query
        self.query = self.transform_query(request_data.get("query"))

        #ids returned by query -> for iiif
        self.query_ids =[]

        self.match_all_query = request_data.get("match_all")
        self.ids = request_data.get("ids")


        self.search_from = request_data.get(RQ_QUERY_PARAMS, {}).get("start", 0)
        self.search_rows = request_data.get(RQ_QUERY_PARAMS, {}).get("size", 0)
        self.sort_fields = request_data.get(RQ_QUERY_PARAMS, {}).get("sort")
        self.language = request_data.get(RQ_LANG, "de")
        self.return_selection = request_data.get(RQ_SELECTION, True)
        self.selection = {"selection": {"facets": {}}}

        # js -es int problem
        self.search_after_values = json.loads(request_data.get(RQ_SORTAFTER_VALUES, [])) if request_data.get(RQ_SORTAFTER_VALUES, []) else []
        self.search_after_order = request_data.get(RQ_SORTAFTER_ORDER)

        self.filter_query = request_data.get(RQ_FILTER)
        self.visual_aggs = request_data.get(RQ_VISUAL_AGGS, {})

        # Facetten
        RQ_OPEN_FACETS = "open_facets"
        open_facets = request_data.get(RQ_OPEN_FACETS, {})
        facets = request_data.get(RQ_FACETS, {})


        # factten - Filtern
        # hier objektspezifischen Filter für Facetten einbauen bzw. für intern / extern Filter
        # facets = self._remove_filters(facets, ["Sprache"])
        # add_filter2 = {"descr_person":{"field":"descr_person.id.keyword","values":[],"operator":"OR","facet_type":"basic","size":100}}
        # facets.update(add_filter)

        self.facet_start_chars = {}
        self.facet_excl_entries = {}
        if request_data.get(RQ_OPEN_FACETS, {}):
            for key, value in request_data[RQ_OPEN_FACETS].items():
                if value:
                    self.facet_start_chars[key] = value
                    self.facet_excl_entries[key] = request_data.get("exclude_entries", {})

        if self.autocomplete:
            self.pre_search = request_data.get("added_entries", [])
            basic_facets = self._get_rangetype_open_filters(facets, "basic", open_facets)
            self.basic_facets = {}
            for k, v in basic_facets.items():
                if k in request_data.get("query_params",{}).get("facet_size", {}):
                    self.basic_facets[k] = v
            self.basic_sub_facets = {}
            self.int_ranges = {}
            self.date_filters = {}
            self.geo_filters = {}
            self.hierarchy_facets = {}
            self.sub_facets ={}
        else:
            # Todo: als eigene Subkategorie definieren
            self.subfacet_name2 = "Dokumentation Konservierung"
            self.subfacet_name = "rel_persons2"
            if self.subfacet_name in facets:
                self.sub_facets = {self.subfacet_name: facets[self.subfacet_name]}
                facets = self._remove_filters(facets, [self.subfacet_name])
                if self.sub_facets[self.subfacet_name].get("values", []):
                    sub_key = self.sub_facets.get(self.subfacet_name, {}).get("values", [])[0].get("id")
                    facets.get("rel_persons2 Auswahl", {})["es_field"] = "{}.{}.id.keyword".format(self.subfacet_name, sub_key)
            else:
                self.sub_facets = self._get_rangetype_open_filters(facets, "sub_facet", open_facets)

            self.basic_facets = self._get_rangetype_open_filters(facets, "basic", open_facets)
            self.basic_sub_facets = self._get_rangetype_open_filters(facets, "subcat", open_facets)
            self.int_ranges = self._get_rangetype_open_filters(facets, "int", open_facets)
            self.date_filters = self._get_rangetype_open_filters(facets, "date", open_facets)
            self.geo_filters = self._get_rangetype_open_filters(facets, "geo", open_facets)
            self.hierarchy_facets = self._get_rangetype_open_filters(facets, "hierarchy", open_facets)

    def rewrite_value(self, key, value, reverse=False):
        return value

    def rewrite_value_old(self, key, value, reverse=False):
        """function to rewrite terms results"""
        used_dict = {}
        if reverse == True:
            for k, rewrite_value in self.rewrite_dict.get(key,{}).items():
                if isinstance(rewrite_value, list):
                    for v in rewrite_value:
                        used_dict.setdefault(v, []).append(k)
                else:
                    used_dict.setdefault(rewrite_value, []).append(k)
        else:
            used_dict = self.rewrite_dict.get(key)

        # if no rewrite dict is defined return normal value
        if not used_dict:
            return value
        else:
            return used_dict.get(value, "tbd: {}".format(value))

    def split_search_phrase(self, search_phrase):
        if isinstance(search_phrase, list) and len(search_phrase) != 1:
            return search_phrase
        if len(search_phrase) == 1:
            search_phrase = search_phrase[0]
        return [sp for sp in re.split("[ ]", search_phrase) if sp]

    def transform_query(self, query):
        # hack to adapt format todo: sebastian fragen

        json_string = json.dumps(query)
        json_string = json_string.replace('"searchFields":', '"fields":')
        # json_string = json_string.replace('"query_string":', '"simple_query_string":')
        query_def = json.loads(json_string)
        if query:
            query_params = query_def.get("bool",{}).get("must",[{}])[0].get("query_string",{})
            # todo: anpassen: self autocomplete
            # if self.autocomplete:
            #    query_params["query"] = query_params["query"] + "*"
            if query_params and query_params.get("op"):
                op = query_params.get("op", "AND")
                op = " {} ".format(op)
                search_phrase = self.split_search_phrase(query_params.get("query"))
                # for asterisk search
                if len(search_phrase) == 1:
                    es_query = op.join(["{}".format(w) for w in search_phrase])
                else:
                    es_query = op.join(["\"{}\"".format(w) for w in search_phrase])
                del query_params["op"]
                query_params["query"] = es_query

        return query_def

    def build_id_query(self):
        # rewrite to id query
        es_query = deepcopy(self.body)
        es_query["size"] = self.search_rows if self.search_rows and self.search_rows < self.max_ids_dyn_manif else self.max_ids_dyn_manif
        es_query["from"] = self.search_from if self.search_from and self.search_from < 9900 else 0
        es_query["_source"] = False
        if not self.query_ids:
            self.query_ids = [r["_id"] for r in self.es.search(index=self.index, body=es_query)["hits"]["hits"]]

        id_query = {
            "_source": [], "query": {
            "bool": {"must": [{
                "ids": {
                    # "type" : "_doc",
                    "values": self.query_ids
                }}
            ]}
        },
        # es_query["sort"] und nicht self.sort_fields because not prepared
                    # size because otherwise only 10 items are returned
        "sort": es_query["sort"],
                    "size": self.max_ids_dyn_manif}
        id_query["query"]["bool"]["must"].extend(self.main_query["bool"]["must"])
        if self.debug:
            print("ID-QUERY", json.dumps(id_query))
        return id_query

    def get_default_field_type(self, field):
        result =self.es.indices.get_field_mapping(fields=field, index=self.index)
        for index, mapping in result.items():
            type_ = list(mapping.get("mappings",{}).get(field,{}).get("mapping",{}).values())
            if type_:
                mapping_type = type_[0].get("type")
                return mapping_type

    def build_search_body(self):
        """build body for search with simple params"""
        search_body = nested_dict()

        # general search params

        if self.sort_fields:
            for n, sort_data in enumerate(self.sort_fields):
                order = sort_data.get(RQ_ORDER)
                sortfield = sort_data.get(RQ_FIELD)

                sort_settings = {"order": order}
                default_set = False
                if not self.query and n == 0 and sortfield == "_score" and self.default_sort:
                    search_body.setdefault("sort", []).extend(self.default_sort)
                    default_set = True
                if sortfield not in ["_score", "_id"]:
                    default_type = self.get_default_field_type(sortfield)
                    if default_type:
                        print("DEFAULT TYPE", default_type)
                        sort_settings.update({"unmapped_type": default_type})
                    else:
                        sort_settings.update({"unmapped_type": "date"})
                search_body.setdefault("sort", []).append({sortfield: sort_settings})
                if not default_set:
                    search_body.setdefault("sort", []).extend(self.default_sort)
        search_body.setdefault("sort", []).append({"_score": {"order": "desc"}})
        search_body.setdefault("sort", []).append({"_id": {"order": "asc"}})

        if self.search_from:
            search_body["from"] = self.search_from
        if self.search_rows:
            search_body["size"] = self.search_rows
        if self.source_fields:
            search_body["_source"] = self.source_fields
        # for exact number of hits
        search_body["track_total_hits"] = True
        # get search_after values in response
        search_body["track_scores"] = True

        # build query
        if self.query:
            # todo: anpassen, suche in all fields zu langsam
            for q in self.query["bool"]["must"]:
                if q.get("query_string"):
                    if self.simple_query_fields:
                        q["query_string"]["fields"] = self.simple_query_fields
                    q["query_string"]["rewrite"] = "top_terms_1000"
                    # causes maxClauseCount Exception q["query_string"]["rewrite"] = "scoring_boolean"
            search_body["query"] ={"bool": {"must": [deepcopy(self.query)], "should": []}}
            search_body["query"]["bool"]["should"].extend(self.get_boost_query())
            main_query = self.query
            # für impresso
            search_body["highlight"]["fields"][self.highlight_field] = {}

        elif self.match_all_query:
            match_all = {"bool": {"must": [{"match_all": {}}]}}
            search_body["query"] = deepcopy(match_all)
            main_query = match_all
        elif self.ids:
            id_query = {"ids": {"values": self.ids}}
            search_body["query"] = deepcopy(id_query)
            main_query = id_query
        else:
            main_query = {"bool": {"must": [{"match_all": {}}]}}

        if self.filter_query:
            search_body["query"]["bool"]["filter"] = deepcopy(self.filter_query)

        # also convert deepcopies to defaultdict
        search_body = self._to_nesteddict(search_body, lambda: defaultdict(nested_dict))
        # needs to be set here, so it is possible to overwrite certain values in subclasses
        if self.autocomplete:
            if self.pre_search:
                #todo: op auslesen
                # op = query_def.get("bool", {}).get("must", [{}])[0].get("query_string", {}).get("op")
                op = "AND"
                op = " {} ".format(op)
                es_query = op.join(["\"{}\"".format(w) for w in self.pre_search])
                self.main_query = {"bool": {"must": [{"query_string": {"query": es_query,
                                                              "fields": ["fulltext", "descr_fuv.*", "stw_ids.*",
                                                                         "descr_person.*"], "rewrite": "top_terms_1000"}}]}}
            else:
                self.main_query = {'bool': {'must': []}}
        else:
            self.main_query  = main_query
        self.search_body = search_body
        # search body is reused in other contexts
        self.body = deepcopy(self.search_body)

    def get_boost_query(self):
        return []

    def build_selection(self, field, id_, operator):
        try:
            s_facets = self.selection["selection"]["facets"]
            label = self.descriptor_label(field, id_)
            s_facets.setdefault(field, {}).setdefault("values",[]).append({"value": {"id":id_}, "label": label})
            s_facets[field]["operator"] = operator
        except KeyError:
            import traceback
            traceback.print_exc()
            pass

    def build_hier_selection(self, field, id_, operator):
        pass
        #to be overwritten

    # helpers to build results for angular app
    def descriptor_label(self, field, id_):
        if ".id" in field:
            main_field = field.split(".id")[0]
            query = {"query": {"terms": {field: [id_]}}, "_source": {"includes": [main_field]}, "size": 1}
            results = self.es.search(index=self.index, body=query)
            if results["hits"]["hits"]:
                lookup = {v["id"]: v["label"] for v in results["hits"]["hits"][0]["_source"][main_field]}
            else:
                lookup = {}
            label = lookup.get(id_, id_)
            return label
        else:
            return id_

    def _build_filters(self):
        """build filters from facets and ranges"""
        # shortcuts to set values
        body_query = defaultdict(nested_dict)
        body_query_filter = body_query["filter"]["bool"]

        # build search filters
        # build facet filter for query
        for key, facet_data in self.basic_facets.items():
            operator = self._get_bool_operator(facet_data[RQ_OPERATOR])
            facet_term_filter = []
            facet_query = {"bool": {operator: facet_term_filter}}
            es_field = facet_data.get("es_field") or facet_data[RQ_FIELD]
            for facet_value in facet_data[RQ_VALUES]:
                facet_value = facet_value["id"]
                self.build_selection(es_field, facet_value ,facet_data[RQ_OPERATOR])

                # reverse values
                facet_value = self.rewrite_value(key, facet_value, reverse=True)
                facet_value = facet_value if isinstance(facet_value, list) else [facet_value]
                facet_term_filter.append({"terms": {es_field: facet_value}})
            if facet_term_filter:
                body_query_filter.setdefault("must", []).append(facet_query)

        for key, sub_facet_data in self.basic_sub_facets.items():
            operator = self._get_bool_operator(sub_facet_data[RQ_OPERATOR])
            facet_term_filter = []
            facet_query = {"bool": {operator: facet_term_filter}}
            for sub_facet_value in sub_facet_data[RQ_VALUES]:
                sub_facet_value = sub_facet_value["id"]
                self.build_selection(sub_facet_data[RQ_FIELD], sub_facet_value, sub_facet_data[RQ_OPERATOR])

                # reverse values
                sub_facet_value = self.rewrite_value(key, sub_facet_value, reverse=True)
                sub_facet_value = sub_facet_value if isinstance(sub_facet_value, list) else [sub_facet_value]
                facet_term_filter.append({"terms": {sub_facet_data[RQ_FIELD]: sub_facet_value}})
            if facet_term_filter:
                body_query_filter.setdefault("must", []).append(facet_query)

        # build hierarchy filter for query
        for key, hierarchy_data in self.hierarchy_facets.items():
            # todo: überlegen ob auch should funktioniert
            operator = "must"
            hierarchy_term_filter = []
            hierarchy_query = {"bool": {operator: hierarchy_term_filter}}
            for hierarchy_value in hierarchy_data[RQ_VALUES]:
                # todo gegen den ID-Index
                hier_stw_id = hierarchy_value.get("id")
                hier_es_field = hierarchy_data[RQ_FIELD]
                hier_pos, hier_id = self.get_broader_id(hierarchy_value, hier_es_field, rdv_facet_key=key)
                self.build_hier_selection(hierarchy_data[RQ_FIELD], hier_stw_id, hierarchy_data[RQ_OPERATOR])
                hierarchy_term_filter.append({"prefix": {hierarchy_data[RQ_FIELD]: {"value": hier_pos}}})
            if hierarchy_term_filter:
                body_query_filter.setdefault("must", []).append(hierarchy_query)

        # build date filter
        for key, date_data in self.date_filters.items():
            operator = self._get_bool_operator(date_data[RQ_OPERATOR])
            date_filter = []
            date_query = {"bool": {operator: date_filter}}
            for date_value in date_data[RQ_VALUES]:
                # todo: durch date agg laufen lassen, operator ergänzen
                self.selection["selection"]["facets"].setdefault(date_data[RQ_FIELD], {}).setdefault("values",[]).append({"value": date_value})
                # todo: Abfrag Datumsformat anpassen
                date_filter.append({"range": {date_data[RQ_FIELD]:
                                                  {"gte": date_value.get(RQ_GTE, "0001-01-01"),
                                                   "lte": date_value.get(RQ_LTE, "9999-12-31")}}})
            if date_filter:
                body_query_filter.setdefault("must", []).append(date_query)

        # build int filter
        for key, int_data in self.int_ranges.items():
            operator = self._get_bool_operator(int_data[RQ_OPERATOR])
            int_filter = []
            int_query = {"bool": {operator: int_filter}}
            es_field = int_data.get("es_field") or int_data[RQ_FIELD]
            for int_value in int_data[RQ_VALUES]:
                # todo: durch date agg laufen lassen, operator ergänzen
                self.selection["selection"]["facets"].setdefault(es_field, {}).setdefault("values",[])\
                    .append({"value": int_value, "label":"{}-{}".format(int_value.get(RQ_GTE, 0),int_value.get(RQ_LTE, 99999999999))})
                int_filter.append({"range": {
                    es_field: {"gte": int_value.get(RQ_GTE, 0), "lte": int_value.get(RQ_LTE, 99999999999)}}})
            if int_filter:
                body_query_filter.setdefault("must", []).append(int_query)

        # build visual aggs
        # todo: lässt sich das nicht über andere range queries lösen?
        for key, visual_agg in self.visual_aggs.items():
            # momentan nur einmal möglich
            for value in visual_agg[RQ_VALUES]:
                range_query = {"bool": {
                    "should": [{"range": {visual_agg[RQ_FIELD]: {"gte": value[RQ_GTE], "lte": value[RQ_LTE]}}}]}}
                if visual_agg.get("showMissingValues"):
                    range_query["bool"]["should"].append(
                        {"bool": {"must_not": {"exists": {"field": visual_agg[RQ_FIELD]}}}})
                body_query.setdefault("must", []).append(range_query)

        filter_queries = body_query.get("must", []) + body_query_filter.get("must", [])

        return body_query, filter_queries

    def _build_aggregations(self):
        """build aggregations/ranges and include filters in aggregations for facet counts"""
        aggregations = defaultdict(nested_dict)
        self.set_aggs_filter = self._build_aggs_filter_func(aggregations)

        # build histogram and missing aggregations
        # todo: umschreiben für range
        for key, visual_agg in self.visual_aggs.items():
            aggregations["histogram_" + key]["aggs"] = \
                {"filtered_histogram_" + key:
                     {"histogram": {"field": visual_agg[RQ_FIELD], "interval": 1,
                                    "extended_bounds": {"min": visual_agg["min"], "max": visual_agg["max"]}}}}

            aggregations["missing_" + key]["aggs"] = {
                "filtered_missing_" + key: {"missing": {"field": visual_agg[RQ_FIELD]}}}
            # build filter aggregation for ranges
            self._set_misshist_filter(key, self.main_query)
            self._set_misshist_filter(key, self.filter_query)
            for filter_query in self.filter_queries:
                # exclude filter from own facet if operator is OR (except construction)
                # Begründung: Jetzt noch die Range-Termqueries der ANDEREN RANGES hier einfuegen, eigene range-term-Query dabei ignorieren (sonst wuerde man die anderen Optionen dieser Range nicht sehen, also z.B. Jahreswerte ausserhalb des ausgewaehlte Bereichs)
                try:
                    filter_query["range"][key]
                except (KeyError, TypeError):
                    self._set_misshist_filter(key, filter_query)


        # build term aggregations for each facet
        for key, facet_data in self.basic_facets.items():
            if facet_data.get(RQ_FACET_VISIBLE):
                # build terms aggregation
                sort_order = self.facet_query_order2(facet_data)
                field_name = facet_data.get("es_field") or facet_data[RQ_FIELD]

                # for label ordering
                old_field_name = field_name
                if facet_data.get(RQ_ORDER) == "label":
                    field_name = field_name.replace(".id", ".label")

                size = facet_data[RQ_SIZE]
                aggregations[key]["aggs"] = {"filtered_" + old_field_name: {"terms": {"field": field_name, "size": size, "order": sort_order}}}

                if ".id" in field_name:
                    main_field = field_name.split(".id")[0]
                    top_hits = {"aggs": {"example": {"top_hits": {"size": 1, "_source": {"includes": [main_field]}}}}}
                    aggregations[key]["aggs"]["filtered_" + field_name].update(top_hits)
                # todo: old field name könnte hier keine gute Idee sein? Probleme bei der Zuordnung später?
                elif ".label" in field_name:
                    main_field = field_name.split(".label")[0]
                    top_hits = {"aggs": {"example": {"top_hits": {"size": 1, "_source": {"includes": [main_field]}}}}}
                    aggregations[key]["aggs"]["filtered_" + old_field_name].update(top_hits)
                elif ".keyword" in field_name:
                    main_field = field_name.split(".keyword")[0]
                    top_hits = {"aggs": {"example": {"top_hits": {"size": 1, "_source": {"includes": [main_field]}}}}}
                    aggregations[key]["aggs"]["filtered_" + field_name].update(top_hits)

                # build filter aggregation
                self.set_aggs_filter(key, self.main_query)
                self.set_aggs_filter(key, self.filter_query)
                # add filters from other facets / ranges
                for filter_query in self.filter_queries:
                    # exclude filter from own facet if operator is OR (except / if construction)
                    try:
                        filter_query["bool"]["should"][0]["terms"][field_name]
                    except (KeyError, TypeError):
                        self.set_aggs_filter(key, filter_query)
                    if facet_data[RQ_OPERATOR] != "OR":
                        self.set_aggs_filter(key, filter_query)

        # build term aggregations for each facet
        for key, sub_facet_data in self.basic_sub_facets.items():
            if sub_facet_data.get(RQ_FACET_VISIBLE):
                # build terms aggregation
                field_name = sub_facet_data[RQ_FIELD]
                size = sub_facet_data[RQ_SIZE]
                aggregations[key]["aggs"] = {"filtered_" + field_name: {"terms": {"field": field_name, "size": size}}}

                if ".id" in field_name:
                    main_field = field_name.split(".id")[0]
                    top_hits = {"aggs": {"example": {"top_hits": {"size": 1, "_source": {"includes": [main_field]}}}}}
                    aggregations[key]["aggs"]["filtered_" + field_name].update(top_hits)
                elif ".keyword" in field_name:
                    main_field = key.split(".keyword")[0]
                    top_hits = {"aggs": {"example": {"top_hits": {"size": 1, "_source": {"includes": [main_field]}}}}}
                    aggregations[key]["aggs"]["filtered_" + field_name].update(top_hits)

                # build filter aggregation
                self.set_aggs_filter(key, self.main_query)
                self.set_aggs_filter(key, self.filter_query)
                # add filters from other facets / ranges
                for filter_query in self.filter_queries:
                    # exclude filter from own facet if operator is OR (except / if construction)
                    try:
                        filter_query["bool"]["should"][0]["terms"][field_name]
                    except (KeyError, TypeError):
                        self.set_aggs_filter(key, filter_query)
                    if sub_facet_data[RQ_OPERATOR] != "OR":
                        self.set_aggs_filter(key, filter_query)

        # build term aggregations for date facets
        for key, facet_data in self.date_filters.items():
            # build terms aggregation
            if facet_data.get(RQ_SHOW_AGGS) and facet_data.get(RQ_FACET_VISIBLE):
                field_name = facet_data[RQ_FIELD]
                operator = facet_data[RQ_OPERATOR]
                es_interval, _ = self.get_date_agg_interval(facet_data[RQ_VALUES] or [self.default_date_range], operator, field=key)

                aggregations[key]["aggs"] = {"filtered_" + field_name: {
                    "date_histogram": {"field": field_name, "interval": es_interval, "order": {"_count": "desc"}}}}
                # build filter aggregation
                self.set_aggs_filter(key, self.main_query)
                self.set_aggs_filter(key, self.filter_query)
                # add filters from other facets / ranges
                for filter_query in self.filter_queries:
                    # exclude filter from own facet if operator is OR (except / if construction)
                    try:
                        filter_query["bool"]["should"][0]["terms"][field_name]
                    except (KeyError, TypeError):
                        self.set_aggs_filter(key, filter_query)
                    if facet_data[RQ_OPERATOR] != "OR":
                        self.set_aggs_filter(key, filter_query)

        # build term aggregations for int facets
        for key, facet_data in self.int_ranges.items():
            # build terms aggregation
            if facet_data.get(RQ_SHOW_AGGS) and facet_data.get(RQ_FACET_VISIBLE):
                # todo: beastandsanppsung
                field_name = facet_data.get("es_field") or facet_data[RQ_FIELD]
                operator = facet_data[RQ_OPERATOR]
                es_interval, _ = self.get_int_agg_interval(facet_data[RQ_VALUES], operator, field_name)

                aggregations[key]["aggs"] = {"filtered_" + field_name: {
                    "histogram": {"field": field_name, "interval": es_interval, "order": {"_count": "desc"}}}}
                # build filter aggregation
                self.set_aggs_filter(key, self.main_query)
                self.set_aggs_filter(key, self.filter_query)
                # add filters from other facets / ranges
                for filter_query in self.filter_queries:
                    # exclude filter from own facet if operator is OR (except / if construction)
                    try:
                        filter_query["bool"]["should"][0]["term"][field_name]
                    except (KeyError, TypeError):
                        self.set_aggs_filter(key, filter_query)
                    if facet_data[RQ_OPERATOR] != "OR":
                        self.set_aggs_filter(key, filter_query)

        return aggregations

    def build_complex_search(self):
        """add facet filters and aggregations to search"""
        body_query, self.filter_queries = self._build_filters()
        if self.filter_queries:
            self.body["query"]["bool"].update(body_query)

        aggregations = self._build_aggregations()
        if aggregations:
            # todo was bedeutet das: # //Global-Filter setzen, damit nicht auf gefilterte Dokumente aggregiert wird
            self.body["aggs"]["all_facets"]["global"] = {}
            self.body["aggs"]["all_facets"]["aggs"] = aggregations

        self.get_buckets = self._get_filtered_buckets
        json.dumps(self.body)

    def query_hierarchy_index(self, hierarchy_facet_data, hits_subcat=False, rdv_facet_key=None):
        # TODO: momentan nur eine Kategorie auswählbar
        # todo: replace keyword nicht sehr elegant
        hier_pos_field = hierarchy_facet_data[RQ_FIELD].replace(".keyword","")
        #hier_pos_field = HIER_ES_POSITION
        if hierarchy_facet_data and hierarchy_facet_data[RQ_VALUES] and hierarchy_facet_data.get(RQ_FACET_VISIBLE):
            hierarchy_value = hierarchy_facet_data[RQ_VALUES][-1]
            hier_pos, hier_id = self.get_broader_id(hierarchy_value, hier_pos_field, rdv_facet_key=rdv_facet_key)
            # todo: size variabel machen
            # for hierarchy index
            narrower_body = {"size": 500, "query": {
                "bool": {"must": [{"term": {"{}.keyword".format(HIER_ES_BROADER): hier_id}}, {"exists": {"field": HIER_ES_POSITION}}]}}}
            start = time.time()
            subcats = self.get_hierarchy_subcat_msearch(narrower_body, hits_subcat, hier_pos_field, parent_hier_id = hier_pos, rdv_facet_key=rdv_facet_key)
            end = time.time()
            if self.debug:
                print("Hierarchy-Query Time", hierarchy_value, end - start)
        # if no parent is given get root (= not broader + hierarchy filter)
        elif hierarchy_facet_data.get(RQ_FACET_VISIBLE):
            # TODO: hier
            body = self.get_top_hier_query(hier_pos_field, rdv_facet_key=rdv_facet_key)
            start = time.time()
            hier_top_id = self.get_top_hier_id(hier_pos_field, rdv_facet_key=rdv_facet_key)
            subcats = self.get_hierarchy_subcat_msearch(body, hits_subcat, hier_pos_field, parent_hier_id = hier_top_id, rdv_facet_key=rdv_facet_key)
            end = time.time()
            if self.debug:
                print("Hierarchy-Query Time nocat", end - start)
        else:
            subcats =[]
        return subcats

    def get_hier_index(self, hier_es_field, rdv_facet_key=None):
        hier_es_field = hier_es_field.replace(".keyword", "")
        if isinstance(self.hierarchy_index, dict):
            index_name = self.hierarchy_index.get(rdv_facet_key, {}).get("index_name") or self.hierarchy_index.get(hier_es_field,{}).get("index_name")
            return index_name
        # for old solution
        else:
            return self.hierarchy_index

    def get_top_hier_query(self, hier_es_field, rdv_facet_key=None):
        hier_es_field = hier_es_field.replace(".keyword", "")
        if isinstance(self.hierarchy_index, dict):
            top_query = self.hierarchy_index.get(rdv_facet_key, {}).get("top_query") or self.hierarchy_index.get(
                hier_es_field, {}).get("top_query")
            return top_query
        # for old solution
        else:
            return self.top_hier_query

    def get_top_hier_id(self, hier_es_field, rdv_facet_key=None):
        hier_es_field = hier_es_field.replace(".keyword", "")
        if isinstance(self.hierarchy_index, dict):
            top_id = self.hierarchy_index.get(rdv_facet_key, {}).get("top_id") or self.hierarchy_index.get(
                hier_es_field, {}).get("top_id")
            # darf nicht none zurückliefern
            return top_id or ""
        # for old solution
        else:
            return self.hier_top_id

    def get_broader_id(self, hier_value, hier_es_field, rdv_facet_key=None):
        # todo id extraction anpassen
        hier_index = self.get_hier_index(hier_es_field, rdv_facet_key=rdv_facet_key)
        hier_id = hier_value.get("id").replace(hier_index + "_", "")
        return hier_value.get(RQ_POS), hier_id

    def get_pref_label_hier(self, es_rec, hier_pos_field):
        return es_rec["_source"][HIER_ES_LABEL][0] if isinstance(es_rec["_source"][HIER_ES_LABEL], list) \
            else es_rec["_source"][HIER_ES_LABEL]

    def get_sort_hier(self, es_rec):
        if not es_rec["_source"][HIER_ES_SORT]:
            return ""
        return es_rec["_source"][HIER_ES_SORT][0] if isinstance(es_rec["_source"][HIER_ES_SORT], list) \
            else es_rec["_source"][HIER_ES_SORT]

    def sort_sign(self, sign: str) -> tuple:
        """
        extract numeric part and return tuple with extracted part and original value to sort on
        >>> clean_parts(["1234-1297a"])
        [(1234, '1234-1297a')]

        :param sign_parts: value to extract numeric part
        :return: tuple, containing extracted part and original value to sort on
        """
        sign_split = re.compile("[ :]+")
        sign_parts = sign_split.split(sign.get("label", ""))
        cleansed_parts = []
        for p in sign_parts:

            p_mod = p.split("-")[0]

            if p_mod and p_mod[0].isnumeric():
                p_mod_n = ""
                for l in p_mod:
                    if l.isnumeric():
                        p_mod_n += l
                    else:
                        break
                p_mod = p_mod_n
            try:
                p_mod = int(p_mod)
            except ValueError:
                p_mod = p_mod
            cleansed_parts.append((p_mod, p))
        return cleansed_parts

    def get_hierarchy_subcat_msearch(self, query, hits_subcat, hier_pos_field, parent_hier_id="", rdv_facet_key=None):
        # todo: only for digidata hier tryout
        hier_index = self.get_hier_index(hier_pos_field, rdv_facet_key=rdv_facet_key)
        if not hier_index:
            return []
        if self.hierarchy_index_host:
            from elasticsearch import Elasticsearch
            result_narrower = Elasticsearch(self.hierarchy_index_host).search(index=hier_index, body=query)["hits"]["hits"]
        else:
            result_narrower = self.es.search(index=hier_index, body=query)["hits"]["hits"]
        if self.debug:
            print("hier query of get_hierarchy_subcat_msearch (query, result_narrower)")
        hier_results = []
        es_msearch = ""
        if result_narrower:
            for narrower in result_narrower:
                # was tun mit alten einträgen
                #hier_pos = narrower["_source"][hier_pos_field][0]
                hier_position = ""
                for h in narrower["_source"][HIER_ES_POSITION]:
                    if h.startswith(parent_hier_id) and len(parent_hier_id.split(".")) + 1 == len (h.split(".")):
                        hier_position = h
                        break
                id_ = narrower["_id"]
                #hier_sort = self.get_sort_hier(narrower)
                pref = self.get_pref_label_hier(narrower, hier_pos_field)
                prefLabel = pref

                if hier_position or 1:
                    entry = {RDV_VALUE: {"id": id_, RQ_POS: hier_position}, RDV_LABEL: prefLabel} #RDV_SORT: hier_sort}
                    hier_results.append(entry)

                if hits_subcat:
                    # for speedup: curl -X GET "localhost:9200/_search?q=message:number&size=0&terminate_after=1&pretty"?
                    count_body = self.build_hierarchy_search_body(hier_position, hier_pos_field)
                    es_msearch += json.dumps({"index": self.index}) + "\n" + json.dumps(count_body) + "\n"

            if es_msearch:

                result_count = self.es.msearch(body=es_msearch)

                for n, response in enumerate(result_count["responses"]):
                    try:
                        count = response["hits"]["total"]["value"]
                    # for es6
                    except TypeError:
                        count = response["hits"]["total"]
                    hier_results[n][RDV_COUNT] = count
            # TODO: anpassen
            hier_results.sort(key=self.hierarchy_facet_sort)
            # fallback 1 if no_hier count is enabled
            hier_results = list(filter(lambda x: x.get(RDV_COUNT,1) > 0, hier_results))

        return hier_results

    def get_hierarchy_subcat_count(self, query, hits_subcat, hier_pos_field):
        """msearch seems to be faster than n count queries"""
        result_narrower = self.es.search(index=self.hierarchy_index, body=query)
        hier_results = []

        for narrower in result_narrower["hits"]["hits"]:
            # was tun mit alten einträgen
            hier_pos = narrower["_source"][hier_pos_field][0]
            prefLabel = narrower["_source"][HIER_ES_LABEL][0]

            count_body = self.build_hierarchy_search_body(hier_pos, hier_pos_field)
            count_body = {"query": count_body["query"]}
            result_count = self.es.count(index=self.index, body=count_body)

            hits = result_count["count"]
            if hits:
                hier_results.append((prefLabel, hits))
        hier_results.sort(key=self.hierarchy_facet_sort)
        return hier_results

    def build_hierarchy_search_body(self, hier_pos, hier_pos_field):
        #TODO: hier_pos_field im obj index nicht im hier index

        count_body = deepcopy(self.search_body)
        count_body["size"] = 0
        if not hier_pos:
            must_filters = [
                {"prefix": {"{}.keyword".format(hier_pos_field): {"value": "no entry available"}}}
            ]
        else:
            must_filters = [
                {"prefix": {"{}.keyword".format(hier_pos_field): {"value": hier_pos}}}
            ]
        must_filters.extend(self.filter_queries)
        count_query_args = count_body["query"]["bool"]["must"]
        if not isinstance(count_query_args, list):
            count_body["query"]["bool"]["must"] = [count_query_args] if count_query_args else []
        count_body["query"]["bool"]["filter"] = must_filters
        return count_body

    def search_subcategories(self):
        # TODO: subcategories nutzen, subcats - anzeigen und label definieren
        main_category_field_name = ""
        # es get mapping, which firelds are copied to that field
        # rewrite fieldnames to be readable
        # make msearch request /or count/ or exist
        # return fieldnames, label and count


    def build_simplefacetsearch_format(self):

        for key in ["sort", "query", "track_total_hits", "track_scores", "_source"]:
            try:
                del self.body[key]
            except KeyError:
                pass
        self.body["size"] = 0
        if self.debug:
            print("FACET QUERY", json.dumps(self.body))
        result = self.es.search(index=self.index, body=self.body)
        if self.debug:
            print("FACET RESULT", json.dumps(result))
        facet_prefix_search_hits = {}


        for key, facet_data in self.facet_start_chars.items():
            facet_key = key.split(".")[0]
            facet_prefix_search_hits[key] = self._get_facetsearch_buckets(result, facet_key, key)
        return facet_prefix_search_hits

    def build_facetsearch_format(self):

        for key in ["sort", "query", "track_total_hits", "track_scores", "_source"]:
            try:
                del self.body[key]
            except KeyError:
                pass
        self.body["size"] = 0
        if self.debug:
            print("FACET QUERY", json.dumps(self.body))

        result = self.es.search(index=self.index, body=self.body)
        if self.debug:
            print("FACET RESULT", json.dumps(result))
        facet_prefix_search_hits = {}

        for key, facet_data in self.facet_start_chars.items():
            facet_key = key.split(".")[0]
            facet_prefix_search_hits[key] = self._get_filtered_buckets(result, facet_key, key)
        return self._return_response(facet_prefix_search_hits)

    def get_next_objectview(self):
        previous = False
        first_sort_field = ""
        frontend_sort_start = True
        # da default sort vorangestellt wird, muss über die sort_fields aus dem Frontend der Abgleich der
        # Suchrichtung erfolgen und nicht über den ersten Sort Eintrag (da dieser default sein kann)
        if self.sort_fields[0]["order"] != self.search_after_order:
            previous = True
            first_sort_field = self.sort_fields[0]["field"]


        for n, sort_fields in enumerate(self.body["sort"]):
            for field, sort_data in sort_fields.items():
                # die erste Position ist zentral
                # bei anderen Sortierungen, ändere auch Sortierung falls zurückgeblättert werden soll
                if field == first_sort_field:
                    frontend_sort_start = True
                if previous and frontend_sort_start:
                    sort_data["order"] = "asc" if sort_data["order"] == "desc" else "desc"
        result = self.es.search(index=self.index, body=self.body)
        COMPARE = False

        if COMPARE:
            from rdv_es_dsl_ubit import NextObject
            es_query = NextObject(config_store=self.pj_conf, request_data=self.request_data)
            s = es_query.get_next_object()
            self.new_body = s.to_dict()
            print(json.dumps(self.new_body, indent=3))
            result_new = self.es.search(index=self.index, body=self.new_body)
            del result_new["took"]
            del result["took"]
            store_examples_path = "/tmp"
            if result_new != result:
                json.dump(result_new, open(os.path.join(store_examples_path, "new_results"), "w"), indent=2, sort_keys=True)
                json.dump(result, open(os.path.join(store_examples_path, "old_results"), "w"), indent=2, sort_keys=True)

    def get_nextobj_id(self):
        result = self.es.search(index=self.index, body=self.body)
        obj = result.get("hits", {}).get("hits", [])[0]
        obj_id = obj.get("_id")
        obj_sort = obj.get("sort")
        return obj_id, obj_sort

    def build_preview_snippet(self, result):
        rdv_objects = [self._es2dict(hit) for hit in result["hits"]["hits"]]
        preview_objects = []
        for page in rdv_objects:
            rdv_view = self.view_class(page_id = page.get(SNIPPET_ID),
                                       page=page,
                                       lang=self.language,
                                       project_config_store=self.pj_conf)
            preview_objects.append(rdv_view.build_snippet())
        return preview_objects

    def next_snippets(self):
        if self.debug:
            print("Snippet Query", json.dumps(self.body) )
        result = self.es.search(index=self.index, body=self.body)

        # todo: warum hier Fehlermeldung mit self._return_response()
        COMPARE = False
        if COMPARE:
            from rdv_es_dsl_ubit.rdv_es_dsl_builder import NextSnippets
            next_snippets = NextSnippets(config_store = self.pj_conf, request_data=self.request_data)
            #             es_query = RDVESDslQuery(config_store=self.pj_conf, request_data=self.request_data)
            query = next_snippets.get_next_snippets_query()
            self.new_body = query.to_dict()
            result_new = self.es.search(index=self.index, body=self.new_body)
            del result_new["took"]
            del result["took"]
            if result_new != result:
                store_examples_path = "/tmp/"
                json.dump(result_new, open(os.path.join(store_examples_path, "new_results"), "w"), indent=2, sort_keys=True)
                json.dump(result, open(os.path.join(store_examples_path, "old_results"), "w"), indent=2, sort_keys=True)
        hits = self.build_preview_snippet(result)
        return hits

    def get_hierarchy_data(self, hits_subcat=False):
        if hits_subcat:
            hierarchy_fields = {facet_data[RQ_FIELD]:
                                    self.query_hierarchy_index(facet_data,
                                                               hits_subcat=hits_subcat,
                                                               rdv_facet_key=key)
                                for key, facet_data in self.hierarchy_facets.items()}
        else:
            hierarchy_fields = {}

        return hierarchy_fields

    def get_dsl_hierarchy_data(self, hits_subcat=False):
        if hits_subcat:
            from rdv_es_dsl_ubit.rdv_es_hierarchy_facet import HierarchyRDVESFacet
            hierarchy_fields_new = {}
            for key, facet_data in self.hierarchy_facets.items():
                facet_data["rdv_facet_key"] = key
                facet = HierarchyRDVESFacet(config_store=self.pj_conf,
                                            facet=facet_data,
                                            request_data=self.request_data)
                hierarchy_fields_new[facet_data[RQ_FIELD]]= facet.build_hierarchy_aggregation()
        else:
            hierarchy_fields_new = {}

        return hierarchy_fields_new

    def pre_build_rdvformat(self, hits_subcat=False, only_aggs = False, dsl = False):
        """send the search request to elastic and transform es result to rdv output format"""
        if self.debug and 0:
            print("query of pre_build_rdvformat", json.dumps(self.body))

        aggs = True
        if not aggs:
            # performance Test Quelldatum
            quelldatum = self.body["aggs"]["all_facets"]["aggs"]["Quelldatum"]
            del self.body["aggs"]
            self.body["aggs"] = {"all_facets": {"global": {},"aggs":{"Quelldatum":quelldatum}}}
        # that's where the query is sent to elasticsearch
        result = self.es.search(index=self.index, body=self.body)

        # Alte und neue Version werden verglichen für gesamten Search-Body (funktioniert nur für ZAS)
        COMPARE = False
        store_examples_path = "/tmp/"
        if COMPARE:
            from rdv_es_dsl_ubit import RDVESDslQuery
            es_query = RDVESDslQuery(config_store=self.pj_conf, request_data=self.request_data)
            s = es_query.build_complex_facet_search_body()
            self.new_body = s.to_dict()
            result_new = self.es.search(index=self.index, body=self.new_body)
            del result_new["took"]
            del result["took"]
            if result_new != result:
                json.dump(result_new, open(os.path.join(store_examples_path, "new_results"), "w"), indent=2, sort_keys=True)
                json.dump(result, open(os.path.join(store_examples_path, "old_results"), "w"), indent=2, sort_keys=True)

        # for iiif
        self.query_ids = [r["_id"] for r in result["hits"]["hits"]]
        snippets = self.build_preview_snippet(result)
        geo_snippets = self.build_geo_snippets(result)
        # build output


        if aggs:

            facet_fields = {facet_data[RQ_FIELD]: self.get_buckets(result, key, facet_data.get("es_field") or facet_data[RQ_FIELD])
                            for key, facet_data in self.basic_facets.items()}

            facet_subcat_fields = {facet_sub_data[RQ_FIELD]: self.build_subcat_aggs(result, key, facet_sub_data[RQ_FIELD]) for key, facet_sub_data in self.basic_sub_facets.items()}
            daterange_fields = self.build_date_aggs(result)
            int_range_fields = self.build_int_aggs(result)
            geo_fields = self.build_geo_aggs(result)

            # show geohash values in facet for better analysis
            if "fct_coord_str.keyword" in facet_fields:
                dup_list = []
                for k, v2 in geo_fields.items():
                    for v in v2:
                        v_copy = deepcopy(v)
                        #del v_copy["field"]
                        #del v_copy["centroid"]
                        dup_list.append(v_copy)
                    facet_fields["fct_coord_str.keyword"] = dup_list

            if dsl:
                hierarchy_fields = self.get_dsl_hierarchy_data(hits_subcat=hits_subcat)
            else:
                hierarchy_fields = self.get_hierarchy_data(hits_subcat=hits_subcat)

                # Alte und neue Version werden verglichen für Hierarchy-Facette (funktioniert nur für ZAS)
            if COMPARE:
                hierarchy_fields_new = self.get_dsl_hierarchy_data(hits_subcat=hits_subcat)
                hierarchy_fields_old = self.get_hierarchy_data(hits_subcat=hits_subcat)
                if hierarchy_fields_new != hierarchy_fields_old:
                    json.dump(hierarchy_fields_new, open(os.path.join(store_examples_path, "new_hier_results"), "w"), indent=2, sort_keys=True)
                    json.dump(hierarchy_fields_old, open(os.path.join(store_examples_path, "old_hier_results"), "w"), indent=2, sort_keys=True)

            if self.debug:
                print("HIER-FIELD", self.hierarchy_facets.items())
            # todo: anpassen bestandserhaltung
            if self.sub_facets:
                facet_fields[self.subfacet_name] = self.build_subfacet_entries()
            # todo: anpassen subcat
            facet_fields.update(facet_subcat_fields)
            facet_fields.update(daterange_fields)
            facet_fields.update(int_range_fields)
            facet_fields.update(hierarchy_fields)
            facet_fields.update(geo_fields)
            visual_aggs = {range_data[RQ_FIELD]: {"counts": self.get_buckets(result, "histogram_" + key)}
                           for key, range_data in self.visual_aggs.items()}

            facet_queries = {
                "{{!ex={}}}{}:0".format(visual_agg["field"], visual_agg["field"]): self._get_doc_count(result,
                                                                                                       "missing_" + key)
                for key, visual_agg in self.visual_aggs.items()}
            aggs_results = {
                RDV_FACETPAIRS: facet_fields,
                RDV_VISUAL_AGGS: visual_aggs,
                RDV_NO_ENTRY: facet_queries}
            if only_aggs:
                return aggs_results

        else:
            aggs_results = {}

        try:
            total_hits = result["hits"]["total"]["value"]
        #for es6
        except TypeError:
            total_hits = result["hits"]["total"]

        output = {
            RDV_HIT_COUNT: total_hits,
            RDV_SNIPPETS: snippets,
            RDV_GEO_SNIPPETS: geo_snippets
        }

        output.update(aggs_results)
        if self.return_selection:
            output.update(self.selection)
        if self.debug:
            print("test of pre_build_rdvformat (self.index, aggs_Result, result, output)")
        return output

    def build_facet_autocomplete(self):
        if self.debug:
            print("query of build_facet_autocomplete (self.body)")
        result = self.es.search(index=self.index, body=self.body)

        # --------------- TEST -------------------
        COMPARE = False

        if COMPARE:
            store_examples_path = "/tmp"
            from rdv_es_dsl_ubit import RDVAutocomplete
            es_query = RDVAutocomplete(config_store=self.pj_conf, request_data=self.request_data)
            s = es_query.build_complex_facet_search_body()
            self.new_body = s.to_dict()
            result_new = self.es.search(index=self.index, body=self.new_body)
            del result_new["took"]
            del result["took"]
            if result_new != result:
                json.dump(result_new, open(os.path.join(store_examples_path, "new_results"), "w"), indent=2, sort_keys=True)
                json.dump(result, open(os.path.join(store_examples_path, "old_results"), "w"), indent=2, sort_keys=True)
        # --------------- TEST -------------------


        # sortierung der Vorschläge überprüfen
        autocomplete_data = [{"label": b.get("label"), "value": {"id": b.get("label",{})}, "service_label": b.get("label"), "group": key  }
                        for key, facet_data in self.basic_facets.items() for b in self.get_buckets(result, key, facet_data[RQ_FIELD])]
        return autocomplete_data

    def build_rdvformat(self, hits_subcat=False, only_aggs=False, dsl=False):
        output = self.pre_build_rdvformat(hits_subcat=hits_subcat,only_aggs=only_aggs, dsl=dsl)
        return self._return_response(output)

    def factet_query_def(self, facet_data, facet_search, label_field):


        def strip_accents(s):
            return ''.join((c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn'))


        facet_search = "".join([strip_accents(s) for s in facet_search])
        if self.autocomplete:
            return f"(({facet_search}.*)|(.*[ ,]{facet_search}.*))"
        elif facet_data and facet_data.get(RQ_ORDER) == "label":
            return facet_search + ".*"
        else:
            return ".*" + facet_search + ".*"

    def facet_query_order(self, label_field):
        return {"_count": "desc"}

    def facet_query_order2(self, facet_data):
        if facet_data and facet_data.get(RQ_ORDER) == "label":
            return {"_key": "asc"}
        else:
            return {"_count": "desc"}

    def build_facet_search(self, agg_prefix = "filtered_", facet_search_endpoint= False, aggs_size = 0):
        """get facets for values that start with specific characters: search within facet"""

        # delete not necessary agg definitions to speed up query
        if facet_search_endpoint:
            facet_search = {facet_search_key.split(".")[0] for facet_search_key in self.facet_start_chars.keys()}
            aggs = self.body.get("aggs", {}).get("all_facets", {}).get("aggs", {})
            for key in list(aggs.keys()):
                if key not in facet_search:
                    del aggs[key]

        for key, facet_search in self.facet_start_chars.items():

            #because no search_analyzer for term aggregations in ES (as far as I know)
            facet_search = facet_search.lower()
            facet_lookup_dict = {v.get(RDV_FIELD): v for k, v in self.basic_facets.items()}
            facet_data = facet_lookup_dict.get(key)
            if ".id" in key:
                label_field = key.replace(".id", ".label")
            else:
                label_field = key

            size = facet_data.get("size") if facet_data.get("size") and aggs_size < self.max_facet_values else self.max_facet_values
            include_query =  self.factet_query_def(facet_data, facet_search, label_field)

            sort_order = self.facet_query_order2(facet_data)
            search_query_facets = {
                "aggs": {
                    agg_prefix + key: {
                        "terms": {
                            "field": label_field,
                            "include": include_query,
                            "size": size,
                            "order": sort_order
                        }
                    }
                }
            }

            if ".id" in key:
                main_field = key.split(".id")[0]
                top_hits = {"aggs": {"example": {"top_hits": {"size": 1, "_source": {"includes": [main_field]}}}}}
                search_query_facets["aggs"][agg_prefix + key].update(top_hits)
            elif ".label" in key:
                main_field = key.split(".label")[0]
                top_hits = {"aggs": {"example": {"top_hits": {"size": 1, "_source": {"includes": [main_field]}}}}}
                search_query_facets["aggs"][agg_prefix + key].update(top_hits)
            elif ".keyword" in key:
                main_field = key.split(".keyword")[0]
                top_hits = {"aggs": {"example": {"top_hits": {"size": 1, "_source": {"includes": [main_field]}}}}}
                search_query_facets["aggs"][agg_prefix + key].update(top_hits)

            facet_key = label_field.split(".")[0]


            try:
                if not "global" in self.body["aggs"]["all_facets"]:
                    self.body["aggs"]["all_facets"]["global"] = {}
                if not self.body["aggs"]["all_facets"]["aggs"][facet_key]:
                    self.body["aggs"]["all_facets"]["aggs"][facet_key] = {"filter": {
                            "bool": {
                              "must": [{
                                "bool": {
                                  "must": [{
                                    "match_all": {}
                                  }]
                                }
                              }]
                            }
                          }}
                self.body["aggs"]["all_facets"]["aggs"][facet_key].update(search_query_facets)
            except KeyError:
                pass

    def build_simplefacet_search(self, agg_prefix = "filtered_", facet_search_endpoint= False, aggs_size = 0):
        """get facets for values that start with specific characters: search within facet"""

        for key, facet_search in self.facet_start_chars.items():
            facet_lookup_dict = {v.get(RDV_FIELD): v for k, v in self.basic_facets.items()}
            facet_data = facet_lookup_dict.get(key)
            #because no search_analyzer for term aggregations in ES (as far as I know)
            facet_search = facet_search.lower()

            if ".id" in key:
                label_field = key.replace(".id", ".label")
                excludes = [re.escape(f.get("label").lower()) if isinstance(f, dict) else re.escape(f.lower()) for f in self.facet_excl_entries.get(key, [])]
            else:
                label_field = key
                excludes = [re.escape(f.get("label").lower()) if isinstance(f, dict) else re.escape(f.lower()) for f in self.facet_excl_entries.get(key, [])]
            size = aggs_size if aggs_size and aggs_size < self.max_facet_values else self.max_facet_values


            sort_order = self.facet_query_order2(facet_data)

            search_query_facets = {
                "aggs": {
                    agg_prefix + key: {
                        "terms": {
                            "field": label_field,
                            "include": ".*" + facet_search + ".*",
                            # kein array: "Cannot mix a regex-based include with a set-based method"
                            "exclude": "|".join(excludes),
                            "size": size,
                            "order": sort_order
                        }
                    }
                }
            }

            if ".id" in key:
                main_field = key.split(".id")[0]
                top_hits = {"aggs": {"example": {"top_hits": {"size": 1, "_source": {"includes": [main_field]}}}}}
                search_query_facets["aggs"][agg_prefix + key].update(top_hits)
            elif ".label" in key:
                main_field = key.split(".label")[0]
                top_hits = {"aggs": {"example": {"top_hits": {"size": 1, "_source": {"includes": [main_field]}}}}}
                search_query_facets["aggs"][agg_prefix + key].update(top_hits)
            elif ".keyword" in key:
                main_field = key.split(".keyword")[0]
                top_hits = {"aggs": {"example": {"top_hits": {"size": 1, "_source": {"includes": [main_field]}}}}}
                search_query_facets["aggs"][agg_prefix + key].update(top_hits)

            self.body.update(search_query_facets)


    def build_search_after(self, size):
        """get previous / next record for search"""
        search_after = {"search_after": self.search_after_values}
        self.body.update(search_after)
        # to exclude objects without detail page
        if self.search_after_query:
            self.body["query"] = self.search_after_query
        # only id is needed for search_after query no aggregations or source
        self.body["aggs"] = {}
        # for search_after in ES from needs to be 0
        self.body["from"] = 0
        self.body["size"] = size
        if self.debug:
            print("SEARCH_AFter", json.dumps(self.body))

    # helpers to build search body
    def _es2dict(self, hit):
        snippet_highlight = self.get_highlight(hit)
        # TODO: muss eigentlich sort param haben
        doc = {SNIPPET_ID: hit["_id"], SNIPPET_SEARCH_AFTER: hit.get("sort"),
               SNIPPET_FULLTEXT: snippet_highlight}
        doc.update(hit["_source"])
        return doc

    def get_excluded_highlight_fields(self):
        return set()

    def get_highlight(self, hit):
        view_object = self.view_class(page_id=hit.get(SNIPPET_ID),
                                      page=hit.get("_source", hit),
                                      lang=self.language,
                                      project_config_store=self.pj_conf)
        obj_type = view_object.get_inst_kuerzel()
        fields_def = view_object.get_fields_def(obj_type)
        if self.highlight_included_fieldname:
            hightlights = []
            fields_excluded = self.get_excluded_highlight_fields()
            fields_included = set()
            for k, v in hit.get("highlight", {}).items():

                field_esname = k.split(".")[0]
                if field_esname in fields_excluded:
                    continue
                field_data =fields_def.get(field_esname)
                field_label = view_object.get_lang_label(field_data, view_object.lang)
                if field_label and field_label not in fields_included:
                    fields_included.add(field_label)
                    highlight_entry = "<i>{}</i>: {}".format(field_label, ";".join(v) if isinstance(v, list) else v)
                    hightlights.append(highlight_entry)
            return hightlights
        else:
            return hit.get("highlight", {}).get(self.highlight_field)

    def build_subfacet_count_body(self, subfacet):
        count_body = deepcopy(self.search_body)
        filter = deepcopy(self.filter_queries)
        count_body["size"] = 0
        must_filters = [
            {"exists": {"field": subfacet}}
        ]
        filter = json.dumps(filter)
        filter = filter.replace("rel_persons2.Drucker.id", subfacet)
        filter = json.loads(filter)
        must_filters.extend(filter)
        count_query_args = count_body["query"]["bool"]["must"]
        if not isinstance(count_query_args, list):
            count_body["query"]["bool"]["must"] = [count_query_args] if count_query_args else []
        count_body["query"]["bool"]["filter"] = must_filters
        return count_body

    def build_subfacet_entries(self):
        subfield_results = []
        for subfacet in self.sub_facets:
            es_msearch = ""

            subfields = self.es.indices.get_field_mapping(fields=["{}.*".format(subfacet)], index=self.index)
            for index_mapping in list(subfields.values()):
                for subfield in index_mapping.get("mappings", {}).items():
                    # todo: für bestandserhaltung long
                    if list(subfield[1].get("mapping", {}).values())[0].get("type") == "long":
                        es_subfield = subfield[0]
                        prefLabel = es_subfield.split(".")[-1]
                        entry = {RDV_VALUE: {"id": prefLabel}, RDV_LABEL: prefLabel}
                        subfield_results.append(entry)
                        count_body = self.build_subfacet_count_body(es_subfield)
                        es_msearch += json.dumps({"index": self.index}) + "\n" + json.dumps(count_body) + "\n"
                    # for itb
                    elif subfield[0].endswith("id"):
                        es_subfield = subfield[0]
                        prefLabel = es_subfield.split(".")[-2]
                        entry = {RDV_VALUE: {"id": prefLabel}, RDV_LABEL: prefLabel}
                        subfield_results.append(entry)
                        count_body = self.build_subfacet_count_body(es_subfield)
                        es_msearch += json.dumps({"index": self.index}) + "\n" + json.dumps(count_body) + "\n"

            if es_msearch:
                result_count = self.es.msearch(body=es_msearch)
                for n, response in enumerate(result_count["responses"]):
                    try:
                        count = response["hits"]["total"]["value"]
                    # for es6
                    except TypeError:
                        count = response["hits"]["total"]
                    subfield_results[n][RDV_COUNT] = count
        subfield_results = sorted(filter(lambda x: x.get(RDV_COUNT), subfield_results), key=lambda x: x.get(RDV_LABEL))
        return subfield_results

    @staticmethod
    def get_nested_values(field_parts, values):
        f = field_parts.pop(0)
        values = values[f]
        if isinstance(values, list):
            return values
        else:
            return RDV2ES.get_nested_values(field_parts, values)

    @staticmethod
    def _bucket_map(bucket, field_name="", type_="id"):
        key = bucket.get("key_as_string") or bucket["key"]
        start_key = key
        label = key
        coord = None
        if "example" in bucket:
            # to show label and not id

            if ".id" in field_name:
                field_name = field_name.split(".id")[0]
                field_name_parts = field_name.split(".")
                # es-proxy, aus url, via catalog, facet search
                #id + id.lower because no terms search_normalizer
                # [field_name]
                values = bucket["example"]["hits"]["hits"][0]["_source"]


                try:
                    values = RDV2ES.get_nested_values(field_name_parts, values)
                except IndexError:
                    values = [{'label': 'missing', 'id': 'missing'}]
                values = [v for v in values if v]
                lookup = {v["id"]: v["label"] for v in values}
                lookup.update({str(v["id"]).lower(): v["label"] for v in values})

                lookup_coord = {v["id"]: v.get("location") for v in values}
                lookup_coord.update({str(v["id"]).lower(): v.get("location") for v in values})

                lookup_label = {str(v["label"]).lower(): v["id"] for v in values}
                lookup_label.update({str(v["label"]): v["id"] for v in values})
                lookup_label.update({unidecode(str(v["label"]).lower()): v["id"] for v in values})

                lookup2 = {str(v["label"]).lower(): v["label"] for v in values}
                lookup2.update({unidecode(str(v["label"]).lower()): v["label"] for v in values})
                # für facet_search, reihenfolge wichtig, zuerst key dann label
                key = lookup_label.get(key, key)
                label = lookup.get(key, lookup2.get(key, key))
                coord = lookup_coord.get(key, None)
            elif ".label" in field_name:
                field_name = field_name.split(".label")[0]
                field_name_parts = field_name.split(".")
                values = bucket["example"]["hits"]["hits"][0]["_source"]
                values = RDV2ES.get_nested_values(field_name_parts, values)
                lookup2 = {str(v["label"]).lower(): v["label"] for v in values}
                lookup2.update({unidecode(str(v["label"]).lower()): v["label"] for v in values})
                label = lookup2.get(key, key)
            # to correct lower-case analyser
            elif ".keyword" in field_name:
                field_name = field_name.split(".keyword")[0]
                try:
                    values = bucket["example"]["hits"]["hits"][0]["_source"][field_name]
                # for nested fields e.g. tags.tag
                except KeyError:
                    values = [start_key]
                if "." in field_name:
                    key = start_key
                    label = start_key
                else:
                    lookup_values = values
                    if isinstance(lookup_values, list):
                        lookup_label = {str(v).lower(): v for v in lookup_values}
                        lookup_label.update({unidecode(str(v).lower()): v for v in lookup_values})
                    elif isinstance(lookup_values, str):
                        lookup_label = {str(lookup_values).lower(): lookup_values}
                        lookup_label.update({unidecode(str(lookup_values).lower()): lookup_values})
                    else:
                        lookup_label = {lookup_values: lookup_values}
                    # für facet_search, reihenfolge wichtig, zuerst key dann label
                    label = lookup_label.get(key, key)
                    key = label
                    # e.g pages.issue_type

        facet_value = {RDV_LABEL: label, RDV_COUNT: bucket["doc_count"]}
        facet_value.update({RDV_VALUE: {"id": key}})
        if coord:
            facet_value.update({RDV_GEOPOINT: coord})
        return facet_value


    @staticmethod
    def _bucket_subcat_map(bucket, subvalues, es_buckets, pos):
        sub_buckets = []
        if subvalues:
            for n, k in enumerate(subvalues, start=1):
                if k in es_buckets:
                    es_buckets[k].update({RQ_POS: "{}.{}.".format(pos,n)})
                    sub_buckets.append(es_buckets[k])
            bucket.update({RDV_SUBVALUES: sub_buckets, RQ_POS: "{}.".format(pos,n)})
        return bucket

    @staticmethod
    def _set_key_filter(body_part, key, value):
        body_part[key]["filter"]["bool"].setdefault("must", []).append(value)

    @staticmethod
    def _build_aggs_filter_func(body_part):
        def aggs_filter(key, value, body_part=body_part):
            if value:
                body_part[key]["filter"]["bool"].setdefault("must", []).append(value)
        return aggs_filter

    @staticmethod
    def _get_bool_operator(operator):
        if operator == "OR":
            return "should"
        elif operator == "AND":
            return "must"
        elif operator == "NOT":
            return "must_not"

    def _set_misshist_filter(self, key, query):
        if query:
            self.set_aggs_filter("histogram_" + key, query)
            self.set_aggs_filter("missing_" + key, query)

    def _get_rangetype_open_filters(self, range, type, open_facets):
        filtered_facets = {x: range[x] for x in filter(lambda x: range[x][RQ_FACET_TYPE] == type, range.keys())}

        for k, v in filtered_facets.items():
            filtered_facets[k][RQ_FACET_VISIBLE] = False if self.load_aggs_async else True
            if v[RQ_FIELD] in open_facets:
                filtered_facets[k][RQ_FACET_VISIBLE] = True
        return filtered_facets



    @staticmethod
    def _remove_filters(range, del_filters: []):
        return {x: range[x] for x in filter(lambda x: x not in del_filters, range.keys())}

    @staticmethod
    def buildiso8601(value, fromto):
        from_split_char = "/"
        date_split_char = "-"

        values = value.split(from_split_char)
        if fromto == "from":
            from_value = values[0]
            if len(from_value) == 4:
                iso8601 = "{}-01-01".format(from_value)
            elif len(from_value) == 7:
                year,month  = from_value.split(date_split_char)
                iso8601 = "{:04d}-{:02d}-01".format(int(year),int(month))
            else:
                iso8601 = from_value
        else:
            to_value = values[1] if len(values) > 1 else values[0]
            if len(to_value) == 4:
                iso8601 = "{}-12-31".format(to_value)
            elif len(to_value) == 7:
                year,month = to_value.split(date_split_char)
                weekday, days = calendar.monthrange(int(year), int(month))
                iso8601 = "{:04d}-{:02d}-{}".format(int(year),int(month),days)
            else:
                iso8601 = to_value
        return iso8601

    def _calculate_agg_interval(self, values, operator, field_name=None):
        if operator == "OR":
            min_int = min(m.get(RQ_GTE, 0) for m in values)
            max_int = max(m.get(RQ_LTE, 50000) for m in values)
        else:
            min_int = max(m.get(RQ_GTE, 0) for m in values)
            max_int = min(m.get(RQ_LTE, 50000) for m in values)

        zeros = len(str(int(max_int) - int(min_int))) -1
        interval = int("1" + ("0" * zeros))
        return interval, interval

    def get_int_agg_interval(self, values, operator, field_name=None):
        if values:
            return self._calculate_agg_interval(values=values, operator=operator, field_name=field_name)
        else:
            return self._calculate_agg_interval(values=[self.default_int_range], operator=operator, field_name=field_name)


    def get_date_agg_interval(self, values, operator, max_days_aggr= 36551, min_date_aggr = 1, field=None):
        return self._get_date_agg_interval(values, operator, max_days_aggr, min_date_aggr)

    def _get_date_agg_interval(self, values, operator, max_days_aggr, min_date_aggr = 1):
        # for AND min/max needs to be reverted
        date_format = "%Y-%m-%d"
        now_iso8601 = datetime.strftime(datetime.now(), date_format)

        if operator == "OR":
            min_date = min(m.get(RQ_GTE, "0001-01-01") for m in values)
            max_date = max(m.get(RQ_LTE, now_iso8601) for m in values)
        else:
            min_date = max(m.get(RQ_GTE, "0001-01-01") for m in values)
            max_date = min(m.get(RQ_LTE, now_iso8601) for m in values)

        date_format = "%Y-%m-%d"
        delta = datetime.strptime(max_date, date_format) - datetime.strptime(min_date, date_format)
        delta_days = delta.days
        agg_interval = ""

        if delta_days > 36550 <= max_days_aggr:
            agg_interval = "century"
        elif delta_days > 3655 <= max_days_aggr:
            agg_interval = "decade"
        elif delta_days > 366:
            agg_interval = "year"
        elif delta_days > 31 >= min_date_aggr:
            agg_interval = "month"
        elif delta_days < 31 >= min_date_aggr:
            agg_interval = "day"
        else:
            agg_interval = "year"
        es_interval = "year" if delta_days > 365 else agg_interval

        return es_interval, agg_interval

    @staticmethod
    # at the moment solved with json.dumps and json.loads
    def sanitize_js_response(value):
        """javascript returns a to big value when x > js_max
        that causes problems within ES when x > es_max
        therefore when abs(x) > js_max x is mapped to es_max"""
        # Nach https://www.avioconsulting.com/blog/overcoming-javascript-numeric-precision-issues i
        #  st die größte ohne Verluste darstellbare Zahl 9,007,199,254,740,991
        js_max = 9007199254740991
        # Elasticsearch sort value for date entries without values https://github.com/elastic/elasticsearch/issues/28806
        es_max = 9223372036854775808
        if isinstance(value, int):
            if abs(value) > js_max:
                return es_max * -1 if value < 0 else es_max
            else:
                return value
        else:
            return value

    # helpers to build results for angular app

    def build_geo_snippets(self, result):
        geo_preview_objects = []
        for rq_key, facet_data in self.geo_filters.items():
            es_field_name = facet_data.get("es_field") or facet_data[RQ_FIELD]
            if rq_key == "fct_coord":
                for bucket in result.get("aggregations",{}).get("all_facets",{})\
                        .get(rq_key,{}).get("filtered_" + es_field_name,{}).get("buckets",[]):
                    if bucket.get("doc_count") == 1:
                        key = bucket.get("key")
                        hits = bucket.get("example", {}).get("hits", {}).get("hits", [])
                        if hits:
                            page_source = hits[0].get("_source")
                            page_id = bucket.get("example").get("hits").get("hits")[0].get("_id")
                            rdv_view = self.view_class(page_id = page_id,
                                                       page=page_source,
                                                       lang=self.language,
                                                       project_config_store=self.pj_conf)
                            snippet = rdv_view.build_snippet()
                            snippet.update({RDV_CENTROID: bucket.get("centroid"), RDV_GEOHASH: key})
                            geo_preview_objects.append(snippet)
                return geo_preview_objects


    def build_geo_aggs(self, result):
        """build geo aggregations the Elasticsearch cant build out of the box"""
        geo_aggs = {}
        for rq_key, facet_data in self.geo_filters.items():
            bucket_counter = Counter()
            operator = facet_data[RQ_OPERATOR]
            # für die Rückgabe des Werts
            field_name = facet_data[RQ_FIELD]
            es_field_name = facet_data.get("es_field") or facet_data[RQ_FIELD]
            for bucket in result.get("aggregations",{}).get("all_facets",{}).get(rq_key,{}).get("filtered_" + es_field_name,{}).get("buckets",[]):
                hits = bucket.get("example", {}).get("hits", {}).get("hits", [])
                if hits:
                    page_source = hits[0].get("_source")

                key = bucket.get("key")
                bucket_counter[key]= {"doc_count": bucket.get("doc_count"), RDV_CENTROID: bucket.get("centroid")}
            geo_aggs[field_name] = sorted([{RDV_LABEL: k, RDV_FIELD: field_name, RDV_COUNT: v["doc_count"],
                                            RDV_VALUE: {"id": k}, "centroid": v.get(RDV_CENTROID)} for k, v in
                                           bucket_counter.items() if v["doc_count"] > 1],
                                          key=lambda x: int(x[RDV_COUNT]), reverse=True)
        return geo_aggs


    def build_int_aggs(self, result):
        """build date aggregations the Elasticsearch cant build out of the box"""
        int_aggs = {}
        for rq_key, facet_data in self.int_ranges.items():

            operator = facet_data[RQ_OPERATOR]
            # für die Rückgabe des Werts
            field_name = facet_data[RQ_FIELD]
            #todo: bestandsaufnahe anpassung
            es_field_name = facet_data.get("es_field") or facet_data[RQ_FIELD]
            _, agg_interval = self.get_int_agg_interval(facet_data[RQ_VALUES],
                                                         operator, field_name)
            bucket_counter = Counter()

            for bucket in result.get("aggregations",{}).get("all_facets",{}).get(rq_key,{}).get("filtered_" + es_field_name,{}).get("buckets",[]):
                start = int(bucket.get("key"))
                end = start + agg_interval - 1

                # build
                try:
                    # 100 -> 3
                    len_default_int_range = len(str(self.default_int_range.get(RQ_LTE)))
                except Exception:
                    len_default_int_range = 0

                start_str = ("{:0"+ str(len_default_int_range) +"d}.").format(start)
                # anzahl an .-Notierungen
                pos_len = len_default_int_range + 1 - len(str(agg_interval))
                # if e.g. 1400 and standard range is only 100, first value needs to b 14aaaaaa.
                r = len(start_str) - len_default_int_range if (len(start_str) - len_default_int_range) >= 0 else 0
                pos = ""

                for p in range(0, pos_len):
                    num = start_str[0:p +r] if len(start_str) >= p else "0"
                    # maximal 8 stellige Nummern
                    pos += "{:a<8s}.".format(num)

                if start == end:
                    key=str(start)
                else:
                    key = "{} - {}".format(start, end)

                bucket_counter[key]= {"doc_count": bucket.get("doc_count"), "pos": pos, "gte": str(start), "lte": str(end)}

            int_aggs[field_name] = sorted([{RDV_LABEL: k, RDV_FIELD: field_name, RDV_COUNT: v["doc_count"],
                                             RDV_VALUE: {"gte": v["gte"],"lte": v["lte"],
                                                         "pos": v["pos"]}} for k, v in
                                            bucket_counter.items() if v["doc_count"]],
                                           key=lambda x: int(x[RDV_VALUE]["gte"]), reverse=False)
        return int_aggs


    def build_subcat_aggs(self, result, rq_key, key):
        """nur eine Ebene, Vokabular wird in getrenntem Index vorgehalten"""

        #rewritten_results = self.rewrite_bucket_values(result["aggregations"]["all_facets"][rq_key]["filtered_" + key]["buckets"], key)
        rewritten_results = result.get("aggregations",{}).get("all_facets",{}).get(rq_key,{}).get("filtered_" + key,{}).get("buckets",[])
        # es_buckets = {b["key"]: {RDV_LABEL: b["key"], RDV_COUNT: b["doc_count"]} for b in rewritten_results}
        es_buckets = {self._bucket_map(bucket, key, type_="id")["label"]: self._bucket_map(bucket, key) for bucket in rewritten_results}

        # todo: Index-Query um top level values und subvalues zu erhalten
        # todo: Dossier noch richtig indexieren
        top_level_lookup = {"object_type" : ["Zeitungsausschnitt", "Dokumentensammlung"]}
        sub_values_lookup = {"Zeitungsausschnitt": ["retrodigitalisierter Zeitungsausschnitt", "elektronischer Zeitungsausschnitt (ab 2013)", "swissdox"],
                             "Dokumentensammlung": ["retrodigitalisierte Dokumentensammlung", "elektronische Dokumentensammlung (ab 2013)"]}
        top_level_values = top_level_lookup.get("object_type")

        es_sub_buckets = self.query_subcats(es_buckets, sub_values_lookup, rq_key, key)
        es_buckets.update(es_sub_buckets)

        # todo: anpassen für subcategories
        buckets = []
        for n, k in enumerate(top_level_values, start=1):
            try:
                if k in es_buckets and es_buckets[k][RDV_COUNT]:
                    top_bucket = self._bucket_subcat_map(es_buckets[k], sub_values_lookup.get(k), es_buckets, pos=1)
                    buckets.append(top_bucket)
            except KeyError:
                pass

        return buckets

    def query_subcats(self, es_buckets, sub_values_lookup, rq_key, key):
        # not tested
        max_buckets = self.max_facet_values
        # get subcats that have not been part of first aggregation query
        missing_aggr_keys = []
        if len(es_buckets) > max_buckets:
            for k, vs in sub_values_lookup.items():
                missing_aggr_keys.extend([v for v in vs if v not in es_buckets])

        agg = deepcopy(self.body["aggs"]["all_facets"]["aggs"][rq_key])
        es_sub_buckets = {}
        if agg:
            agg["aggs"]["filtered_"+ key]["terms"]["include"] = missing_aggr_keys
            agg_query = {"size": 0, "aggs": {"all_facets": {"global": {}, "aggs": {key: agg}}}}
            sub_results = self.es.search(index=self.index, body=agg_query)
            # rewritten_sub_results = self.rewrite_bucket_values(sub_results["aggregations"]["all_facets"][rq_key]["filtered_" + key]["buckets"], key)
            # todo: anpassen
            rewritten_sub_results =  sub_results["aggregations"]["all_facets"][key]["filtered_" + key]["buckets"]
            #es_sub_buckets = {b["key"]: {RDV_LABEL: b["key"], RDV_COUNT: b["doc_count"]} for b in rewritten_sub_results}
            es_sub_buckets = {b["key"]: self._bucket_map(b, key, type="id") for b in rewritten_sub_results}
        return es_sub_buckets


    def build_date_aggs(self, result):
        return self._build_date_aggs(result, strict_default=False, displaymode="Jahrzehnte")

    def _build_date_aggs(self, result, strict_default=False, displaymode="Jahrzehnte"):
        """build date aggregations the Elasticsearch cant build out of the box"""
        date_aggs = {}
        for rq_key, facet_data in self.date_filters.items():
            operator = facet_data[RQ_OPERATOR]
            field_name = facet_data[RQ_FIELD]
            _, agg_interval = self.get_date_agg_interval(facet_data[RQ_VALUES] or [self.default_date_range], operator, field=rq_key)
            date_format = "%Y-%m-%d"
            now = datetime.now()
            if strict_default and self.default_date_range:
                default_gte_date = datetime.strptime(self.default_date_range.get(RQ_GTE) or "0001-01-01", date_format)
                default_lte_date = datetime.strptime(self.default_date_range.get(RQ_LTE) or now.strftime(date_format), date_format)
            else:
                default_gte_date = datetime.strptime("0001-01-01", date_format)
                default_lte_date = now
            bucket_counter = {}
            for bucket in result.get("aggregations",{}).get("all_facets",{}).get(rq_key,{}).get("filtered_" + field_name,{}).get("buckets",[]):
                # to exclude none
                if bucket.get("key_as_string"):
                    key = bucket.get("key_as_string").split("T")[0]
                    key = key.split(" ")[0]
                    date_key = datetime.strptime(key, date_format)
                    if strict_default and (default_gte_date > date_key or default_lte_date < date_key):
                        continue
                    year = "{:04d}".format(date_key.year)
                    month = "{:02d}".format(date_key.month)
                    max_year = str(default_lte_date.year) or "{:04d}".format(now.year)
                    pos = ""
                    if agg_interval == "century":
                        cent_prefix = year[0:2]
                        cent_suffix = year[2:4]
                        # auskommentiert, da Jahrhundert-Modus nicht gut funktioniert
                        if displaymode == "Jahrhundert":
                            if cent_suffix == "00":
                                to_year = "{}00".format(cent_prefix)
                                # needs to be done after to_year calculations
                                cent_prefix = str(int(cent_prefix) - 1)
                            else:
                                to_cent_prefix = str(int(cent_prefix) + 1)
                                to_year = max_year if max_year.startswith(cent_prefix) else "{}00".format(to_cent_prefix)
                            sum_key = "{}01/{}".format(cent_prefix, to_year)
                        else:
                            to_year = max_year if max_year.startswith(cent_prefix) else "{}99".format(cent_prefix)
                            sum_key = "{}00/{}".format(cent_prefix, to_year)
                        pos = cent_prefix
                    elif agg_interval == "decade":
                        dec_prefix = year[0:3]
                        dec_suffix = year[3:4]
                        if displaymode == "Jahrhundert":
                            if dec_suffix == "0":
                                to_year = "{}0".format(dec_prefix)
                                # needs to be done after to_year calculations
                                dec_prefix = str(int(dec_prefix) - 1)
                            else:
                                to_dec_prefix = str(int(dec_prefix) + 1)
                                to_year = max_year if max_year.startswith(dec_prefix) else "{}0".format(to_dec_prefix)
                            sum_key = "{}1/{}".format(dec_prefix, to_year)
                        else:
                            to_year = max_year if max_year.startswith(dec_prefix) else "{}9".format(dec_prefix)
                            sum_key = "{}0/{}".format(dec_prefix, to_year)
                        pos = dec_prefix
                    elif agg_interval == "year":
                        sum_key = year
                    elif agg_interval == "month":
                        sum_key = "-".join([year, month])
                    else:
                        display_date_format = "%d.%m.%Y"
                        display_date = datetime.strftime(date_key, display_date_format)
                        sum_key = key
                    if not pos:
                        pos = sum_key
                    if bucket["doc_count"]:
                        bucket_counter.setdefault(sum_key, {"count": 0, "pos": pos})
                        bucket_counter[sum_key]["count"] += bucket["doc_count"]

            date_aggs[field_name] = sorted([{RDV_LABEL: k, RDV_FIELD: field_name, RDV_COUNT: v["count"],
                                             RDV_VALUE: {"type": agg_interval,"pos": v["pos"],"gte": self.buildiso8601(k, "from"),
                                                         "lte": self.buildiso8601(k,"to")}} for k, v in bucket_counter.items()],
                                           key=lambda x: x[RDV_LABEL], reverse=False)
        return date_aggs


    def build_date_aggs_old(self, result):
        """ersetzt durch build_date_aggs"""
        date_aggs = {}
        for rq_key, facet_data in self.date_filters.items():
            operator = facet_data[RQ_OPERATOR]
            field_name = facet_data[RQ_FIELD]
            _, agg_interval = self.get_date_agg_interval(facet_data[RQ_VALUES] or [self.default_date_range], operator, field=rq_key)
            bucket_counter = {}
            for bucket in result.get("aggregations",{}).get("all_facets",{}).get(rq_key,{}).get("filtered_" + field_name,{}).get("buckets",[]):
                # to exclude none
                if bucket.get("key_as_string"):
                    key = bucket.get("key_as_string").split("T")[0]
                    key = key.split(" ")[0]
                    date_format = "%Y-%m-%d"
                    date_key = datetime.strptime(key, date_format)
                    year = "{:04d}".format(date_key.year)
                    month = "{:02d}".format(date_key.month)
                    max_year = "{:04d}".format(datetime.now().year)
                    pos = ""
                    if agg_interval == "century":
                        cent_prefix = year[0:2]
                        cent_suffix = year[2:4]
                        # auskommentiert, da Jahrhundert-Modus nicht gut funktioniert
                        if "displaymode" == "Jahrhundert":
                            if cent_suffix == "00":
                                to_year = "{}00".format(cent_prefix)
                                # needs to be done after to_year calculations
                                cent_prefix = str(int(cent_prefix) - 1)
                            else:
                                to_cent_prefix = str(int(cent_prefix) + 1)
                                to_year = max_year if max_year.startswith(cent_prefix) else "{}00".format(to_cent_prefix)
                        else:
                            to_year = max_year if max_year.startswith(cent_prefix) else "{}99".format(cent_prefix)
                        sum_key = "{}00/{}".format(cent_prefix, to_year)
                        pos = cent_prefix
                    elif agg_interval == "decade":
                        dec_prefix = year[0:3]
                        to_year = max_year if max_year.startswith(dec_prefix) else "{}9".format(dec_prefix)
                        sum_key = "{}0/{}".format(dec_prefix, to_year)
                        pos = dec_prefix
                    elif agg_interval == "year":
                        sum_key = year
                    elif agg_interval == "month":
                        sum_key = "-".join([year, month])
                    else:
                        display_date_format = "%d.%m.%Y"
                        display_date = datetime.strftime(date_key, display_date_format)
                        sum_key = key
                    if not pos:
                        pos = sum_key
                    if bucket["doc_count"]:
                        bucket_counter.setdefault(sum_key, {"count": 0, "pos": pos})
                        bucket_counter[sum_key]["count"] += bucket["doc_count"]

            date_aggs[field_name] = sorted([{RDV_LABEL: k, RDV_FIELD: field_name, RDV_COUNT: v["count"],
                                             RDV_VALUE: {"type": agg_interval,"pos": v["pos"],"gte": self.buildiso8601(k, "from"),
                                                   "lte": self.buildiso8601(k,"to")}} for k, v in bucket_counter.items()],
                                           key=lambda x: x[RDV_LABEL], reverse=False)
        return date_aggs

    def rewrite_bucket_values(self, result, key):
        new_bucket = Counter()
        for item in result:
            value = self.rewrite_value(key, item["key"])
            count = item["doc_count"]
            if value:
                if isinstance(value, list):
                    for v in value:
                        new_bucket[v] += count
                else:
                    new_bucket[value] += count
        return sorted([{"key": l, "doc_count": c} for l, c  in new_bucket.most_common()], key = lambda x: (x["doc_count"] * -1, x["key"]))

    @classmethod
    def _get_facetsearch_buckets(cls, result, rq_key, key):
        buckets = result["aggregations"]["filtered_" + key]["buckets"]
        try:
            buckets = [cls._bucket_map(b, field_name=key, type_="id") for b in buckets if b["doc_count"]]
        except KeyError:
            buckets = []

        return buckets


    def _get_filtered_buckets(self, result, rq_key, key):
        # todo: rausnehmen: keine gute Idee
        #rewritten_results = self.rewrite_bucket_values(result["aggregations"]["all_facets"][key]["filtered_" + key]["buckets"], key)
        #print(key, result["aggregations"]["all_facets"][rq_key])
        buckets = result.get("aggregations",{}).get("all_facets",{}).get(rq_key,{}).get("filtered_" + key,{}).get("buckets",[])

        try:
            buckets = [self._bucket_map(b, field_name=key, type_="id") for b in buckets if b["doc_count"]]
        except KeyError:
            import traceback
            traceback.print_exc()
            buckets = []

        # what shall be done if facet is empty due to async loading
        if not buckets and self.load_aggs_async:
            facet_value = {RDV_LABEL: "empty", RDV_COUNT: 0}
            facet_value.update({RDV_VALUE: {"id": "empty"}})
            buckets = [facet_value]
        return buckets

    @staticmethod
    def _get_doc_count(result, key):
        try:
            doc_count = result["aggregations"]["all_facets"]["missing_" + key]["filtered_missing_" + key]["doc_count"]
        except KeyError:
            doc_count = 0
        return doc_count

    @classmethod
    def _to_nesteddict(cls, d, factory):
        result = defaultdict(factory)
        for key, value in d.items():
            if isinstance(value, dict):
                result[key] = cls._to_nesteddict(value, factory)
            else:
                result[key] = value
        return result

    @staticmethod
    def _return_response(r: str) -> Response:
        """ return flask Response, set Access-Control-Allow-Origin

        :param r: IIIF-Object
        :return: flask Response with necessary headers
        """
        if isinstance(r, dict):
            r = json.dumps(r)
        resp = Response(r)
        # ansonsten werden mehrere Allow origin headers gesetzt
        # resp.headers['Access-Control-Allow-Origin'] = '*'
        resp.headers['Content-Type'] = "application/json"
        return resp

    def highlight_fulltext(self):
        pass
        # encoder: "html", pre_tags, post_tags
