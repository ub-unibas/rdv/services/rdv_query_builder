import requests
import openpyxl
import time
import uuid
import os
from copy import deepcopy
import datetime
import pygsheets
import unicodedata

from dateutil import tz
HERE = tz.tzlocal()
UTC = tz.gettz('UTC')
from elasticsearch import helpers, Elasticsearch
from cache_decorator_redis_ubit import CacheDecorator1h, NoCacheDecorator

class RDVForm:
    facet_search_endpoint = "{}v1/rdv_query/simple_facet_search/{}"
    lobid_api = "https://lobid.org/gnd/"
    lobid_search_api = "{}search".format(lobid_api)
    gd_cache_decorator = NoCacheDecorator

    def __init__(self, project_config_store, host="http://127.0.0.1:5000", project=None):
        self.pj_conf = project_config_store

        self.gd_service_file = self.pj_conf.get_value('view.gd_service_file')
        self.gd_service_account_environment_variable = self.pj_conf.get_value('view.gd_service_account_environment_variable')

        self.size = self.pj_conf.get_value('form.size')
        self.lookup_spreadsheet = self.pj_conf.get_value("form.lookup_spreadsheet")
        self.lookup_file = self.pj_conf.get_value("form.lookup_file")

        self.host = host
        self.es_host = self.pj_conf.get_value('hosts.index')
        self.es = Elasticsearch(self.es_host)
        self.index= self.pj_conf.get_value('databases.index_name')
        self.project=project
        self.selected_entries = []
        self.field_sheetname_lookup = {}
        self.field_service = {
        }

    def __call__(self, field, query, object_id=None,selected_entries=[]):
        self.object_id = object_id
        self.selected_entries = selected_entries
        set_ids = self.get_stored_ids(field.split(".")[0])
        results = self.field_service.get(field, self.default_lookup_func)(query, field=field)
        cleansed_results= []
        for r in results:
            if r["value"]["id"] not in set_ids:
                cleansed_results.append(r)
        return cleansed_results[0:self.size]

    def default_lookup_func(self, query, field=""):
        return lambda x: x

    def get_gnd_id(self, gnd_id):
        # todo lookup im index
        lobid_url = "{}{}.json".format(self.lobid_api, gnd_id)
        lobid_json = requests.get(lobid_url).json()
        name = lobid_json.get("preferredName")
        gnd_id = lobid_json.get("id")
        entries = [{'label': name,
          'service_label': name,
          'group': {"de": "GND"},
          'value': {'id': gnd_id}}]
        return entries

    # Helpers
    def query_facets(self, query, field, remove_content_fields=[], service_label_format="{label} | Anzahl: {count}"):

        def strip_accents(s):
            return ''.join((c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn'))
        if not query:
            query = "*"

        query = strip_accents(query)
        facet_query = {
          "query_params": {
            "size": 1,
            "sort": [{
              "field": "_score",
              "order": "desc"
            }],
            "start": 0,
            "source": []
          },
          "facets": {},
          "match_all": True,
          "open_facets": {field: query},
          "lang": "en",
          "exclude_entries": self.selected_entries,
          "aggs_size": 10,
        }
        if 1:
            url = self.facet_search_endpoint.format(self.host, self.project)
            response = requests.post(url=url, json = facet_query)
            if response:
                response = response.json()
        # test to check if direct call is faster = no
        else:
            from rdv_query_builder import RDV2ES4ZAS
            es_response = RDV2ES4ZAS(facet_query, Elasticsearch("localhost:9209"), "zas2", True, iiif_host="127.0.0.1:5001")
            es_response.build_search_body()
            es_response.build_complex_search()
            es_response.build_simplefacet_search(facet_search_endpoint=True, aggs_size=10)
            response = es_response.build_facetsearch_format()
        if response:
            # todo vielleicht besser über exclude lösen
            set_ids = self.get_stored_ids(field.split(".")[0])
            for f in remove_content_fields:
                set_ids = set_ids | self.get_stored_ids(f)

            results = response.get(field,[])
            cleansed_results = []
            for r in results:
                #braucht es nicht mehr: and r["value"]["id"] not in self.selected_entries
                if r["value"]["id"] not in set_ids:
                    # show id to be referenced in service_label
                    r["show_id"] = r["value"]["id"]
                    r["service_label"] = service_label_format.format_map(r)
                    r["group"] = {"de": "Facetten-Werte"}
                    del r["count"]
                    del r["show_id"]
                    cleansed_results.append(r)
            return cleansed_results
        else:
            return []



    def get_stored_ids(self, field):
        # not necessary anymore, solved in Frontend
        # stored_ids = set([v.get("id") if isinstance(v, dict) else v for v in self.get_obj_entries(field)])
        return set()

    def get_obj_entries(self, field):
        if self.object_id:
            id_query = {"query": {"ids": {"values": [self.object_id]}}, "_source": [field]}
            result = self.es.search(index=self.index, body=id_query)
            try:
                field_entries = result["hits"]["hits"][0]["_source"][field]
                if not isinstance(field_entries, list):
                    field_entries = [field_entries]
            except KeyError:
                field_entries = []
        else:
            field_entries = []

        return field_entries

    def search_gnd(self, query, params, field, suggest = {}):
        results = self.query_facets(query, field=field)
        # todo anpassen
        results_lookup = {r["value"]["id"].replace("http://","https://"): 1 for r in results}

        if len(results) < self.size and query:
            lobid_result = requests.get(self.lobid_search_api, params=params)
            lobid_result = lobid_result.json()
            new_lobid_entries = [e for e in lobid_result if e["id"] not in results_lookup]

            if suggest:
                params.update(suggest)
                suggestions = self.get_suggestions(params)
            else:
                suggestions = {}

            for l in new_lobid_entries:
                l.setdefault("value",{})["id"] = l["id"]
                l["service_label"] = suggestions.get(l["id"], l["label"])
                l['group']= {"de": "GND"}
                del l["id"]
                del l["category"]

            return results + new_lobid_entries
        else:
            return results

    def get_suggestions(self, params):
        try:
            sugg_dic = {l["id"]:l["label"] for l in  requests.get(self.lobid_search_api, params=params).json()}
        except Exception:
            sugg_dic = {}
        return sugg_dic

    def search_lookup_value(self, field, sheet, key, query=""):
        results = self.query_facets(query, field=field)
        results_lookup = {r["value"]["id"].lower(): 1 for r in results}

        if len(results) < self.size:
            lookup = self.contr_vocab_lookup(sheet, key, query)
            new_lookup_entries = sorted([v for k, v in lookup.items() if k not in results_lookup], key=lambda x: x["label"].lower())
            return results + new_lookup_entries
        else:
            return results

    def contr_vocab_lookup(self, sheet, key, query=""):
        if self.lookup_spreadsheet:
            results = self.get_autofill_from_gdoc(sheet, key)
        elif self.lookup_file:
            results = self.get_xlsxdict(sheet, key)
        results_lookup = {}
        for k,v in results.items():
            if query.lower() in k.lower():
                value = v.get(key)
                entry = {"label": value, "group": {"de": "def. Vokabular"}, "service_label": "{} | neuer Eintrag".format(value), "value": {"id": value}}
                results_lookup[k.lower()] = entry
        return results_lookup

    def get_xlsxdict(self, sheet_name, key_column):
        """get lookup from xlsx file"""

        try:
            drucke_hss = openpyxl.load_workbook(self.lookup_file, data_only=True)
            sheet = drucke_hss[sheet_name]
        except IOError:
            print("Input file {} or sheet {} does not exist".format(self.file, sheet_name))
            return {}

        rows = sheet.rows
        keys = [c.value for c in next(rows) if c.value]
        druckehss_sys = {}

        for row in rows:
            values = [c.value for c in row]
            row_dict = dict(zip(keys, values))
            row_key = row_dict.get(key_column)
            if row_key:
                druckehss_sys[row_key] = row_dict
        return druckehss_sys

    def get_autofill_from_gdoc(self, sheet_name, key_column):
        from gdspreadsheets_ubit import PygExtendedClient, authorize_pyg
        PygExtendedClient.gc = authorize_pyg(gd_service_account_env_var=self.gd_service_account_environment_variable,
                                             service_file=self.gd_service_file,
                                             cache_decorator=self.gd_cache_decorator)

        func_key_args = {"key_field": self.field_sheetname_lookup.get(sheet_name, sheet_name), "key_func": lambda x: x.lower()}
        try:
            spreadsheet = PygExtendedClient.gc.open_by_key(self.lookup_spreadsheet)
            sheet = spreadsheet.worksheet_by_title(sheet_name)
        except pygsheets.exceptions.WorksheetNotFound:
            print("Sheet {} for spreadsheet {} did not exist, created".format(sheet_name, self.lookup_spreadsheet))
            sheet = spreadsheet.add_worksheet(sheet_name)
        except Exception:
            import traceback
            traceback.print_exc()
            print("Error opening sheet {} from spreadsheet {}".format(sheet_name, self.lookup_spreadsheet))
        form_autofill_data = sheet.gSpreadsheetKeyDict(**func_key_args)

        return form_autofill_data

import pymongo
from pymongo import MongoClient
from bson.objectid import ObjectId


class RDVDatabase:

    db_name = "tbd"
    # needs to be defined in every subclass, so it is not inherited
    initialized = {}

    def __init__(self, request_data, project_config_store, obj={}, obj_type="", old_obj={}):

        self.request_data = request_data
        self.obj = obj
        self.old_obj = old_obj
        self.obj_id = self.request_data["object_id"]
        self.obj_type = obj_type or self.get_object_type(self.request_data)
        self.pj_conf = project_config_store
        self.db_name = self.pj_conf.get_value('databases.mongo_dbname')
        self.db_read_name = self.pj_conf.get_value('databases.mongo_dbname_read')
        self.es_host = self.pj_conf.get_value('hosts.index')
        self.index = self.pj_conf.get_value('databases.index_name')
        self.es = Elasticsearch(self.es_host)
        if not self.initialized:
            self.initialize(self.obj_type, self.db_host)

    @classmethod
    def initialize(cls, obj_type, db_host):
        if not obj_type in cls.initialized:
            client = MongoClient(db_host)

            db = client.get_database(cls.db_name)
            if not obj_type in db.list_collection_names():
                collection = db.get_collection(obj_type)
                collection.create_index([('_obj_id', pymongo.ASCENDING)], unique=False)
            cls.initialized[obj_type] = 1
            cls.db = db

    def filter_fields(self, obj):
        return obj

    @classmethod
    def get_versions(cls, obj_id, col_name):
        versions = []
        collection = cls.db.get_collection(col_name)
        for obj in collection.find({"_obj_id": obj_id}):
            store_id = obj["_id"]
            time_stamp = ObjectId(store_id).generation_time.astimezone(HERE)

            datetime = time_stamp.isoformat()
            label_datetime =  time_stamp.strftime("%Y-%m-%d %H:%M:%S")
            versions.append({"user": "tbd", "label": label_datetime, "datetime": datetime, "version_id": str(store_id)})
        versions.sort(key=lambda x: x.get("datetime"), reverse=True)
        if versions:
            versions[-1]["label"] += " (Original)"
        return versions

    def store_version(self, obj_data={}, old_obj_data= {}, new_obj_id=None):
        # for new objects id is passed
        obj_id = new_obj_id or self.obj_id
        collection = self.db.get_collection(self.obj_type)
        versions = self.get_versions(obj_id=obj_id, col_name=self.obj_type)
        # falls noch keine Version speichere Original-Version
        if not versions and old_obj_data:
            old_obj = self.filter_fields(old_obj_data)
            old_obj.update({"_obj_id": obj_id})
            collection.insert_one(old_obj)
            # sleep 1 second so orig version timestamp is always first
            time.sleep(1)
        filtered_obj = self.filter_fields(obj_data)
        filtered_obj.update({"_obj_id": obj_id})
        result = collection.insert_one(filtered_obj)
        return result.inserted_id

    @classmethod
    def load_version(cls, store_id, col_name):
        collection = cls.db.get_collection(col_name)
        last_obj = collection.find_one({'_id': ObjectId(store_id)})
        del last_obj["_id"]
        del last_obj["_obj_id"]
        return last_obj

    @classmethod
    def get_all_obj_ids(cls, col_name):
        collection = cls.db.get_collection(col_name)
        last_objs = collection.find().distinct('_obj_id')
        return set(last_objs)

    def get_index_name(self, index_alias):
        index_name = list(self.es.indices.get_alias(index_alias).keys())[0]
        return index_name

    @classmethod
    def get_object_type(cls, request_data):
        object_type = request_data.get("object_type", {}).get("value") or request_data.get("objectType")
        return object_type

    def get_mapping_field_type(self):
        mapping = self.es.indices.get_mapping(self.index)
        field_def = {}
        for _, i_mapping in mapping.items():
            for field, def_ in i_mapping.get("mappings", {}).get("properties", {}).items():
                if def_.get("properties", {}).get("id") and def_.get("properties", {}).get("label"):
                    field_def[field] = "id_label"
                elif def_.get("properties", {}):
                    field_def[field] = "nested"
        return field_def

    def build_objstruct4es(self):
        field_def = self.get_mapping_field_type()
        update_obj = {}
        for field in self.request_data["fields"]:
            key = field.get("field_id")
            type_ = field_def.get(key) or field.get("edit_service", {}).get("data_type", "str")  # data/int/str/id_label
            ingest_values = []
            update_obj[key] = ingest_values
            for value in field.get("values", []):
                if type_ == "id_label":
                    i_v = {"id": value.get("value", {}).get("id"), "label": value.get("label")}
                    ingest_values.append(i_v)
                elif type_ == "int":
                    try:
                        ingest_values.append(int(value.get("value", {}).get("id")))
                    except (TypeError, ValueError):
                        pass
                elif type_ in ["str", "string_long", "html"]:
                    ingest_values.append(value.get("label"))
                elif type_ in ["date"]:
                    ingest_values.append(value.get("value", {}).get("id"))
                elif type_ in ["boolean"]:
                    ingest_values.append(value.get("value", {}).get("id"))
                    break
                else:
                    ingest_values.append(value)

        return update_obj

    def update_hook(self, obj_data={}, old_obj_data={}):
        return obj_data

    def update_es_hook(self, obj_data={}, old_obj_data={}):
        return True


    def update_obj_data(self):
        update_obj = self.build_objstruct4es()
        id_query = {"query": {"bool": {"must": [{"ids": {"values": [self.obj_id]}}]}}}
        old_obj = self.es.search(index=self.index, body=id_query)["hits"]["hits"][0]
        old_obj_data = old_obj.get("_source")
        old_obj_index = old_obj.get("_index")

        # deepcopy, da ansonsten schreiben in Mongo-DB zum record Infos hinzufügt?
        old_store_data = deepcopy(old_obj_data)
        old_obj_data.update(update_obj)
        store_data = deepcopy(old_obj_data)
        store_data = self.update_hook(obj_data=store_data)
        store_data["sys_updated"] = datetime.datetime.now().strftime("%Y-%m-%d")
        # weil anders gespeichert del store_data["author"]
        response = self.es.update(index=old_obj_index, id=self.obj_id, body={"doc": store_data}, refresh=True)
        if response.get("result") == "updated":
            if not self.update_es_hook(obj_data=store_data, old_obj_data=old_store_data):
                return "undefined error", 500
            # beim ersten Mal bearbeiten wird auch Original gespeichert
            # todo: beim nächsten Speichern sollte das aber nicht der Fall sein?

            self.store_version(obj_data=store_data, old_obj_data=old_store_data)
            return "okay", 200
        elif response.get("result") == "noop":
            return "nothing changed", 200
        else:
            return "undefined error", 500

    def create_new_id(self):
        return str(uuid.uuid4())

    def create_new_es_id(self, index_alias, new_obj_id):
        return "{}_{}".format(index_alias, new_obj_id)

    def create_new_record(self):

        object_type = self.get_object_type()
        new_obj_id = self.create_new_id()
        update_obj = self.build_objstruct4es()

        # todo: anpassen
        index_alias = "ezas_test"
        update_obj["proj_id"] = object_type
        update_obj["zas_id"] = new_obj_id

        self.store_version(obj_data=update_obj)
        index = self.get_index_name(index_alias)
        response = self.es.index(index=index, body=update_obj, id=self.create_new_es_id(), refresh=True)
        return response