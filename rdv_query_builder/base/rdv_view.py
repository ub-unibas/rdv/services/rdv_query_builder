import openpyxl
import traceback
import json
import os
import datetime
import csv
import urllib.parse
from elasticsearch import Elasticsearch
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import SNIPPET_SEARCH_AFTER, SNIPPET_TITLE, SNIPPET_OBJ_TYPE, SNIPPET_LINE1, \
    SNIPPET_FULLTEXT, SNIPPET_IIIF_PREVIEW, SNIPPET_ID
from cache_decorator_redis_ubit import NoCacheDecorator, SettingsCacheDecorator1h
#import cache_decorator_redis_ubit
from pkg_resources import Requirement, resource_filename
from rdv_data_helpers_ubit import IIIF_PREVIEW_ESFIELD, IIIF_MANIF_ESFIELD

COLUMN_ORDER = "Order"
COLUMN_EDIT_ORDER = "Edit Order"
COLUMN_DISPLAY_LABEL = "Feldlabel"
COLUMN_ES_FIELD = "Feld"
COLUMN_REPEATABLE = "repeatable"
COLUMN_DATATYPE = "data_type"
COLUMN_SHOWEMPTY = "show_empty"
COLUMN_REQUIRED = "required"
COLUMN_GROUP = "group"
COLUMN_STRICT = "strict"
COLUMN_EDIT_SERVICE = "service"
headers = [COLUMN_ES_FIELD, COLUMN_DISPLAY_LABEL, COLUMN_ORDER, COLUMN_EDIT_ORDER, COLUMN_DATATYPE,
           COLUMN_REQUIRED, COLUMN_STRICT, COLUMN_REPEATABLE, COLUMN_SHOWEMPTY, COLUMN_EDIT_SERVICE, COLUMN_GROUP]

class RDVView:
    fields_def_lookup = {}
    # for old projects without gd spreadsheets
    config_folder = "rdv_view_configs"
    # Todo anpassen
    gd_cache_decorator = NoCacheDecorator

    def __init__(self, page_id, page, lang, project_config_store, obj_type=None, es=""):
        self.pj_conf = project_config_store
        self.es_host = self.pj_conf.get_value('hosts.index')
        self.iiif_host = self.pj_conf.get_value('hosts.iiif_presentation')
        self.index = self.pj_conf.get_value('databases.index_name')
        self.spreadsheet = self.pj_conf.get_value('view.spreadsheet')
        self.gd_service_file = self.pj_conf.get_value('view.gd_service_file')
        self.gd_service_account_environment_variable = self.pj_conf.get_value('view.gd_service_account_environment_variable')
        # main google docs folder where all project subdirectories are stored
        self.config_folder = self.pj_conf.get_value('view.config_folder')
        self.main_folder = self.pj_conf.get_value('view.main_folder')
        # if permissions on folder shall be given to specific google account
        self.project_gaccount = self.pj_conf.get_value('view.project_gaccount')
        self.mapping_template_spreadsheet = self.pj_conf.get_value('view.mapping_template_spreadsheet')
        self.show_versions = self.pj_conf.get_value('view.show_versions')

        self.page_id = page_id
        if es:
            self.es = es
        else:
            self.es = Elasticsearch(self.es_host)

        self.page = page
        self.lang = lang
        # for empty record
        self.new_obj_type = obj_type or self.get_inst_kuerzel() or "tbd"
        self.fields_def = self.get_fields_def(self.new_obj_type)

    def enrich_page(self):
        pass

    def get_fields_def(self, proj):
        if proj not in self.fields_def_lookup:
            fields_def = self.get_xlsxdict(proj, COLUMN_ES_FIELD)
            if not fields_def:
                fields_def = self.create_viewdef_from_mapping()
                self.write_es_mapping(proj, fields_def)
            self.fields_def_lookup[proj] = fields_def
        else:
            fields_def = self.fields_def_lookup[proj]
        return fields_def

    def create_mapping_spreadsheet(self, gc):

        project_name = self.__class__.__name__
        folder_exists = gc.drive.folder_metadata(query="parents in '{}' and name = '{}'".
                                                      format(self.main_folder, project_name))
        if not folder_exists:
            folder_id = gc.drive.create_folder(project_name, folder=self.main_folder)
            if self.project_gaccount:
                gc.drive.create_permission(folder_id, role="writer", type='user',
                                                emailAddress=self.project_gaccount)
            print("Google Drive Folder {} {} with permissions for user {} created"
                             .format(project_name, folder_id, self.project_gaccount))
        else:
            folder_id = folder_exists[0].get("id")
            print("Google Drive Folder {} {} already exists"
                              .format(project_name, folder_id))

        spreadsheet_name = "Mapping {}".format(project_name)
        file_exists = gc.drive.spreadsheet_metadata(query="parents in '{}' and name = '{}'".
                                                         format(folder_id, spreadsheet_name))
        if not file_exists:
            template = gc.open_by_key(self.mapping_template_spreadsheet)
            spreadsheet = gc.create(title= spreadsheet_name,
                                         folder=folder_id, template=template)
            spreadsheet_id = spreadsheet.id
            print("Google Spreadsheet {} {} created"
                             .format(spreadsheet_name, spreadsheet_id))
        else:
            spreadsheet_id = file_exists[0].get("id")
            print("Google Spreadsheet {} {} already exists"
                              .format(spreadsheet_name, spreadsheet_id))

        return spreadsheet_id

    def get_gdocs(self, sheet_name, key_field, spreadsheet_id=""):
        from gdspreadsheets_ubit import PygExtendedClient, authorize_pyg
        from cache_decorator_redis_ubit import NoCacheDecorator
        import pygsheets

        PygExtendedClient.gc = authorize_pyg(gd_service_account_env_var=self.gd_service_account_environment_variable, service_file=self.gd_service_file, cache_decorator=self.gd_cache_decorator)
        fields_def = {}
        if not spreadsheet_id:
            spreadsheet_id = self.create_mapping_spreadsheet(PygExtendedClient.gc)
            fields_def = self.create_viewdef_from_mapping()
            self.write_mapping2gd(gc=PygExtendedClient.gc, spreadsheet_id=spreadsheet_id,
                                  sheetname=sheet_name, fields_def=fields_def)
        else:
            func_key_args = {"key_field": key_field, "key_func": lambda x: x.lower()}
            try:
                spreadsheet = PygExtendedClient.gc.open_by_key(spreadsheet_id)
                sheet = spreadsheet.worksheet_by_title(sheet_name)
                fields_def = sheet.gSpreadsheetKeyDict(**func_key_args)
            except pygsheets.exceptions.WorksheetNotFound:
                print("Sheet {} for spreadsheet {} did not exist, created".format(sheet_name, spreadsheet_id))
                spreadsheet.add_worksheet(sheet_name)
                fields_def = self.create_viewdef_from_mapping()
                self.write_mapping2gd(gc=PygExtendedClient.gc, spreadsheet_id=spreadsheet_id,
                                      sheetname=sheet_name, fields_def=fields_def)
            except Exception as e:
                traceback.print_exc()
                print("Error opening sheet {} from spreadsheet {}: {}".format(sheet_name, spreadsheet_id, e))
        return fields_def

    def write_mapping2gd(self, gc, spreadsheet_id, sheetname, fields_def):
        import pygsheets
        spreadsheet = gc.open_by_key(spreadsheet_id)
        try:
            mapping_sheet = spreadsheet.worksheet_by_title(sheetname)
        except pygsheets.exceptions.WorksheetNotFound:
            print("Sheet {} for spreadsheet {} did not exist, created".format(sheetname, spreadsheet_id))
            mapping_sheet = spreadsheet.add_worksheet(sheetname)
        headers = mapping_sheet.gSpreadsheetGetHeaders()
        def sort_mapping_lines(items: dict):
            # exclude first "Feld"-column sort after es_field name (column)
            key, value = items
            mapped_values = sorted(list(filter(lambda x: value[x] if x != COLUMN_ES_FIELD else "", value.keys())))
            field = mapped_values[0] if mapped_values else "keine Zuordnung"
            return field, key

        mapping_sheet.gSpreadsheetsSheetWriter(fields_def, key_field=COLUMN_ES_FIELD, sort_func=sort_mapping_lines,
                                               headers=headers)
        print("Mapping written to Spreadsheet {} {}".format(spreadsheet.id, mapping_sheet.title))

    def return_rdv_view(self, edit=False) -> str:
        return self.get_rdv_view(edit=edit)

    def create_viewdef_from_mapping(self, proj=None):

        mapping = self.es.indices.get_mapping(index=self.index)
        field_def = {}
        n = 0
        for _, i_mapping in mapping.items():
            for field, def_ in i_mapping.get("mappings", {}).get("properties", {}).items():
                n +=1
                new_field = {}

                type_ = def_.get("type")

                if def_.get("properties", {}).get("id") and def_.get("properties", {}).get("label"):
                    service_type = "id_label"
                elif def_.get("properties", {}).get("link") and def_.get("properties", {}).get("label"):
                    # todo: link- label
                    service_type = "str"
                elif def_.get("properties", {}):
                    service_type = "nested"
                    continue
                elif type_ in ["text"]:
                    service_type = "str"
                elif type_ in ["date"]:
                    service_type = "date"
                elif type_ in ["long"]:
                    service_type = "int"
                else:
                    service_type = type_


                for setting in [COLUMN_REPEATABLE, COLUMN_STRICT, COLUMN_EDIT_SERVICE, COLUMN_REQUIRED]:
                    new_field[setting] = False

                if service_type == "id_label":
                    new_field[COLUMN_DATATYPE] = service_type
                    new_field[COLUMN_EDIT_SERVICE] = True
                    new_field[COLUMN_REPEATABLE] = True
                elif field == "Quelle":
                    new_field[COLUMN_DATATYPE] = service_type
                    new_field[COLUMN_EDIT_SERVICE] = True
                    new_field[COLUMN_REPEATABLE] = True
                elif service_type == "str":
                    new_field[COLUMN_DATATYPE] = service_type
                    new_field[COLUMN_EDIT_SERVICE] = True
                    new_field[COLUMN_REPEATABLE] = True
                else:
                    new_field[COLUMN_DATATYPE] = service_type

                new_field[COLUMN_ES_FIELD] = field
                new_field[COLUMN_SHOWEMPTY] = False
                new_field[COLUMN_EDIT_ORDER] = n
                new_field[COLUMN_ORDER] = n
                field_def[field] = new_field

        return field_def

    def write_es_mapping(self, proj, fields_def):
        if 1:
            filename = "fields_def_{}.xlsx".format(self.__class__.__name__)
            import openpyxl
            try:
                mapping_out = openpyxl.Workbook()
                proj_sheet = mapping_out.create_sheet(title=proj)
            except (IOError, TypeError):
                print("Input file {} or sheet {} does not exist".format(filename, proj))
                return {}
            for n, header in enumerate(headers, start=1):
                proj_sheet.cell(row=1, column=n).value = header

            for l, f in enumerate(fields_def.items(), start=2):
                field, values = f
                for k, v in values.items():
                    pos = headers.index(k) +1
                    proj_sheet.cell(row=l, column=pos).value = v
            mapping_out.save(os.path.join("/tmp",filename))


    @classmethod
    def get_page(cls, page_id: str, lang, project_config_store):
        """ extract only those metadatafields to be shown for an object that are defined in Google Spreadsheet

        :param page_id: id of a page
        :return: ajson dict for page
        """
        index = project_config_store.get_value("databases.index_name")
        es_host = project_config_store.get_value("hosts.index")
        page_id = urllib.parse.unquote(page_id)
        query = {
            "query": {
                "ids": {
                    "values": [page_id]
                }
            }
        }
        es = Elasticsearch(es_host)
        es_results = es.search(index=index, body=query)

        try:
            page = es_results["hits"]["hits"][0]["_source"]
            return cls(page_id=page_id, page=page, lang=lang,
                       project_config_store=project_config_store)
        except (KeyError, IndexError):
            import traceback
            traceback.print_exc()
            return None

    @classmethod
    def get_empty_page(cls, project_config_store, lang = "de", obj_type=None):
        """ get an empty json definition for an object

        :param page_id: id of a page
        :return: a json definiton for an empty page
        """

        return cls(page_id=None, page={}, lang=lang, obj_type=obj_type, project_config_store=project_config_store)

    def add_descr_data(self):
        return []

    def get_rdv_view(self, edit=False):
        """ extract only those metadatafields to be shown for an object that are defined in Google Spreadsheet

        :param page_id: id of a page
        :param lang: language setting for metadata
        :return: dict for building rdv view
        """

        manif_url = self.get_manifest_url()
        inst_kuerzel = self.new_obj_type

        if edit:
            descr_metadata = self.get_edit_metadata(dict_type="edit_view")
        else:
            descr_metadata = self.get_edit_metadata(dict_type="view")
        def_metadata = self.get_def_metadata()
        def_metadata.update(self.get_groups())

        if manif_url:
            def_metadata.update(self.get_viewer())
            def_metadata.update({"iiif_manifest": manif_url})
            descr_manif = {"value": {"de": [{"label": "Link zum Manifest", "link": manif_url}]},"label": {"de": ["IIIF Manifest"]}}
        # ZasView
        elif 0 and "ZasView":
            # def_metadata.update({"viewer": None})
            descr_manif ={"value": {"de": ["Kein Manifest, da zu viele Artikel, nutzen sie die Viewer Suchseite zum Filtern und Anzeigen"]},
                 "label": {"de": ["IIIF Manifest"]}}
        else:
            descr_manif = {}
        # add empty service_info for manif
        edit_info = self.get_edit_info()
        descr_manif.update(edit_info)
        descr_metadata.append(descr_manif)
        if self.add_descr_data():
            descr_metadata.extend(self.add_descr_data())
        return {"desc_metadata": descr_metadata, "func_metadata": def_metadata}

    def get_def_metadata(self):
        def_metadata = {
                        "preview_image": self.get_preview_image(),
                        "title": self.get_title(),
                        "id": self.page_id,
                        "object_type": self.get_obj_type(),
                        # Todo: anpassen
                        #"object_type": {"label": self.get_obj_type(), "value": self.new_obj_type},
                        }
        return def_metadata

    def get_viewer(self):
        return {"viewer": ["Mirador"]}

    @classmethod
    def get_groups(cls):
        return {}

    def get_selection(self, v, es_field):
        selection = {}
        if "id" in v and isinstance(v, dict):
            # todo hier kann auch ein Wert gesetzt werden
            selection = {
            '{}.id.keyword'.format(es_field): {'values': [
                {'value': {'id': v.get("id")},
                 'label': v.get("label")}], 'operator': 'OR'}}
            # v["selection"] = "{}/?facets={}~{}".format(self.rdv_host, es_field, v.get("id"))
        elif es_field == "Quelldatum":
            selection = {"Quelldatum": {"values": [{"value": {"gte": v, "lte": v}}]}}
        return selection

    def get_page_values(self, es_field):
        return self.page.get(es_field)

    def get_edit_metadata(self, exclude_fields = [], dict_type="view"):
        """ extract only those metadatafields to be shown from Object that are defined in Google Spreadsheet

        :param page: metadata for a page
        :param institution: name of institution
        :return: array containing metadata in IIIF Standard
        """
        descr_metadata = []
        self.enrich_page()
        # all metadata shall be shown, if no fields defined
        if not self.fields_def:
            filtered_fields = [{COLUMN_ES_FIELD: x} for x in self.page]
        else:
            # no order or order = 0 -> don't show
            if dict_type == "edit_view":
                filtered_fields = filter(lambda x: x.get(COLUMN_EDIT_ORDER) or x.get(COLUMN_ORDER), self.fields_def.values())
            else:
                filtered_fields = filter(lambda x: x.get(COLUMN_ORDER),
                                         self.fields_def.values())
        fields = sorted(list(filtered_fields), key=lambda x: self.order_labels(x, dict_type=dict_type))

        for field in fields:
            label = self.get_lang_label(field, self.lang)
            labels = self.get_lang_labels(field)
            edit_info = self.get_edit_info(field)
            es_field = field[COLUMN_ES_FIELD]

            if not es_field in exclude_fields:

                values = self.get_page_values(es_field)

                if values:
                    if isinstance(self.page[es_field], list):
                        value = self.extract_values(self.page[es_field])
                    else:
                        value = [self.page[es_field]]
                else:
                    value = ""
                # todo: anpassen
                if dict_type == "flat":
                    value = "; ".join([str(v) for v in value])
                    label = label
                elif dict_type in ["edit_view", "view"]:
                    if value:
                        if isinstance(values, list):
                            value = {"de": values}
                            for v in values:
                                # todo: anpassen
                                # v_selection = self.get_selection(v, es_field, selections)
                                # todo: title exception nur für ZAS
                                if isinstance(v, bool):
                                    value = {"de": [{"label": v, "id": v}]}
                                elif isinstance(v, dict) and "id" in v  :
                                    v["selection"] = self.get_idlab_select(v, es_field)
                                    #v["selection"] = "{}/?facets={}~{}".format(self.rdv_host, es_field, v.get("id"))
                                elif es_field == "Quelldatum":
                                    date_value = datetime.datetime.strptime(v, "%Y-%m-%d")
                                    de_value = date_value.strftime("%d.%m.%Y")
                                    en_value = date_value.strftime("%m/%d/%Y")

                                    value = {"de": [{"label": de_value, "id": v}]}
                                    value.update({"en": [{"label": en_value, "id": v}]})
                                    value["de"][0]["selection"] = {"Quelldatum": {"values": [{"value": {"gte": v, "lte": v}}]}}
                                    value["en"][0]["selection"] = {
                                        "Quelldatum": {"values": [{"value": {"gte": v, "lte": v}}]}}
                        elif isinstance(values, dict):
                            if "link" in values and "label" in values:
                                value.setdefault("de",[]).append(values)
                            else:
                                value = {"de":[json.dumps(values, indent=True)] }
                        else:
                            value = {"de": value}
                    label = labels
                elif dict_type == "iiif_v3":
                    if value:
                        value = {"de": value}
                        label = labels
                elif dict_type == "iiif_v2":
                    if value:
                        value = {"de": "; ".join([str(v) for v in value])}
                        label = labels
                if value or edit_info:
                    if not value:
                        # do not include empty values
                        if dict_type == "view":
                            continue
                        else:
                            value = []
                    entry = {"label": label, "value": value}
                    # todo: anpassen
                    if field.get(COLUMN_GROUP):
                        entry.update({"group": field.get(COLUMN_GROUP)})
                    if dict_type != "iiif_v3":
                        entry.update(edit_info)
                        entry.update({"field_id": es_field})
                    descr_metadata.append(entry)
        return descr_metadata

    def get_idlab_select(self, v, es_field):
        selection = {'{}.id.keyword'.format(es_field): {'values': [
            {'value': {'id': v.get("id")},
             'label': v.get("label")}], 'operator': 'OR'}}
        return selection

    def get_label_value_metadata(self, exclude_fields = [], dict_type="flat"):
        """ extract only those metadatafields to be shown from Object that are defined in Google Spreadsheet

        :param institution: name of institution
        :return: array containing metadata in IIIF Standard
        """

        descr_metadata = []

        # all metadata shall be shown, if no fields defined
        if not self.fields_def:
            filtered_fields = [{COLUMN_ES_FIELD: x} for x in self.page]
        else:
            # no order or order = 0 -> don't show
            filtered_fields = filter(lambda x: x.get(COLUMN_ORDER), self.fields_def.values())

        fields = sorted(list(filtered_fields), key=lambda x: self.order_labels(x))
        for field in fields:
            label =self.get_lang_label(field, self.lang)
            labels = self.get_lang_labels(field)
            es_field = field[COLUMN_ES_FIELD]
            view_info = self.get_view_info(field)

            if not es_field in exclude_fields:
                if self.page.get(es_field):
                    if isinstance(self.page[es_field], list):
                        value= self.extract_values(self.page[es_field])
                    else:
                        value = [self.page[es_field]]
                    # todo: anpassen
                    if dict_type == "flat":
                        value = "; ".join([str(v) for v in value])
                        label = label
                    elif dict_type == "iiif_v3":
                        if value:
                            value = {"de": value}
                            label = labels
                    elif dict_type == "iiif_v2":
                        if value:
                            value = {"de": "; ".join([str(v) for v in value])}
                            label = labels

                    if value:
                        entry = {"label": label, "value": value}
                        if dict_type != "iiif_v3":
                            entry.update(view_info)
                        descr_metadata.append(entry)
        return descr_metadata

    def get_inst_kuerzel(self):
        return "not defined"

    def get_title(self):
        return self.page.get("Title")

    def get_obj_type(self, lang="de"):
        return "not defined"

    def get_manifest_id(self):
        """return path to manifest without http-Domain,
        e.g. "/".join(["dizas", zas_id, "manifest"])"""
        manif_id = self.page.get(IIIF_MANIF_ESFIELD,[])
        if manif_id:
            return manif_id[0]

    def get_manifest_url(self):
        manif_id = self.get_manifest_id()
        if manif_id and (manif_id.startswith("https://") or manif_id.startswith("http://")):
            return manif_id
        elif manif_id:
            return "{}/{}".format(self.iiif_host, manif_id)
        else:
            return ""

    def add_snippet_link(self) -> dict:
        """add link to snippet (instead of detail page), e.g. swisscovery link
        format: {"url": "", "label": ""}
        """
        link = self.page.get("link_field")
        return link

    def add_snippet_fulltext(self) -> str:
        """add expandabel fulltext to snippet (instead of detail page), e.g. abstract
        """
        fulltext = self.page.get("fulltext_field")
        return fulltext

    def build_main_snippet(self):
        snippet = {}
        snippet[SNIPPET_ID] = self.page_id
        snippet[SNIPPET_TITLE] = self.get_title()
        snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type()
        # js -es int problem
        snippet[SNIPPET_SEARCH_AFTER] = json.dumps(self.page.get(SNIPPET_SEARCH_AFTER))
        snippet[SNIPPET_IIIF_PREVIEW] = self.get_preview_image()
        snippet_fulltext = self.page.get(SNIPPET_FULLTEXT, [])
        if snippet_fulltext:
            snippet[SNIPPET_FULLTEXT] = "...</br>".join(snippet_fulltext)

        return snippet

    def build_snippet(self):

        snippet = self.build_main_snippet()

        snippet["i18n"] = {}

        # languages definieren
        for lang in ["de", "en"]:
            snippet_values = {}
            snippet["i18n"][lang] = {}
            lang_snippet = snippet["i18n"][lang]
            for es_field in self.snippet_fields:
                # lookup einbauen
                field_def = self.fields_def.get(es_field.lower())
                label = self.get_lang_label(field_def, lang)
                es_value = self.page.get(es_field)
                if isinstance(es_value, list):
                    values = self.extract_values(es_value)
                    value = "<br/>".join(values)
                else:
                    value = es_value
                    value = value.strip() if isinstance(value, str) else value

                # for zero
                if value is not None and value != "":
                    snippet_values[es_field] = "<b>{}</b>: {}".format(label, value).strip(":")
            # combine all snippet values in SNIPPET_LINE1
            lang_snippet[SNIPPET_LINE1] = "<br/>".join(
                [snippet_values[f] for f in self.snippet_fields if f in snippet_values])
            lang_snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type(lang=lang)

        if self.add_snippet_link():
            snippet["link"] = self.add_snippet_link()
        if self.add_snippet_fulltext():
            snippet["fulltext"] = self.add_snippet_fulltext()

        return snippet

    def get_view_info(self, field):
        edit_infos = {"edit_service": {}}
        if field:
            for column_name in [COLUMN_REPEATABLE, COLUMN_DATATYPE]:
                value = field.get(column_name)
                if column_name in [COLUMN_REPEATABLE]:
                    edit_infos["edit_service"][column_name] = True if value else False
                elif column_name in [COLUMN_DATATYPE]:
                    edit_infos["edit_service"][column_name] = value if value else "str"
        return edit_infos

    def get_edit_info(self, field=None):
        edit_infos = {"edit_service": {}}
        if field:
            es_field = field[COLUMN_ES_FIELD]
            for column_name in [COLUMN_EDIT_ORDER, COLUMN_REPEATABLE, COLUMN_DATATYPE, COLUMN_SHOWEMPTY, COLUMN_STRICT, COLUMN_EDIT_SERVICE, COLUMN_REQUIRED, COLUMN_GROUP]:
                value = field.get(column_name)
                if column_name in [COLUMN_REPEATABLE, COLUMN_SHOWEMPTY, COLUMN_STRICT, COLUMN_REQUIRED, COLUMN_EDIT_SERVICE]:
                    edit_infos["edit_service"][column_name] = True if value else False
                elif column_name in [COLUMN_EDIT_ORDER]:
                    edit_infos["edit_service"]["edit"] = True if value else False
                elif column_name in [COLUMN_DATATYPE]:
                    edit_infos["edit_service"][column_name] = value if value else "str"
                elif column_name in [COLUMN_GROUP] and value:
                    edit_infos["edit_service"][column_name] = value
        else:
            field = {}
            for column_name in [COLUMN_EDIT_ORDER, COLUMN_REPEATABLE, COLUMN_DATATYPE, COLUMN_SHOWEMPTY, COLUMN_STRICT, COLUMN_EDIT_SERVICE, COLUMN_REQUIRED]:
                value = field.get(column_name)
                if column_name in [COLUMN_REPEATABLE, COLUMN_SHOWEMPTY, COLUMN_STRICT, COLUMN_REQUIRED, COLUMN_EDIT_SERVICE]:
                    edit_infos["edit_service"][column_name] = True if value else False
                elif column_name in [COLUMN_EDIT_ORDER]:
                    edit_infos["edit_service"]["edit"] = True if value else False
                elif column_name in [COLUMN_DATATYPE]:
                    edit_infos["edit_service"][column_name] = value if value else "str"
                elif column_name in [COLUMN_GROUP] and value:
                    edit_infos["edit_service"][column_name] = value
        return edit_infos

    @classmethod
    def get_lang_label(self, field, lang):
        if not lang:
            lang = self.lang
        label = None
        if field:
            lang_label = "{} {}".format(COLUMN_DISPLAY_LABEL, lang)
            for field_name in [lang_label, COLUMN_DISPLAY_LABEL, COLUMN_ES_FIELD]:
                label = field.get(field_name)
                if label:
                    break
            else:
                label = "label not defined"
        return label

    def get_lang_labels(self, field):
        labels = {}
        langs =  [k.split(COLUMN_DISPLAY_LABEL)[1].strip() for k in field.keys() if k and len(k.split(COLUMN_DISPLAY_LABEL)) > 1]
        for lang in langs:
            lang_label = "{} {}".format(COLUMN_DISPLAY_LABEL, lang)
            if field.get(lang_label):
                labels[lang] = [field.get(lang_label)]
        if not labels:
            labels["de"] = [field.get(COLUMN_ES_FIELD)]
        return labels

    def get_preview_image(self) -> str:
        """returns url for preview image in snippet view"""
        iiif_images = self.page.get(IIIF_PREVIEW_ESFIELD, [])
        if iiif_images:
            return iiif_images[0].split("info.json")[0]
        else:
            return ""

    @staticmethod
    def order_labels(x, dict_type="view"):
        try:
            if dict_type=="edit":
                return int(x.get(COLUMN_EDIT_ORDER, 0)) or 0
            else:
                return int(x.get(COLUMN_ORDER, 0)) or 0
        except (ValueError, TypeError):
            return 999

    @staticmethod
    def extract_values(input_values):
        values = []
        for t in input_values:
            if isinstance(t, str) or isinstance(t, int) or isinstance(t, float):
                values.append(str(t))
            elif isinstance(t, dict) and "label" in t and "link" in t:
                values.append('<a href="{}">{}</a>'.format(t["link"], t["label"]))
            elif isinstance(t, dict) and "label" in t:
                values.append(t["label"])
            elif isinstance(t, list):
                values.extend(t)
        return [v.strip() for v in values if isinstance(v, str)]

    def get_xlsxdict(self, sheet_name, key_column):
        """get lookup from xlsx file"""

        try:
            if not self.file:
                raise IOError
            file_path = resource_filename(Requirement.parse("rdv_query_builder"), self.file)
            drucke_hss = openpyxl.load_workbook(file_path, data_only=True)
            sheet = drucke_hss[sheet_name]
        except (IOError, TypeError, KeyError, openpyxl.utils.exceptions.InvalidFileException):
            print("Input file {} or sheet {} does not exist".format(self.file, sheet_name))
            return {}

        rows = sheet.rows
        keys = [c.value for c in next(rows)]
        druckehss_sys = {}

        for row in rows:
            values = [c.value for c in row]
            row_dict = dict(zip(keys, values))
            row_key = row_dict.get(key_column)
            if row_key:
                druckehss_sys[row_key] = row_dict
        return druckehss_sys
