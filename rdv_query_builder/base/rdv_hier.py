
class RDVViewHier():

    hierarchy_index = ""
    hier_level4links = 0

    def enrich_page(self):
        self.get_hier_entries(hier_field="hierarchy_filter_han2")
        self.get_hiersub_entries(hier_field="hierarchy_filter_han2")

    def build_simple_hierarchy_search_body(self, hier_pos, hier_pos_field="hierarchy_filter_han2"):

        must_filters = [
            {"prefix": {"{}.keyword".format(hier_pos_field): {"value": hier_pos}}}
        ]
        count_body = {"query":{"bool":{"filter": must_filters}}}

        return count_body

    def get_hiersub_entries(self, hier_field):

        bsiz_id = self.page.get("bsiz_id")
        entries = []
        for entry in self.es.search(index=self.hierarchy_index, body={"query":{"term":{"sys_id.keyword": bsiz_id}}}).get("hits", {}).get("hits", []):
            try:
                count = self.get_count(entry.get("_source",{}).get("hierarchy_filter")[0])
                if not count:
                    return
                entries.append({
                    "hierarchy_filter": entry.get("_source",{}).get("hierarchy_filter"),
                    "label" : "{} ({} zugeordnete Objekte)".format(entry.get("_source",{}).get("prefLabel")[0], count),
                    "id": entry.get("_id")
                    })
            except IndexError:
                return

        entries = sorted(entries, key=lambda x : sorted(x.get("hierarchy_filter", [])))
        self.page["hier_path"].extend(entries)



    def get_count(self, hier_pos):
        query = self.build_simple_hierarchy_search_body(hier_pos)
        count = self.es.count(index=self.index,body=query).get("count")
        return count

    def get_hier_entries(self, hier_field):
        hierarchy_filters = self.page.get(hier_field) or []
        ids = []
        for h_filters in hierarchy_filters:
            for h_filter in h_filters:
                len_ids = len(h_filter.split(".")) +1
                id_parts = h_filter.split(".")
                # TODO: han_hier_553.14.23.2
                while len_ids > self.hier_level4links:
                    ids.append(".".join(id_parts[0:len_ids]))
                    len_ids -= 1
        ids = sorted(ids)
        entries = []
        for entry in self.es.search(index=self.hierarchy_index, body={"query":{"terms":{"hierarchy_filter": ids}}}).get("hits", {}).get("hits", []):
            count = self.get_count(entry.get("_source", {}).get("hierarchy_filter")[0])
            entries.append({
                "hierarchy_filter": entry.get("_source",{}).get("hierarchy_filter"),
                "label" : "{} ({} zugeordnete Objekte)".format(entry.get("_source",{}).get("prefLabel")[0], count),
                "id": entry.get("_id")
                })
        entries = sorted(entries, key=lambda x : sorted(x.get("hierarchy_filter", [])))
        self.page["hier_path"] = entries
