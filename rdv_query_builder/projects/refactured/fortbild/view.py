import json

from rdv_query_builder import RDVView, RDVDatabase
from rdv_query_builder.projects.refactured.fortbild import FortbildDatabase
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import SNIPPET_SEARCH_AFTER, SNIPPET_TITLE, SNIPPET_OBJ_TYPE, SNIPPET_LINE1, \
    SNIPPET_FULLTEXT, SNIPPET_IIIF_PREVIEW, SNIPPET_ID

# todo: link als link ingestieren (label, link dictionary), datum zusammenfassen
# editierbar machen

class FortbildView(RDVView):
    """class to define snippet and view fields and settings"""
    # lookup file for form definition (same as view definition), needs to be defined, including config folder, e.g.: "{}/ZAS Vocab.xlsx".format(RDVView.config_folder)
    file = "{}/fields_def_FortbildView.xlsx".format(RDVView.config_folder)
    # needs to be defined in every subclass, so it is not inherited, stores data from form/view definition, do not overwrite
    fields_def_lookup = {}
    # which fields should be included in the snippet, order defined by order
    snippet_fields = ["Name", "Datum_von", "Datum_bis", "Stunden_total", "Kommentar"]
    # class to write form data to database, subclass from RDVDatabase
    db_class = FortbildDatabase

    def get_inst_kuerzel(self) -> str:
        """returns object type to control code flow (e.g. based on index)"""
        proj = "fortbild"
        return proj

    def get_obj_type(self, lang="de") -> str:
        """returns obj type for presentation, pass Es field for "obj_field" """
        obj_type = self.page.get("Kategorie")
        return obj_type

    def get_title(self) -> str:
        """returns title for presentation, pass Es field for "title_field" """
        title = self.page.get("Bezeichnung")
        return title