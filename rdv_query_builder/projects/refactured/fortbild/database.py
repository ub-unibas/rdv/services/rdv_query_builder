from rdv_query_builder import RDVDatabase

class FortbildDatabase(RDVDatabase):
    """class to define database access/storage"""
    # name of the mongo db database
    db_name = "fortbild"
    # needs to be defined in every subclass, so it is not inherited
    initialized = {}

    def __init__(self, **kwargs):
        """
        :param obj:
        :param obj_id:
        :param obj_type:
        :param old_obj:
        :param db_host:
        """
        super().__init__(**kwargs)

    def get_obj_type(self, lang="de") -> str:
        """returns obj type for presentation, pass Es field for "obj_field"
         see also RDVView class
         """
        obj_type = "fortbild"
        return obj_type