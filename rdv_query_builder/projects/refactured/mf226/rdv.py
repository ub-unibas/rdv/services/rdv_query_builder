from rdv_query_builder import RDV2ES, RDVView, RDVForm
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import RQ_GTE, RQ_LTE

from rdv_query_builder.projects.refactured.mf226 import MF226View
# obligatory: index and view_class

class RDV2ES4MF226(RDV2ES):
    """class to define es functionalities (query builder)"""
    # define alias for index/indices
    index= "mf226"
    # what is the name of the view class
    view_class = MF226View
    # are there iiif compontents? = documentViewerProxyUrl in RDV Frontend
    iiif = True
    # how many objects shall be combined to a dynamic manifest
    # limit size to a certain limit, because it is frontend controlled
    max_ids_dyn_manif = 5
    # what is the url prefix (ini-file name in es config) for dynamic manifests
    dyn_manif= "mf226"

    def __init__(self, *args, **kwargs):
        """

        :param request_data:
        :param es_client:
        :param index: is ignored, used from class var
        :param debug:
        :param iiif_host:
        :param es_host:
        :param autocomplete:
        """
        super().__init__(*args, **kwargs)
        # fields to be returned from Elasticsearch (_source), controlled from frontend if deleted here
        # use it here, if some fields shall not be made public or for performance reasons (e.g. large data/fulltext fields)
        self.source_fields = []
