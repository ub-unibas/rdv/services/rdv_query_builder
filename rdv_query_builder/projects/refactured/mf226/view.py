import json

from rdv_query_builder import RDVView, RDVDatabase

# obligatory: get_title, get_obj_type

IIIF_IMAGE_SERVER = "https://ub-sipi.ub.unibas.ch"

class MF226View(RDVView):
    """class to define snippet and view fields and settings"""

    # lookup file for form definition (same as view definition), needs to be defined, including config folder, e.g.: "{}/ZAS Vocab.xlsx".format(RDVView.config_folder)
    file = "{}/fields_def_MF226View.xlsx".format(RDVView.config_folder)
    # needs to be defined in every subclass, so it is not inherited, stores data from form/view definition, do not overwrite
    fields_def_lookup = {}
    # which fields should be included in the snippet, order defined by order
    snippet_fields = ["orig_sign", "sru_sign"]

    def get_obj_type(self, lang="de") -> str:
        """returns obj type for presentation, pass Es field for "obj_field" """
        obj_type = self.page.get("520_a") or "Musik-Mikrofilm"
        if isinstance(obj_type, list):
            obj_type = ", ".join([t.get("label") for t in obj_type if t])
        return obj_type

    def get_title(self) -> str:
        """returns title for presentation, pass Es field for "title_field" """
        title = self.page.get("245_ab")
        if isinstance(title, list):
            title = ", ".join([t for t in title if t])
        return title

    def get_preview_image(self) -> str:
        """returns url for preview image in snippet view"""
        id_jp2 = self.page.get("sru_iiif_preview")
        if id_jp2:
            return id_jp2
        elif self.page.get("jp2_files"):
            link = "{}/{}/{}".format(IIIF_IMAGE_SERVER, "mf226", self.page.get("jp2_files")[0])
            return link
        else:
            return ""

    def get_manifest_id(self) -> str:
        """return path to manifest without http-Domain,
        e.g. "/".join(["dizas", zas_id, "manifest"])"""
        manif_id = self.page.get("sru_iiif_manif","")
        if not manif_id and self.page.get("jp2_files"):
            manif_id = "/".join(["mf226", self.page.get("bsiz_id"), "manifest"])
        return manif_id

    def get_viewer(self) -> dict:
        """define which viewer shall be shown in detail page, refers to key in documentViewer Frontend Configuration
        use case: for AV material only use Viewer x, for Images only y, ...
        e.g. {"viewer": ["MiradorSinglePage", "UV"]}"""
        return {"viewer": ["MiradorSinglePage"]}