import json
import urllib.parse
from elasticsearch import Elasticsearch
from rdv_query_builder import RDVView, RDVDatabase, RDVViewHier
from cache_decorator_redis_ubit import NoCacheDecorator
# obligatory: get_title, get_obj_type

IIIF_IMAGE_SERVER = "https://ub-sipi.ub.unibas.ch"
from rdv_data_helpers_ubit import IIIF_PREVIEW_ESFIELD, IIIF_MANIF_ESFIELD

class GBooksView(RDVView, RDVViewHier):
    """class to define snippet and view fields and settings"""
    # lookup file for form definition (same as view definition), needs to be defined, including config folder, e.g.: "{}/ZAS Vocab.xlsx".format(RDVView.config_folder)
    file = None #"{}/fields_def_PortraetView.xlsx".format(RDVView.config_folder)
    # needs to be defined in every subclass, so it is not inherited, stores data from form/view definition, do not overwrite
    fields_def_lookup = {}
    # which fields should be included in the snippet, order defined by order
    snippet_fields = ["author", "normdate", "exakte_lebensdaten", "wiki_extract"]
    # needs to be defined here to next to RDV2ES class
    hierarchy_index = "han_hier2"
    # up to which level links at detail page shall be provided
    hier_level4links = 0
    gd_cache_decorator = NoCacheDecorator
    spreadsheet = "1dMdNj_N3yb5mwEjQHY-ULEyzYmWawZDaH7qdCCjhcLM"

    def get_inst_kuerzel(self):
        return self.get_obj_type()

    def get_fields_def(self, proj):
        if proj not in self.fields_def_lookup:
            spreadsheet_id = self.spreadsheet
            fields_def = self.get_gdocs(spreadsheet_id=spreadsheet_id, sheet_name=proj, key_field="Feld")
            self.fields_def_lookup[proj] = fields_def
        else:
            fields_def = self.fields_def_lookup[proj]
        return fields_def

    def get_obj_type(self, lang="de") -> str:
        """returns obj type for presentation, pass Es field for "obj_field" """
        obj_type = self.page.get("type")
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        return obj_type

    def get_title(self) -> str:
        """returns title for presentation, pass Es field for "title_field" """
        title = (self.page.get("title") or self.page.get("gnd_name_id"))[0].get("label")

        return title

    def get_viewer(self) -> dict:
        """define which viewer shall be shown in detail page, refers to key in documentViewer Frontend Configuration
        use case: for AV material only use Viewer x, for Images only y, ...
        e.g. {"viewer": ["MiradorSinglePage", "UV"]}"""
        return {"viewer": ["MiradorSinglePage", "UV"]}

    def get_preview_image(self) -> str:
        """returns url for preview image in snippet view"""
        iiif_images = self.page.get(IIIF_PREVIEW_ESFIELD, [])
        thumbnail = self.page.get("wiki_thumbnail")
        if iiif_images:
            return iiif_images[0].split("info.json")[0] + "/full/120,/0/default.jpg"
        elif thumbnail:
            return thumbnail
        else:
            return ""

    def add_descr_data(self):
        vlm_id = self.page.get("vlm_id")
        bsiz_id = self.page.get("bsiz_id")

        add_descr = [{"value": {"de": [{"label": "swisscovery",
                                        "link": "https://basel.swisscovery.org/discovery/fulldisplay?"
                                                "docid=alma{}&context=L&vid=41SLSP_UBS:live".format(bsiz_id)}]},
                      "label": {"de": ["swisscovery Link"],
                                "en": ["swisscovery Link"]}
                      }]

        if vlm_id and "e-rara" in self.page.get("iiif_manifest", [""])[0]:
            erara_link = {"value": {"de": [{"label": "Link auf e-rara",
                                                 "link": "https://www.e-rara.ch/bau_1/content/titleinfo/{}"
                                                     .format(self.page.get("vlm_id"))}]},
                               "label": {"de": ["e-rara Link"],
                                         "en": ["e-rara Link"]}
                               }
            add_descr.append(erara_link)
        return add_descr

    def get_page_values(self, es_field):
        if es_field == "rel_persons":

            return_fields = []
            field_content = self.page.get(es_field)
            for f in field_content:
                return_fields.append("{} (Rolle: {})".format(f.get("label"), f.get("role_label")))
            return return_fields
        else:
            return self.page.get(es_field)


