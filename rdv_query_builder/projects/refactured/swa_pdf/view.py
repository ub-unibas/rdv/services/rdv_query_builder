import json

from rdv_query_builder import RDVView, RDVDatabase
from rdv_query_builder.projects.refactured.swa_pdf import SWAPDFTestDatabase, SWAPDFDatabase
from rdv_data_helpers_ubit.projects.swasachdok.swasachdok import PDF_FILEID, PDF_FILENAME, PDF_RENAMED, PDF_LINKS, LOOKUP_TITLE, SWA_PDF_TITLE_FIELD
# obligatory: get_title, get_obj_type


class SWAPDFView(RDVView):
    """class to define snippet and view fields and settings"""
    # to define whether it is a test class, functionality needs to be defined in subclasses, e.g. adapt path, ...
    test = False
    # lookup file for form definition (same as view definition), needs to be defined, including config folder, e.g.: "{}/ZAS Vocab.xlsx".format(RDVView.config_folder)
    file = None #"{}/fields_def_SWAPDFView.xlsx".format(RDVView.config_folder)
    # needs to be defined in every subclass, so it is not inherited, stores data from form/view definition, do not overwrite
    fields_def_lookup = {}
    # which fields should be included in the snippet, order defined by order
    snippet_fields = [PDF_FILENAME, PDF_RENAMED, "parent_accordion", PDF_LINKS, "old_url",  "swisscovery_link", "delete"]
    # class to write form data to database, subclass from RDVDatabase
    db_class = SWAPDFDatabase
    # whether versions stored in mongodb for an object shall be accessible while editing
    show_versions = True
    spreadsheet = "1owbtGuV8-ZJza3UXGpIRu-cT6qCQcG4uLVN0ANot3Ys"
    # Domain-Name for swa_search_host
    swa_search_host = "https://ub-swasearch.ub.unibas.ch"


    def get_fields_def(self, proj):
        if proj not in self.fields_def_lookup:
            spreadsheet_id = self.spreadsheet
            fields_def = self.get_gdocs(spreadsheet_id=spreadsheet_id, sheet_name=proj, key_field="Feld")
            self.fields_def_lookup[proj] = fields_def
        else:
            fields_def = self.fields_def_lookup[proj]
        return fields_def

    def get_inst_kuerzel(self) -> str:
        """returns object type to control code flow (e.g. based on index)"""
        return "swadok_pdfs"

    def get_obj_type(self, lang="de") -> str:
        """returns obj type for presentation, pass Es field for "obj_field" """
        obj_type = self.page.get("type")
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        if not obj_type:
            obj_type = ", ".join([t.get("label") for t in self.page.get("type_autocomplete") if t.get("label")])
        return obj_type

    def get_title(self) -> str:
        """returns title for presentation, pass Es field for "title_field" """
        try:
            title = str(self.page.get(SWA_PDF_TITLE_FIELD)[0])
        except (KeyError, TypeError):
            return "Kein IZID Titel für {}".format(self.page.get(PDF_FILENAME))
        return title

    def get_manifest_id(self) -> str:
        """return path to manifest without http-Domain,
        e.g. "/".join(["dizas", zas_id, "manifest"])"""
        manif_id = "swa_pdf/{}/manifest".format(self.page.get(PDF_FILEID))
        return manif_id

    def get_viewer(self) -> dict:
        """define which viewer shall be shown in detail page, refers to key in documentViewer Frontend Configuration
        use case: for AV material only use Viewer x, for Images only y, ...
        e.g. {"viewer": ["MiradorSinglePage", "UV"]}"""
        return {"viewer":["UV"]}

    def add_descr_data(self):
        add_descr = []
        if self.page.get(LOOKUP_TITLE):
            swasearch_links =[{"label": "Anzeige SWA-Search {}".format(bsiz_id.get("id")),
              "link": "{}/de/detail/swasearch_{}".format(self.swa_search_host, bsiz_id.get("id"))} for bsiz_id in self.page.get(LOOKUP_TITLE, [{}])]
            swa_search_link = {"value": {"de": swasearch_links},
                               "label": {"de": ["SWASearch Link"],
                                         "en": ["SWASearch Link"]}
                               }
            edit_info = self.get_edit_info()
            swa_search_link.update(edit_info)
            add_descr = [swa_search_link]
        return add_descr

    def get_preview_image(self) -> str:
        """returns url for preview image in snippet view"""
        pdf_link = self.page.get(PDF_LINKS, [{}])[0].get("link","")
        if "/sachdok/" in pdf_link:
            pdf_id = "-".join(pdf_link.split("/sachdok/")[-1].split("/"))
        else:
            pdf_id = "-".join(pdf_link.split("https://ub-sachdokpdf.ub.unibas.ch/")[-1].split("/"))
        if pdf_id:
            return "https://ub-sipi.ub.unibas.ch/swa_pdfs/{}@1".format(pdf_id)
        else:
            return "https://ub-sipi.ub.unibas.ch/logos/icon-SWASearch.jpg"

class SWAPDFTestView(SWAPDFView):
    # class to write form data to database, subclass from RDVDatabase
    db_class = SWAPDFTestDatabase
    swa_search_host = "https://ub-swasearch-test.ub.unibas.ch"

    def get_manifest_id(self) -> str:
        """return path to manifest without http-Domain,
        e.g. "/".join(["dizas", zas_id, "manifest"])"""
        manif_id = "swa_pdf_test/{}/manifest".format(self.page.get(PDF_FILEID))
        return manif_id

class SWAPDFLocalView(SWAPDFView):
    # class to write form data to database, subclass from RDVDatabase
    db_class = SWAPDFTestDatabase
    swa_search_host = "http://localhost:4208"

    def get_manifest_id(self) -> str:
        """return path to manifest without http-Domain,
        e.g. "/".join(["dizas", zas_id, "manifest"])"""
        manif_id = "swa_pdf_test/{}/manifest".format(self.page.get(PDF_FILEID))
        return manif_id

