import logging
import re
import traceback

from rdv_query_builder import RDVDatabase
from copy import deepcopy
import os
import uuid
import requests
import datetime
import PyPDF2
import elasticsearch
import shutil

from rdv_marc_ubit import SWAJSONRecord, MarcTransformRule
from cache_decorator_redis_ubit import ShortCacheDecorator
from pathvalidate import sanitize_filename
from rdv_data_helpers_ubit import IZID_ESFIELD
from rdv_data_helpers_ubit.projects.swasachdok.swasachdok import E_MEDIUM, LOOKUP_TITLE, TYPE_ID_FIELD, PDF_DATE, HEFT_NR, QUARTAL, PDF_FILEID, PDF_FILENAME, \
    NO_LINK_PDF, NO_IZID_CON, LINK_TEXT, PDF_LINKS, PDF_COUNT, PDF_DELETED, PDF_CREATED, SACHDOK_INDEX_PREFIX, build_dhq_filename, create_new_id, extract_marc_data, \
    E_MONO, E_GESCHAEFTSB, E_ZEITSCHRIFT


from rdv_data_helpers_ubit.projects.swasearch.swasearch import SWASEARCH_INDEX_PREFIX, SWASEARCH_INDEX, SWASEARCH_TEST_INDEX

class SWAPDFDatabase(RDVDatabase):
    """class to define database access/storage"""
    # name of the mongo db database
    db_name = "swadok_pdfs"
    # needs to be defined in every subclass, so it is not inherited
    initialized = {}
    # update_index for merged e-zeitschriften
    merge_index= SWASEARCH_INDEX
    merge_index_prefix = SWASEARCH_INDEX_PREFIX

    def __init__(self, **kwargs):
        """
        :param obj:
        :param obj_id:
        :param obj_type:
        :param old_obj:
        :param db_host:
        """
        super().__init__(**kwargs)


    def build_pdf_filename(self, update_obj):
        bsiz_id = update_obj[LOOKUP_TITLE][0].get("id")
        dhq = build_dhq_filename(update_obj)
        link_text = update_obj.get(LINK_TEXT)[0] if update_obj.get(LINK_TEXT) else ""
        title = "_".join([p for p in [bsiz_id, dhq, link_text] if p])
        return sanitize_filename(title)

    def download_pdf_file(self, update_obj, bsiz_id):
        store_folder =  os.getenv("PDF_STORE_FOLDER")
        url_prefix =  os.getenv("UPLOAD_PREFIX")
        pdf_filename = self.build_pdf_filename(update_obj)
        download_link = update_obj["download_link"][0] if update_obj.get("download_link") else ""
        izid_folder = os.path.join(store_folder, bsiz_id)
        if not os.path.exists(izid_folder):
            os.mkdir(izid_folder)
        store_path = os.path.join(izid_folder, "{}.pdf".format(pdf_filename))
        if download_link:
            if not os.path.exists(store_path):
                if download_link.startswith("http"):
                    pdf_data = requests.get(download_link,verify=False).content
                else:
                    with open(os.path.join(os.getenv("PDF_UPLOAD_FOLDER"), download_link), "rb") as local_pdf:
                        pdf_data = local_pdf.read()

                try:
                    import io
                    str_io = io.BytesIO(pdf_data)
                    PyPDF2.PdfFileReader(str_io)
                except PyPDF2.utils.PdfReadError:
                    print("invalid PDF file")
                    return None, None, store_path
                with open(store_path, "wb") as fh:
                    fh.write(pdf_data)
                file_url = url_prefix.format(os.path.join(bsiz_id, "{}.pdf".format(pdf_filename)))
                return pdf_filename, file_url, store_path
            else:
                print("File {} exists".format(store_path))
                return None, None, store_path

    def extract_same_izid_pdfs(self, bsiz_id):
        all_objects = []

        query = {
         "size": 9999,
            "sort": [
                {PDF_DATE: {"order": "desc"}},
                {HEFT_NR: {"order": "desc"}},
                {QUARTAL: {"order": "desc"}},
                {"{}.keyword".format(LINK_TEXT): {"order": "desc"}},
            ],
          "query": {
            "bool": {
              "must": [
                {
                  "terms": {
                    "{}.id.keyword".format(LOOKUP_TITLE): [
                      bsiz_id
                    ]
                  }
                }
              ],
              "must_not": [
                {
                  "terms": {
                    PDF_DELETED: [
                      True
                    ]
                  }
                },
              {
                  "terms": {
                      "{}.id.keyword".format(TYPE_ID_FIELD): [
                          NO_LINK_PDF,
                          NO_IZID_CON
                      ]
                  }
              }
              ]
            }
          }
        }
        reverse_marker = False
        for entry in self.es.search(index=self.index, body=query).get("hits").get("hits"):
            rs = entry.get("_source")
            pdf_links = rs.get(PDF_LINKS) or []
            all_objects.extend(pdf_links)
            # E-Monos sollen umgedreht sortiert werden
            if E_MONO in rs["type"] and not  E_ZEITSCHRIFT in rs["type"] and not E_GESCHAEFTSB in rs["type"]:
                reverse_marker = True
        if reverse_marker:
            all_objects = all_objects[::-1]
        return all_objects

    def update_sachdokmerged_index(self, add_bsiz_ids, remove_bsiz_ids, obj={}, main_bsiz_id=None):
        bsiz_ids = add_bsiz_ids + remove_bsiz_ids
        for bsiz_id in bsiz_ids:
            all_pdfs = self.extract_same_izid_pdfs(bsiz_id)
            entry = {PDF_LINKS: all_pdfs, PDF_COUNT: len(all_pdfs)}
            entry.update(self.add_metadata(bsiz_id))
            pdf_types = [o.get("label") for o in obj.get(TYPE_ID_FIELD,[]) if o]

            try:
                entry.setdefault("swa_objecttype",[]).extend(pdf_types)
                entry["swa_objecttype"] =list(set(entry["swa_objecttype"]))
                es_responses = self.es.update(index=self.merge_index, id="{}_{}".format(self.merge_index_prefix, bsiz_id),
                                              body={"doc": entry})
            except elasticsearch.exceptions.NotFoundError:
                # otherwise wrong metadata is added when two izids are referred
                full_obj = deepcopy(obj)
                full_obj.update(entry)

                # add pdf type
                full_obj.setdefault("swa_objecttype",[]).extend(pdf_types)
                full_obj["swa_objecttype"] =list(set(full_obj["swa_objecttype"]))
                es_responses = self.es.index(index=self.merge_index, id="{}_{}".format(self.merge_index_prefix, bsiz_id),
                                              body=full_obj)
        if es_responses.get("result") in ["updated", "created", "deleted"]:
            return True
        elif es_responses.get("result") == "noop":
            return True
        else:
            return False

    def update_es_hook(self, obj_data={}, old_obj_data={}):
        main_bsiz_id = obj_data.get(LOOKUP_TITLE)[0]["id"]
        new_bsiz_ids = set([o.get("id") for o in obj_data.get(LOOKUP_TITLE, [])])
        old_bsiz_ids = set([o.get("id") for o in old_obj_data.get(LOOKUP_TITLE, [])])
        obj_data[IZID_ESFIELD] = main_bsiz_id
        old_main_bsiz_id = old_obj_data.get(IZID_ESFIELD)
        removed_bsiz_ids = list(old_bsiz_ids - new_bsiz_ids)
        store_folder = os.getenv("PDF_STORE_FOLDER")

        rename_pdfs = False
        del_pdfs = False
        if rename_pdfs or del_pdfs:
            oldizid_folder = os.path.join(store_folder, old_main_bsiz_id)
            old_pdffilename = obj_data[PDF_FILENAME]
            old_store_path = os.path.join(oldizid_folder, old_pdffilename)

        # move PDF to del folder
        if del_pdfs and obj_data[PDF_DELETED]:
            del_folder = os.path.join(store_folder, "deleted")
            del_store_path = os.path.join(del_folder, old_pdffilename)
            if not os.path.exists(del_folder):
                os.mkdir(del_folder)
            shutil.move(old_store_path, del_store_path)
        # move PDF to new filename
        # TODO eventuell wieder einkommentieren, falls erwünscht, sollte aber gut getestet werden
        elif rename_pdfs:
            pdf_filename = self.build_pdf_filename(obj_data)
            izid_folder = os.path.join(store_folder, main_bsiz_id)
            if not os.path.exists(izid_folder):
                os.mkdir(izid_folder)
            new_store_path = os.path.join(izid_folder, "{}.pdf".format(pdf_filename))
            #
            if not os.path.exists(new_store_path) and not new_store_path.startswith("/tmp") :
                shutil.move(old_store_path, new_store_path)

        if self.update_sachdokmerged_index(add_bsiz_ids=list(new_bsiz_ids), remove_bsiz_ids=removed_bsiz_ids, obj=obj_data, main_bsiz_id=main_bsiz_id):
            return True
        else:
            return False

    def update_hook(self, obj_data, old_obj_data={}):
        link_text = obj_data.get(LINK_TEXT)[0] if obj_data.get(LINK_TEXT) else ""
        dhq_filename = build_dhq_filename(obj_data)
        bsiz_id = obj_data.get(LOOKUP_TITLE)[0]["id"]
        # otherwise multiple references are overwritten
        lookup_titles = obj_data.get(LOOKUP_TITLE)
        obj_data.update(self.add_metadata(bsiz_id))
        obj_data[LOOKUP_TITLE] = lookup_titles
        obj_data["type"] = [o.get("label") for o in obj_data.get(TYPE_ID_FIELD, []) if o.get("label")] or [E_MEDIUM]
        if obj_data.get(PDF_LINKS):
            obj_data[PDF_LINKS][0]["label"] = link_text.strip() or dhq_filename.replace("_", " ").strip() or bsiz_id
        return obj_data

    def enrich_download(self, obj, bsiz_id, file_url, pdf_filename):
        entry = self.add_metadata(bsiz_id)
        link_text = obj.get(LINK_TEXT)[0] if obj.get(LINK_TEXT) else ""
        dhq_filename = build_dhq_filename(obj)
        entry["type"] = [(obj.get(TYPE_ID_FIELD) or [{}])[0].get("label",[])] or [E_MEDIUM]
        entry[PDF_CREATED] = datetime.datetime.now().strftime("%Y-%m-%d")
        #entry[PDF_CREATED] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        entry[PDF_LINKS] = [{"link": file_url, "label": link_text.strip() or dhq_filename.replace("_", " ").strip() or bsiz_id}]
        return entry

    @classmethod
    def get_object_type(cls, request_data):
        object_type = request_data.get("object_type", {}).get("value") or request_data.get("objectType") or "swadok_pdfs"
        return object_type

    def add_metadata(self, bsiz_id):

        marc_rule = MarcTransformRule(
            gd_service_file=os.getenv("GD_SERVICE_FILE"),
            gd_service_account_env_var = None,
            marc_spreadsheet=os.getenv("SWA_MARC_GDSHEET"), marc_record_class=SWAJSONRecord,
            gd_cache_decorator=ShortCacheDecorator)
        al = SWAJSONRecord.get_sysid(bsiz_id)
        add_metadata = extract_marc_data(al, marc_rule=marc_rule)
        return add_metadata

    def create_new_record(self):
        update_obj = self.build_objstruct4es()
        index = self.get_index_name(index_alias=self.index)
        bsiz_id = update_obj[LOOKUP_TITLE][0].get("id")
        bsiz_ids = [o.get("id") for o in update_obj.get(LOOKUP_TITLE, [])]
        update_obj[PDF_FILEID] = create_new_id(bsiz_id)
        new_obj_id = self.create_new_es_id(SACHDOK_INDEX_PREFIX, create_new_id(bsiz_id))
        filename, file_url, store_path = self.download_pdf_file(update_obj, bsiz_id)
        if filename is None:
            return "error", 404
        update_obj[PDF_FILENAME] = "{}.pdf".format(filename)
        update_obj.update(self.enrich_download(update_obj, bsiz_id, file_url, filename))

        try:
            object_check = self.es.get(index, new_obj_id)
            print("Object exists", object_check)
            return "Object exists", 404
        except elasticsearch.exceptions.NotFoundError:
            # copy, otherwise cant be indexed because obj_id is added to dict from mongodb -> change in db_class
            store_data = deepcopy(update_obj)
            try:
                self.store_version(obj_data=store_data, new_obj_id=new_obj_id)
                response = self.es.index(index=index, body=update_obj, id=new_obj_id, refresh=True)
                obj_id = response.get("_id")
            except Exception as e:
                # TODO: delete Mongo entry
                import traceback
                traceback.print_exc()
                os.remove(store_path)
                obj_id = None
                return "problem storing entry", 404
            # not store_data als obj, da für monog bson.objectid.ObjectId hinzugefügt wird
            self.update_sachdokmerged_index(add_bsiz_ids=bsiz_ids, remove_bsiz_ids=[], obj=update_obj, main_bsiz_id=bsiz_id)
            return obj_id

class SWAPDFTestDatabase(SWAPDFDatabase):
    # name of the mongo db database
    db_name = "swadok_pdfs_test"
    merge_index= SWASEARCH_TEST_INDEX