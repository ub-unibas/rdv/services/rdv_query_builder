from rdv_query_builder import RDV2ES, RDVView, RDVForm
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import RQ_GTE, RQ_LTE
from rdv_query_builder.projects.refactured.swa_pdf import SWAPDFView, SWAPDFForm, SWAPDFTestView, SWAPDFLocalView
# obligatory: index and view_class
from rdv_data_helpers_ubit.projects.swasachdok.swasachdok import SACHDOK_INDEX, SACHDOK_TEST_INDEX, PDF_CREATED, PDF_DATE, HEFT_NR, QUARTAL

class RDV2ES4SWAPDF(RDV2ES):
    """class to define es functionalities (query builder)"""
    # to define whether it is a test class, functionality needs to be defined in subclasses, e.g. adapt path, ...
    test = False
    # define alias for index/indices
    index= SACHDOK_INDEX
    # what is the name of the view class
    view_class = SWAPDFView
    # what is the name of the form class
    form_class = SWAPDFForm
    # are there iiif compontents? = documentViewerProxyUrl in RDV Frontend
    iiif = True
    # how many objects shall be combined to a dynamic manifest
    # limit size to a certain limit, because it is frontend controlled
    max_ids_dyn_manif = 5
    # what is the url prefix (ini-file name in es config) for dynamic manifests
    dyn_manif= ""
    # the content from which field should be used for es_highlighting
    highlight_field = ""
    # if highlighting is used for multiple fields if fieldname shall be incluede
    highlight_included_fieldname = False
    # which default sort order shall be applied
    # e.g.: [{"Quelldatum": {"order": "desc", "unmapped_type": "date"}}]
    default_sort_old = [{PDF_DATE: {"order": "desc", "unmapped_type": "int"}},
                    {HEFT_NR: {"order": "desc", "unmapped_type": "int"}},
                    {QUARTAL: {"order": "desc", "unmapped_type": "int"}}]
    default_sort = [{PDF_CREATED: {"order": "desc", "unmapped_type": "date"}}]
    # condition to exclude objects without detail page, to get next/previous detail page,
    # e.g. in bla only authors have detail page, records only links to swisscovery
    search_after_query = {}
    # how many results within a facet shall be calculated by elasticsearch
    # to limit aggregation size to a certain limit, because it is frontend controlled
    max_facet_values = 100

    # not used: concept to load snippets and facets async, for faster loading
    load_aggs_async = False


    def __init__(self, *args, **kwargs):
        """

        :param request_data:
        :param es_client:
        :param index: is ignored, used from class var
        :param debug:
        :param iiif_host:
        :param es_host:
        :param autocomplete:
        """
        super().__init__(*args, **kwargs)
        # I think the only reason why some values are overwritten here is legacy
        self.hierarchy_index = ""
        # definition how the hierarchy tree shall be sorted
        self.hierarchy_facet_sort = lambda x: str(x.get("label",""))
        # fields used for full text query, if empty list, it searchs within all fields
        self.simple_query_fields = []
        # fields to be returned from Elasticsearch (_source), controlled from frontend if deleted here
        # use it here, if some fields shall not be made public or for performance reasons (e.g. large data/fulltext fields)
        self.source_fields = []
        # for which range date ranges shall be created, defines start level of ranges, steps: century, decade, year, day, ...
        # function get_date_agg_interval can be used to exclude to small aggregation, e.g. if only year values, no month aggregation is needed
        self.default_date_range = {RQ_GTE: "1800-01-01", RQ_LTE: "2050-01-01"}
        # definition for int ranges to calculate range sizes, defines start level of ranges, steps: 1, 10, 100, 1000, ...
        self.default_int_range = {RQ_GTE: 0, RQ_LTE: 1000}


class RDV2ES4TESTSWAPDF(RDV2ES4SWAPDF):
    # define alias for index/indices
    index= SACHDOK_TEST_INDEX
    # what is the name of the view class
    view_class = SWAPDFTestView

class RDV2ES4LOCALSWAPDF(RDV2ES4SWAPDF):
    # define alias for index/indices
    index= SACHDOK_TEST_INDEX
    # what is the name of the view class
    view_class = SWAPDFLocalView