import os
from rdv_query_builder import RDVForm, RDVView
from rdv_marc_ubit import SWAJSONRecord, ALMA_BIB_REST
from rdv_data_helpers_ubit.projects.swasachdok.swasachdok import LOOKUP_TITLE, TYPE_ID_FIELD

class SWAPDFForm(RDVForm):
    """class to define lookup functions for forms"""

    def __init__(self, **kwargs):
        """
        :param host:
        :param es_host:
        :param index:
        """
        super().__init__(**kwargs)
        # field name and lookup function for each field is defined
        self.field_service = {
            LOOKUP_TITLE: self.search_sru_bsid_id,
            TYPE_ID_FIELD: self.search_type_id
        }

    def default_lookup_func(self, query, field):
        return self.search_varia(query, field)

    def search_type_id(self, query="", field=""):
        results = self.query_facets(query, field="{}.id.keyword".format(field))
        return results

    def search_varia(self, query="", field=""):
        results = self.query_facets(query, field="{}.keyword".format(field))
        return results

    def search_sru_bsid_id(self, query="", field=""):
        old_query = query
        facets = self.query_facets(field="{}.id.keyword".format(LOOKUP_TITLE), query=query)
        if facets:
            return facets
        # rewirte query from NZID to IZID not via SRU because NZID doesnot contain IZID for the first ~10 Minutes after creating a record
        if query.startswith("99") and query.endswith("5501"):
            nz = ALMA_BIB_REST(api_key=os.getenv("ALMA_API_KEY"))
            query = nz.get_iz_id(query)
        if not query:
            print("Probably no IZ-ID Found for {}".format(old_query))
        if (query.startswith("99") and query.endswith("5504")) or query.startswith("1_"):
            al = SWAJSONRecord.get_sysid(query)
            if al:
                title = al.get_title()
                # add subfield c for title label
                title[0]["label"] = ". ".join(
                    al.get_field_data(marc_fields=["245"], marc_ind1s=[], marc_ind2s=[], subfields=["a", "b", "c", "p"]))
                facets = [{'label': "{id} | {label}".format(**title[0]), 'value': {'id': title[0].get("id")},
                           'service_label': "{id} | {label}".format(**title[0]),
                           "group": {"de": "ALMA-Eintrag"}}]

        return facets