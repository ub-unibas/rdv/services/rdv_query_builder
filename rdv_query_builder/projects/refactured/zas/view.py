import json
import datetime

from rdv_query_builder import RDVView
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import SNIPPET_SEARCH_AFTER, SNIPPET_TITLE, SNIPPET_OBJ_TYPE, SNIPPET_LINE1, SNIPPET_FULLTEXT, \
    SNIPPET_IIIF_PREVIEW, SNIPPET_ID
from rdv_data_helpers_ubit import OLDSYS_ESFIELD, RDV_CATEGORY_ESFIELD, IIIF_IMGS_ESFIELD, RDV_TITLE, RDV_OBJECTTYPE_ESFIELD
from rdv_data_helpers_ubit.projects.zas.zas import ZAS_DATE_ESFIELD, ZAS_SOURCE_ESFIELD, \
    ZAS_MINDATE_ESFIELD, ZAS_MAXDATE_ESFIELD, ZAS_ARTICLES_ESFIELD, ZAS_DOC_TYPES, ZAS_ID_ESFIELD, ZAS_TITLE_ESFIELD
from rdv_data_helpers_ubit.projects.swasearch.swasearch import SWASEARCH_DESCR_ALL

class ZasView(RDVView):
    # if only defined in super class not "safe" against ZasIntView-Def
    fields_def_lookup = {}
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # add zas specific configs
        self.snippet_fields_clipping = self.pj_conf.get_value('view.snippet_fields_clipping')
        self.snippet_fields_collection = self.pj_conf.get_value('view.snippet_fields_collection')

    def get_fields_def(self, proj):
        if proj not in self.fields_def_lookup:
            spreadsheet_id = self.spreadsheet
            fields_def = self.get_gdocs(spreadsheet_id=spreadsheet_id, sheet_name=proj, key_field="Feld")
            self.fields_def_lookup[proj] = fields_def
        else:
            fields_def = self.fields_def_lookup[proj]
        return fields_def

    @property
    def zas_type(self):
        return self.new_obj_type

    def get_inst_kuerzel(self):
        proj = self.page.get(RDV_CATEGORY_ESFIELD) or self.page.get(RDV_OBJECTTYPE_ESFIELD,[""])[0] or self.page.get("type")
        return proj

    def get_obj_type_new(self, lang="de"):
        obj_type = self.page.get(RDV_OBJECTTYPE_ESFIELD,[""])[0] or self.page.get("type")
        obj_id = self.get_inst_kuerzel()
        if self.zas_type in ZAS_DOC_TYPES:
            obj_label= "Zeitungsausschnitt" if lang == "de" else "Clipping"
        elif obj_type:
            obj_label = obj_type
        else:
            obj_label = "Dokumentensammlung" if lang == "de" else "Document collection"
        return {"label": obj_label, "value": obj_id}


    def get_obj_type(self, lang="de"):
        obj_type = self.page.get(RDV_OBJECTTYPE_ESFIELD,[""])[0] or self.page.get("type")
        if self.zas_type in ZAS_DOC_TYPES:
            return "Zeitungsausschnitt" if lang == "de" else "Clipping"
        elif obj_type:
            return obj_type
        else:
            return "Dokumentensammlung" if lang == "de" else "Document collection"

    def get_title(self):
        title = self.page.get(ZAS_TITLE_ESFIELD) or self.page.get(RDV_TITLE) or self.page.get(SWASEARCH_DESCR_ALL) or self.page.get("gnd_name_id")
        if isinstance(title, list):
            values = self.extract_values(title)
            title = ", ".join(values)
        return title

    def get_manifest_id(self):
        # exclude too big manifests
        if self.zas_type not in ZAS_DOC_TYPES and self.page.get(ZAS_ARTICLES_ESFIELD, 0) > 300:
            return ""
        elif self.page.get(ZAS_ID_ESFIELD):
            test = "" if not self.test else "test"
            zas_id = self.page.get(ZAS_ID_ESFIELD)
            manif_id = "/".join([test + self.zas_type, zas_id, "manifest"])
            return manif_id
        else:
            return None

    def get_preview_image(self) -> str:
        """returns url for preview image in snippet view"""
        id_jp2 = (self.page.get(IIIF_IMGS_ESFIELD) or [{}])[0].get("id") or (self.page.get("jp2_files") or [""])[0]
        thumbnails = [self.page.get("wiki_thumbnail","")]
        if id_jp2:
            return "{}/{}/{}".format("https://ub-sipi.ub.unibas.ch", self.zas_type, id_jp2) + "/full/120,/0/default.jpg"
        elif thumbnails:
            return thumbnails
        else:
            return ""

    def get_viewer(self):
        if self.zas_type in ZAS_DOC_TYPES:
            return {"viewer": ["MiradorSinglePage", "UV"]}
        else:
            return {"viewer": ["UV"]}

    def build_snippets(self):
        lang_snippets = {}
        for lang in ["de", "en"]:
            lang_snippets[lang] = self.build_snippet(lang)
        return lang_snippets


    def build_snippet(self):

        snippet = {}
        snippet[SNIPPET_ID] = self.page_id
        snippet[SNIPPET_TITLE] = self.get_title()
        snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type()
        # js -es int problem
        snippet[SNIPPET_SEARCH_AFTER] = json.dumps(self.page.get(SNIPPET_SEARCH_AFTER))
        if "wiki_thumbnail" in self.page:
            snippet[SNIPPET_IIIF_PREVIEW] = self.get_preview_image()
        if self.zas_type in ZAS_DOC_TYPES:
            snippet[SNIPPET_IIIF_PREVIEW] = self.get_preview_image()
            if self.page.get(SNIPPET_FULLTEXT):
                snippet[SNIPPET_FULLTEXT] = " ...</br>... ".join(self.page.get(SNIPPET_FULLTEXT, []))

        snippet["i18n"] = {}
        for lang in ["de", "en"]:
            snippet_values = {}
            snippet["i18n"][lang] = {}
            lang_snippet = snippet["i18n"][lang]
            zas_fields = self.snippet_fields_clipping + self.snippet_fields_collection
            for es_field in zas_fields:
                label =self.get_lang_label(self.fields_def.get(es_field.lower()), lang)
                if isinstance(self.page.get(es_field,[]), list):
                    values = self.extract_values(self.page.get(es_field,[]))
                    value = ", ".join(values)
                else:
                    value = self.page[es_field]
                    value = value.strip() if isinstance(value, str) else value

                if value:
                    # todo:
                    if es_field == ZAS_DATE_ESFIELD:
                        date_value = datetime.datetime.strptime(value, "%Y-%m-%d")
                        if lang == "de":
                            value = date_value.strftime("%d.%m.%Y")
                        elif lang == "en":
                            value = date_value.strftime("%m/%d/%Y")
                    snippet_values[es_field] = "{}: {}".format(label, value)

            lang_snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type(lang)
            if self.zas_type in ZAS_DOC_TYPES:
                lang_snippet[SNIPPET_LINE1] = ", ".join([snippet_values[f] for f in self.snippet_fields_clipping if f in snippet_values])
            else:
                lang_snippet[SNIPPET_LINE1] = ", ".join(
                    [snippet_values[f] for f in self.snippet_fields_collection if f in snippet_values])

        return snippet

class ZasTestView(ZasView):
    fields_def_lookup = {}

    def get_manifest_id(self):
        # exclude too big manifests
        if self.zas_type not in ZAS_DOC_TYPES and self.page.get(ZAS_ARTICLES_ESFIELD, 0) > 300:
            return ""
        elif self.page.get(ZAS_ID_ESFIELD):
            test = "test"
            zas_id = self.page.get(ZAS_ID_ESFIELD)
            manif_id = "/".join([test + self.zas_type, zas_id, "manifest"])
            return manif_id
        else:
            return None

class ZasDevView(ZasView):
    fields_def_lookup = {}

    def get_manifest_id(self):
        # exclude too big manifests
        if self.zas_type not in ZAS_DOC_TYPES and self.page.get(ZAS_ARTICLES_ESFIELD, 0) > 300:
            return ""
        elif self.page.get(ZAS_ID_ESFIELD):
            test = "dev"
            zas_id = self.page.get(ZAS_ID_ESFIELD)
            manif_id = "/".join([test + self.zas_type, zas_id, "manifest"])
            return manif_id
        else:
            return None
