import re
from copy import deepcopy
import json


from rdv_query_builder import RDV2ES, RDVView, RDVForm
from rdv_query_builder.projects.refactured.zas import ZasView, ZasTestView, ZasDevView
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import SNIPPET_SEARCH_AFTER, SNIPPET_TITLE, SNIPPET_OBJ_TYPE, SNIPPET_LINE1, SNIPPET_FULLTEXT, \
    SNIPPET_IIIF_PREVIEW, SNIPPET_ID, RQ_GTE, RQ_LTE, RDV_COUNT, RDV_LABEL, RQ_FIELD

from rdv_data_helpers_ubit import FULLTEXT_ESFIELD, RDV_TITLE, RDV_CATEGORY_ESFIELD, IIIF_IMGS_ESFIELD
from rdv_data_helpers_ubit.projects.swasearch.swasearch import \
    SWASEARCH_DESCR_PLACE, SWASEARCH_DESCR_PERSON, SWASEARCH_DESCR_FUV, SWASEARCH_DESCR_STW, SWASEARCH_DESCR_ALL

from rdv_data_helpers_ubit.projects.hierarchy.hierarchy import HIERARCHY_FIELD, HIERARCHY_LABEL_ESFIELD
from rdv_data_helpers_ubit.projects.zas.zas import ZAS_DATE_ESFIELD, ZAS_SOURCE_ESFIELD, \
    ZAS_MINDATE_ESFIELD, ZAS_MAXDATE_ESFIELD, ZAS_ARTICLES_ESFIELD, ZAS_ID_ESFIELD, ZAS_TITLE_ESFIELD, ZAS_PAGES_ESFIELD
from rdv_data_helpers_ubit.projects.zas.zas import ZAS_DATE_ESFIELD

class RDV2ES4ZAS(RDV2ES):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.hierarchy_facet_sort = self.sort_stw

    def build_hier_selection(self, field, id_, operator):
        try:
            # field = "hierarchy_filter"
            field = "{}.keyword".format(HIERARCHY_FIELD)
            s_facets = self.selection["selection"]["facets"]
            label, pos = self.get_hier_descriptor_label_pos(id_)
            s_facets.setdefault(field, {}).setdefault("values",[]).append({"value": {"id":id_, "pos": pos}, "label": label})
            s_facets[field]["operator"] = operator
        except KeyError:
            import traceback
            traceback.print_exc()
            pass

    def get_hier_descriptor_label_pos(self, id_):
        #id_ = "stw_" + id_
        query = {"query": {"ids": {"values": [id_]}}, "_source": {"includes": [HIERARCHY_LABEL_ESFIELD, HIERARCHY_FIELD]}, "size": 1}
        results = self.es.search(index=self.hierarchy_index, body=query)
        if results["hits"]["hits"]:
            label = results["hits"]["hits"][0]["_source"][HIERARCHY_LABEL_ESFIELD][0]
            pos = results["hits"]["hits"][0]["_source"][HIERARCHY_FIELD][0]
        else:
            label = ""
            pos= ""

        return label, pos


    def sort_stw(self, x):
        stw_re = re.compile("^[ABGNPVW][ \.]")
        if stw_re.search(x[RDV_LABEL]):
            try:
                return (0, x[RDV_LABEL], x.get(RDV_COUNT, ""))
            except IndexError:
                return (0, x[RDV_LABEL])
        else:
            # * -1 to get correct order
            try:
                return (1, x.get(RDV_COUNT, 0) * -1, x[RDV_LABEL])
            except IndexError:
                return (1, x[RDV_LABEL])

    def get_stw_autocomplete_data(self):
        """not used at the moment"""
        stw_query = deepcopy(self.body)
        aggs = stw_query["aggs"]["all_facets"]["aggs"]
        del_keys = list(aggs.keys())
        del_keys.remove("stw_ids")
        for del_key in del_keys:
            del aggs[del_key]
        query_blob = json.dumps(stw_query)
        query_blob = query_blob.replace("stw_ids.id", "altLabel")
        query_blob = query_blob.replace("stw_ids.label", "altLabel")
        query_blob = query_blob.replace("[\"stw_ids\"]", "[\"altLabel\",\"prefLabel\"]")
        query_blob = query_blob.replace("stw_ids", "altLabel")
        query_blob = query_blob.replace("atomenergie.*", "Atomenergie")
        stw_query = json.loads(query_blob)
        result_stw = self.es.search(index=self.hierarchy_index, body=stw_query)
        stw_entries = result_stw["aggregations"]["all_facets"]["altLabel"]["filtered_altLabel.keyword"]["buckets"]
        add_labels = {}
        for stw_entry in stw_entries:
            key = stw_entry["key"]
            add_labels.setdefault(key, [])
            s = stw_entry["example"]["hits"]["hits"][0]["_source"]
            add_labels[key].extend(s.get("altLabel", []))
            add_labels[key].extend(s.get("prefLabel", []))
        autocomplete_stw_data = [{"label": b, "value": {"id": b}, "group": {"de": "STW Altlabels"},
                                  "service_label": "altLabels {}: {}".format(k, b)}
                                 for k, values in add_labels.items() for b in values]
        return autocomplete_stw_data

    def build_facet_autocomplete(self):
        if self.debug:
            print("query of build_facet_autocomplete (self.body)")
        result = self.es.search(index=self.index, body=self.body)
        try:
            autocomplete_stw_data = self.get_stw_autocomplete_dat()
        except Exception:
            autocomplete_stw_data = []
            # sortierung der Vorschläge überprüfen
        autocomplete_data = [{"label": b.get("label"), "value": {"id": b.get("value",{}).get("id", b.get("label"))}, "group": key,"service_label": b.get("label")  }
                             for key, facet_data in self.basic_facets.items() for b in self.get_buckets(result, key, facet_data[RQ_FIELD])]
        enriched_autocomplete = []

        # enrich with altlabels
        # todo: verbessern auf altlabels umstellen
        for field in autocomplete_data + autocomplete_stw_data:
            enriched_autocomplete.append(field)
            id_ = field.get("value",{}).get("id", field.get("label"))
            label = field.get("label")
            field.get("value", {})["id"] = field.get("label")
            group = field.get("group",{})
            if id_.startswith("http://zbw.eu/stw"):
                id_ = "{}_{}".format(self.hierarchy_index, id_)
                query = {"query":{"ids":{"values":[id_]}}}
                alt_labels = self.es.search(index=self.hierarchy_index, body=query)["hits"]["hits"][0].get("_source",{}).get("altLabel",[])
                for a_label in alt_labels:
                    #a_label = ", ".join(alt_labels)
                    enriched_autocomplete.append({"group": group, "label": a_label, "value": {"id": a_label}, "service_label": "{} (Synonym: {})".format(a_label, label)})

        return enriched_autocomplete