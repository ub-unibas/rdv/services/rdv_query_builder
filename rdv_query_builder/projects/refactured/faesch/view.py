import json
import urllib.parse
from elasticsearch import Elasticsearch
from rdv_query_builder import RDVView, RDVDatabase, RDVViewHier
from cache_decorator_redis_ubit import NoCacheDecorator
# obligatory: get_title, get_obj_type

IIIF_IMAGE_SERVER = "https://ub-sipi.ub.unibas.ch"
from rdv_data_helpers_ubit import IIIF_PREVIEW_ESFIELD, IIIF_MANIF_ESFIELD

class FaeschView(RDVView, RDVViewHier):
    """class to define snippet and view fields and settings"""
    # lookup file for form definition (same as view definition), needs to be defined, including config folder, e.g.: "{}/ZAS Vocab.xlsx".format(RDVView.config_folder)
    file = None #"{}/fields_def_PortraetView.xlsx".format(RDVView.config_folder)
    # needs to be defined in every subclass, so it is not inherited, stores data from form/view definition, do not overwrite
    fields_def_lookup = {}
    # which fields should be included in the snippet, order defined by order
    snippet_fields = ["author", "normdate", "exakte_lebensdaten", "wiki_extract"]
    # needs to be defined here to next to RDV2ES class
    hierarchy_index = "han_hier2"
    # up to which level links at detail page shall be provided
    hier_level4links = 0
    gd_cache_decorator = NoCacheDecorator
    spreadsheet = "1im8S4qL3I3O42lpoyTm8YUES1jKYRpI5lnEc6zF_KFg"

    def get_inst_kuerzel(self):
        return self.get_obj_type()

    def get_fields_def(self, proj):
        if proj not in self.fields_def_lookup:
            spreadsheet_id = self.spreadsheet
            fields_def = self.get_gdocs(spreadsheet_id=spreadsheet_id, sheet_name=proj, key_field="Feld")
            self.fields_def_lookup[proj] = fields_def
        else:
            fields_def = self.fields_def_lookup[proj]
        return fields_def

    def get_obj_type(self, lang="de") -> str:
        """returns obj type for presentation, pass Es field for "obj_field" """
        obj_type = self.page.get("type")
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        return obj_type

    def get_title(self) -> str:
        """returns title for presentation, pass Es field for "title_field" """
        title = (self.page.get("title") or self.page.get("gnd_name_id"))[0].get("label")

        return title

    def get_viewer(self) -> dict:
        """define which viewer shall be shown in detail page, refers to key in documentViewer Frontend Configuration
        use case: for AV material only use Viewer x, for Images only y, ...
        e.g. {"viewer": ["MiradorSinglePage", "UV"]}"""
        return {"viewer": ["MiradorSinglePage", "UV"]}

    def enrich_page(self):
        self.get_hier_entries(hier_field="hierarchy_filter_han2")

    def get_preview_image(self) -> str:
        """returns url for preview image in snippet view"""
        iiif_images = self.page.get(IIIF_PREVIEW_ESFIELD, [])
        try:
            thumbnails = [(self.page.get("thumbnail",{}) or {}).get("source")]
        except AttributeError:
            thumbnails = [self.page.get("thumbnail","")]
        if iiif_images:
            return iiif_images[0].split("info.json")[0] + "/full/120,/0/default.jpg"
        elif thumbnails:
            return thumbnails[0]
        else:
            return ""

    def build_simple_hierarchy_search_body(self, hier_pos, hier_pos_field="hierarchy_filter_han2"):

        must_filters = [
            {"prefix": {"{}.keyword".format(hier_pos_field): {"value": hier_pos}}}
        ]
        count_body = {"query":{"bool":{"filter": must_filters}}}

        return count_body

    def add_descr_data(self):
        vlm_id = self.page.get("vlm_id")
        add_descr = []

        if vlm_id and "e-manuscripta" in self.page.get("iiif_manifest", [""])[0]:
            e_manu_link = {"value": {"de": [{"label": "Link auf e-manuscripta",
                                             "link": "https://www.e-manuscripta.ch/bau/content/titleinfo/{}"
                                                 .format(self.page.get("vlm_id"))}]},
                           "label": {"de": ["Transkriptionslink"],
                                     "en": ["Transkriptionslink"]}
                           }
            add_descr = [e_manu_link]
        elif vlm_id and "e-rara" in self.page.get("iiif_manifest", [""])[0]:
            e_manu_link = {"value": {"de": [{"label": "Link auf e-rara",
                                             "link": "https://www.e-rara.ch/bau_1/content/titleinfo/{}"
                                                 .format(self.page.get("vlm_id"))}]},
                           "label": {"de": ["Transkriptionslink"],
                                     "en": ["Transkriptionslink"]}
                           }
            add_descr = [e_manu_link]
        return add_descr

    def get_page_values(self, es_field):
        if es_field == "rel_persons":

            return_fields = []
            field_content = self.page.get(es_field)
            for f in field_content:
                return_fields.append("{} (Rolle: {})".format(f.get("label"), f.get("role_label")))
            return return_fields
        else:
            return self.page.get(es_field)

    def get_idlab_select(self, v, es_field):

        if es_field == "hier_path":
            values = []
            for e in self.page["hier_path"]:
                if e.get("hierarchy_filter")[0] in v.get("hierarchy_filter")[0]:
                    values.append({'value': {'id': e.get("id"), "pos": e.get("hierarchy_filter")[0]},
                                   'label': e.get("label"), 'operator': 'AND'})
            selection = {'hierarchy_filter_han2.keyword': {'values': values}}
            return selection
        elif es_field == "adress_empf":
            selection = {'{}.id.keyword'.format(es_field): {'values': [
                {'value': {'id': v.get("id")},
                 'label': v.get("label")}], 'operator': 'OR'}}
            return selection
        else:
            return super().get_idlab_select(v, es_field)


