import json

from rdv_query_builder import RDVView, RDVDatabase, RDVViewHier
from rdv_query_builder.projects.refactured.itb import ITBDatabase, ITBTestDatabase
from rdv_data_helpers_ubit import IIIF_PREVIEW_ESFIELD
# obligatory: get_title, get_obj_type



class ITBView(RDVView):
    """class to define snippet and view fields and settings"""
    # to define whether it is a test class, functionality needs to be defined in subclasses, e.g. adapt path, ...
    test = False
    # lookup file for form definition (same as view definition), needs to be defined, including config folder, e.g.: "{}/ZAS Vocab.xlsx".format(RDVView.config_folder)
    file = "{}/fields_def_ITBView.xlsx".format(RDVView.config_folder)
    # needs to be defined in every subclass, so it is not inherited, stores data from form/view definition, do not overwrite
    fields_def_lookup = {}
    # which fields should be included in the snippet, order defined by order
    snippet_fields = ["Wirkungszeit", "year", "Drucker" ]
    # class to write form data to database, subclass from RDVDatabase
    db_class = ITBDatabase
    # whether versions stored in mongodb for an object shall be accessible while editing
    show_versions = False

    ## if also inherits from RDVViewHier class
    # needs to be defined here to next to RDV2ES class
    hierarchy_index = ""
    # at which level links at detail page shall be provided
    hier_level4links = 0

    spreadsheet = "1aCGVZ8pXp2bRoY0aHVs9NmgSi7vcqu55jcN4NcD1AzY"

    def get_fields_def(self, proj):
        if proj not in self.fields_def_lookup:
            spreadsheet_id = self.spreadsheet
            fields_def = self.get_gdocs(spreadsheet_id=spreadsheet_id, sheet_name=proj, key_field="Feld")
            self.fields_def_lookup[proj] = fields_def
        else:
            fields_def = self.fields_def_lookup[proj]
        return fields_def

    def get_inst_kuerzel(self) -> str:
        """returns object type to control code flow (e.g. based on index)"""
        proj = self.page.get("type")
        if isinstance(proj, list):
            return proj[0] or "not defined"
        else:
            return proj or "not defined"

    def get_obj_type(self, lang="de") -> str:
        """returns obj type for presentation, pass Es field for "obj_field" """
        obj_type = self.page.get("type")
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        return obj_type

    def get_title(self) -> str:
        """returns title for presentation, pass Es field for "title_field" """
        type_ = self.get_obj_type()
        if type_ in ["Literatur", "BBKSKAUT"]:
            title = self.page.get("title", [None, None])
        elif type_ in ["Buchdrucker"]:
            #lebenszeit = self.page.get("Wirkungszeit", "")
            title = self.page.get("Drucker","")

        else:
            title = self.page.get("title", [None, None])
        if isinstance(title, list):
            title = ", ".join([t["label"] for t in title if t])
        return title

    def get_viewer(self) -> dict:
        """define which viewer shall be shown in detail page, refers to key in documentViewer Frontend Configuration
        use case: for AV material only use Viewer x, for Images only y, ...
        e.g. {"viewer": ["MiradorSinglePage", "UV"]}"""
        if not self.page.get("mets_iiifmanifest",""):
            return {"viewer": []}
        else:
            return {}

    def get_rdv_view(self, edit=False):
        view_format = super().get_rdv_view(edit=edit)
        cat_link = self.page.get("cat_link")
        if cat_link:
            descr_aleph_link = {"value": {"de": [{"label": "Primo",
                                                  "link": cat_link}]},
                                "label": {"de": ["Katalog-Link"]}}
            # add empty service_info for manif
            edit_info = self.get_edit_info()
            descr_aleph_link.update(edit_info)
            view_format["desc_metadata"].append(descr_aleph_link)
        return view_format

    def get_preview_image(self) -> str:
        """returns url for preview image in snippet view"""
        iiif_images = self.page.get(IIIF_PREVIEW_ESFIELD, [])
        thumbnails = [self.page.get("wiki_thumbnail","")]
        if iiif_images:
            return iiif_images[0].split("info.json")[0] + "/full/120,/0/default.jpg"
        elif thumbnails:
            return thumbnails
        else:
            return ""

    def get_idlab_select(self, v, es_field):
        selection = {}
        if self.get_obj_type() in ["Buchdrucker"]:
            if es_field == "Drucker":
                selection = {'{}.id.keyword'.format(es_field): {'values': [
                    {'value': {'id': v.get("id")},
                     'label': v.get("label")}], 'operator': 'OR'},
                    '751_a.id.keyword'.format(es_field): {'values': [
                        {'value': {'id': "(DE-588)4004617-5"},
                         'label': "Basel"}], 'operator': 'OR'},
                    '852_b_4.keyword'.format(es_field): {'values': [
                        {'value': {'id': "A100"},
                         'label': "UBH Basel"}], 'operator': 'OR'},
                }
                if self.page.get("Wirkungszeit_von"):
                    selection.update({"year": {"values": [{"value": {"gte": "{}-01-01".format(self.page.get("Wirkungszeit_von")),
                                                                     "lte": "{}-12-31".format(self.page.get("Wirkungszeit_bis"))}}]}})
            elif es_field == "BBKSKAUT_Drucker":
                selection = {'Drucker.id.keyword'.format(es_field): {'values': [
                    {'value': {'id': v.get("id")},
                     'label': v.get("label")}], 'operator': 'OR'},
                    '751_a.id.keyword'.format(es_field): {'values': [
                        {'value': {'id': "(DE-588)4004617-5"},
                         'label': "Basel"}], 'operator': 'OR'}
                    ,
                    'local_code.id.keyword'.format(es_field): {'values': [
                        {'value': {'id': "Referenzdruck UB Basel"},
                         'label': "Referenzdruck UB Basel"}], 'operator': 'OR'},
                }
                if self.page.get("Wirkungszeit_von"):
                    selection.update({"year": {"values": [{"value": {"gte": "{}-01-01".format(self.page.get("Wirkungszeit_von")),
                                                                     "lte": "{}-12-31".format(self.page.get("Wirkungszeit_bis"))}}]}})
        elif self.get_obj_type() in ["Drucke"]:
            if es_field == "Drucker":
                selection = {'{}.id.keyword'.format(es_field): {'values': [
                    {'value': {'id': v.get("id")},
                     'label': v.get("label")}], 'operator': 'OR'},
                }
        if not selection:
            selection = {'{}.id.keyword'.format(es_field): {'values': [
                {'value': {'id': v.get("id")},
                 'label': v.get("label")}], 'operator': 'OR'}}
        return selection

    def get_page_values(self, es_field):
        if es_field == "rel_persons":

            return_fields = []
            field_content = self.page.get(es_field, [])
            for f in field_content:
                return_fields.append("{} (Rolle: {})".format(f.get("label"), f.get("role_label")))
            return return_fields
        else:
            return self.page.get(es_field)

    @classmethod
    def get_groups(cls):
        return {"groups": {"verworfene Felder": {"open": True, "order": 1},
                           "Drucke": {"open": True, "order": 2},
                           "GND Informationen": {"open": False, "order": 3},
                           "Sortier-/Facetteninformationen": {"open": True, "order": 4}}}

    def add_snippet_link(self) -> dict:
        """add link to snippet (instead of detail page), e.g. swisscovery link
        format: {"url": "", "label": ""}
        """
        link = self.page.get("link_field")
        return link

    def add_snippet_fulltext(self) -> str:
        """add expandabel fulltext to snippet (instead of detail page), e.g. abstract
        """
        fulltext = self.page.get("fulltext_field")
        return fulltext

class ITBTestView(ITBView):
    # needs to be defined in every subclass, so it is not inherited, stores data from form/view definition, do not overwrite
    fields_def_lookup = {}
    # class to write form data to database, subclass from RDVDatabase
    db_class = ITBTestDatabase