from rdv_query_builder import RDV2ES, RDVView, RDVForm
from rdv_query_builder.projects.refactured.itb import ITBView, ITBForm, ITBTestView
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import RQ_GTE, RQ_LTE
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import RQ_FIELD, RDV_FIELD, RQ_OPERATOR, RQ_VALUES, RDV_VALUE, RDV_LABEL, RDV_COUNT

from datetime import datetime

class RDV2ES4ITB(RDV2ES):
    """class to define es functionalities (query builder)"""
    # to define whether it is a test class, functionality needs to be defined in subclasses, e.g. adapt path, ...
    test = False
    # define alias for index/indices
    index= "itb2" #itb2
    # what is the name of the view class
    view_class = ITBView
    # what is the name of the form class
    form_class = ITBForm
    # are there iiif compontents? = documentViewerProxyUrl in RDV Frontend
    iiif = False
    # how many objects shall be combined to a dynamic manifest
    # limit size to a certain limit, because it is frontend controlled
    max_ids_dyn_manif = 5
    # what is the url prefix (ini-file name in es config) for dynamic manifests
    dyn_manif= ""
    # the content from which field should be used for es_highlighting
    highlight_field = "*"
    # if highlighting is used for multiple fields if fieldname shall be incluede
    highlight_included_fieldname = True
    # which default sort order shall be applied
    # e.g.: [{"Quelldatum": {"order": "desc", "unmapped_type": "date"}}]
    default_sort = [{"type.keyword": {"order": "asc", "unmapped_type": "keyword", }},
                    {"sort_verleger.keyword": {"order": "asc", "unmapped_type": "keyword"}},
                    {"year": {"order": "asc", "unmapped_type": "date"}}]
    # condition to exclude objects without detail page, to get next/previous detail page,
    # e.g. in bla only authors have detail page, records only links to swisscovery
    search_after_query = {}
    # how many results within a facet shall be calculated by elasticsearch
    # to limit aggregation size to a certain limit, because it is frontend controlled
    max_facet_values = 100

    # not used: concept to load snippets and facets async, for faster loading
    load_aggs_async = False


    def __init__(self, *args, **kwargs):
        """

        :param request_data:
        :param es_client:
        :param index: is ignored, used from class var
        :param debug:
        :param iiif_host:
        :param es_host:
        :param autocomplete:
        """
        super().__init__(*args, **kwargs)

        # I think the only reason why some values are overwritten here is legacy

        # definte hierarchy index: dictionary containing index_name, top_query, and top_id
        # e.g.             "hierarchy_filter_han2": {
        #                 "index_name": "han_hier2",
        #                 "top_query": {"size": 500, "query": {"bool": {"must": {"ids": {
        #             "values": ["han_hier2_9972407385605504","han_hier2_9972407388605504","han_hier2_9972407388105504","han_hier2_9972407386705504"]}}}}},
        #                 #todo: sollte ausgelesen und nicht statisch gesetz werden
        #                 "top_id": "han_hier2_553.14.23.2."
        #             }
        self.hierarchy_index = {}
        # definition how the hierarchy tree shall be sorted
        self.hierarchy_facet_sort = lambda x: str(x.get("label",""))
        # fields used for full text query, if empty list, it searchs within all fields
        self.simple_query_fields = []
        # fields to be returned from Elasticsearch (_source), controlled from frontend if deleted here
        # use it here, if some fields shall not be made public or for performance reasons (e.g. large data/fulltext fields)
        self.source_fields = []
        # for which range date ranges shall be created, defines start level of ranges, steps: century, decade, year, day, ...
        # function get_date_agg_interval can be used to exclude to small aggregation, e.g. if only year values, no month aggregation is needed
        self.default_date_range = {RQ_GTE: "1400-01-01"}#, RQ_LTE: "2050-12-31"}
        # definition for int ranges to calculate range sizes, defines start level of ranges, steps: 1, 10, 100, 1000, ...
        self.default_int_range = {RQ_GTE: 0, RQ_LTE: 1000}

    def get_date_agg_interval(self, values, operator, max_days_aggr=36551, min_date_aggr=365, field=None):
        if field == "oai_datestamp":
            return self._get_date_agg_interval(values, operator, 3650, 31)
        else:
            return self._get_date_agg_interval(values, operator, max_days_aggr, min_date_aggr)

    def build_date_aggs(self, result):
        return self._build_date_aggs(result, strict_default=True, displaymode="Jahrhundert")


# obligatory: index and view_class
class RDV2ES4TESTITB(RDV2ES4ITB):
    # what is the name of the view class
    view_class = ITBTestView