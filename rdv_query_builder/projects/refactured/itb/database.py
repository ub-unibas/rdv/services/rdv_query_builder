from rdv_query_builder import RDVDatabase
from elasticsearch import Elasticsearch
from rdv_marc_ubit import GNDMarcJSONRecord
from rdv_data_helpers_ubit import get_gnd_portraets

import unicodedata


# Felder definieren: Sortierfeld Zeit
# Feld anlegen für GND-Eingabe
# Feld anlegen, für Ansetzungsform, wird im Hintergrund gemergt
# Link Generierung für Neue Einträge
# Erstellen der DAten aus DB
# ID auf GND umstellen

class ITBDatabase(RDVDatabase):
    """class to define database access/storage"""
    # name of the mongo db database
    db_name = "mongo_dbname"
    # needs to be defined in every subclass, so it is not inherited
    initialized = {}

    def __init__(self, **kwargs):
        """
        :param obj:
        :param obj_id:
        :param obj_type:
        :param old_obj:
        :param db_host:
        """
        super().__init__(**kwargs)
        # TODO: anpassen
        self.obj_type = "Buchdrucker"

    def get_obj_type(self, lang="de") -> str:
        """returns obj type for presentation, pass Es field for "obj_field"
         see also RDVView class
         """
        obj_type = self.page.get("obj_field")
        return obj_type

    def get_gnd_data(self, gnd_id, druckername):
        if gnd_id:
            gnd_pers = GNDMarcJSONRecord.get_gndid(gnd_id)
            gnd_data = gnd_pers.extract_gnd_pers_data()
            gnd_data["Drucker"] = gnd_data["gnd_name_id"]
            gnd_data["BBKSKAUT_Drucker"] = gnd_data["gnd_name_id"]
            return gnd_data
        else:
            return {"Drucker": [{"label": druckername, "id": gnd_id or druckername}],
                    "BBKSKAUT_Drucker": [{"label": druckername, "id": gnd_id or druckername}]}

    def get_portraet(self, gnd_id):
        gnd_portraets = get_gnd_portraets(self.es_host)
        return gnd_portraets.get("(DE-588){}".format(gnd_id))

    def get_anzahl_drucke(self, gnd_id):
        query = {"query": {"terms": {
            "Drucker.id.keyword": [
                "http://d-nb.info/gnd/{}".format(gnd_id)
            ]
        }}}
        count = self.es.count(index="itb_bbkskaut", body=query).get("count")
        return count

    def enrich_itb_data(self, gnd_id, printer):
        # TODO: Literatur und Sortierung

        page_id = printer.get("Pages ID")
        if not page_id:
            printer["Pages ID"] = gnd_id

        anzahl_drucke = self.get_anzahl_drucke(gnd_id)
        enrich_item = {"type": "Buchdrucker",
                       "Felder": list(printer.keys()),
                       "Anzahl Drucke": anzahl_drucke,
                       "portraet": self.get_portraet(gnd_id)}
        enrich_item.update(self.get_gnd_data(gnd_id, printer.get("Druckername_Typo3", "")))
        enrich_item.update(self.build_swisscol_links(enrich_item))

        if not anzahl_drucke:
            enrich_item["Felder"].append("Kein BBKSKAUT Eintrag")
        if not "/druckerverleger" in str(enrich_item):
            enrich_item["Felder"].append("ITB nicht als Quelle in GND")

        # Freitext-Feld und Lookup-Feld kombinieren
        # enrich_item["title"] = self.rewrite_lit_link_label(lit_data, printer)

        printer.update(enrich_item)

        return printer

    def update_hook(self, obj_data={}, old_obj_data={}):
        gnd_id = obj_data["Drucker"][0].get("id").split("/")[-1]
        obj_data = self.enrich_itb_data(gnd_id=gnd_id, printer=obj_data)
        return obj_data

    def create_new_record(self):

        object_type = self.get_object_type()
        new_obj_id = self.create_new_id()
        update_obj = self.build_objstruct4es()
        gnd_id = update_obj["Drucker"][0].get("id")
        update_obj = self.update_hook(printer=update_obj, gnd_id=gnd_id)

        # todo: anpassen
        index_alias = "ezas_test"
        update_obj["proj_id"] = object_type

        self.store_version(obj_data=update_obj)
        index = self.get_index_name(index_alias)
        response = self.es.index(index=index, body=update_obj, id=self.create_new_es_id(), refresh=True)
        return response

    def build_swisscol_base_link(self, printer):
        swisscol_lebensdaten = "({})".format(printer["lebensdaten"][0]) if (printer["lebensdaten"] or [""])[0] else ""
        ansetzungsform = " ".join([w for w in [unicodedata.normalize('NFC', printer["Drucker"][0].get("label"))
            , swisscol_lebensdaten] if w])
        ansetzungsform_nfd = " ".join([w for w in [unicodedata.normalize('NFD', printer["Drucker"][0].get("label"))
            , swisscol_lebensdaten] if w])

        printer_query = 'filter%5B%5D=~navAuthor_hierarchical%3A"1%2F{}%2Fprt%2F"&filter%5B%5D=~navAuthor_hierarchical%3A"1%2F{}%2Fpbl%2F"'.format(
            printer.get("ansetzungsform"), ansetzungsform)
        printer_query_nfd = 'filter%5B%5D=~navAuthor_hierarchical%3A"1%2F{}%2Fprt%2F"&filter%5B%5D=~navAuthor_hierarchical%3A"1%2F{}%2Fpbl%2F"'.format(
            printer.get("ansetzungsform_nfd"), ansetzungsform_nfd)

        if printer_query == printer_query_nfd:
            swisscollections_url = "https://swisscollections.ch/Search/Results?filter%5B%5D=%7EnavPublPlace_str_mv%3A%22Basel%22&" + printer_query
        else:
            swisscollections_url = "https://swisscollections.ch/Search/Results?filter%5B%5D=%7EnavPublPlace_str_mv%3A%22Basel%22&" + printer_query + "&" + printer_query_nfd

        wirkungszeit_parts = printer.get("Wirkungszeit","").split("-")
        if all(w.isdigit() for w in wirkungszeit_parts) and len(wirkungszeit_parts) == 2:
            swiss_date = "&daterange%5B%5D=publishDate&publishDatefrom={}&publishDateto={}".format(wirkungszeit_parts[0], wirkungszeit_parts[1])
            swisscollections_url += swiss_date
        return swisscollections_url

    def build_swisscol_ubh_link(self, printer):
        ubh_entries = '&filter%5B%5D=~institution%3A"A100"'
        swisscollections_url = self.build_swisscol_base_link(printer)
        ubh_entries_link = swisscollections_url + ubh_entries
        return ubh_entries_link

    def build_swisscol_bbkskaut_link(self, printer):
        bbkskaut_entries = '&filter%5B%5D=~institution%3A"A116"'
        swisscollections_url = self.build_swisscol_base_link(printer)
        bbkskaut_entries_link = swisscollections_url + bbkskaut_entries
        return bbkskaut_entries_link

    def build_swisscol_links(self, printer):
        return {"swisscollections_UBH": [{"link": self.build_swisscol_ubh_link(printer),
                                          "label": "UBH Drucke in swisscollections"}],
                "swisscollections_bbkskaut": [{"link": self.build_swisscol_bbkskaut_link(printer),
                                               "label": "Liste aller bekannten Drucke in swisscollections"}]}

    def build_typo3_swisscol_link(self, ubh_entries_link, bbkskaut_entries_link):
        ubh_entries_typo3 = ubh_entries_link.replace(" ", "+")
        bbkskaut_entries_typo3 = bbkskaut_entries_link.replace(" ", "+")
        typo3_link = '<p><b>Produktion:</b><br />a) <a data-htmlarea-external="1" title="Opens external link in current window" ' \
                     'class="external-link" target="_self" href=\'{}\'>Liste aller bekannten Drucke</a><br />b) ' \
                     '<a data-htmlarea-external="1" title="Opens external link in current window" class="external-link" ' \
                     'target="_self" href=\'{}\'>Exemplare der UB Basel<br /></a></p>'.format(bbkskaut_entries_typo3, ubh_entries_typo3)
        return typo3_link


class ITBTestDatabase(ITBDatabase):
    # name of the mongo db database
    db_name = "itb_test"
    # needs to be defined in every subclass, so it is not inherited
    initialized = {}