from rdv_query_builder import RDVForm, RDVView

class ITBForm(RDVForm):
    """class to define lookup functions for forms"""
    # lookup file for form definition (same as view definition), needs to be defined, including config folder,
    # e.g.: "{}/ZAS Vocab.xlsx".format(RDVView.config_folder)
    lookup_file = "{}/fields_def_ITBView.xlsx".format(RDVView.config_folder)

    def __init__(self, **kwargs):
        """
        :param host:
        :param es_host:
        :param index:
        """
        super().__init__(**kwargs)
        # field name and lookup function for each field is defined
        self.field_service = {
            "Druckername_Typo3": self.search_person_gnd,
            "Druckername_GND": self.search_person_gnd,
        }

    def search_person_gnd(self, query="", field=""):
        params = {"q": query,
                  "format": "json:preferredName",
                  "filter": "+(type:DifferentiatedPerson) OR (type:Family)",
                  "size": self.size}
        field = "descr_person.id.keyword"
        suggest = {"format": "json:suggest"}
        return self.search_gnd(query, params, field, suggest)

    def default_lookup_func(self, query="", field=""):
        """lookup function if no other function is defined"""
        return self.search_varia(query=query, field=field)

    def search_varia(self, query="", field=""):
        results = self.query_facets(query, field="{}.keyword".format(field))
        return results