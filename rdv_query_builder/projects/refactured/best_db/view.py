import json

from rdv_query_builder import RDVView, RDVDatabase, RDVViewHier

# obligatory: get_title, get_obj_type

IIIF_IMAGE_SERVER = "https://ub-sipi.ub.unibas.ch"

class BestView(RDVView):
    """class to define snippet and view fields and settings"""
    # lookup file for form definition (same as view definition), needs to be defined, including config folder, e.g.: "{}/ZAS Vocab.xlsx".format(RDVView.config_folder)
    file = ""
    # needs to be defined in every subclass, so it is not inherited, stores data from form/view definition, do not overwrite
    fields_def_lookup = {}
    # which fields should be included in the snippet, order defined by order
    snippet_fields = ["Schäden/Besonderheiten"]

    def get_obj_type(self, lang="de") -> str:
        """returns obj type for presentation, pass Es field for "obj_field" """
        obj_type = self.page.get("Objekttyp2") or "Buch"
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        return obj_type

    def get_title(self) -> str:
        """returns title for presentation, pass Es field for "title_field" """
        title = self.page.get("title", [None, None])[0]
        signatur = self.page.get("Signatur",[None])[0]
        title = [signatur, title]
        if isinstance(title, list):
            title = ", ".join([t for t in title if t])
        return title

    def get_preview_image(self) -> str:
        """returns url for preview image in snippet view"""
        iiif_images = self.page.get("mets_iiifpages", [{}])
        iiif_image = iiif_images[0].get("url")
        if iiif_image:
            return iiif_image
        else:
            return ""

    def get_manifest_id(self) -> str:
        """return path to manifest without http-Domain,
        e.g. "/".join(["dizas", zas_id, "manifest"])"""
        manif_id = self.page.get("mets_iiifmanifest","")
        return manif_id

    def get_viewer(self) -> dict:
        """define which viewer shall be shown in detail page, refers to key in documentViewer Frontend Configuration
        use case: for AV material only use Viewer x, for Images only y, ...
        e.g. {"viewer": ["MiradorSinglePage", "UV"]}"""
        if not self.page.get("mets_iiifmanifest",""):
            return {"viewer": []}
        else:
            return {}

    def get_rdv_view(self, edit=False):
        view_format = super().get_rdv_view(edit=edit)
        cat_link = self.page.get("cat_link")
        if cat_link:
            descr_aleph_link = {"value": {"de": [{"label": "Primo",
                                                  "link": cat_link}]},
                                "label": {"de": ["Katalog-Link"]}}
            # add empty service_info for manif
            edit_info = self.get_edit_info()
            descr_aleph_link.update(edit_info)
            view_format["desc_metadata"].append(descr_aleph_link)
        return view_format