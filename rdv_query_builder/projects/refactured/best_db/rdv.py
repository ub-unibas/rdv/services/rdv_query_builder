from rdv_query_builder import RDV2ES, RDVView, RDVForm
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import RQ_GTE, RQ_LTE

from rdv_query_builder.projects.refactured.best_db import BestView

# obligatory: index and view_class

class RDV2ES4Bestand(RDV2ES):
    """class to define es functionalities (query builder)"""
    # define alias for index/indices
    index= "bestandeserhaltung"
    # what is the name of the view class
    view_class = BestView
    # are there iiif compontents? = documentViewerProxyUrl in RDV Frontend
    iiif = False
    # how many objects shall be combined to a dynamic manifest
    # limit size to a certain limit, because it is frontend controlled
    max_ids_dyn_manif = 200
    # what is the url prefix (ini-file name in es config) for dynamic manifests
    dyn_manif= "bestandeserhaltung"
    # the content from which field should be used for es_highlighting
    highlight_field = "*"
    # if highlighting is used for multiple fields if fieldname shall be incluede
    highlight_included_fieldname = False


    def __init__(self, *args, **kwargs):
        """

        :param request_data:
        :param es_client:
        :param index: is ignored, used from class var
        :param debug:
        :param iiif_host:
        :param es_host:
        :param autocomplete:
        """
        super().__init__(*args, **kwargs)

        self.hierarchy_index = "dsv05_hier"
        # fields used for full text query, if empty list, it searchs within all fields
        self.simple_query_fields = []
        # fields to be returned from Elasticsearch (_source), controlled from frontend if deleted here
        # use it here, if some fields shall not be made public or for performance reasons (e.g. large data/fulltext fields)
        self.source_fields = ["Projektname Digi-Projekt", "Schäden/Besonderheiten", "Quelle", "BearbeiterIn", "Datum.Eingang", "Signatur", "mets_iiifmanifest", "mets_iiifpages", "title"]