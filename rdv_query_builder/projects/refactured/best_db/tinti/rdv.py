from rdv_query_builder import RDV2ES, RDVView, RDVForm
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import RQ_GTE, RQ_LTE

from rdv_query_builder.projects.refactured.best_db.tinti.view import TintiView
# obligatory: index and view_class

class RDV2ES4Tinti(RDV2ES):
    """class to define es functionalities (query builder)"""
    # define alias for index/indices
    index= "tinti_test"
    # what is the name of the view class
    view_class = TintiView
    # are there iiif compontents? = documentViewerProxyUrl in RDV Frontend
    iiif = True
    # how many objects shall be combined to a dynamic manifest
    # limit size to a certain limit, because it is frontend controlled
    max_ids_dyn_manif = 200
    # what is the url prefix (ini-file name in es config) for dynamic manifests
    dyn_manif= "tinti"
    # the content from which field should be used for es_highlighting
    highlight_field = "Freitext*"
    # if highlighting is used for multiple fields if fieldname shall be incluede
    highlight_included_fieldname = True


    def __init__(self, *args, **kwargs):
        """

        :param request_data:
        :param es_client:
        :param index: is ignored, used from class var
        :param debug:
        :param iiif_host:
        :param es_host:
        :param autocomplete:
        """
        super().__init__(*args, **kwargs)

        # I think the only reason why some values are overwritten here is legacy
        self.hierarchy_index = ""  # "dsv05_hier"
        self.source_fields = []
        self.simple_query_fields = ["Freitext*"]

    def get_date_agg_interval(self, values, operator, max_days_aggr=367, field=None):
        return self._get_date_agg_interval(values, operator, max_days_aggr)