import json
import urllib.parse
from elasticsearch import Elasticsearch
from rdv_query_builder import RDVView, RDVDatabase, RDVViewHier
from cache_decorator_redis_ubit import NoCacheDecorator
# obligatory: get_title, get_obj_type

IIIF_IMAGE_SERVER = "https://ub-sipi.ub.unibas.ch"
from rdv_data_helpers_ubit import IIIF_PREVIEW_ESFIELD, IIIF_MANIF_ESFIELD

# verweis auf Literatur
# überführung typo3 Felder / Anzeige
# Neuerscheinungscodes
#

# gutes Autocomplete
# Werke auf Seite ergänzen
# alle Links auf externe Quellen
# GND Daten anzeigen, wo keine GND -> eigene Objekttyp?
# Problem Facettensuche

class BlaView(RDVView, RDVViewHier):
    """class to define snippet and view fields and settings"""
    # lookup file for form definition (same as view definition), needs to be defined, including config folder, e.g.: "{}/ZAS Vocab.xlsx".format(RDVView.config_folder)
    file = None #"{}/fields_def_PortraetView.xlsx".format(RDVView.config_folder)
    # needs to be defined in every subclass, so it is not inherited, stores data from form/view definition, do not overwrite
    fields_def_lookup = {}
    # which fields should be included in the snippet, order defined by order
    snippet_fields = ["author", "normdate", "exakte_lebensdaten", "wiki_extract", "typo3_lebensdaten"]
    # needs to be defined here to next to RDV2ES class
    hierarchy_index = ""
    # up to which level links at detail page shall be provided
    hier_level4links = 0
    gd_cache_decorator = NoCacheDecorator
    spreadsheet = "1_hOpdLwgabXUAJJkZyAWkmhTreDQY0vG8544ul_9QR0"

    def get_inst_kuerzel(self):
        return self.get_obj_type()

    def get_fields_def(self, proj):
        if proj not in self.fields_def_lookup:
            spreadsheet_id = self.spreadsheet
            fields_def = self.get_gdocs(spreadsheet_id=spreadsheet_id, sheet_name=proj, key_field="Feld")
            self.fields_def_lookup[proj] = fields_def
        else:
            fields_def = self.fields_def_lookup[proj]
        return fields_def

    def get_obj_type(self, lang="de") -> str:
        """returns obj type for presentation, pass Es field for "obj_field" """
        obj_type = self.page.get("type")
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        return obj_type

    def get_title(self) -> str:
        """returns title for presentation, pass Es field for "title_field" """
        title = (self.page.get("title") or self.page.get("gnd_name_id"))[0].get("label")

        return title

    def get_viewer(self) -> dict:
        """define which viewer shall be shown in detail page, refers to key in documentViewer Frontend Configuration
        use case: for AV material only use Viewer x, for Images only y, ...
        e.g. {"viewer": ["MiradorSinglePage", "UV"]}"""
        return {"viewer": [None]}

    @classmethod
    def get_groups(cls):
        return {"groups": {"leben": {"open": False, "order": 2}, "werk": {"open": True, "order": 1},
                           "name": {"open": True, "order": 1}}}

    def add_snippet_link(self):
        if self.get_obj_type() != "AutorIn":
            bsiz_id = self.page.get("bsiz_id")
            swisscovery_link = [{"url": "https://basel.swisscovery.org/discovery/fulldisplay?docid=alma{}&context=L&vid=41SLSP_UBS:live".format(bsiz_id), "label": "swisscovery"}]
            try:

                id_ = self.page.get("author", [])[0].get("id","").split("/")[-1]
                name = self.page.get("author", [])[0].get("label","").split("/")[-1]
                return swisscovery_link + [{"url": "{}/de/detail/bla_authors_{}".format("https://ub-bla.ub.unibas.ch", id_),"label": "AutorIn im BLA: {}".format(name)}]
            except IndexError:
                return swisscovery_link

    def add_descr_data(self):
        if self.get_obj_type() == "AutorIn":
            edit_info = self.get_edit_info()

            autor = self.page["author"][0].get("label")
            primo_link = "https://slsp-ubs.primo.exlibrisgroup.com/discovery/search?query=" \
                         "creator,contains,{},OR&query=sub,contains,{},AND" \
                         "&tab=41SLSP_DN_CI&search_scope=DN_and_CI&vid=41SLSP_UBS:live&mode=advanced&offset=0".format(autor, autor)
            primo_entry = {"value": {"de": [{"label": "Werke von / Dokumente über: {}".format(autor),
                                             "link": primo_link}]}
                ,"label": {"de": ["swisscovery"]}}
            primo_entry.update(edit_info)
            add_descr = [primo_entry]
        else:
            add_descr = []
        return add_descr


    def get_preview_image(self) -> str:
        """returns url for preview image in snippet view"""
        iiif_images = self.page.get(IIIF_PREVIEW_ESFIELD, [])
        thumbnails = [(self.page.get("thumbnail",{}) or {}).get("source")]
        try:
            thumbnails = [(self.page.get("thumbnail",{}) or {}).get("source")]
        except AttributeError:
            thumbnails = [self.page.get("thumbnail","")]
        if iiif_images:
            return iiif_images[0].split("info.json")[0] + "/full/120,/0/default.jpg"
        elif thumbnails:
            return thumbnails[0]


