from rdv_query_builder import RDV2ES, RDVView, RDVForm
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import RQ_GTE, RQ_LTE

from rdv_query_builder.projects.refactured.bla import BlaView
# obligatory: index and view_class

class RDV2ES4Bla(RDV2ES):
    """class to define es functionalities (query builder)"""
    # define alias for index/indices
    index= "blardv"
    # what is the name of the view class
    view_class = BlaView
    # are there iiif compontents? = documentViewerProxyUrl in RDV Frontend
    iiif = False
    # how many objects shall be combined to a dynamic manifest
    # limit size to a certain limit, because it is frontend controlled
    max_ids_dyn_manif = 200
    # what is the url prefix (ini-file name in es config) for dynamic manifests
    dyn_manif= ""
    # which default sort order shall be applied
    # e.g.: [{"Quelldatum": {"order": "desc", "unmapped_type": "date"}}]
    default_sort = [{"type.keyword": {"order": "asc", "unmapped_type": "keyword"}}, {"signatur.keyword": {"order": "asc", "unmapped_type": "keyword"}}]
    #search_after_query = {"bool": {"must": [{"terms": {"type.keyword": ["AutorIn"]}}]}}

    def __init__(self, *args, **kwargs):
        """

        :param request_data:
        :param es_client:
        :param index: is ignored, used from class var
        :param debug:
        :param iiif_host:
        :param es_host:
        :param autocomplete:
        """
        super().__init__(*args, **kwargs)
        self.hierarchy_index = {}
        # definition how the hierarchy tree shall be sorted
        self.hierarchy_facet_sort = lambda x: x.get("count","") * -1
        # fields used for full text query, if empty list, it searchs within all fields
        self.simple_query_fields = []
        # fields to be returned from Elasticsearch (_source), controlled from frontend if deleted here
        # use it here, if some fields shall not be made public or for performance reasons (e.g. large data/fulltext fields)
        self.source_fields = []
        # for which range date ranges shall be created, defines start level of ranges, steps: century, decade, year, day, ...
        # function get_date_agg_interval can be used to exclude to small aggregation, e.g. if only year values, no month aggregation is needed
        self.default_date_range = {RQ_GTE: "1800-01-01", RQ_LTE: "2050-01-01"}
        # definition for int ranges to calculate range sizes, defines start level of ranges, steps: 1, 10, 100, 1000, ...
        self.default_int_range = {RQ_GTE: 0, RQ_LTE: 10000}

    def factet_query_def(self, facet_data, facet_search, label_field):
        if self.autocomplete:
            return "(({}.*)|(.*[ ,]{}.*))".format(facet_search, facet_search)
        elif label_field == "name.label.keyword":
            return facet_search + ".*"
        else:
            return ".*" + facet_search + ".*" if self.autocomplete else ".*" + facet_search + ".*"

    def facet_query_order(self, label_field):
        if label_field == "name.label.keyword":
            return {"_key": "asc"}
        else:
            return {"_count": "desc"}

    def get_date_agg_interval(self, values, operator, max_days_aggr=36551, min_date_aggr=1, field=None):
        if field == "oai_datestamp":
            return self._get_date_agg_interval(values, operator, 3650, 31)
        else:
            return self._get_date_agg_interval(values, operator, max_days_aggr, min_date_aggr)
