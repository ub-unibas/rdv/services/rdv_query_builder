from rdv_query_builder import RDV2ES, RDVView, RDVForm
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import RQ_GTE, RQ_LTE, HIER_ES_LABEL, RDV_SORT

from rdv_query_builder.projects.refactured.burckhardt import BurckhardtView
# obligatory: index and view_class



class RDV2ES4Burckhardt(RDV2ES):
    """class to define es functionalities (query builder)"""
    # define alias for index/indices
    index= "bild_burckhardt"
    # what is the name of the view class
    view_class = BurckhardtView
    # are there iiif compontents? = documentViewerProxyUrl in RDV Frontend
    iiif = True
    # how many objects shall be combined to a dynamic manifest
    # limit size to a certain limit, because it is frontend controlled
    max_ids_dyn_manif = 50
    # what is the url prefix (ini-file name in es config) for dynamic manifests
    dyn_manif= "burckhardt"
    # which default sort order shall be applied
    # e.g.: [{"Quelldatum": {"order": "desc", "unmapped_type": "date"}}]
    default_sort = [{"sort_sign.keyword": {"order": "asc", "unmapped_type": "text"}}]


    def __init__(self, *args, **kwargs):
        """

        :param request_data:
        :param es_client:
        :param index: is ignored, used from class var
        :param debug:
        :param iiif_host:
        :param es_host:
        :param autocomplete:
        """
        super().__init__(*args, **kwargs)

        # I think the only reason why some values are overwritten here is legacy

        # definte hierarchy index: dictionary containing index_name, top_query, and top_id
        # e.g.             "hierarchy_filter_han2": {
        #                 "index_name": "han_hier2",
        #                 "top_query": {"size": 500, "query": {"bool": {"must": {"ids": {
        #             "values": ["han_hier2_9972407385605504","han_hier2_9972407388605504","han_hier2_9972407388105504","han_hier2_9972407386705504"]}}}}},
        #                 #todo: sollte ausgelesen und nicht statisch gesetz werden
        #                 "top_id": "han_hier2_553.14.23.2."
        #             }
        self.hierarchy_index = {
            "hierarchy_filter_han2": {
                "index_name": "han_hier2",
                "top_query": {"size": 500, "query": {"bool": {"must": {"ids": {
            "values": ["han_hier2_9972407385605504","han_hier2_9972407388605504","han_hier2_9972407388105504","han_hier2_9972407386705504"]}}}}},
                #todo: sollte ausgelesen und nicht statisch gesetz werden
                "top_id": "han_hier2_553.14.23.2."
            }
        }
        # definition how the hierarchy tree shall be sorted
        self.hierarchy_facet_sort = self.sort_sign
        # fields to be returned from Elasticsearch (_source), controlled from frontend if deleted here
        # use it here, if some fields shall not be made public or for performance reasons (e.g. large data/fulltext fields)
        self.source_fields = []


    def get_pref_label_hier(self, es_rec, hier_pos_field):
        label = es_rec["_source"][HIER_ES_LABEL][0] if isinstance(es_rec["_source"][HIER_ES_LABEL], list) \
            else es_rec["_source"][HIER_ES_LABEL]
        position = es_rec["_source"]["position_label"][0] if isinstance(es_rec["_source"]["position_label"], list) and es_rec["_source"]["position_label"] \
            else es_rec["_source"]["position_label"]
        return ": ".join([p for p in [position, label] if p])

    def sort_han(self, x):
        return x.get(RDV_SORT, "")