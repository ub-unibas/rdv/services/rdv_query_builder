import json

from rdv_query_builder import RDVView, RDVDatabase, RDVViewHier
import urllib.parse
from elasticsearch import Elasticsearch
# obligatory: get_title, get_obj_type

from rdv_data_helpers_ubit import IIIF_PREVIEW_ESFIELD, IIIF_MANIF_ESFIELD
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import SNIPPET_SEARCH_AFTER
from rdv_data_helpers_ubit import IIIF_PREVIEW_ESFIELD, IIIF_MANIF_ESFIELD, IZID_ESFIELD
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import SNIPPET_SEARCH_AFTER
from rdv_data_helpers_ubit.projects.swasachdok.swasachdok import PDF_LINKS
from rdv_data_helpers_ubit.projects.swasearch.swasearch import SWASEARCH_ZEITSCHR_TYPE

class SWAView(RDVView, RDVViewHier, object):
    """class to define snippet and view fields and settings"""
    # lookup file for form definition (same as view definition), needs to be defined, including config folder, e.g.: "{}/ZAS Vocab.xlsx".format(RDVView.config_folder)
    file = None # "{}/fields_def_SWAView.xlsx".format(RDVView.config_folder)
    # needs to be defined in every subclass, so it is not inherited, stores data from form/view definition, do not overwrite
    fields_def_lookup = {}
    # which fields should be included in the snippet, order defined by order
    snippet_fields = ["245_c", "264_c", "490_a"]

    ## if also inherits from RDVViewHier class
    # needs to be defined here to next to RDV2ES class
    hierarchy_index = "han_hier2"
    spreadsheet = "1nxN3vN_cnO_hKNVNFkoZVMl2qp2AKPua86rY7ZGJE70"


    def get_fields_def(self, proj):
        if proj not in self.fields_def_lookup:
            spreadsheet_id = self.spreadsheet
            fields_def = self.get_gdocs(spreadsheet_id=spreadsheet_id, sheet_name=proj, key_field="Feld")
            self.fields_def_lookup[proj] = fields_def
        else:
            fields_def = self.fields_def_lookup[proj]
        return fields_def

    def __new__(cls, *args, **kwargs):
        # iiif view calls SWAView without page_id
        if not kwargs["page_id"]:
            return super().__new__(cls)
        try:
            if not kwargs.get("page"):
                page_id = urllib.parse.unquote(kwargs["page_id"])
                query = {
                    "query": {
                        "ids": {
                            "values": [page_id]
                        }
                    }
                }
                es = Elasticsearch(kwargs["es_host"])
                es_results = es.search(index=kwargs["index"], body=query)
                page = es_results["hits"]["hits"][0]["_source"]
            else:
                page = kwargs.get("page", {})
            bsiz_id = page.get("bsiz_id")
            if bsiz_id and page.get(PDF_LINKS):
                from rdv_query_builder.projects.refactured.swa_sachdok.view import SWASachdokView, SWASachdokTestView
                sachdok_class = SWASachdokView if cls.__name__=="SWAView" else SWASachdokTestView
                return sachdok_class(page_id=kwargs["page_id"], page=kwargs.get("page", {}), lang=kwargs.get("lang"),
                                      iiif_host=kwargs["iiif_host"], rdv_host=kwargs.get("rdv_host"),
                           proxy_host=kwargs.get("proxy_host"), es_host=kwargs["es_host"], index=kwargs["index"])
            #elif bsiz_id and "doksDIZS" in page.get("swa_local"):
            elif bsiz_id and page.get("dizas_articles"):
                from rdv_query_builder.projects.refactured.zas.view import ZasView, ZasTestView
                zas_class = ZasView if cls.__name__=="SWAView" else ZasTestView
                zas_class = SWAViewDirect if cls.__name__=="SWAView" else SWAZaslinkView
                return zas_class(page_id=kwargs["page_id"], page=kwargs.get("page", {}), obj_type="dizas_dossiers", lang=kwargs.get("lang"),
                                      iiif_host=kwargs["iiif_host"], rdv_host=kwargs.get("rdv_host"),
                                      proxy_host=kwargs.get("proxy_host"), es_host=kwargs["es_host"], index=kwargs["index"])
            elif page.get(IIIF_MANIF_ESFIELD):
                from rdv_query_builder.projects.refactured.swa_search.view import SWAIIIFView
                return SWAIIIFView(page_id=kwargs["page_id"], page=kwargs.get("page", {}), lang=kwargs.get("lang"),
                            iiif_host=kwargs["iiif_host"], rdv_host=kwargs.get("rdv_host"),
                            proxy_host=kwargs.get("proxy_host"), es_host=kwargs["es_host"], index=kwargs["index"])
        except IndexError:
            pass
        return super().__new__(cls)

    def get_obj_type(self, lang="de") -> str:
        """returns obj type for presentation, pass Es field for "obj_field" """
        obj_type = self.page.get("swa_objecttype") or "Objekt"
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        return obj_type

    def get_title(self) -> str:
        """returns title for presentation, pass Es field for "title_field" """
        title = self.page.get("title", [None, None])
        if isinstance(title, list):
            title = ", ".join([t["label"] for t in title if t])
        return title

    def get_viewer(self) -> dict:
        """define which viewer shall be shown in detail page, refers to key in documentViewer Frontend Configuration
        use case: for AV material only use Viewer x, for Images only y, ...
        e.g. {"viewer": ["MiradorSinglePage", "UV"]}"""
        return {"viewer": ["MiradorSinglePage", "UV"]}

    def get_preview_image(self) -> str:
        """returns url for preview image in snippet view"""
        iiif_images = self.page.get(IIIF_PREVIEW_ESFIELD, [])
        if iiif_images:
            return iiif_images[0].split("info.json")[0]
        elif "HANunikat" in self.page.get("900_a"):
            return "https://ub-sipi.ub.unibas.ch/logos/SWA-archivmaterial.jpg"
        elif set([s.lower() for s in self.page.get("990_f", [])]) & set(["dokss", "doksf", "doksb"]):
            return "https://ub-sipi.ub.unibas.ch/logos/SWA-dok-sammlungen.jpg"
        else:
            return "https://ub-sipi.ub.unibas.ch/logos/SWA-standard.jpg"

    def add_snippet_link(self) -> dict:
        """add link to snippet (instead of detail page), e.g. swisscovery link
        format: {"url": "", "label": ""}
        """

        links = []
        signatur = ", ".join(filter(lambda x: x.startswith("SWA") or x.startswith("UBW"), self.page.get("signatur")))
        alma_id = self.page.get("bsiz_id", [])
        local_code = self.page.get("swa_local", [])
        link = True
        if link:
            if alma_id and "HANunikat" in local_code:
                links.append({
                    "url": "https://swisscollections.ch/Record/{}".format(
                        alma_id),
                    "label": "swisscollections: {}".format(signatur)})
            elif alma_id:
                links.append({
                    "url": "https://slsp-ubs.primo.exlibrisgroup.com/discovery/fulldisplay?docid=alma{}&vid=41SLSP_UBS:live".format(
                        alma_id),
                    "label": "swisscovery Basel: {}".format(
                        signatur) if signatur and not SWASEARCH_ZEITSCHR_TYPE in self.page.get("swa_objecttype") else "swisscovery Basel"})
            for url856 in self.page.get("856uz", []):
                url856["url"] = url856.get("link") or url856.get("url") or None
                if not url856["url"]:
                    continue
                try:
                    del url856["link"]
                except KeyError:
                    pass
                links.append(url856)
        return links

    def get_idlab_select(self, v, es_field):
        if es_field == "hier_path":
            values = []
            for e in self.page["hier_path"]:
                if e.get("hierarchy_filter")[0] in v.get("hierarchy_filter")[0]:
                    values.append({'value': {'id': e.get("id"), "pos": e.get("hierarchy_filter")[0]},
                                   'label': e.get("label"), 'operator': 'AND'})
            selection = {'hierarchy_filter_han2.keyword': {'values': values}}
            return selection

class SWAIIIFView(SWAView, object):

    def __new__(cls, *args, **kwargs):
        return super(SWAView, cls).__new__(cls)

    def add_snippet_link(self) -> dict:
        """add link to snippet (instead of detail page), e.g. swisscovery link
        format: {"url": "", "label": ""}
        """
        links = []
        return links

    def add_descr_data(self):
        add_descr = []
        emanu_links = [l for l in self.page.get("links_856", []) if "e-manuscripta" in l.get("link")]
        erara_links = [l for l in self.page.get("links_856", []) if "e-rara" in l.get("link")]

        if emanu_links:

            e_manu_link = {"value": {"de": emanu_links},
                           "label": {"de": ["e-manuscripta"],
                                     "en": ["e-manuscripta"]}
                           }
            add_descr.append(e_manu_link)
        if erara_links:

            e_rara_link = {"value": {"de": erara_links},
                           "label": {"de": ["e-rara"],
                                     "en": ["e-rara"]}
                           }
            add_descr.append(e_rara_link)

        return add_descr

class SWAViewDirect(SWAView, object):

    def __new__(cls, *args, **kwargs):
        return super(SWAView, cls).__new__(cls)

class SWAZaslinkView(SWAView, object):

    def __new__(cls, *args, **kwargs):
        return super(SWAView, cls).__new__(cls)

    def add_snippet_link(self) -> dict:
        """add link to snippet (instead of detail page), e.g. swisscovery link
        format: {"url": "", "label": ""}
        """
        izid = self.page.get(IZID_ESFIELD)
        links = [{"url": f"https://ub-zas-dev.ub.unibas.ch/popup?popupid={izid}",
                  "label":"Digitalisierte Artikel zugänglich für Hochschulangehörige im ZAS-Portal"}]
        return links

class SWATestView(SWAView, object):

    pass