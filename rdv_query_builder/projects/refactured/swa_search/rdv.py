import re

from rdv_query_builder import RDV2ES, RDVView, RDVForm
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import RQ_GTE, RQ_LTE, HIER_ES_LABEL, RDV_LABEL, RDV_COUNT, SNIPPET_ID

from rdv_query_builder.projects.refactured.swa_search import SWAView, SWATestView
from rdv_data_helpers_ubit.projects.swasearch.swasearch import SWASEARCH_INDEX, SWASEARCH_TEST_INDEX
from rdv_data_helpers_ubit import IZID_ESFIELD, NZID_ESFIELD
# obligatory: index and view_class

class RDV2ES4SWA(RDV2ES):
    """class to define es functionalities (query builder)"""
    # define alias for index/indices
    index= SWASEARCH_INDEX
    # what is the name of the view class
    view_class = SWAView
    # are there iiif compontents? = documentViewerProxyUrl in RDV Frontend
    iiif = False
    # how many objects shall be combined to a dynamic manifest
    # limit size to a certain limit, because it is frontend controlled
    max_ids_dyn_manif = 50
    # what is the url prefix (ini-file name in es config) for dynamic manifests
    dyn_manif= "swasearch"
    # the content from which field should be used for es_highlighting
    highlight_field = "*"
    # if highlighting is used for multiple fields if fieldname shall be incluede
    highlight_included_fieldname = True


    def __init__(self, *args, **kwargs):
        """

        :param request_data:
        :param es_client:
        :param index: is ignored, used from class var
        :param debug:
        :param iiif_host:
        :param es_host:
        :param autocomplete:
        """
        super().__init__(*args, **kwargs)

        # I think the only reason why some values are overwritten here is legacy

        # definte hierarchy index: dictionary containing index_name, top_query, and top_id
        self.hierarchy_index = {
            "hierarchy_filter": {
                "index_name": "stw",
                "top_query": {"size": 500, "query": {"bool": {"must_not": {"exists": {"field": "broader"}},
                                                              "must": {"exists": {"field": "hierarchy_filter"}}}}},
                "top_id": ""
            },
            "hierarchy_filter_han_hotel": {
                "index_name": "han_hier2",
                "top_query": {"size": 500, "query": {"bool": {"must": {"ids": {
                    "values": ["han_hier_9972407514805504", "han_hier_9972407511105504",
                               "han_hier_9972407512305504"]}}}}},
                "top_id": "han_hier_495."
            },
            "hierarchy_filter_han": {
                "index_name": "han_hier2",
                "top_query": {"size": 500, "query": {"bool": {"must_not": {"exists": {"field": "broader"}},
                                                              "must": {"exists": {"field": "hierarchy_filter"}}}}},
                "top_id": ""
            },
            "hierarchy_filter_person": {
                "index_name": "han_hier2",
                "top_query": {"size": 500, "query": {"bool": {"must_not": {"exists": {"field": "broader"}},
                                                              "must": {"terms": {"localcode": ["swapapnl"]}}}}},
                "top_id": ""
            },
            "hierarchy_filter_fuv": {
                "index_name": "han_hier2",
                "top_query": {"size": 500, "query": {"bool": {"must_not": {"exists": {"field": "broader"}},
                                                              "must": {"terms": {
                                                                  "localcode": ["swapafirma", "swapaverband"]}}}}},
                "top_id": ""
            },
            "hierarchy_filter_han": {
                "index_name": "han_hier2",
                "top_query": {"size": 500, "query": {"bool": {"must_not": {"exists": {"field": "broader"}},
                                                              "must": {"terms": {"localcode": ["swapapnl", "swapafirma",
                                                                                               "swapaverband"]}}}}},
                "top_id": ""
            },
        }
        # definition how the hierarchy tree shall be sorted
        self.hierarchy_facet_sort = lambda x: str(x.get("label",""))
        # fields used for full text query, if empty list, it searchs within all fields
        self.simple_query_fields = ["descr_fuv.label", "descr_person.label", "title.label^3", "520_a", "author.label",
                                    "stw_ids.label", "descr_place.label", "690_a", "264_a", "264_b", "264_c", "490_a",
                                    IZID_ESFIELD, NZID_ESFIELD]
        # fields to be returned from Elasticsearch (_source), controlled from frontend if deleted here
        # use it here, if some fields shall not be made public or for performance reasons (e.g. large data/fulltext fields)
        self.source_fields = []
        # for which range date ranges shall be created, defines start level of ranges, steps: century, decade, year, day, ...
        # function get_date_agg_interval can be used to exclude to small aggregation, e.g. if only year values, no month aggregation is needed
        self.default_date_range = {RQ_GTE: "1800-01-01", RQ_LTE: "2050-01-01"}
        # definition for int ranges to calculate range sizes, defines start level of ranges, steps: 1, 10, 100, 1000, ...
        self.default_int_range = {RQ_GTE: 0, RQ_LTE: 1000}

    def get_boost_query(self):
        return [{"terms": {"level.keyword": ["Bestand / Teilbestand"], "boost" : 5.0 }},
         {"terms": {"990_f.keyword": ["doksS", "doksF"], "boost" : 3.0 }}]

    def build_preview_snippet(self, result):
        rdv_objects = [self._es2dict(hit) for hit in result["hits"]["hits"]]
        preview_objects = []
        for page in rdv_objects:
            rdv_view = self.view_class(page_id = page.get(SNIPPET_ID), page=page, lang=self.language,
                                       iiif_host=self.iiif_host, es_host=self.es_host, index=self.index)
            preview_objects.append(rdv_view.build_snippet())
        return preview_objects

    def get_pref_label_hier(self, es_rec, hier_pos_field):
        #HIER_ES_LABEL

        label = es_rec["_source"]["aktenbildner"][0].get("label") if isinstance(es_rec["_source"].get("aktenbildner"), list) \
            else es_rec["_source"].get("aktenbildner")
        if not label:
            label = es_rec["_source"][HIER_ES_LABEL][0] if isinstance(es_rec["_source"][HIER_ES_LABEL], list) \
                else es_rec["_source"][HIER_ES_LABEL]
        if hier_pos_field == "hierarchy_filter_han2":
            position = es_rec["_source"]["position_label"][0] if isinstance(es_rec["_source"]["position_label"], list) \
                                                                 and es_rec["_source"]["position_label"] \
                else es_rec["_source"]["position_label"]
            return " ".join([p for p in [position, label] if p])
        else:
            return label

    def sort_stw(self, x):
        stw_re = re.compile("^[ABGNPVW][ \.]")
        if stw_re.search(x[RDV_LABEL]):
            try:
                return (0, x[RDV_LABEL], x.get(RDV_COUNT, ""))
            except IndexError:
                return (0, x[RDV_LABEL])
        else:
            # * -1 to get correct order
            try:
                return (1, x.get(RDV_COUNT, 0) * -1, x[RDV_LABEL])
            except IndexError:
                return (1, x[RDV_LABEL])

    def get_date_agg_interval(self, values, operator, max_days_aggr=36551, min_date_aggr=365, field=None):
        if field == "oai_datestamp":
            return self._get_date_agg_interval(values, operator, 3650, 31)
        else:
            return self._get_date_agg_interval(values, operator, max_days_aggr, min_date_aggr)

class RDV2ES4TestSWA(RDV2ES4SWA):
    """class to define es functionalities (query builder)"""
    # define alias for index/indices
    index= SWASEARCH_TEST_INDEX
    # what is the name of the view class
    view_class = SWATestView