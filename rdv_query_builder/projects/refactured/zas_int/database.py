from rdv_query_builder import RDVDatabase
from elasticsearch import Elasticsearch

class ZasDatabase(RDVDatabase):
    # needs to be defined in every subclass, so it is not inherited
    initialized = {}

    def __init__(self, request_data, project_config_store, obj={}, obj_type="", old_obj={}):
        self.request_data = request_data
        self.pj_conf = project_config_store
        self.db_name = self.pj_conf.get_value('databases.mongo_dbname')
        self.db_read_name = self.pj_conf.get_value('databases.mongo_dbname_read')
        self.es_host = self.pj_conf.get_value('hosts.index')
        self.db_host = self.pj_conf.get_value('hosts.db')
        self.index = self.pj_conf.get_value('databases.index_name')
        self.obj = obj
        self.old_obj = old_obj

        self.obj_id = self.request_data["object_id"]
        #extracting obj_type reason for duplicating init function from RDVDatabase
        self.obj_type = obj_type or self.obj_id.split("_")[0]
        self.es = Elasticsearch(self.es_host)
        if not self.initialized:
            self.initialize(self.obj_type, self.db_host)


class ZasTestDatabase(ZasDatabase):
    # needs to be defined in every subclass, so it is not inherited
    initialized = {}

class ZasLocalTestDatabase(ZasDatabase):
    # needs to be defined in every subclass, so it is not inherited
    initialized = {}

class ZasLocalDevDatabase(ZasDatabase):
    # needs to be defined in every subclass, so it is not inherited
    initialized = {}