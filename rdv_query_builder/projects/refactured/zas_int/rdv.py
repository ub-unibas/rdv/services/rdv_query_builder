from rdv_query_builder.projects.refactured.zas import RDV2ES4ZAS
from rdv_query_builder.projects.refactured.zas_int import ZASForm, ZasIntView, ZasIntTestView, ZasIntDevView, \
    ZasDatabase, ZasTestDatabase, ZasLocalTestDatabase, ZasLocalDevDatabase

from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import RDV_COUNT

from rdv_data_helpers_ubit import FULLTEXT_ESFIELD, RDV_TITLE, RDV_CATEGORY_ESFIELD, IIIF_IMGS_ESFIELD, RDV_OBJECTTYPE_ESFIELD
from rdv_data_helpers_ubit.projects.swasearch.swasearch import \
    SWASEARCH_DESCR_PLACE, SWASEARCH_DESCR_PERSON, SWASEARCH_DESCR_FUV, SWASEARCH_DESCR_STW, SWASEARCH_DESCR_ALL

from rdv_data_helpers_ubit.projects.hierarchy.hierarchy import HIERARCHY_FIELD, HIERARCHY_LABEL_ESFIELD
from rdv_data_helpers_ubit.projects.zas.zas import ZAS_DATE_ESFIELD, ZAS_SOURCE_ESFIELD, \
    ZAS_MINDATE_ESFIELD, ZAS_MAXDATE_ESFIELD, ZAS_ARTICLES_ESFIELD, ZAS_ID_ESFIELD, ZAS_TITLE_ESFIELD, ZAS_PAGES_ESFIELD

class RDV2ES4ZASINT(RDV2ES4ZAS):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


    def get_date_agg_interval(self, values, operator, max_days_aggr=36551, min_date_aggr=365, field=None):
        if field == "sys_created":
            return self._get_date_agg_interval(values, operator, 365, 31)
        else:
            return self._get_date_agg_interval(values, operator, max_days_aggr, min_date_aggr=1)

    def build_subcat_aggs(self, result, rq_key, key):
        """nur eine Ebene, Vokabular wird in getrenntem Index vorgehalten"""

        #rewritten_results = self.rewrite_bucket_values(result["aggregations"]["all_facets"][rq_key]["filtered_" + key]["buckets"], key)
        rewritten_results = result.get("aggregations",{}).get("all_facets",{}).get(rq_key,{}).get("filtered_" + key,{}).get("buckets",[])
        # es_buckets = {b["key"]: {RDV_LABEL: b["key"], RDV_COUNT: b["doc_count"]} for b in rewritten_results}
        es_buckets = {self._bucket_map(bucket, key, type_="id")["label"]: self._bucket_map(bucket, key) for bucket in rewritten_results}

        # todo: Index-Query um top level values und subvalues zu erhalten
        # todo: Dossier noch richtig indexieren
        top_level_lookup = {RDV_OBJECTTYPE_ESFIELD : ["Zeitungsausschnitt", "Dokumentensammlung"]}
        sub_values_lookup = {"Zeitungsausschnitt": ["retrodigitalisierter Zeitungsausschnitt", "elektronischer Zeitungsausschnitt (ab 2013)", "swissdox"],
                             "Dokumentensammlung": ["retrodigitalisierte Dokumentensammlung", "elektronische Dokumentensammlung (ab 2013)"]}
        top_level_values = top_level_lookup.get(RDV_OBJECTTYPE_ESFIELD)

        es_sub_buckets = self.query_subcats(es_buckets, sub_values_lookup, rq_key, key)
        es_buckets.update(es_sub_buckets)

        # todo: anpassen für subcategories
        buckets = []
        for n, k in enumerate(top_level_values, start=1):
            try:
                if k in es_buckets and es_buckets[k][RDV_COUNT]:
                    top_bucket = self._bucket_subcat_map(es_buckets[k], sub_values_lookup.get(k), es_buckets, pos=1)
                    buckets.append(top_bucket)
            except KeyError:
                pass

        return buckets

