import json
import os

import requests
import socket
import html

from rdv_query_builder import RDVView, RDVForm

# TODO loc Form
from rdv_data_helpers_ubit import RDV_TITLE, FULLTEXT_ESFIELD
from rdv_data_helpers_ubit.projects.swasearch.swasearch import \
    SWASEARCH_DESCR_PLACE, SWASEARCH_DESCR_PERSON, SWASEARCH_DESCR_FUV, SWASEARCH_DESCR_STW, SWASEARCH_DESCR_ALL, \
    SWASEARCH_DESCR_STW_KI, SWASEARCH_DESCR_PLACE_KI

from rdv_data_helpers_ubit.projects.hierarchy.hierarchy import HIERARCHY_FIELD, HIERARCHY_LABEL_ESFIELD
from rdv_data_helpers_ubit.projects.zas.zas import ZAS_DATE_ESFIELD, ZAS_SOURCE_ESFIELD, \
    ZAS_MINDATE_ESFIELD, ZAS_MAXDATE_ESFIELD, ZAS_ARTICLES_ESFIELD, ZAS_ID_ESFIELD, ZAS_TITLE_ESFIELD, \
    ZAS_PAGES_ESFIELD, ZAS_INTCODE_ESFIELD, ZAS_LANG_ESFIELD, ZAS_AUTHOR_ESFIELD, OCR_LANG_ESFIELD

class ZASForm(RDVForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # todo: lookup für titel mit Systemnummer
        self.field_sheetname_lookup = {"Zeitungen": "Zeitung"}
        self.field_service = {
            SWASEARCH_DESCR_FUV: self.search_fuv,
            SWASEARCH_DESCR_PERSON: self.search_person,
            SWASEARCH_DESCR_PLACE: self.search_place,
            SWASEARCH_DESCR_STW: self.search_stw,
            ZAS_SOURCE_ESFIELD: self.search_newspaper,
            ZAS_SOURCE_ESFIELD.lower(): self.search_newspaper,
            ZAS_INTCODE_ESFIELD: self.search_code,
            ZAS_LANG_ESFIELD: self.search_language,
            ZAS_AUTHOR_ESFIELD: self.search_author,
            RDV_TITLE: self.search_cat_collection
        }

    def search_fuv_gnd(self, query="", field=""):
        params = {"q": "type:CorporateBody AND preferredName:{}".format(query),
                  "format": "json:preferredName",
                  "size": self.size}
        field = "{}.id.keyword".format(SWASEARCH_DESCR_FUV)
        suggest = {"format": "json:suggest"}
        return self.search_gnd(query, params, field, suggest)

    def search_fuv(self, query="", field=""):
        results = self.query_facets(query, field="{}.id.keyword".format(SWASEARCH_DESCR_FUV),
                                    service_label_format="{label} ID: {show_id}| Anzahl: {count}")
        return results

    def search_person_gnd(self, query="", field=""):
        params = {"q": query,
                  "format": "json:preferredName",
                  "filter": "+(type:DifferentiatedPerson) OR (type:Family)",
                  "size": self.size}
        field = "{}.id.keyword".format(SWASEARCH_DESCR_PERSON)
        suggest = {"format": "json:suggest"}
        return self.search_gnd(query, params, field, suggest)

    def search_person(self, query="", field=""):
        results = self.query_facets(query, field="{}.id.keyword".format(SWASEARCH_DESCR_PERSON),
                                    service_label_format="{label} ID: {show_id}| Anzahl: {count}")
        return results

    def search_place_gnd(self, query="", field=""):
        results_ki = self.get_ki_proposals(field=SWASEARCH_DESCR_PLACE_KI)
        params = {"q": query,
                  "format": "json:preferredName",
                  "filter": "+(type:PlaceOrGeographicName)",
                  "size": self.size}
        field = "{}.id.keyword".format(SWASEARCH_DESCR_PLACE)
        suggest = {"format": "json:suggest"}
        results = self.search_gnd(query, params, field, suggest)
        return results_ki + results

    def search_place(self, query="", field=""):
        results_ki = self.get_ki_proposals(field=SWASEARCH_DESCR_PLACE_KI)
        # to filter old not stw entries, those have no id -> id == label
        filter_not_place = lambda x: x.get("label") != x.get("value",{}).get("id")
        results = list(filter(filter_not_place, self.query_facets(query,
                                                                field="{}.id.keyword".format(SWASEARCH_DESCR_PLACE),
                                                                remove_content_fields=[SWASEARCH_DESCR_PLACE_KI])))
        return results_ki + results

    def search_cat_collection(self, query="", field=""):
        results = self.query_facets(query, field="title.id.keyword")
        return results

    def search_newspaper(self, query="", field=""):
        return self.search_lookup_value(field="{}.keyword".format(ZAS_SOURCE_ESFIELD), sheet="Zeitungen", key="Zeitung", query=query)

    def search_code(self, query="", field=""):
        return self.search_lookup_value(field="{}.keyword".format(ZAS_INTCODE_ESFIELD), sheet="interne Farbcodes", key="interne Farbcodes", query=query)

    def search_language(self, query="", field=""):
        return self.search_lookup_value(field="{}.keyword".format(ZAS_LANG_ESFIELD), sheet="Sprache", key="Sprache", query=query)

    def get_ki_proposals_api(self, field):
        # todo: deactived to avoid dependencies
        try:
            fulltext = self.get_obj_entries(field=FULLTEXT_ESFIELD)
            ocr_lang = self.get_obj_entries(field=OCR_LANG_ESFIELD)
            if fulltext:
                fulltext = fulltext[0]
                lang = "FRENCH" if ocr_lang in ["OldFrench"] else "GERMAN"
                xml_temp = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n<RecordList>\r\n  <Record>\r\n  <Field name=\"Sprache\">{}</Field>" \
                           "\r\n       <Field name=\"Volltext\">{}</Field>\r\n </Record>\r\n</RecordList>\r\n".format(
                    lang, html.escape(fulltext))
                ki_values = self.get_deeptext_results(xml_temp).get(field, [])
        except Exception as e:
            ki_values = [{"value": {"id": "error"}, "service_label": "KI-Error {}, bitte UB-IT (mre) informieren".format(e), "group": {"de": "ZAS KI"}}]
            import traceback
            traceback.print_exc()
            return ki_values
        for ki_prop in ki_values:
            ki_prop.setdefault("value",{})["id"] = ki_prop["id"]
            del ki_prop["id"]
            ki_prop["service_label"] = "KI Vorschlag: {}".format(ki_prop["label"])
            ki_prop["group"] = {"de": "ZAS KI"}
        return ki_values

    def get_deeptext_results(self, doc):
        machine_values = {}
        threshold_level = 1
        from iiif_index_utils.enrichment.stw2es import DataThesaurusZas
        zhaw_ki_api = "http://172.17.0.2:9999/api/categories/" if os.environ.get('RDV_DEBUG') else "http://172.17.0.2:9999/api/categories/"
        thesaurus_zas = DataThesaurusZas(None)
        stw_concordance = thesaurus_zas.get_stw_concordance()
        payload = json.dumps({"abbyyOutput": doc, "fullpath": "true"})
        response = requests.post(zhaw_ki_api, data=payload).json()


        from iiif_index import DataZas
        for descriptor, accurancy in response.items():
            # level damit nicht gleichtlautende Werte von höheren Ebenen berücksichtigt werden
            level_max = max([item[0][1] for item in accurancy[2:]]) + 1
            new_t = stw_concordance.get(descriptor)
            if accurancy[0] > threshold_level and level_max == 4 and new_t:
                if new_t.get("successors"):
                    machine_values.setdefault("ki_" + DataZas.fieldname_stw_ids, []).extend(new_t["successors"])
                if new_t.get("places"):
                    machine_values.setdefault("ki_" + DataZas.fieldname_descr_place, []).extend(new_t["places"])
        return machine_values


    def get_ki_proposals(self, field):
        field_entries = self.get_obj_entries(field=field)
        for ki_prop in field_entries:
            ki_prop.setdefault("value",{})["id"] = ki_prop["id"]
            del ki_prop["id"]
            ki_prop["service_label"] = "KI Vorschlag: {}".format(ki_prop["label"])
            ki_prop["group"] = {"de": "ZAS KI"}
        return field_entries

    def search_stw(self, query="", field=""):
        # todo später erweitern um echte STW-IDs
        # todo ergänzen um KI
        #results_ki = self.query_facets("", field="ki_stw_ids.id.keyword")
        #results_ki = self.get_ki_proposals(field="ki_stw_ids")
        results_ki = self.get_ki_proposals(field=SWASEARCH_DESCR_STW_KI)
        # to filter old not stw entries, those have no id -> id == label
        filter_not_stw = lambda x: x.get("label") != x.get("value",{}).get("id")
        results = list(filter(filter_not_stw, self.query_facets(query, field="{}.id.keyword".format(SWASEARCH_DESCR_STW), remove_content_fields=[SWASEARCH_DESCR_STW_KI])))
        return results_ki + results

    def search_author(self, query="", field=""):
        results = self.query_facets(query, field="{}.keyword".format(ZAS_AUTHOR_ESFIELD))
        return results


