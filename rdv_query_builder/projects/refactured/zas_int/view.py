from rdv_query_builder import RDVView
from rdv_query_builder.projects.refactured.zas import ZasView
from rdv_query_builder.projects.refactured.zas_int import ZasDatabase, ZasLocalTestDatabase, ZasLocalDevDatabase

from rdv_data_helpers_ubit.projects.swasearch.swasearch import \
    SWASEARCH_DESCR_PLACE, SWASEARCH_DESCR_PERSON, SWASEARCH_DESCR_FUV, SWASEARCH_DESCR_STW, SWASEARCH_DESCR_ALL

from rdv_data_helpers_ubit.projects.zas.zas import ZAS_DATE_ESFIELD, ZAS_SOURCE_ESFIELD, \
    ZAS_MINDATE_ESFIELD, ZAS_MAXDATE_ESFIELD, ZAS_ARTICLES_ESFIELD, ZAS_DOC_TYPES, ZAS_ID_ESFIELD, ZAS_TITLE_ESFIELD

class ZasIntView(ZasView):
    # if only defined in super class not "safe" against ZasView-Def
    fields_def_lookup = {}


class ZasIntTestView(ZasIntView):
    # if only defined in super class not "safe" against ZasView-Def
    fields_def_lookup = {}

    def get_manifest_id(self):
        # exclude too big manifests
        if self.zas_type not in ZAS_DOC_TYPES and self.page.get(ZAS_ARTICLES_ESFIELD, 0) > 300:
            return ""
        elif self.page.get(ZAS_ID_ESFIELD):
            test = "atestset"
            zas_id = self.page.get(ZAS_ID_ESFIELD)
            manif_id = "/".join([test + self.zas_type, zas_id, "manifest"])
            return manif_id
        else:
            return None

class ZasIntDevView(ZasIntView):
    # if only defined in super class not "safe" against ZasView-Def
    fields_def_lookup = {}

    def get_manifest_id(self):
        # exclude too big manifests
        if self.zas_type not in ZAS_DOC_TYPES and self.page.get(ZAS_ARTICLES_ESFIELD, 0) > 300:
            return ""
        elif self.page.get(ZAS_ID_ESFIELD):
            test = "dev"
            zas_id = self.page.get(ZAS_ID_ESFIELD)
            manif_id = "/".join([test + self.zas_type, zas_id, "manifest"])
            return manif_id
        else:
            return None
