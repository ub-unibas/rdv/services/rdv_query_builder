import json
from rdv_query_builder.projects.refactured.zas import ZasView
from rdv_query_builder.projects.refactured.zas_int import ZASForm


if __name__ == "__main__":
    ZasView.file = "../" + ZasView.file
    zas = ZasView.get_page(page_id="dizas_681730", lang="de", index="zas", es_host="localhost:9209",
                           iiif_host="127.0.0.1:5001", rdv_host="127.0.0.1:5000", proxy_host="http://127.0.0.1:5000/")
    import pprint
    print(json.dumps(zas.return_rdv_view(edit=True)))
    #print(json.dumps(zas.build_snippet()))

if __name__ == "__main__":
    if 1:
        import pprint
        d = ZASForm(host="http://127.0.0.1:5000/")
        d.search_fuv_gnd("Suchard")
        d.search_author("Gm")
        #d.search_stw("ba")
        #d.search_person("blo")
        pprint.pprint(d.search_person_gnd("kreisky"))
        pprint.pprint(d.contr_vocab_lookup("Zeitungen", "Zeitung"))
        pprint.pprint(d.search_newspaper("Vater"))
        pprint.pprint(d.search_code("bl"))
        pprint.pprint(d.get_gnd_id("120607190"))
        pprint.pprint(d(field="descr_fuv", query="Suchard"))
