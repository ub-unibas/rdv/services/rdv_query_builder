from rdv_query_builder.projects.refactured.zas import RDV2ES4ZAS, ZasView
from rdv_query_builder.projects.refactured.zas_int import RDV2ES4ZASINT, ZASForm, ZasDatabase, ZasTestDatabase, \
    ZasLocalTestDatabase, ZasIntDevView, ZasLocalDevDatabase


# refactured projects
from rdv_query_builder.projects.refactured.autographen import AutographenView, RDV2ES4Autographen
from rdv_query_builder.projects.refactured.bernoulli import RDV2ES4Bernoulli, BernoulliView
from rdv_query_builder.projects.refactured.best_db import RDV2ES4Bestand, BestView
from rdv_query_builder.projects.refactured.best_db.tinti import RDV2ES4Tinti, TintiView
from rdv_query_builder.projects.refactured.bla import RDV2ES4Bla, BlaView
from rdv_query_builder.projects.refactured.burckhardt import RDV2ES4Burckhardt, BurckhardtView
from rdv_query_builder.projects.refactured.digispace import RDV2ES4Digispace, DigispaceView
from rdv_query_builder.projects.refactured.fortbild import RDV2ES4Fortbild, FortbildView
from rdv_query_builder.projects.refactured.faesch import RDV2ES4Faesch, FaeschView
from rdv_query_builder.projects.refactured.googlebooks import RDV2ES4GBooks, GBooksView
from rdv_query_builder.projects.refactured.itb import RDV2ES4ITB, ITBView, ITBDatabase, RDV2ES4TESTITB, ITBTestView
from rdv_query_builder.projects.refactured.persdb import RDV2ES4PersDB, PersDBView
from rdv_query_builder.projects.refactured.mf226 import RDV2ES4MF226, MF226View
from rdv_query_builder.projects.refactured.nl_351 import RDV2ES4NL351, NL351View
from rdv_query_builder.projects.refactured.portraets import RDV2ES4Port, PortraetView
from rdv_query_builder.projects.refactured.swa_pdf import RDV2ES4SWAPDF, SWAPDFView, RDV2ES4TESTSWAPDF, RDV2ES4LOCALSWAPDF, \
    SWAPDFView, SWAPDFTestView, SWAPDFLocalView
from rdv_query_builder.projects.refactured.swa_sachdok import RDV2ES4SWASachdok, SWASachdokView, SWASachdokTestView
from rdv_query_builder.projects.refactured.swa_search import RDV2ES4SWA, SWAView, SWATestView, RDV2ES4TestSWA
from rdv_query_builder.projects.refactured.vise import RDV2ES4Vise, ViseView
from rdv_query_builder.projects.refactured.newspapers import RDV2ES4Newspaper, NewspapersView

import os

IIIF_LOC_HOST = "127.0.0.1:5001"
IIIF_TEST_HOST = "ub-test-iiifpresentation.ub.unibas.ch"
IIIF_K8S_TEST_HOST = "test.es-iiif-service.ub-digitale-dienste.k8s-001.unibas.ch"
IIIF_K8S_DEV_HOST = "dev.es-iiif-service.ub-digitale-dienste.k8s-001.unibas.ch"
IIIF_HOST = "ub-iiifpresentation.ub.unibas.ch"

ES_SB_LOC_HOST = "localhost:9209"
ES_SB_HOST = "sb-ues5.swissbib.unibas.ch:8080"

ES_AP_HOST = "ub-afrikaportal.ub.unibas.ch:9200"
ES_AP_LOC_HOST = "localhost:9208"

MONGO_HOST = os.environ.get('MONGO_HOST')
MONGO_TEST_HOST = os.environ.get('MONGO_TEST_HOST')
MONGO_DEV_HOST = os.environ.get('MONGO_DEV_HOST')
MONGO_SWAPDF_TEST_HOST = os.environ.get('MONGO_SWAPDF_TEST_HOST')
MONGO_SWAPDF_HOST = os.environ.get('MONGO_SWAPDF_HOST')
MONGO_ITB_LOC_HOST = os.environ.get('MONGO_ITB_LOC_HOST')
MONGO_ITB_TEST_HOST = os.environ.get('MONGO_ITB_TEST_HOST')
MONGO_LOC_HOST = "localhost:27017"


RDV_DEFINITIONS = {

    # Portraets
    "portraets-loc": {"es_host": ES_SB_LOC_HOST,
                      "rdv_class": RDV2ES4Port,
                      "iiif_host": IIIF_LOC_HOST},
    "portraets-test": {"es_host": ES_SB_HOST,
                       "rdv_class": RDV2ES4Port,
                       "iiif_host": IIIF_TEST_HOST},
    "portraets": {"es_host": ES_SB_HOST,
                  "rdv_class": RDV2ES4Port,
                  "iiif_host": IIIF_HOST},
    "portraets-dev": {"es_host": ES_SB_HOST,
                      "rdv_class": RDV2ES4Port,
                      "iiif_host": IIIF_TEST_HOST},

    # NL 351
    "nl_351-loc": {"es_host": ES_SB_LOC_HOST,
                   "rdv_class": RDV2ES4NL351,
                   "iiif_host": IIIF_LOC_HOST},
    "nl_351-test": {"es_host": ES_SB_HOST,
                    "rdv_class": RDV2ES4NL351,
                    "iiif_host": IIIF_TEST_HOST},
    "nl_351-prod": {"es_host": ES_SB_HOST,
                    "rdv_class": RDV2ES4NL351,
                    "iiif_host": IIIF_HOST},

    # Autographen Sammlung Geigy Hagenbach
    "autographen-loc": {"es_host": ES_SB_LOC_HOST,
                        "rdv_class": RDV2ES4Autographen,
                        "iiif_host": IIIF_LOC_HOST},
    "autographen-dev": {"es_host": ES_SB_HOST,
                        "rdv_class": RDV2ES4Autographen,
                        "iiif_host": IIIF_TEST_HOST},
    "autographen-test": {"es_host": ES_SB_HOST,
                         "rdv_class": RDV2ES4Autographen,
                         "iiif_host": IIIF_TEST_HOST},
    "autographen-prod": {"es_host": ES_SB_HOST,
                         "rdv_class": RDV2ES4Autographen,
                         "iiif_host": IIIF_HOST},

    # Museum Faesch
    "faesch-loc": {"es_host": ES_SB_LOC_HOST,
                   "rdv_class": RDV2ES4Faesch,
                   "iiif_host": IIIF_LOC_HOST},
    "faesch-dev": {"es_host": ES_SB_HOST,
                   "rdv_class": RDV2ES4Faesch,
                   "iiif_host": IIIF_TEST_HOST},
    "faesch-test": {"es_host": ES_SB_HOST,
                    "rdv_class": RDV2ES4Faesch,
                    "iiif_host": IIIF_TEST_HOST},
    "faesch-prod": {"es_host": ES_SB_HOST,
                    "rdv_class": RDV2ES4Faesch,
                    "iiif_host": IIIF_HOST},

    # Google Books
    "gbooks-loc": {"es_host": ES_SB_LOC_HOST,
                   "rdv_class": RDV2ES4GBooks,
                   "iiif_host": IIIF_LOC_HOST},
    "gbooks-dev": {"es_host": ES_SB_HOST,
                   "rdv_class": RDV2ES4GBooks,
                   "iiif_host": IIIF_TEST_HOST},
    "gbooks-test": {"es_host": ES_SB_HOST,
                    "rdv_class": RDV2ES4GBooks,
                    "iiif_host": IIIF_TEST_HOST},
    "gbooks-prod": {"es_host": ES_SB_HOST,
                    "rdv_class": RDV2ES4GBooks,
                    "iiif_host": IIIF_HOST},

    # Vise
    "vise-test": {"es_host": ES_SB_HOST,
                  "rdv_class": RDV2ES4Vise,
                  "iiif_host": IIIF_HOST},
    "vise-loc": {"es_host": ES_SB_LOC_HOST,
                 "rdv_class": RDV2ES4Vise,
                 "iiif_host": IIIF_LOC_HOST},

    # Newspaper
    "newspapers-loc": {"es_host": ES_SB_LOC_HOST,
                       "rdv_class": RDV2ES4Newspaper,
                       "iiif_host": IIIF_LOC_HOST},
    "newspapers-dev": {"es_host": ES_SB_HOST,
                       "rdv_class": RDV2ES4Newspaper,
                       "iiif_host": IIIF_TEST_HOST},
    "newspapers-test": {"es_host": ES_SB_HOST,
                        "rdv_class": RDV2ES4Newspaper,
                        "iiif_host": IIIF_TEST_HOST},


    # bestandsdb
    "bestandsdb-loc": {"es_host": ES_SB_LOC_HOST,
                       "rdv_class": RDV2ES4Bestand,
                       "iiif_host": IIIF_LOC_HOST},
    "bestandsdb-test": {"es_host": ES_SB_HOST,
                        "rdv_class": RDV2ES4Bestand,
                        "iiif_host": IIIF_HOST},
    "bestandsdb-dev": {"es_host": ES_SB_HOST,
                       "rdv_class": RDV2ES4Bestand,
                       "iiif_host": IIIF_TEST_HOST},

    # zas-int
    "zas_int-loc": {
        "es_host": ES_SB_LOC_HOST,
        "iiif_host": IIIF_LOC_HOST,
        "db_host": MONGO_DEV_HOST},
    "zas_int-dev": {
        "es_host": ES_SB_HOST,
        "iiif_host": IIIF_TEST_HOST,
        "db_host": MONGO_DEV_HOST},
    "zas_int": {
        "es_host": ES_SB_HOST,
        "iiif_host": IIIF_HOST,
        "db_host": MONGO_HOST},
    "zas_int-test": {
        "es_host": ES_SB_HOST,
        "iiif_host": IIIF_TEST_HOST,
        "db_host": MONGO_DEV_HOST},

    # bla
    "bla-loc": {"es_host": ES_SB_LOC_HOST,
                "rdv_class": RDV2ES4Bla,
                "iiif_host": IIIF_LOC_HOST},
    "bla-test": {"es_host": ES_SB_HOST,
                 "rdv_class": RDV2ES4Bla,
                 "iiif_host": IIIF_HOST},
    "bla-dev": {"es_host": ES_SB_HOST,
                "rdv_class": RDV2ES4Bla,
                "iiif_host": IIIF_HOST},


    # itb
    "itb": {"es_host": ES_SB_HOST,
            "rdv_class": RDV2ES4ITB,
            "iiif_host": IIIF_HOST,
            "db_host": MONGO_ITB_TEST_HOST},
    "itb-test": {"es_host": ES_SB_HOST,
                 "rdv_class": RDV2ES4TESTITB,
                 "iiif_host": IIIF_TEST_HOST,
                 "db_host": MONGO_ITB_TEST_HOST},
    "itb-dev": {"es_host": ES_SB_HOST,
                "rdv_class": RDV2ES4TESTITB,
                "iiif_host": IIIF_TEST_HOST,
                "db_host": MONGO_ITB_TEST_HOST},

    "itb-loc": {"es_host": ES_SB_LOC_HOST,
                "rdv_class": RDV2ES4TESTITB,
                "iiif_host": IIIF_LOC_HOST,
                "db_host": MONGO_ITB_LOC_HOST},



    # digibas
    "digibas-test": {"es_host": ES_SB_HOST,
                     "rdv_class": RDV2ES4Digispace,
                     "iiif_host": IIIF_HOST},
    "digibas-loc": {"es_host": ES_SB_LOC_HOST,
                    "rdv_class": RDV2ES4Digispace,
                    "iiif_host": IIIF_LOC_HOST},



    # swa
    "swa": {"es_host": ES_SB_HOST,
            "rdv_class": RDV2ES4SWA,
            "iiif_host": IIIF_HOST},
    "swa-test": {"es_host": ES_SB_HOST,
                 "rdv_class": RDV2ES4TestSWA,
                 "iiif_host": IIIF_TEST_HOST},
    "swa-dev": {"es_host": ES_SB_HOST,
                "rdv_class": RDV2ES4TestSWA,
                "iiif_host": IIIF_TEST_HOST},
    "swa-loc": {"es_host": ES_SB_LOC_HOST,
                "rdv_class": RDV2ES4TestSWA,
                "iiif_host": IIIF_LOC_HOST},

    # tinti
    "tinti-test": {"es_host": ES_SB_HOST,
                   "rdv_class": RDV2ES4Tinti,
                   "iiif_host": IIIF_HOST},
    "tinti-loc": {"es_host": ES_SB_LOC_HOST,
                  "rdv_class": RDV2ES4Tinti,
                  "iiif_host": IIIF_LOC_HOST},

    # mf226
    "mf226-test": {"es_host": ES_SB_HOST,
                   "rdv_class": RDV2ES4MF226,
                   "iiif_host": IIIF_TEST_HOST},
    "mf226-loc": {"es_host": ES_SB_LOC_HOST,
                  "rdv_class": RDV2ES4MF226,
                  "iiif_host": IIIF_LOC_HOST},

    # swa_sachdok
    "swa_pdfs-test": {"es_host": ES_SB_HOST,
                      "rdv_class": RDV2ES4TESTSWAPDF,
                      "iiif_host": IIIF_TEST_HOST,
                      "db_host": MONGO_SWAPDF_TEST_HOST},
    "swa_pdfs-dev": {"es_host": ES_SB_HOST,
                     "rdv_class": RDV2ES4TESTSWAPDF,
                     "iiif_host": IIIF_TEST_HOST,
                     "db_host": MONGO_SWAPDF_TEST_HOST},
    "swa_pdfs-loc": {"es_host": ES_SB_LOC_HOST,
                     "rdv_class": RDV2ES4LOCALSWAPDF,
                     "iiif_host": IIIF_LOC_HOST},
    "swa_pdfs": {"es_host": ES_SB_HOST,
                 "rdv_class": RDV2ES4SWAPDF,
                 "iiif_host": IIIF_HOST,
                 "db_host": MONGO_SWAPDF_HOST},

    # swa_sachdok
    "swa_sachdok-test": {"es_host": ES_SB_HOST,
                         "rdv_class": RDV2ES4SWASachdok,
                         "iiif_host": IIIF_TEST_HOST},
    "swa_sachdok-dev": {"es_host": ES_SB_HOST,
                        "rdv_class": RDV2ES4SWASachdok,
                        "iiif_host": IIIF_TEST_HOST},
    "swa_sachdok-loc": {"es_host": ES_SB_LOC_HOST,
                        "rdv_class": RDV2ES4SWASachdok,
                        "iiif_host": IIIF_LOC_HOST},
    "swa_sachdok": {"es_host": ES_SB_HOST,
                    "rdv_class": RDV2ES4SWASachdok,
                    "iiif_host": IIIF_HOST},


    # burckhardt
    "burckhardt-test": {"es_host": ES_SB_HOST,
                        "rdv_class": RDV2ES4Burckhardt,
                        "iiif_host": IIIF_TEST_HOST},
    "burckhardt-loc": {"es_host": ES_SB_LOC_HOST,
                       "rdv_class": RDV2ES4Burckhardt,
                       "iiif_host": IIIF_LOC_HOST},

    # bernoulli
    "bernoulli-test": {"es_host": ES_SB_HOST,
                       "rdv_class": RDV2ES4Bernoulli,
                       "iiif_host": IIIF_TEST_HOST},
    "bernoulli-loc": {"es_host": ES_SB_LOC_HOST,
                      "rdv_class": RDV2ES4Bernoulli,
                      "iiif_host": IIIF_LOC_HOST},

    # fortbild
    "fortbild-test": {"es_host": ES_SB_HOST,
                      "rdv_class": RDV2ES4Fortbild,
                      "iiif_host": IIIF_TEST_HOST},
    "fortbild-loc": {"es_host": ES_SB_LOC_HOST,
                     "rdv_class": RDV2ES4Fortbild,
                     "iiif_host": IIIF_LOC_HOST},

    # fortbild
    "persdb-test": {"es_host": ES_SB_HOST,
                    "rdv_class": RDV2ES4PersDB,
                    "iiif_host": IIIF_TEST_HOST},
    "persdb-loc": {"es_host": ES_SB_LOC_HOST,
                   "rdv_class": RDV2ES4PersDB,
                   "iiif_host": IIIF_LOC_HOST},

    # ????
    "localhost": {"es_host": ES_SB_LOC_HOST,
                  "rdv_class": RDV2ES4ZAS,
                  "iiif_host": IIIF_LOC_HOST},
}
