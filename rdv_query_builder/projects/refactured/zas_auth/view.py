from rdv_query_builder.projects.refactured.zas import ZasView

from rdv_data_helpers_ubit.projects.zas.zas import ZAS_DOC_TYPES, ZAS_ARTICLES_ESFIELD, ZAS_ID_ESFIELD
from rdv_data_helpers_ubit import IIIF_IMGS_ESFIELD

class ZasAuthView(ZasView):

    def get_manifest_id(self):
        # exclude too big manifests
        if self.zas_type not in ZAS_DOC_TYPES and self.page.get(ZAS_ARTICLES_ESFIELD, 0) > 300:
            return ""
        else:
            test = "" if not self.test else "test"
            zas_id = self.page.get(ZAS_ID_ESFIELD)
            manif_id = "/".join(["auth" + self.zas_type, zas_id, "manifest"])
            return manif_id

    def get_preview_image(self) -> str:
        """returns url for preview image in snippet view"""
        id_jp2 = (self.page.get(IIIF_IMGS_ESFIELD) or [""])[0]
        thumbnails = [self.page.get("wiki_thumbnail","")]
        if id_jp2:
            return "{}/{}/{}".format("https://ub-sipi.ub.unibas.ch", self.zas_type, id_jp2) + "/full/120,/0/default.jpg"
        elif thumbnails:
            return thumbnails
        else:
            return ""