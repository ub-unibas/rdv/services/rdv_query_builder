from rdv_query_builder.projects.refactured.zas_auth import ZasAuthView
from rdv_query_builder.projects.refactured.zas import RDV2ES4ZAS

from rdv_data_helpers_ubit.projects.zas.zas import ZAS_DOC_TYPES, ZAS_ARTICLES_ESFIELD, ZAS_ID_ESFIELD

class RDV2ES4ZASPWD(RDV2ES4ZAS):
    def get_manifest_id(self):
        # exclude too big manifests
        if self.zas_type not in ZAS_DOC_TYPES and self.page.get(ZAS_ARTICLES_ESFIELD, 0) > 300:
            return ""
        else:
            test = "" if not self.test else "test"
            zas_id = self.page.get(ZAS_ID_ESFIELD)
            manif_id = "/".join([test + self.zas_type + "_pwd", zas_id, "manifest"])
            return manif_id