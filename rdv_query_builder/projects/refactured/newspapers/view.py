import json

from rdv_query_builder import RDVView, RDVDatabase, RDVViewHier

# obligatory: get_title, get_obj_type

class NewspapersView(RDVView):
    """class to define snippet and view fields and settings"""
    # to define whether it is a test class, functionality needs to be defined in subclasses, e.g. adapt path, ...
    test = False
    # lookup file for form definition (same as view definition), needs to be defined, including config folder, e.g.: "{}/ZAS Vocab.xlsx".format(RDVView.config_folder)
    file = "{}/fields_def_NewspapersView.xlsx".format(RDVView.config_folder)
    # needs to be defined in every subclass, so it is not inherited, stores data from form/view definition, do not overwrite
    fields_def_lookup = {}
    # which fields should be included in the snippet, order defined by order
    snippet_fields = ["date_str", "newspaper"]
    # whether versions stored in mongodb for an object shall be accessible while editing
    show_versions = False

    ## if also inherits from RDVViewHier class
    # needs to be defined here to next to RDV2ES class
    hierarchy_index = ""
    # at which level links at detail page shall be provided
    hier_level4links = 0

    def get_inst_kuerzel(self) -> str:
        """returns object type to control code flow (e.g. based on index)"""
        proj = self.page.get("proj_field")
        return "avis-blatt"

    def get_obj_type(self, lang="de") -> str:
        """returns obj type for presentation, pass Es field for "obj_field" """
        obj_type = self.page["pages"][0].get("issue_type")
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        return obj_type

    def get_title(self) -> str:
        """returns title for presentation, pass Es field for "title_field" """
        title = self.page.get("date_str")
        if isinstance(title, list):
            title = ", ".join([t for t in title if t])
        ausgabe = self.page.get("issue_number")

        if isinstance(ausgabe, list):
            ausgabe_str = "Nr. " + ", ".join([t for t in ausgabe if t])
        elif ausgabe:
            ausgabe_str = "Nr. " + ausgabe
        else:
            ausgabe_str = ""
        return " ".join([title, ausgabe_str])

    def get_preview_image(self) -> str:
        """returns url for preview image in snippet view"""
        preview_link = ""
        if self.page.get("newspaper") in ["arbeitgeber", "wirtsozpol", "handelsztg"]:
            preview_link = "https://ub-sipi.ub.unibas.ch/impresso/{}".format(
                self.page["pages"][0].get("filename").split(".")[0])
        elif self.page.get("newspaper") in ["chrivoaub", "wandindes"]:
            preview_link = "https://ub-digiworker.ub.unibas.ch/volksbote/{}".format(
                self.page["pages"][0].get("filename"))
        elif self.page.get("newspaper") in ["avis", "avisba", "hochprdo"]:
            preview_link = "https://ub-digiworker.ub.unibas.ch/avis-blatt/{}".format(
                self.page["pages"][0].get("filename"))
        elif self.page.get("newspaper") in ["wochnaaud"]:
            preview_link = "https://ub-sipi.ub.unibas.ch/avis_ocr/{}.tif".format(
                self.page["pages"][0].get("filename").split(".")[0])
        elif self.page.get("newspaper") in ["AVIS-Blatt"]:
            preview_link = self.page["pages"][0].get("iiif_url")
        return preview_link

    def get_manifest_id(self) -> str:
        """return path to manifest without http-Domain,
        e.g. "/".join(["dizas", zas_id, "manifest"])"""
        manif_id = "/".join(["avis_ocr", self.page.get("manifest_id"), "manifest"])
        return manif_id

    def get_viewer(self) -> dict:
        """define which viewer shall be shown in detail page, refers to key in documentViewer Frontend Configuration
        use case: for AV material only use Viewer x, for Images only y, ...
        e.g. {"viewer": ["MiradorSinglePage", "UV"]}"""
        return {"viewer": ["MiradorSinglePage", "UV"]}

    def add_snippet_fulltext(self) -> str:
        """add expandabel fulltext to snippet (instead of detail page), e.g. abstract
        """
        fulltext = self.page.get("fulltext")
        return fulltext
