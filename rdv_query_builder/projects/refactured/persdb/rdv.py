from rdv_query_builder import RDV2ES, RDVView, RDVForm
from rdv_query_builder.projects.refactured.persdb import PersDBView, PersDBForm
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import RQ_GTE, RQ_LTE

class RDV2ES4PersDB(RDV2ES):
    """class to define es functionalities (query builder)"""
    # define alias for index/indices
    index= "persdb"
    # what is the name of the view class
    view_class = PersDBView
    # what is the name of the form class
    form_class = PersDBForm
    # the content from which field should be used for es_highlighting
    highlight_field = "*"
    # if highlighting is used for multiple fields if fieldname shall be incluede
    highlight_included_fieldname = True
    # are there iiif compontents? = documentViewerProxyUrl in RDV Frontend
    iiif = True
    # how many objects shall be combined to a dynamic manifest
    # limit size to a certain limit, because it is frontend controlled
    max_ids_dyn_manif = 200
    # what is the url prefix (ini-file name in es config) for dynamic manifests
    dyn_manif= "persdb"

    def __init__(self, *args, **kwargs):
        """

        :param request_data:
        :param es_client:
        :param index: is ignored, used from class var
        :param debug:
        :param iiif_host:
        :param es_host:
        :param autocomplete:
        """
        super().__init__(*args, **kwargs)
        # fields used for full text query, if empty list, it searchs within all fields
        self.simple_query_fields = []
        # fields to be returned from Elasticsearch (_source), controlled from frontend if deleted here
        # use it here, if some fields shall not be made public or for performance reasons (e.g. large data/fulltext fields)
        self.source_fields = []
        # for which range date ranges shall be created, defines start level of ranges, steps: century, decade, year, day, ...
        # function get_date_agg_interval can be used to exclude to small aggregation, e.g. if only year values, no month aggregation is needed
        self.default_date_range = {RQ_GTE: "2000-01-01", RQ_LTE: "2050-01-01"}
        # definition for int ranges to calculate range sizes, defines start level of ranges, steps: 1, 10, 100, 1000, ...
        self.default_int_range = {RQ_GTE: 0, RQ_LTE: 50}