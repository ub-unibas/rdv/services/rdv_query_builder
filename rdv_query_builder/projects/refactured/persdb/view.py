import json

from rdv_query_builder import RDVView, RDVDatabase
from rdv_query_builder.projects.refactured.fortbild import FortbildDatabase
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import SNIPPET_SEARCH_AFTER, SNIPPET_TITLE, SNIPPET_OBJ_TYPE, SNIPPET_LINE1, \
    SNIPPET_FULLTEXT, SNIPPET_IIIF_PREVIEW, SNIPPET_ID

# todo: link als link ingestieren (label, link dictionary), datum zusammenfassen
# editierbar machen

IIIF_IMAGE_SERVER = "https://ub-sipi.ub.unibas.ch"

class PersDBView(RDVView):
    """class to define snippet and view fields and settings"""
    # lookup file for form definition (same as view definition), needs to be defined, including config folder, e.g.: "{}/ZAS Vocab.xlsx".format(RDVView.config_folder)
    file = None #"{}/fields_def_PersDBView.xlsx".format(RDVView.config_folder)
    # needs to be defined in every subclass, so it is not inherited, stores data from form/view definition, do not overwrite
    fields_def_lookup = {}
    # which fields should be included in the snippet, order defined by order
    snippet_fields = ["Abteilung", "Team", "Bemerkung", "Email", "Funktion", "Gruppe", "Stellenprozent"]
    # class to write form data to database, subclass from RDVDatabase
    db_class = FortbildDatabase

    def get_inst_kuerzel(self) -> str:
        """returns object type to control code flow (e.g. based on index)"""
        proj = "persdb"
        return proj

    def get_obj_type(self, lang="de") -> str:
        """returns obj type for presentation, pass Es field for "obj_field" """
        obj_type = "Mitarbeiterin"
        return obj_type

    def get_title(self) -> str:
        """returns title for presentation, pass Es field for "title_field" """
        title = self.page.get("Name")
        return title

    def get_preview_image(self) -> str:
        """returns url for preview image in snippet view"""
        id_jp2 = self.page.get("Foto")
        if id_jp2:
            link = "{}/{}/{}".format(IIIF_IMAGE_SERVER, "personal_photo", id_jp2)
            return link
        else:
            return ""

    def get_manifest_id(self) -> str:
        """return path to manifest without http-Domain,
        e.g. "/".join(["dizas", zas_id, "manifest"])"""
        manif_id = "/".join(["persdb", str(self.page.get("pers_id")), "manifest"])
        return manif_id

    def get_viewer(self) -> dict:
        """define which viewer shall be shown in detail page, refers to key in documentViewer Frontend Configuration
        use case: for AV material only use Viewer x, for Images only y, ...
        e.g. {"viewer": ["MiradorSinglePage", "UV"]}"""
        return {"viewer": ["MiradorSinglePage"]}