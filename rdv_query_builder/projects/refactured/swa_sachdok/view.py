import json

from rdv_query_builder import RDVView, RDVDatabase
from rdv_query_builder.projects.refactured.swa_pdf import SWAPDFView

from rdv_data_helpers_ubit.projects.swasachdok.swasachdok import SWA_PDF_TITLE_FIELD, PDF_FILENAME, PDF_LINKS

class SWASachdokView(SWAPDFView):
    fields_def_lookup = {}
    # file = "{}/fields_def_SWASachdokView.xlsx".format(RDVView.config_folder)
    snippet_fields = ["245_c", "264_c", "490_a"]
    spreadsheet = "1nxN3vN_cnO_hKNVNFkoZVMl2qp2AKPua86rY7ZGJE70"


    def get_fields_def(self, proj):
        if proj not in self.fields_def_lookup:
            spreadsheet_id = self.spreadsheet
            fields_def = self.get_gdocs(spreadsheet_id=spreadsheet_id, sheet_name=proj, key_field="Feld")
            self.fields_def_lookup[proj] = fields_def
        else:
            fields_def = self.fields_def_lookup[proj]
        return fields_def

    def get_inst_kuerzel(self) -> str:
        """returns object type to control code flow (e.g. based on index)"""
        return "not defined"

    def get_obj_type(self, lang="de") -> str:
        """returns obj type for presentation, pass Es field for "obj_field" """
        obj_type = self.page.get("type_swapdf") or self.page.get("type")
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        return "Archivierte E-Kleinschrift"

    def get_title(self) -> str:
        """returns title for presentation, pass Es field for "title_field" """
        try:
            title = str(". ".join(self.page.get(SWA_PDF_TITLE_FIELD, [])))
        except (KeyError, TypeError):
            return "Kein IZID Titel für {}".format(self.page.get(PDF_FILENAME))
        return title

    def get_manifest_id(self) -> str:
        """return path to manifest without http-Domain,
        e.g. "/".join(["dizas", zas_id, "manifest"])"""
        manif_id = "swa_kleinschriften/{}/manifest".format(self.page.get("bsiz_id"))
        return manif_id

    def get_viewer(self) -> dict:
        """define which viewer shall be shown in detail page, refers to key in documentViewer Frontend Configuration
        use case: for AV material only use Viewer x, for Images only y, ...
        e.g. {"viewer": ["MiradorSinglePage", "UV"]}"""
        return {"viewer":["UV"]}

    def add_descr_data(self):
        # so that not descr metadata from parent class SWAPDFView is added
        return {}

    def get_preview_image(self) -> str:
        """returns url for preview image in snippet view"""
        pdf_link = self.page.get(PDF_LINKS, [{}])[0].get("link","")
        if "/sachdok/" in pdf_link:
            pdf_id = "-".join(pdf_link.split("/sachdok/")[-1].split("/"))
        else:
            pdf_id = "-".join(pdf_link.split("https://ub-sachdokpdf.ub.unibas.ch/")[-1].split("/"))
        if pdf_id:
            return "https://ub-sipi.ub.unibas.ch/swa_pdfs/{}@1".format(pdf_id)
        else:
            return "https://ub-sipi.ub.unibas.ch/logos/icon-SWASearch.jpg"

class SWASachdokTestView(SWASachdokView):

    def get_manifest_id(self) -> str:
        """return path to manifest without http-Domain,
        e.g. "/".join(["dizas", zas_id, "manifest"])"""
        manif_id = "swa_kleinschriften_test/{}/manifest".format(self.page.get("bsiz_id"))
        return manif_id