from rdv_query_builder import RDV2ES, RDVView, RDVForm
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import RQ_GTE, RQ_LTE, RDV_SORT

from rdv_query_builder.projects.refactured.bernoulli import BernoulliView
# obligatory: index and view_class



class RDV2ES4Bernoulli(RDV2ES):
    """class to define es functionalities (query builder)"""
    # define alias for index/indices
    index= "bernoulli"
    # what is the name of the view class
    view_class = BernoulliView
    # are there iiif compontents? = documentViewerProxyUrl in RDV Frontend
    iiif = True
    # how many objects shall be combined to a dynamic manifest
    # limit size to a certain limit, because it is frontend controlled
    max_ids_dyn_manif = 200
    # what is the url prefix (ini-file name in es config) for dynamic manifests
    dyn_manif= "bernoulli"

    def __init__(self, *args, **kwargs):
        """

        :param request_data:
        :param es_client:
        :param index: is ignored, used from class var
        :param debug:
        :param iiif_host:
        :param es_host:
        :param autocomplete:
        """
        super().__init__(*args, **kwargs)
        # fields to be returned from Elasticsearch (_source), controlled from frontend if deleted here
        # use it here, if some fields shall not be made public or for performance reasons (e.g. large data/fulltext fields)
        self.source_fields = []

        self.hierarchy_index = "han_hier"

        # todo: sollte ausgelesen und nicht statisch gesetz werden
        #self.hier_top_id = "han_hier_553.14.23.2." # "han_hier_9972407387805504" #
        #self.top_hier_query = {"size": 500, "query": {"bool": {"must": {"ids": {"values": ["han_hier_9972407385605504","han_hier_9972407388605504","han_hier_9972407388105504","han_hier_9972407386705504"]}}}}}
        #self.hierarchy_facet_sort = self.sort_han

    def sort_han(self, x):
        return x.get(RDV_SORT, "")