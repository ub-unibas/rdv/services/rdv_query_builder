import json

from rdv_query_builder import RDVView, RDVDatabase, RDVViewHier

# obligatory: get_title, get_obj_type

IIIF_IMAGE_SERVER = "https://ub-sipi.ub.unibas.ch"

class BernoulliView(RDVView, RDVViewHier):
    """class to define snippet and view fields and settings"""
    # lookup file for form definition (same as view definition), needs to be defined, including config folder, e.g.: "{}/ZAS Vocab.xlsx".format(RDVView.config_folder)
    file = "{}/fields_def_BernoulliView.xlsx".format(RDVView.config_folder)
    # needs to be defined in every subclass, so it is not inherited, stores data from form/view definition, do not overwrite
    fields_def_lookup = {}
    # which fields should be included in the snippet, order defined by order
    snippet_fields = ["264__1_ac"]

    ## if also inherits from RDVViewHier class
    # needs to be defined here to next to RDV2ES class
    hierarchy_index = "han_hier"
    # up to which level links at detail page shall be provided
    hier_level4links = 3

    def get_obj_type(self, lang="de") -> str:
        """returns obj type for presentation, pass Es field for "obj_field" """
        obj_type = self.page.get("Objekttyp2") or "Brief"
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        return obj_type

    def get_title(self) -> str:
        """returns title for presentation, pass Es field for "title_field" """
        title = self.page.get("245_ab")
        if isinstance(title, list):
            title = ", ".join([t for t in title if t])
        return title

    def get_preview_image(self) -> str:
        """returns url for preview image in snippet view"""
        iiif_images = self.page.get("iiif_imgs", [])
        if iiif_images:
            pos = 0
            iiif_image = iiif_images[pos].get("url")
            if iiif_image:
                return iiif_image
            else:
                return ""

    def get_manifest_id(self) -> str:
        """return path to manifest without http-Domain,
        e.g. "/".join(["dizas", zas_id, "manifest"])"""
        manif_id = self.page.get("mets_iiifmanifest","")
        return manif_id

    def get_viewer(self) -> dict:
        """define which viewer shall be shown in detail page, refers to key in documentViewer Frontend Configuration
        use case: for AV material only use Viewer x, for Images only y, ...
        e.g. {"viewer": ["MiradorSinglePage", "UV"]}"""
        return {"viewer": ["MiradorSinglePage"]}

    def enrich_page(self):
        self.get_hier_entries(hier_field="hierarchy_filter")
        self.get_hiersub_entries(hier_field="hierarchy_filter")

    def build_simple_hierarchy_search_body(self, hier_pos, hier_pos_field="hierarchy_filter"):

        must_filters = [
            {"prefix": {"{}.keyword".format(hier_pos_field): {"value": hier_pos}}}
        ]
        count_body = {"query":{"bool":{"filter": must_filters}}}

        return count_body

    def get_idlab_select(self, v, es_field):
        if es_field == "hier_path":
            values = []
            for e in self.page["hier_path"]:
                if e.get("hierarchy_filter")[0] in v.get("hierarchy_filter")[0]:
                    values.append({'value': {'id': e.get("id"), "pos": e.get("hierarchy_filter")[0]},
                                   'label': e.get("label"), 'operator': 'AND'})
            selection = {'hierarchy_filter.keyword': {'values': values}}
        else:
            selection = {'{}.id.keyword'.format(es_field): {'values': [
                {'value': {'id': v.get("id")},
                 'label': v.get("label")}], 'operator': 'OR'}}
        return selection