from rdv_query_builder import RDVView, RDVDatabase, RDVViewHier
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import SNIPPET_OBJ_TYPE, SNIPPET_LINE1

# obligatory: get_title, get_obj_type

class ViseView(RDVView):
    """class to define snippet and view fields and settings"""
    # lookup file for form definition (same as view definition), needs to be defined, including config folder, e.g.: "{}/ZAS Vocab.xlsx".format(RDVView.config_folder)
    file = ""
    # needs to be defined in every subclass, so it is not inherited, stores data from form/view definition, do not overwrite
    fields_def_lookup = {}
    # which fields should be included in the snippet, order defined by order
    snippet_fields = ["attach_note", "lehrveranstaltung", "prio"]

    def get_obj_type(self, lang="de") -> str:
        """returns obj type for presentation, pass Es field for "obj_field" """
        obj_type = ""#self.page.get("itemType") or ""
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        return obj_type

    def get_title(self) -> str:
        """returns title for presentation, pass Es field for "title_field" """
        title = self.page.get("bib")
        if isinstance(title, list):
            title = ", ".join([t for t in title if t])
        return title

    def get_preview_image(self) -> str:
        """returns url for preview image in snippet view"""
        return self.page.get("thumbnail")

    def add_snippet_link(self) -> dict:
        """add link to snippet (instead of detail page), e.g. swisscovery link
        format: {"url": "", "label": ""}
        """
        links = []
        for url in self.page.get("links", []):
            links.append({"url": url.get("url"), "label": url.get("label")})
        return links

    def build_snippet(self):

        snippet = self.build_main_snippet()

        snippet["i18n"] = {}
        # languages definieren
        for lang in ["de", "en"]:
            snippet_values = {}
            snippet["i18n"][lang] = {}
            lang_snippet = snippet["i18n"][lang]
            for es_field in self.snippet_fields:
                # lookup einbauen
                field_def = self.fields_def.get(es_field)
                # change to base function
                label = {"lehrveranstaltung": "Lehrveranstaltung", "attach_note": "Dozierendenkommentar", "prio": "Relevanz"}.get(es_field, es_field)
                es_value = self.page.get(es_field)
                if isinstance(es_value, list):
                    values = self.extract_values(es_value)
                    value = "<br/>".join(values)
                else:
                    value = es_value
                    value = value.strip() if isinstance(value, str) else value

                # change to base function
                # damit label "Dozierendenkommentar" nicht angezeigt wird
                if es_field == "attach_note":
                    snippet_values[es_field] = "{}".format(value)
                elif value is not None and value != "":
                    snippet_values[es_field] = "<b>{}</b>:{}".format(label, value).strip(":")
            # combine all snippet values in SNIPPET_LINE1
            lang_snippet[SNIPPET_LINE1] = "<br/>".join(
                [snippet_values[f] for f in self.snippet_fields if f in snippet_values])
            lang_snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type(lang=lang)

        if self.add_snippet_link():
            snippet["link"] = self.add_snippet_link()
        if self.add_snippet_fulltext():
            snippet["fulltext"] = self.add_snippet_fulltext()

        return snippet