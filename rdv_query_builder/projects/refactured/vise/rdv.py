from rdv_query_builder import RDV2ES, RDVView, RDVForm
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import RQ_GTE, RQ_LTE

from rdv_query_builder.projects.refactured.vise import ViseView
# obligatory: index and view_class

class RDV2ES4Vise(RDV2ES):
    """class to define es functionalities (query builder)"""
    # define alias for index/indices
    index= "vise"
    # what is the name of the view class
    view_class = ViseView
    # are there iiif compontents? = documentViewerProxyUrl in RDV Frontend
    iiif = False
    # the content from which field should be used for es_highlighting
    highlight_field = "bib"
    # if highlighting is used for multiple fields if fieldname shall be incluede
    highlight_included_fieldname = False


    def __init__(self, *args, **kwargs):
        """

        :param request_data:
        :param es_client:
        :param index: is ignored, used from class var
        :param debug:
        :param iiif_host:
        :param es_host:
        :param autocomplete:
        """
        super().__init__(*args, **kwargs)
        self.source_fields = []
        self.hierarchy_index = "vise-hier"
        self.simple_query_fields = ["bib", "links.label", "attach_note", "lehrveranstaltung.*", "author.label"]
        self.default_int_range = {RQ_GTE: 0, RQ_LTE: 1000}
        self.hierarchy_facet_sort = self.sort_semester

    def sort_semester(self, hier_entry):
        sort_value = hier_entry.get("label", "")
        sort_value = sort_value.replace(" HS", " AA")
        sort_value = sort_value.replace(" FS", " ZZ")
        return sort_value

    def get_date_agg_interval(self, values, operator, max_days_aggr= 367, field=None):
        return self._get_date_agg_interval(values, operator, max_days_aggr,min_date_aggr=1)