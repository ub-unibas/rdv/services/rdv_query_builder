import json

from rdv_query_builder import RDVView, RDVDatabase, RDVViewHier

# obligatory: get_title, get_obj_type
IIIF_IMAGE_SERVER = "https://ub-sipi.ub.unibas.ch"

class DigispaceView(RDVView):
    """class to define snippet and view fields and settings"""
    # to define whether it is a test class, functionality needs to be defined in subclasses, e.g. adapt path, ...
    test = False
    # lookup file for form definition (same as view definition), needs to be defined, including config folder, e.g.: "{}/ZAS Vocab.xlsx".format(RDVView.config_folder)
    file = ""
    # needs to be defined in every subclass, so it is not inherited, stores data from form/view definition, do not overwrite
    fields_def_lookup = {}
    # which fields should be included in the snippet, order defined by order
    snippet_fields = ["author", "signatur", "year"]
    # class to write form data to database, subclass from RDVDatabase
    db_class = RDVDatabase
    # whether versions stored in mongodb for an object shall be accessible while editing
    show_versions = False

    ## if also inherits from RDVViewHier class
    # needs to be defined here to next to RDV2ES class
    hierarchy_index = ""
    # at which level links at detail page shall be provided
    hier_level4links = 0
    spreadsheet = "1SRwSdueGpxItne4KKLhM6uvsXJVByP879Vxpx_VFPWg"

    def get_fields_def(self, proj):
        if proj not in self.fields_def_lookup:
            spreadsheet_id = self.spreadsheet
            fields_def = self.get_gdocs(spreadsheet_id=spreadsheet_id, sheet_name=proj, key_field="Feld")
            self.fields_def_lookup[proj] = fields_def
        else:
            fields_def = self.fields_def_lookup[proj]
        return fields_def

    def get_inst_kuerzel(self) -> str:
        """returns object type to control code flow (e.g. based on index)"""
        proj = "tbd"
        return proj

    def get_obj_type(self, lang="de") -> str:
        """returns obj type for presentation, pass Es field for "obj_field" """
        obj_type = self.page.get("platform")
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        return obj_type

    def get_title(self) -> str:
        """returns title for presentation, pass Es field for "title_field" """
        title = self.page.get("title", "")
        if isinstance(title, list):
            title = ", ".join([t["label"] for t in title if t])
        return title

    def get_preview_image(self) -> str:
        """returns url for preview image in snippet view"""
        iiif_images = self.page.get("iiif_preview", [])
        if iiif_images:
            if "portraets" in self.get_obj_type():
                link = "{}/{}/{}".format(IIIF_IMAGE_SERVER, "portraets", iiif_images[0])
                return link
            else:
                return iiif_images[0]
        else:
            return ""

    def get_manifest_id(self) -> str:
        """return path to manifest without http-Domain,
        e.g. "/".join(["dizas", zas_id, "manifest"])"""
        manif_id = self.page.get("iiif_manifest", [])
        if "portraets" in self.get_obj_type():
            return  "/".join(["portraets", self.page.get("bsiz_id"), "manifest"])
        elif manif_id:
            return manif_id[0]
        else:
            return ""

    def get_viewer(self) -> dict:
        """define which viewer shall be shown in detail page, refers to key in documentViewer Frontend Configuration
        use case: for AV material only use Viewer x, for Images only y, ...
        e.g. {"viewer": ["MiradorSinglePage", "UV"]}"""
        if not self.page.get("mets_iiifmanifest",""):
            return {"viewer": []}
        else:
            return {"viewer": ["MiradorSinglePage", "UV"]}

    def add_snippet_link(self) -> dict:
        """add link to snippet (instead of detail page), e.g. swisscovery link
        format: {"url": "", "label": ""}
        """
        link = self.page.get("link_field")
        return link

    def add_snippet_fulltext(self) -> str:
        """add expandabel fulltext to snippet (instead of detail page), e.g. abstract
        """
        fulltext = self.page.get("fulltext_field")
        return fulltext

    def get_rdv_view(self, edit=False):
        view_format = super().get_rdv_view(edit=edit)
        cat_link = self.page.get("cat_link")
        if cat_link:
            descr_aleph_link = {"value": {"de": [{"label": "Primo",
                                                  "link": cat_link}]},
                                "label": {"de": ["Katalog-Link"]}}
            # add empty service_info for manif
            edit_info = self.get_edit_info()
            descr_aleph_link.update(edit_info)
            view_format["desc_metadata"].append(descr_aleph_link)
        return view_format