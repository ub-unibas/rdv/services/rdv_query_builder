import json

from rdv_query_builder import RDVView, RDVDatabase
from rdv_data_helpers_ubit import IIIF_PREVIEW_ESFIELD, IIIF_MANIF_ESFIELD
# obligatory: get_title, get_obj_type

IIIF_IMAGE_SERVER = "https://ub-sipi.ub.unibas.ch"

# TODO: flex manifest grösse extrahieren
# TODO: range Zeitraum

class PortraetView(RDVView):
    """class to define snippet and view fields and settings"""
    # lookup file for form definition (same as view definition), needs to be defined, including config folder, e.g.: "{}/ZAS Vocab.xlsx".format(RDVView.config_folder)
    #file = "{}/fields_def_PortraetView.xlsx".format(RDVView.config_folder)
    # needs to be defined in every subclass, so it is not inherited, stores data from form/view definition, do not overwrite
    fields_def_lookup = {}
    # which fields should be included in the snippet, order defined by order
    snippet_fields = ["600_abcd_1_7", "author", "264_c_1", "wiki_extract", "bsiz_id"]

    spreadsheet = "1qB76SpnYdFoUAYEM4_t1oUFptaFuWnUuLW8o9tQe_7s"
    # Domain-Name for swa_search_host

    def get_fields_def(self, proj):
        if proj not in self.fields_def_lookup:
            spreadsheet_id = self.spreadsheet
            fields_def = self.get_gdocs(spreadsheet_id=spreadsheet_id, sheet_name=proj, key_field="Feld")
            self.fields_def_lookup[proj] = fields_def
        else:
            fields_def = self.fields_def_lookup[proj]
        return fields_def

    def get_obj_type(self, lang="de") -> str:
        """returns obj type for presentation, pass Es field for "obj_field" """
        obj_type = self.page.get("type") or "Porträt"
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        return obj_type

    def get_inst_kuerzel(self):
        proj = self.get_obj_type()
        return proj

    def get_title(self) -> str:
        """returns title for presentation, pass Es field for "title_field" """

        title = []
        if self.page.get("title", {}) or self.page.get("245_ab") or self.page.get("gnd_name"):
            title = self.page.get("245_ab") or self.page.get("gnd_name", "") or self.page.get("title", {})[0].get("label")
        if isinstance(title, list):
            title = ", ".join([t for t in title if t])
        return title

    def get_preview_image(self) -> str:
        """returns url for preview image in snippet view"""
        if 0:
            id_jp2 = self.page.get("jp2_files") or self.page.get("iiif_preview")
            if id_jp2:
                link = "{}/{}/{}".format(IIIF_IMAGE_SERVER, "portraets", id_jp2[0])
                return link
            else:
                return ""
        iiif_images = self.page.get(IIIF_PREVIEW_ESFIELD, [])
        thumbnails = self.page.get("wiki_thumbnail")
        if iiif_images:
            img_name = iiif_images[0].get("url") #if isinstance(iiif_images[0], dict) else iiif_images[0]
            if "e-manuscripta" in img_name:
                return  img_name + "full/120,/0/default.jpg"
            else:
                img_name = img_name.split("info.json")[0]
                link = "{}/{}/{}".format(IIIF_IMAGE_SERVER, "portraets", img_name)
                return  link + "/full/120,/0/default.jpg"
        elif thumbnails:
            return thumbnails
        else:
            return ""

    def get_manifest_id(self) -> str:
        """return path to manifest without http-Domain,
        e.g. "/".join(["dizas", zas_id, "manifest"])"""
        if self.page.get(IIIF_MANIF_ESFIELD):
            return self.page.get(IIIF_MANIF_ESFIELD)[0]
        elif self.page.get("bsiz_id"):
            manif_id = "/".join(["portraets", self.page.get("bsiz_id"), "manifest"])
            return manif_id

    def get_viewer(self) -> dict:
        """define which viewer shall be shown in detail page, refers to key in documentViewer Frontend Configuration
        use case: for AV material only use Viewer x, for Images only y, ...
        e.g. {"viewer": ["MiradorSinglePage", "UV"]}"""
        return {"viewer": ["MiradorSinglePage"]}

    def add_descr_data(self):
        add_descr = []
        emanu_links = [{"label": l.get("z"), "link": l.get("u")} for l in self.page.get("856links", []) if "e-manuscripta" in l.get("u")]

        if emanu_links:

            e_manu_link = {"value": {"de": emanu_links},
                           "label": {"de": ["e-manuscripta"],
                                     "en": ["e-manuscripta"]}
                           }
            add_descr.append(e_manu_link)
        if self.page.get("bsiz_id"):
            try:
                email = {"value": {"de": [{"label": "noah.regenass at unibas.ch",
                                           "link": "mailto:noah.regenass@unibas.ch?subject=Anmerkungen zum Katalogisat " +
                                                   self.page.get("bsiz_id")}]},
                         "label": {"de": [self.fields_def["email"]["Feldlabel de"]],
                                   "en": [self.fields_def["email"]["Feldlabel en"]]}
                         }
                add_descr.append(email)
            except Exception:
                pass
        return add_descr
