from rdv_query_builder import RDV2ES, RDVView, RDVForm
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import RQ_GTE, RQ_LTE
import json
import re
from rdv_query_builder.projects.refactured.portraets import PortraetView
# obligatory: index and view_class

class RDV2ES4Port(RDV2ES):
    """class to define es functionalities (query builder)"""
    # define alias for index/indices
    index= "portraets3"
    # what is the name of the view class
    view_class = PortraetView
    # are there iiif compontents? = documentViewerProxyUrl in RDV Frontend
    iiif = True
    # how many objects shall be combined to a dynamic manifest
    # limit size to a certain limit, because it is frontend controlled
    max_ids_dyn_manif = 200
    # what is the url prefix (ini-file name in es config) for dynamic manifests
    dyn_manif= "portraets"

    def __init__(self, *args, **kwargs):
        """

        :param request_data:
        :param es_client:
        :param index: is ignored, used from class var
        :param debug:
        :param iiif_host:
        :param es_host:
        :param autocomplete:
        """
        super().__init__(*args, **kwargs)
        self.source_fields = []

    def split_search_phrase(self, search_phrase):
        if isinstance(search_phrase, list) and len(search_phrase) != 1:
            return search_phrase
        if len(search_phrase) == 1:
            search_phrase = search_phrase[0]
        return [sp for sp in re.split("[ ,.]", search_phrase) if sp]