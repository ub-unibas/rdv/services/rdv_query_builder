import json

from rdv_query_builder import RDV2ES, RDVView
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import SNIPPET_SEARCH_AFTER, SNIPPET_TITLE, SNIPPET_OBJ_TYPE, SNIPPET_LINE1, SNIPPET_FULLTEXT, SNIPPET_IIIF_PREVIEW, SNIPPET_ID, \
    RQ_GTE, RQ_LTE


# Fehler bei Autor

class EdocView(RDVView):
    fields_def_lookup = {}
    file = None
    snippet_fields_port =["creators", "investigator"]

    def get_obj_type(self, lang="de"):
        obj_type = self.page.get("type") or "Buch"
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        return obj_type

    def get_title(self):
        title = self.page.get("title")
        if isinstance(title, list):
            title = ", ".join([t for t in title if t])
        return title

    def get_manifest_id(self):
        manif_id = ""
        return manif_id

    def get_preview_image(self):
        return self.page.get("thumbnail")

    def get_viewer(self):
        return {[None]}

    def build_snippet(self):

        snippet = {}
        snippet[SNIPPET_ID] = self.page_id
        snippet[SNIPPET_TITLE] = self.get_title()
        snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type()
        # js -es int problem
        snippet[SNIPPET_SEARCH_AFTER] = json.dumps(self.page.get(SNIPPET_SEARCH_AFTER))
        snippet[SNIPPET_IIIF_PREVIEW] = self.get_preview_image()
        if self.page.get(SNIPPET_FULLTEXT):
            snippet[SNIPPET_FULLTEXT] = " ...</br>... ".join(self.page.get(SNIPPET_FULLTEXT, []))

        snippet["i18n"] = {}
        for lang in ["de", "en"]:
            snippet_values = {}
            snippet["i18n"][lang] = {}
            lang_snippet = snippet["i18n"][lang]
            port_fields = self.snippet_fields_port
            for es_field in port_fields:
                label =self.get_lang_label(self.fields_def.get(es_field), lang)
                label = {"creators": "Autoren", "investigator": "Investigator"}.get(es_field, es_field)
                if isinstance(self.page.get(es_field,[]), list):
                    values = self.extract_values(self.page.get(es_field,[]))
                    value = ", ".join(values)
                else:
                    value = self.page[es_field]
                    value = value.strip() if isinstance(value, str) else value
                if value:
                    snippet_values[es_field] = "{}: {}".format(label, value)

            lang_snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type(lang)
            lang_snippet[SNIPPET_LINE1] = ", ".join([snippet_values[f] for f in self.snippet_fields_port if f in snippet_values])
        snippet[SNIPPET_LINE1] = lang_snippet[SNIPPET_LINE1]
        snippet["link"] = []
        if self.page.get("abstract",[]):
            snippet[SNIPPET_FULLTEXT] = "Abstract: {}{}".format(self.page.get("abstract",[])[0:100], "..." if len(self.page.get("abstract",[])) > 100 else "")
            snippet["fulltext"] = self.page.get("abstract",[])
        if self.page.get("uri", ""):
            snippet["link"].append({"url": self.page.get("uri", ""), "label": "edoc-datatest-Link"})

        return snippet

class RDV2ES4Edoc(RDV2ES):
    index= "rdv_edoc"
    iiif = False
    max_ids_dyn_manif = 0
    dyn_manif= ""
    view_class = EdocView
    highlight_field = "*"
    default_sort = {}


    def __init__(self, request_data, es_client, index, debug=False, iiif_host="https://ub-test-iiifpresentation.ub.unibas.ch", es_host="", autocomplete=False):
        super().__init__(request_data, es_client, index, debug= debug, iiif_host=iiif_host, es_host=es_host, autocomplete=autocomplete)
        self.source_fields = ["*"]
        self.index = index
        self.hierarchy_index = "rdvedoc_hier"
        self.simple_query_fields = []
        self.default_int_range = {RQ_GTE: 0, RQ_LTE: 1000}
        self.hierarchy_facet_sort = self.sort_div

    def sort_div(self, hier_entry):
        sort_value = hier_entry.get("label", "")
        sort_value = sort_value.replace(" HS", " AA")
        sort_value = sort_value.replace(" FS", " ZZ")
        return sort_value

    def get_date_agg_interval(self, values, operator, max_days_aggr= 3660, field=None):
        return self._get_date_agg_interval(values, operator, max_days_aggr)