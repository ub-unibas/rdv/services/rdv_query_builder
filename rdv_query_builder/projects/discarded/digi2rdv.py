import json

from rdv_query_builder import RDV2ES
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import SNIPPET_SEARCH_AFTER, SNIPPET_TITLE, SNIPPET_OBJ_TYPE, SNIPPET_LINE1, \
    SNIPPET_IIIF_PREVIEW, SNIPPET_ID


class RDV2ES4Digi(RDV2ES):
    index= "iiif_digispace"
    dyn_manif= "digispace"
    def __init__(self, request_data, es_client, index, debug=False, iiif_host="https://ub-test-iiifpresentation.ub.unibas.ch"):
        # index so anpassen, dass iiif_links im gleichen feld wie bildpfade sind
        # emanuscripta links und erara links ergänzen
        self.simple_query_fields = ["cat_person", "cat_title", "cat_type", "cat_year", "sys_id"]
        super().__init__(request_data, es_client, index, debug, iiif_host=iiif_host)
        self.hierarchy_index = "dsv05_hier"
        self.hierarchy_index_host = "localhost:9209"
        self.hierarchy_facet_sort = lambda x: x.get("count","") * -1
        # todo: anpassen
        self.source_fields = ["cat_person", "cat_title", "cat_type", "cat_year", "sys_id", "files", "sys_id", "mets_iiifmanifest", "mets_iiifpages", "cat_url_type", "mets_iiifmanifest"]

    def build_preview_snippet(self, result):
        port_objects = [self._es2dict(hit) for hit in result["hits"]["hits"]]
        preview_objects = []
        for z in port_objects:
            o = {}
            o[SNIPPET_ID] = z.get(SNIPPET_ID)

            # js -es int problem
            o[SNIPPET_SEARCH_AFTER] = json.dumps(z.get(SNIPPET_SEARCH_AFTER))
            o[SNIPPET_TITLE] = z.get("cat_title")
            if isinstance(o[SNIPPET_TITLE], list):
                o[SNIPPET_TITLE] = ", ".join([i for i in o[SNIPPET_TITLE] if i])

            o[SNIPPET_OBJ_TYPE] = z.get("cat_type")[0] if z.get("cat_type",[]) else "nicht definiert"

            o[SNIPPET_LINE1] = "Aufnahmejahr: {}".format(z.get("cat_year")[0] if z.get("cat_year",[]) else "fehlt")

            if "mets_iiifpages" in z and z["mets_iiifpages"]:
                o[SNIPPET_IIIF_PREVIEW] = z["mets_iiifpages"][0]["url"]
            else:
                if isinstance(z.get("files"), str) and z.get("files") and z.get("files").startswith("http"):
                    o[SNIPPET_IIIF_PREVIEW] = ""
                else:
                    o[SNIPPET_IIIF_PREVIEW] = "https://ub-hymir.ub.unibas.ch/image/v2/{}".format(
                                                                                  z.get("files",[])[0]) if z.get("files",[]) else ""
            preview_objects.append(o)
        return preview_objects
