import json

from rdv_query_builder import RDV2ES, RDVView
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import SNIPPET_SEARCH_AFTER, SNIPPET_TITLE, SNIPPET_OBJ_TYPE, SNIPPET_LINE1, SNIPPET_FULLTEXT, SNIPPET_IIIF_PREVIEW, SNIPPET_ID, \
    HIER_ES_LABEL

IIIF_IMAGE_SERVER = "https://ub-sipi.ub.unibas.ch"

class DigibasView(RDVView):
    fields_def_lookup = {}
    file = None
    snippet_fields_port =[]

    def get_obj_type(self, lang="de"):
        obj_type = self.page.get("type")
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        return obj_type

    def get_title(self):
        type_ = self.get_obj_type()
        title = self.page.get("title", [None, None])
        if isinstance(title, list):
            title = ", ".join([t["label"] for t in title if t])
        return title

    def get_manifest_id(self):
        manif_id = self.page.get("mets_iiifmanifest","")
        return manif_id

    def get_preview_image(self):
        iiif_images = self.page.get("mets_iiifpages", []) or self.page.get("thumbnails", [{}])
        pos = len(iiif_images) - 1 if len(iiif_images) < 3 else 0
        iiif_image = iiif_images[pos].get("url")
        if iiif_image:
            return iiif_image
        else:
            return ""

    def get_viewer(self):
        if not self.page.get("mets_iiifmanifest",""):
            return {"viewer": []}
        else:
            return {}

    def get_rdv_view(self, edit=False):
        view_format = super().get_rdv_view(edit=edit)
        cat_link = self.page.get("cat_link")
        if cat_link:
            descr_aleph_link = {"value": {"de": [{"label": "Primo",
                                                  "link": cat_link}]},
                                "label": {"de": ["Katalog-Link"]}}
            # add empty service_info for manif
            edit_info = self.get_edit_info()
            descr_aleph_link.update(edit_info)
            view_format["desc_metadata"].append(descr_aleph_link)
        return view_format

    def build_snippet(self):
        snippet = {}
        snippet[SNIPPET_ID] = self.page_id
        snippet[SNIPPET_TITLE] = self.get_title()
        snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type()
        # js -es int problem
        snippet[SNIPPET_SEARCH_AFTER] = json.dumps(self.page.get(SNIPPET_SEARCH_AFTER))
        snippet[SNIPPET_IIIF_PREVIEW] = self.get_preview_image()
        if self.page.get(SNIPPET_FULLTEXT):
            snippet[SNIPPET_FULLTEXT] = " ...</br>... ".join(self.page.get(SNIPPET_FULLTEXT, []))

        snippet["i18n"] = {}
        for lang in ["de", "en"]:
            snippet_values = {}
            snippet["i18n"][lang] = {}
            lang_snippet = snippet["i18n"][lang]
            port_fields = self.snippet_fields_port
            for es_field in port_fields:
                label =self.get_lang_label(self.fields_def.get(es_field), lang) or es_field
                if isinstance(self.page.get(es_field,[]), list):
                    values = self.extract_values(self.page.get(es_field,[]))
                    value = ", ".join(values)
                else:
                    value = self.page[es_field]
                    value = value.strip() if isinstance(value, str) else value
                if value:
                    snippet_values[es_field] = "{}: {}".format(label, value)

            lang_snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type(lang)
            lang_snippet[SNIPPET_LINE1] = ", ".join([snippet_values[f] for f in self.snippet_fields_port if f in snippet_values])
        snippet[SNIPPET_LINE1] = lang_snippet[SNIPPET_LINE1]
        snippet["link"] = []
        if self.page.get("sys_id", ""):
            dsv, sys = self.page.get("sys_id", "_").split("_")
            snippet["link"].append({"url": "https://aleph.unibas.ch/F/?local_base=DSV0{}&con_lng=GER&func=find-b&find_code=SYS&request={}".format(dsv, sys), "label": "Aleph-Link"})
        return snippet


class RDV2ES4Digibas(RDV2ES):
    index= "han_obj"
    dyn_manif = "digispace_han"
    max_ids_dyn_manif = 200
    view_class = DigibasView

    iiif = False

    def __init__(self, request_data, es_client, index, debug=False, iiif_host="https://ub-test-iiifpresentation.ub.unibas.ch", autocomplete=False, es_host=""):
        super().__init__(request_data, es_client, index, debug= debug, iiif_host=iiif_host, es_host=es_host, autocomplete=autocomplete)
        self.source_fields = []
        self.hierarchy_index = "han_hier"
        #self.top_hier_query = {"size": 500, "query": {"bool": {"must": {"ids": {"values": ["han_hier_5_000087462","han_hier_5_000087463","han_hier_5_000087464","han_hier_5_000087976"]}}}}}
        #self.hier_top_id = "han_hier_24.55."
        self.hierarchy_facet_sort = self.sort_han

    def get_pref_label_hier(self, es_rec):
        pos = es_rec["_source"]["position_label"][0] if es_rec["_source"]["position_label"] else ""
        label = es_rec["_source"][HIER_ES_LABEL][0] if es_rec["_source"][HIER_ES_LABEL] else ""
        return "{} {}".format(pos, label) if pos else label

    def sort_han(self, hier_entry):
        sort_value = hier_entry.get("label", "")
        return sort_value