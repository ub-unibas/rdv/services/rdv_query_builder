import json
import os
import socket
from rdv_query_builder import RDV2ES, RDVView
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import SNIPPET_SEARCH_AFTER, SNIPPET_TITLE, SNIPPET_OBJ_TYPE, SNIPPET_LINE1, SNIPPET_FULLTEXT, SNIPPET_IIIF_PREVIEW, SNIPPET_ID

IIIF_IMAGE_SERVER = "https://ub-sipi.ub.unibas.ch"

# hierarchy filter
# swiss tph bilder: https://ub-sipi.ub.unibas.ch/afrikaportal/swiss_tph-006080.jpg
# titel bei iiif ansicht


class AfricaView(RDVView):
    fields_def_lookup = {}
    snippet_fields = ["fct_person_organisation"]
    file = ""
    gd_service_file = "/home/martin/PycharmProjects/rdv_middlelayer/iiifafrikaportal-43daa897b38d.json" \
        if os.environ.get('RDV_DEBUG') else "/home/rdv/dev_rdv_proxy/iiifafrikaportal-43daa897b38d.json"
    spreadsheets = {
        "bab_library": "1Swxo2TRxAdx2sDCS7YkPbrsNO82IhuChe7gdk88o4Gk",
        "bab_archive": "1xpPaC6Y0jMTsM8mbT67gLayEPG9TGZVYxGA6Bliwje4",
        "mkb": "1GLJmER5ijvM7_TYZ8w_A2nRr8kKPTzmivqNLUU44Azc",
        "swiss_tph": "12QYqzamSj5-9EIlIQpCwNCOJfdHYAiM95yZkte42I6Y"
    }

    def get_inst_kuerzel(self):
        return self.page.get("main_institution")[0]

    def get_obj_type(self, lang="de"):
        id_jp2 = (self.page.get("field_digitalobject") or [""])[0]
        if id_jp2:
            return "Digitalisat"
        else:
            return self.page.get("providing_institution")[0] if self.page.get("providing_institution",[]) else "nicht definiert"

    def get_title(self):
        title = self.page.get("field_title") or "Kein Titel"
        if isinstance(title, list):
            values = self.extract_values(title)
            title = ", ".join(values)
        return title

    def get_manifest_id(self):
        # exclude too big manifests
        field_id = self.page.get("field_id")[0]
        manif_id = "/".join(["afrikaportal", field_id, "manifest"])
        return manif_id



    def get_preview_image(self):
        id_jp2 = (self.page.get("field_digitalobject") or [""])[0]
        if id_jp2:
            image = "{}/{}/{}".format(IIIF_IMAGE_SERVER, "afrikaportal", id_jp2)
            return image
        else:
            return ""

    def build_snippet(self):
        snippet = {}
        snippet[SNIPPET_ID] = self.page_id
        snippet[SNIPPET_TITLE] = self.get_title()
        snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type()
        # js -es int problem
        snippet[SNIPPET_SEARCH_AFTER] = json.dumps(self.page.get(SNIPPET_SEARCH_AFTER))

        snippet[SNIPPET_IIIF_PREVIEW] = self.get_preview_image()
        if self.page.get(SNIPPET_FULLTEXT):
            snippet[SNIPPET_FULLTEXT] = " ...</br>... ".join(self.page.get(SNIPPET_FULLTEXT, []))

        snippet["i18n"] = {}
        for lang in ["de", "en"]:
            snippet_values = {}
            snippet["i18n"][lang] = {}
            lang_snippet = snippet["i18n"][lang]
            snippet_fields = self.snippet_fields
            for es_field in snippet_fields:
                label = self.get_lang_label(self.fields_def.get(es_field), lang)
                if isinstance(self.page.get(es_field, []), list):
                    values = self.extract_values(self.page.get(es_field, []))
                    value = ", ".join(values)
                else:
                    value = self.page[es_field]
                    value = value.strip() if isinstance(value, str) else value
                if value:
                    snippet_values[es_field] = "{}: {}".format(label, value)

            lang_snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type(lang)
            lang_snippet[SNIPPET_LINE1] = ", ".join(
                [snippet_values[f] for f in self.snippet_fields if f in snippet_values])

        return snippet

class RDV2ES4Afrika(RDV2ES):
    view_class =AfricaView
    max_ids_dyn_manif = 200
    index= "afrikaportal"
    dyn_manif= "afrikaportal"
    highlight_included_fieldname = True


    def __init__(self, request_data, es_client, index, debug=False, iiif_host="https://ub-test-iiifpresentation.ub.unibas.ch", es_host="", autocomplete=False):

        super().__init__(request_data, es_client, index, iiif_host, es_host=es_host, autocomplete=autocomplete)
        self.hierarchy_index = "babthesaurus"
        self.source_fields = ["field_title","fct_institution","fct_person_organisation","field_digitalobject", "field_id", "main_institution", "providing_institution", "fct_topic", "fct_location"]



