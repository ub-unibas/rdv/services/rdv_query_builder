import json

from rdv_query_builder import RDV2ES, RDVView, RDVForm
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import SNIPPET_SEARCH_AFTER, SNIPPET_TITLE, SNIPPET_OBJ_TYPE, SNIPPET_LINE1, SNIPPET_FULLTEXT, SNIPPET_IIIF_PREVIEW, SNIPPET_ID

IIIF_IMAGE_SERVER = "https://ub-sipi.ub.unibas.ch"


class ITBView(RDVView):
    fields_def_lookup = {}
    file = "{}/fields_def_ITBView.xlsx".format(RDVView.config_folder)
    snippet_fields_port =["Wirkungszeit", "author", "year", "Drucker" ]

    def get_obj_type(self, lang="de"):
        obj_type = self.page.get("type")
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        return obj_type

    def get_inst_kuerzel(self):
        proj = self.page.get("proj_id")
        if isinstance(proj, list):
            return proj[0] or "not defined"
        else:
            return proj or "not defined"

    def get_title(self):
        type_ = self.get_obj_type()
        if type_ in ["Literatur", "BBKSKAUT"]:
            title = self.page.get("title", [None, None])
        elif type_ in ["Buchdrucker"]:
            title = self.page.get("Drucker","")
        else:
            title = self.page.get("title", [None, None])
        if isinstance(title, list):
            title = ", ".join([t["label"] for t in title if t])
        return title

    def get_manifest_id(self):
        manif_id = self.page.get("mets_iiifmanifest","")
        return manif_id

    def get_preview_image(self):
        iiif_images = self.page.get("mets_iiifpages", []) or self.page.get("thumbnails", [{}])
        pos = len(iiif_images) - 1 if len(iiif_images) < 3 else 0
        iiif_image = iiif_images[pos].get("url")
        if iiif_image:
            return iiif_image
        else:
            return ""

    def get_viewer(self):
        if not self.page.get("mets_iiifmanifest",""):
            return {"viewer": []}
        else:
            return {}

    def get_rdv_view(self, edit=False):
        view_format = super().get_rdv_view(edit=edit)
        cat_link = self.page.get("cat_link")
        if cat_link:
            descr_aleph_link = {"value": {"de": [{"label": "Primo",
                                                  "link": cat_link}]},
                                "label": {"de": ["Katalog-Link"]}}
            # add empty service_info for manif
            edit_info = self.get_edit_info()
            descr_aleph_link.update(edit_info)
            view_format["desc_metadata"].append(descr_aleph_link)
        return view_format

    def get_idlab_select(self, v, es_field):
        selection = {}
        if self.get_obj_type() in ["Buchdrucker"]:
            if es_field == "Drucker":
                selection = {'{}.id.keyword'.format(es_field): {'values': [
                    {'value': {'id': v.get("id")},
                     'label': v.get("label")}], 'operator': 'OR'},
                    '751a.id.keyword'.format(es_field): {'values': [
                        {'value': {'id': "(DE-588)4004617-5"},
                         'label': "Basel"}], 'operator': 'OR'},
                    '852b_4.keyword'.format(es_field): {'values': [
                        {'value': {'id': "A100"},
                         'label': "UBH Basel"}], 'operator': 'OR'},
                }
                if self.page.get("Wirkungszeit_von"):
                    selection.update({"year": {"values": [{"value": {"gte": "{}-01-01".format(self.page.get("Wirkungszeit_von")),
                                                                     "lte": "{}-12-31".format(self.page.get("Wirkungszeit_bis"))}}]}})
            elif es_field == "BBKSKAUT_Drucker":
                selection = {'Drucker.id.keyword'.format(es_field): {'values': [
                    {'value': {'id': v.get("id")},
                     'label': v.get("label")}], 'operator': 'OR'},
                    '751a.id.keyword'.format(es_field): {'values': [
                        {'value': {'id': "(DE-588)4004617-5"},
                         'label': "Basel"}], 'operator': 'OR'}
                    ,
                    'local_code.id.keyword'.format(es_field): {'values': [
                        {'value': {'id': "Referenzdruck UB Basel"},
                         'label': "Referenzdruck UB Basel"}], 'operator': 'OR'},
                }
                if self.page.get("Wirkungszeit_von"):
                    selection.update({"year": {"values": [{"value": {"gte": "{}-01-01".format(self.page.get("Wirkungszeit_von")),
                                                                     "lte": "{}-12-31".format(self.page.get("Wirkungszeit_bis"))}}]}})
        elif self.get_obj_type() in ["Drucke"]:
            if es_field == "Drucker":
                selection = {'{}.id.keyword'.format(es_field): {'values': [
                    {'value': {'id': v.get("id")},
                     'label': v.get("label")}], 'operator': 'OR'},
                }
        if not selection:
            selection = {'{}.id.keyword'.format(es_field): {'values': [
                {'value': {'id': v.get("id")},
                 'label': v.get("label")}], 'operator': 'OR'}}
        return selection

    def build_snippet(self):
        snippet = {}
        snippet[SNIPPET_ID] = self.page_id
        snippet[SNIPPET_TITLE] = self.get_title()
        snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type()
        # js -es int problem
        snippet[SNIPPET_SEARCH_AFTER] = json.dumps(self.page.get(SNIPPET_SEARCH_AFTER))
        snippet[SNIPPET_IIIF_PREVIEW] = self.get_preview_image()
        if self.page.get(SNIPPET_FULLTEXT):
            snippet[SNIPPET_FULLTEXT] = " ...</br>... ".join(self.page.get(SNIPPET_FULLTEXT, []))

        snippet["i18n"] = {}
        for lang in ["de", "en"]:
            snippet_values = {}
            snippet["i18n"][lang] = {}
            lang_snippet = snippet["i18n"][lang]
            port_fields = self.snippet_fields_port
            for es_field in port_fields:
                label = {"year": "Druckjahr", "author": "Verfasser/in", "Drucker": "Drucker", "Wirkungszeit": "Wirkungszeit"}.get(es_field)
                if es_field in ["Drucker"] and self.get_obj_type() == "Buchdrucker":
                    continue
                if isinstance(self.page.get(es_field,[]), list):
                    values = self.extract_values(self.page.get(es_field,[]))
                    value = ", ".join(values)
                else:
                    value = self.page[es_field]
                    value = value.strip() if isinstance(value, str) else value
                if value:
                    snippet_values[es_field] = "<b>{}</b>: {}".format(label, value)

            lang_snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type(lang)
            lang_snippet[SNIPPET_LINE1] = "<br/>".join([snippet_values[f] for f in self.snippet_fields_port if f in snippet_values])
        snippet[SNIPPET_LINE1] = lang_snippet[SNIPPET_LINE1]
        snippet["link"] = []
        if self.page.get("sys_id", "") and 0:
            dsv, sys = self.page.get("sys_id", "_").split("_")
            snippet["link"].append({"url": "https://aleph.unibas.ch/F/?local_base=DSV0{}&con_lng=GER&func=find-b&find_code=SYS&request={}".format(dsv, sys), "label": "Aleph-Link"})
        return snippet

class ITBForm(RDVForm):
    # todo: definition hier fehleranfällig
    facet_search_endpoint = "{}v1/rdv_query/simple_facet_search/itb-test/"

    def __init__(self, host="http://127.0.0.1:5000", es_host=None, index=None):
        super().__init__(host= host, es_host=es_host, index=index)

        if host in ["http://127.0.0.1:5000/"]:
            self.facet_search_endpoint = "{}v1/rdv_query/simple_facet_search/itb-loc/"
        self.field_service = {
            "Quelle": self.search_quelle
        }

    def __call__(self, field, query, object_id=None,selected_entries=[]):
        self.object_id = object_id
        self.selected_entries = selected_entries
        set_ids = self.get_stored_ids(field.split(".")[0])

        results = self.field_service.get(field, self.search_varia)(query, field=field)
        cleansed_results= []
        for r in results:
            if r["value"]["id"] not in set_ids:
                cleansed_results.append(r)
        return cleansed_results[0:self.size]

    def search_varia(self, query="", field=""):
        results = self.query_facets(query, field="{}.keyword".format(field))
        return results


class RDV2ES4ITB(RDV2ES):
    index= "itb"
    max_ids_dyn_manif = 200
    dyn_manif= ""
    view_class = ITBView
    form_class = ITBForm
    iiif = False

    default_sort = [{"type.keyword": {"order": "asc", "unmapped_type": "text"}}]

    def __init__(self, request_data, es_client, index, debug=False, iiif_host="https://ub-test-iiifpresentation.ub.unibas.ch", autocomplete=False, es_host=""):
        super().__init__(request_data, es_client, index, debug= debug, iiif_host=iiif_host, es_host=es_host, autocomplete=autocomplete)
        self.source_fields = []
