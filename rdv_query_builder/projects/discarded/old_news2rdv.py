import json

from rdv_query_builder import RDV2ES
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import SNIPPET_SEARCH_AFTER, SNIPPET_TITLE, SNIPPET_OBJ_TYPE, SNIPPET_LINE1, SNIPPET_FULLTEXT, \
    SNIPPET_ID


class RDV2ES4OldNews(RDV2ES):
    fields_def_lookup = {}
    index= "old_ub_news"
    iiif = False

    def __init__(self, request_data, es_client, index, debug=False, iiif_host="https://ub-test-iiifpresentation.ub.unibas.ch", es_host="", autocomplete=False):
        # index so anpassen, dass iiif_links im gleichen feld wie bildpfade sind
        # emanuscripta links und erara links ergänzen
        self.simple_query_fields = ["content"]
        super().__init__(request_data, es_client, index,iiif_host=iiif_host, es_host=es_host, autocomplete=autocomplete)
        # todo: anpassen
        self.source_fields = ["title", "description", "fulltext", "published", "category", "autocomplete_fulltext"]

    def build_preview_snippet(self, result):
        port_objects = [self._es2dict(hit) for hit in result["hits"]["hits"]]
        preview_objects = []
        for z in port_objects:
            o = {}
            o[SNIPPET_ID] = z.get(SNIPPET_ID)

            # js -es int problem
            o[SNIPPET_SEARCH_AFTER] = json.dumps(z.get(SNIPPET_SEARCH_AFTER))
            o[SNIPPET_TITLE] = z.get("title")
            if isinstance(o[SNIPPET_TITLE], list):
                o[SNIPPET_TITLE] = ", ".join([i for i in o[SNIPPET_TITLE] if i])

            o[SNIPPET_OBJ_TYPE] = z.get("category")
            if isinstance(o[SNIPPET_OBJ_TYPE], list):
                o[SNIPPET_OBJ_TYPE] = ", ".join([i for i in o[SNIPPET_OBJ_TYPE] if i])

            o[SNIPPET_LINE1] = "Publiziert: {}".format(z.get("published"))

            if z.get(SNIPPET_FULLTEXT):
                o[SNIPPET_FULLTEXT] = " ...</br>... ".join(
                    z.get(SNIPPET_FULLTEXT, []))  # or [z.get("fulltext","")])
            else:
                o[SNIPPET_FULLTEXT] = z.get("description")
            o["fulltext"] = z.get("fulltext")

            preview_objects.append(o)
        return preview_objects
