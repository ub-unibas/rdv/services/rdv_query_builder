import json
from rdv_query_builder import RDV2ES, RDVView
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import SNIPPET_SEARCH_AFTER, SNIPPET_TITLE, SNIPPET_OBJ_TYPE, SNIPPET_LINE1, \
    SNIPPET_FULLTEXT, SNIPPET_IIIF_PREVIEW, SNIPPET_ID

IIIF_IMAGE_SERVER = "https://ub-sipi.ub.unibas.ch"

""" def get_manifest_id(self):
     manif_id = self.page.get("mets_iiifmanifest", "")
     return manif_id

 def get_preview_image(self):
     iiif_images = self.page.get("mets_iiifpages", [{}])
     iiif_image = iiif_images[0].get("url")
     if iiif_image:
         return iiif_image
     else:
         return """""
class TinitView(RDVView):
    fields_def_lookup = {}
    file = "{}/fields_def_TinitView.xlsx".format(RDVView.config_folder)
    snippet_fields_port = ["Signatur", "Schäden durch Objektart/Material"]

    @classmethod
    def get_groups(cls):
        return {"groups": {"Katalogdaten": {"open": True, "order": 1},
                           "Beschreibung Objektebene": {"open": False, "order": 2},
                           "Beschreibung Blattebene": {"open": False, "order": 3},
                           "Beschreibung Schreib- und Malmittelebene": {"open": False, "order": 4},
                           "Schäden auf Objektebene": {"open": False, "order": 5},
                           "Schäden auf Blattebene": {"open": False, "order": 6},
                           "Schäden auf Schreib- und Malmittelebene": {"open": False, "order": 7},
                           }
                }

    def get_obj_type(self, lang="de"):
        obj_type = self.page.get("Objektart") or "Buch"
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        return obj_type

    def get_title(self):
        title = self.page.get("Titel") or self.page.get("title") or self.page.get("descr_all")
        if isinstance(title, list):
            values = self.extract_values(title)
            title = ", ".join(values)
        return title

    def get_manifest_id(self):
        manif_id = "/".join(["tinti", self.page.get("bsiz_id"), "manifest"])
        return manif_id

    def get_preview_image(self):
        id_jp2 = self.page.get("jp2_files")
        if id_jp2:
            link = "{}/{}/{}".format(IIIF_IMAGE_SERVER, "tinti", id_jp2[0])
            return link
        else:
            return ""


    def get_viewer(self):
        if not self.page.get("mets_iiifmanifest", ""):
            return {"viewer": []}
        else:
            return {}

    def get_rdv_view(self, edit=False):
        view_format = super().get_rdv_view(edit=edit)
        cat_link = self.page.get("cat_link")
        if cat_link:
            descr_aleph_link = {"value": {"de": [{"label": "Primo",
                                                  "link": cat_link}]},
                                "label": {"de": ["Katalog-Link"]}}
            # add empty service_info for manif
            edit_info = self.get_edit_info()
            descr_aleph_link.update(edit_info)
            view_format["desc_metadata"].append(descr_aleph_link)
        return view_format

    def build_snippet(self):
        snippet = {}
        snippet[SNIPPET_ID] = self.page_id
        snippet[SNIPPET_TITLE] = self.get_title()
        snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type()
        # js -es int problem
        snippet[SNIPPET_SEARCH_AFTER] = json.dumps(self.page.get(SNIPPET_SEARCH_AFTER))
        snippet[SNIPPET_IIIF_PREVIEW] = self.get_preview_image()
        if self.page.get(SNIPPET_FULLTEXT):
            snippet[SNIPPET_FULLTEXT] = " ...</br>... ".join(self.page.get(SNIPPET_FULLTEXT, []))

        snippet["i18n"] = {}
        for lang in ["de", "en"]:
            snippet_values = {}
            snippet["i18n"][lang] = {}
            lang_snippet = snippet["i18n"][lang]
            port_fields = self.snippet_fields_port
            for es_field in port_fields:
                label = self.get_lang_label(self.fields_def.get(es_field), lang) or es_field
                if isinstance(self.page.get(es_field, []), list):
                    values = self.extract_values(self.page.get(es_field, []))
                    value = ", ".join(values)
                else:
                    value = self.page[es_field]
                    value = value.strip() if isinstance(value, str) else value
                if value:
                    snippet_values[es_field] = "{}: {}".format(label, value)

            lang_snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type(lang)
            lang_snippet[SNIPPET_LINE1] = ", ".join(
                [snippet_values[f] for f in self.snippet_fields_port if f in snippet_values])
        snippet[SNIPPET_LINE1] = lang_snippet[SNIPPET_LINE1]

        return snippet


class RDV2ES4Tinti(RDV2ES):
    index = "tinti_test"
    max_ids_dyn_manif = 200
    dyn_manif = "tinti"
    view_class = TinitView
    highlight_field = "Freitext*"
    highlight_included_fieldname = True
    iiif = True

    def __init__(self, request_data, es_client, index, debug=False,
                 iiif_host="https://ub-test-iiifpresentation.ub.unibas.ch", autocomplete=False, es_host=""):
        super().__init__(request_data, es_client, index, iiif_host=iiif_host, es_host=es_host,
                         autocomplete=autocomplete)
        self.hierarchy_index = ""  # "dsv05_hier"
        self.source_fields = []
        self.simple_query_fields = ["Freitext*"]

    def get_date_agg_interval(self, values, operator, max_days_aggr=367, field=None):
        return self._get_date_agg_interval(values, operator, max_days_aggr)