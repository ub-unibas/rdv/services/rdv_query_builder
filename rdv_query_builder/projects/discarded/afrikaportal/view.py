import json
import os
import socket
from rdv_query_builder import RDVView, RDVDatabase, RDVViewHier
from rdv_config_store_ubit import ConfigStore

config_store = ConfigStore()
config_store.load_project_config('afrikaportal')

# obligatory: get_title, get_obj_type
IIIF_IMAGE_SERVER = config_store.get_value('view.IIIF_IMAGE_SERVER')

class AfricaView(RDVView):
    """class to define snippet and view fields and settings"""
    # lookup file for form definition (same as view definition), needs to be defined, including config folder, e.g.: "{}/ZAS Vocab.xlsx".format(RDVView.config_folder)
    file = ""
    # needs to be defined in every subclass, so it is not inherited, stores data from form/view definition, do not overwrite
    fields_def_lookup = {}
    # which fields should be included in the snippet, order defined by order
    snippet_fields = config_store.get_value('view.snippet_fields')
    gd_service_file = config_store.get_value('view.gd_service_file')
    spreadsheets = {
        "bab_library": config_store.get_value('view.spreadsheets.bab_library'),
        "bab_archive": config_store.get_value('view.spreadsheets.bab_archive'),
        "africa_bsiz": config_store.get_value('view.spreadsheets.africa_bsiz'),
        "mkb": config_store.get_value('view.spreadsheets.mkb'),
        "swiss_tph": config_store.get_value('view.spreadsheets.swiss_tph'),
        "bablibrary2022": config_store.get_value('view.spreadsheets.bablibrary2022'),
        "babarchive2022" : config_store.get_value('view.spreadsheets.babarchive2022'),
        "stph" : config_store.get_value('view.spreadsheets.stph'),
        "stph_projects" : config_store.get_value('view.spreadsheets.stph_projects'),
        "stph_films" : config_store.get_value('view.spreadsheets.stph_films')
    }
    # this is the name of the environment variable which contains the google docs credentials
    # see https://pygsheets.readthedocs.io/en/stable/reference.html
    gd_service_account_environment_variable = config_store.get_value('view.gd_service_account_environment_variable')

    def get_fields_def(self, proj):

        if proj not in self.fields_def_lookup:
            try:
                spreadsheet_id = self.spreadsheets[proj]
                fields_def = self.get_gdocs(sheet_name="view_mapping", key_field="Feld", spreadsheet_id=spreadsheet_id)
                self.fields_def_lookup[proj] = fields_def
            except (KeyError, ImportError) as e:
                fields_def = self.create_viewdef_from_mapping(proj)
                import traceback
                traceback.print_exc()
                print("Institution {} within spreadsheets {} does not exist {}".format(proj, self.spreadsheets, e))
        else:
            fields_def = self.fields_def_lookup[proj]

        return fields_def

    def get_inst_kuerzel(self) -> str:
        """returns object type to control code flow (e.g. based on index)"""

        # old sources for the initial afrika portal use main_institution
        if "main_institution" in self.page:
            return self.page.get("main_institution")[0]
        # new sources use proj_id
        elif "proj_id" in self.page:
            return self.page.get("proj_id")
        else:
            # TODO remove this. For an unknown reason, some records don't have neither main_institution nor proj_id
            return "africa_bsiz"

    def get_obj_type(self, lang="de") -> str:
        """returns obj type for presentation, pass Es field for "obj_field" """
        id_jp2 = (self.page.get("field_digitalobject") or [""])[0]
        if id_jp2:
            return "Digitalisat"
        else:
            return self.page.get("providing_institution")[0] if self.page.get("providing_institution",[]) else "nicht definiert"

    def get_title(self) -> str:
        """returns title for presentation, pass Es field for "title_field" """
        title = self.page.get("field_title") or "Kein Titel"
        if isinstance(title, list):
            values = self.extract_values(title)
            title = ", ".join(values)
        return title

    def get_preview_image(self) -> str:
        """returns url for preview image in snippet view"""
        id_jp2 = (self.page.get("field_digitalobject") or [""])[0]
        if id_jp2:
            image = "{}/{}/{}".format(IIIF_IMAGE_SERVER, "afrikaportal", id_jp2)
            return image
        else:
            return ""

    def get_manifest_id(self) -> str:
        """return path to manifest without http-Domain,
        e.g. "/".join(["dizas", zas_id, "manifest"])"""
        # exclude too big manifests

        if "field_id" in self.page:
            field_id = self.page.get("field_id" or [""])[0]
            manif_id = "/".join(["afrikaportal", field_id, "manifest"])
            return manif_id
        else:
            return ""
