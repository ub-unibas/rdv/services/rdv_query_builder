from rdv_query_builder import RDV2ES
from rdv_config_store_ubit.config_store import ConfigStore
# obligatory: index and view_class

config_store = ConfigStore()
config_store.load_project_config('afrikaportal')

class RDV2ES4Afrika(RDV2ES):
    """class to define es functionalities (query builder)"""
    # define alias for index/indices
    index = config_store.get_value('rdv.index')
    # what is the name of the view class
    view_class = globals()[config_store.get_value('rdv.view_class')]
    # are there iiif compontents? = documentViewerProxyUrl in RDV Frontend
    iiif = config_store.get_value('rdv.iiif')
    # how many objects shall be combined to a dynamic manifest
    # limit size to a certain limit, because it is frontend controlled
    max_ids_dyn_manif = config_store.get_value('rdv.max_ids_dyn_manif')
    # what is the url prefix (ini-file name in es config) for dynamic manifests
    dyn_manif = config_store.get_value('rdv.dyn_manif')
    # the content from which field should be used for es_highlighting
    highlight_field = config_store.get_value('rdv.highlight_field')
    # if highlighting is used for multiple fields if fieldname shall be incluede
    highlight_included_fieldname = config_store.get_value('rdv.highlight_included_fieldname')


    def __init__(self, *args, **kwargs):
        """

        :param request_data:
        :param es_client:
        :param index: is ignored, used from class var
        :param debug:
        :param iiif_host:
        :param es_host:
        :param autocomplete:
        """
        super().__init__(*args, **kwargs)

        self.hierarchy_index = config_store.get_value('rdv.hierarchy_index')
        # definition how the hierarchy tree shall be sorted
        self.hierarchy_facet_sort = lambda x: str(x.get("label",""))
        # fields to be returned from Elasticsearch (_source), controlled from frontend if deleted here
        # use it here, if some fields shall not be made public or for performance reasons (e.g. large data/fulltext fields)
        self.source_fields = config_store.get_value('rdv.source_fields')
