import re
import json
from rdv_query_builder import RDV2ES, RDVView, RDVDatabase, RDVForm
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import SNIPPET_SEARCH_AFTER, SNIPPET_TITLE, SNIPPET_OBJ_TYPE, SNIPPET_LINE1, \
    SNIPPET_FULLTEXT, SNIPPET_IIIF_PREVIEW, SNIPPET_ID, \
    RQ_GTE, RQ_LTE, RDV_COUNT, RDV_LABEL


# bei Hierarchie Darstellung muss auch Information zur Ebene angezeigt werden
## Digitalisate hinzuziehen
## 990 f für Bestandsart
## 856u/z Digitalisate hinzuziehen
# Sortierung nach Hierarchie-Ebene oder nach Position im Bestand
# Kontext beim Record speichern

# Fehler bei Autor
# Sortierung nach Signatur
# korrekte Zuordnung der Signaturen

class SWAPDFDatabase(RDVDatabase):
    db_name = "swadok_pdfs_test"
    # needs to be defined in every subclass, so it is not inherited
    initialized = {}

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

class SWAPDFForm(RDVForm):
    # todo: definition hier fehleranfällig
    facet_search_endpoint = "{}v1/rdv_query/simple_facet_search/swa_pdfs-test/"

    def __init__(self, host="http://127.0.0.1:5000", es_host=None, index=None):
        super().__init__(host=host, es_host=es_host, index=index)

        if host in ["http://127.0.0.1:5000/"]:
            self.facet_search_endpoint = "{}v1/rdv_query/simple_facet_search/swa_pdfs-loc/"
        self.field_service = {
            "title2": self.search_sru_bsid_id
        }

    def __call__(self, field, query, object_id=None, selected_entries=[]):
        self.object_id = object_id
        self.selected_entries = selected_entries
        set_ids = self.get_stored_ids(field.split(".")[0])

        results = self.field_service.get(field, self.search_varia)(query, field=field)
        cleansed_results = []
        for r in results:
            if r["value"]["id"] not in set_ids:
                cleansed_results.append(r)
        return cleansed_results[0:self.size]

    def search_varia(self, query="", field=""):
        results = self.query_facets(query, field="{}.keyword".format(field))
        return results

    def search_sru_bsid_id(self, query="", field=""):
        facets = self.query_facets(field="title2.id.keyword", query=query)

        if (query.startswith("99") and query.endswith("5504")) or query.startswith("1_"):
            from rdv_marc_ubit import AlmaIZMarcJSONRecord
            try:
                al = AlmaIZMarcJSONRecord.get_sysid(query)
                title = al.get_title()
                facets = [{'label': title[0], 'value': {'id': title[1]}, 'service_label': '{} | {}'.format(*title),
                           'group': {'de': 'ALMA-Eintrag'}}]
            except TypeError:
                pass
        return facets


from rdv_query_builder import RDVViewHier


class SWAView(RDVViewHier, RDVView):
    fields_def_lookup = {}
    file = "{}/fields_def_SWAView.xlsx".format(RDVView.config_folder)
    snippet_fields_port = ["245c", "264c", "stw_ids", "descr_fuv", "descr_person", "490a"]
    hierarchy_index = "han_hier2"

    def get_obj_type(self, lang="de"):
        obj_type = self.page.get("swa_objecttype") or "Buch"
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        return obj_type

    def get_title(self):
        title = self.page.get("title", [None, None])
        if isinstance(title, list):
            title = ", ".join([t["label"] for t in title if t])
        return title

    def get_manifest_id(self):
        manif_id = self.page.get("mets_iiifmanifest", "")
        return manif_id

    def get_preview_image(self):
        iiif_images = self.page.get("mets_iiifpages", []) or self.page.get("thumbnails", [{}])
        pos = len(iiif_images) - 1 if len(iiif_images) < 3 else 0
        iiif_image = iiif_images[pos].get("url")
        if iiif_image:
            return iiif_image
        else:
            return ""

    def get_viewer(self):
        return {"viewer": ["MiradorSinglePage", "UV"]}

    def build_snippet(self):
        snippet = {}
        snippet[SNIPPET_ID] = self.page_id
        snippet[SNIPPET_TITLE] = self.get_title()
        snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type()
        # js -es int problem
        snippet[SNIPPET_SEARCH_AFTER] = json.dumps(self.page.get(SNIPPET_SEARCH_AFTER))
        snippet[SNIPPET_IIIF_PREVIEW] = self.get_preview_image()
        if self.page.get(SNIPPET_FULLTEXT):
            snippet[SNIPPET_FULLTEXT] = " ...</br>... ".join(self.page.get(SNIPPET_FULLTEXT, []))

        snippet["i18n"] = {}
        for lang in ["de", "en"]:
            snippet_values = {}
            snippet["i18n"][lang] = {}
            lang_snippet = snippet["i18n"][lang]
            port_fields = self.snippet_fields_port
            for es_field in port_fields:
                label = self.get_lang_label(self.fields_def.get(es_field), lang)
                if isinstance(self.page.get(es_field, []), list):
                    values = self.extract_values(self.page.get(es_field, []))
                    value = ", ".join(values)
                else:
                    value = self.page[es_field]
                    value = value.strip() if isinstance(value, str) else value
                if value:
                    snippet_values[es_field] = "<b>{}</b>: {}".format(label, value)

            lang_snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type(lang)
            lang_snippet[SNIPPET_LINE1] = "<br/>".join(
                [snippet_values[f] for f in self.snippet_fields_port if f in snippet_values])
        snippet[SNIPPET_LINE1] = lang_snippet[SNIPPET_LINE1]
        signatur = ", ".join(filter(lambda x: x.startswith("SWA") or x.startswith("UBW"), self.page.get("signatur")))
        snippet["link"] = []
        alma_id = self.page.get("bsiz_id", [])
        local_code = self.page.get("swa_local", [])
        link = True
        if link:
            if alma_id and "HANunikat" in local_code:
                snippet["link"].append({
                    "url": "https://swisscollections.ch/Record/{}".format(
                        alma_id),
                    "label": "swisscollections: {}".format(signatur)})
            elif alma_id:
                snippet["link"].append({
                    "url": "https://slsp-ubs.primo.exlibrisgroup.com/discovery/fulldisplay?docid=alma{}&vid=41SLSP_UBS:live".format(
                        alma_id),
                    "label": "swisscovery Basel: {}".format(
                        signatur) if signatur else "swisscovery Basel"})
            for url856 in self.page.get("856uz", []):
                url856["url"] = url856["link"]
                del url856["link"]
                snippet["link"].append(url856)
        return snippet

    def get_idlab_select(self, v, es_field):
        if es_field == "hier_path":
            values = []
            for e in self.page["hier_path"]:
                if e.get("hierarchy_filter")[0] in v.get("hierarchy_filter")[0]:
                    values.append({'value': {'id': e.get("id"), "pos": e.get("hierarchy_filter")[0]},
                                   'label': e.get("label"), 'operator': 'AND'})
            selection = {'hierarchy_filter_han2.keyword': {'values': values}}
            return selection

from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import HIER_ES_LABEL
class RDV2ES4SWA(RDV2ES):
    index = "swa_search"
    max_ids_dyn_manif = 50
    dyn_manif = "swasearch"
    view_class = SWAView
    highlight_field = "*"
    highlight_included_fieldname = True

    def __init__(self, request_data, es_client, index, debug=False,
                 iiif_host="https://ub-test-iiifpresentation.ub.unibas.ch", es_host="", autocomplete=False):
        super().__init__(request_data, es_client, index, debug=debug, iiif_host=iiif_host, es_host=es_host,
                         autocomplete=autocomplete)
        self.source_fields = []
        self.index = index
        self.hierarchy_index = {
            "hierarchy_filter": {
                "index_name": "stw",
                "top_query": {"size": 500, "query": {"bool": {"must_not": {"exists": {"field": "broader"}}, "must": {"exists": {"field": "hierarchy_filter"}}}}},
                "top_id": ""
            },
            "hierarchy_filter_han_hotel": {
                "index_name": "han_hier2",
                "top_query": {"size": 500, "query": {"bool": {"must": {"ids": {"values": ["han_hier_9972407514805504", "han_hier_9972407511105504", "han_hier_9972407512305504"]}}}}},
                "top_id": "han_hier_495."
            },
            "hierarchy_filter_han": {
                "index_name": "han_hier2",
                "top_query": {"size": 500, "query": {"bool": {"must_not": {"exists": {"field": "broader"}}, "must": {"exists": {"field": "hierarchy_filter"}}}}},
                "top_id": ""
            },
            "hierarchy_filter_person": {
                "index_name": "han_hier2",
                "top_query": {"size": 500, "query": {"bool": {"must_not": {"exists": {"field": "broader"}},
                                                              "must": {"terms": {"localcode": ["swapapnl"]}}}}},
                "top_id": ""
            },
            "hierarchy_filter_fuv": {
                "index_name": "han_hier2",
                "top_query": {"size": 500, "query": {"bool": {"must_not": {"exists": {"field": "broader"}},
                                                              "must": {"terms": {"localcode": ["swapafirma", "swapaverband"]}}}}},
                "top_id": ""
            },
            "hierarchy_filter_han": {
                "index_name": "han_hier2",
                "top_query": {"size": 500, "query": {"bool": {"must_not": {"exists": {"field": "broader"}},
                                                              "must": {"terms": {"localcode": ["swapapnl", "swapafirma", "swapaverband"]}}}}},
                "top_id": ""
            },
        }
        # self.hierarchy_index = "stw"
        #self.hierarchy_facet_sort = self.sort_sign
        # self.hierarchy_index = "han_hier"
        # self.hier_top_id = "han_hier_495." # "han_hier_9972407378105504" #
        # self.top_hier_query = {"size": 500, "query": {"bool": {"must": {"ids": {"values": ["han_hier_9972407514805504", "han_hier_9972407511105504", "han_hier_9972407512305504"]}}}}}
        self.simple_query_fields = ["descr_fuv.*", "descr_person.*", "decr_person.*", "245c", "title.*", "520a"]

        self.default_int_range = {RQ_GTE: 0, RQ_LTE: 1000}

    def get_pref_label_hier(self, es_rec, hier_pos_field):
        #HIER_ES_LABEL

        label = es_rec["_source"]["aktenbildner"][0].get("label") if isinstance(es_rec["_source"].get("aktenbildner"), list) \
            else es_rec["_source"].get("aktenbildner")
        if not label:
            label = es_rec["_source"][HIER_ES_LABEL][0] if isinstance(es_rec["_source"][HIER_ES_LABEL], list) \
                else es_rec["_source"][HIER_ES_LABEL]
        if hier_pos_field == "hierarchy_filter_han2":
            position = es_rec["_source"]["position_label"][0] if isinstance(es_rec["_source"]["position_label"], list) and es_rec["_source"]["position_label"] \
                else es_rec["_source"]["position_label"]
            return " ".join([p for p in [position, label] if p])
        else:
            return label


    def sort_stw(self, x):
        stw_re = re.compile("^[ABGNPVW][ \.]")
        if stw_re.search(x[RDV_LABEL]):
            try:
                return (0, x[RDV_LABEL], x.get(RDV_COUNT, ""))
            except IndexError:
                return (0, x[RDV_LABEL])
        else:
            # * -1 to get correct order
            try:
                return (1, x.get(RDV_COUNT, 0) * -1, x[RDV_LABEL])
            except IndexError:
                return (1, x[RDV_LABEL])

    def get_date_agg_interval(self, values, operator, max_days_aggr=36551, min_date_aggr=365, field=None):
        return self._get_date_agg_interval(values, operator, max_days_aggr, min_date_aggr)


class SWASachdokView(SWAView):
    fields_def_lookup = {}

    def build_snippet(self):
        snippet = {}
        snippet[SNIPPET_ID] = self.page_id
        snippet[SNIPPET_TITLE] = self.get_title()
        snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type()
        # js -es int problem
        snippet[SNIPPET_SEARCH_AFTER] = json.dumps(self.page.get(SNIPPET_SEARCH_AFTER))
        snippet[SNIPPET_IIIF_PREVIEW] = self.get_preview_image()
        if self.page.get(SNIPPET_FULLTEXT):
            snippet[SNIPPET_FULLTEXT] = " ...</br>... ".join(self.page.get(SNIPPET_FULLTEXT, []))

        snippet["i18n"] = {}
        for lang in ["de", "en"]:
            snippet_values = {}
            snippet["i18n"][lang] = {}
            lang_snippet = snippet["i18n"][lang]
            port_fields = self.snippet_fields_port
            for es_field in port_fields:
                label = {"author": "Autor", "year": "Erscheinungsjahr"}.get(es_field, es_field)
                if isinstance(self.page.get(es_field, []), list):
                    values = self.extract_values(self.page.get(es_field, []))
                    value = ", ".join(values)
                else:
                    value = self.page[es_field]
                    value = value.strip() if isinstance(value, str) else value
                if value:
                    snippet_values[es_field] = "{}: {}".format(label, value)

            lang_snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type(lang)
            lang_snippet[SNIPPET_LINE1] = ", ".join(
                [snippet_values[f] for f in self.snippet_fields_port if f in snippet_values])
        snippet[SNIPPET_LINE1] = lang_snippet[SNIPPET_LINE1]
        signatur = ", ".join(filter(lambda x: x.startswith("SWA") or x.startswith("UBW"), self.page.get("signatur", [])))
        snippet["link"] = []
        alma_id = self.page.get("bsiz_id", [])
        if not self.page.get("pdfs"):
            snippet["link"].append({
                "url": "https://slsp-ubs.primo.exlibrisgroup.com/discovery/fulldisplay?docid=alma{}&vid=41SLSP_UBS:live".format(
                    alma_id),
                "label": "siwsscovery Basel"})
            snippet["link"].extend(
                [{"url": v.get("link"), "label": v.get("label")} for v in self.page.get("swa_links", [])])
        elif 0:
            snippet["link"].append({
                "url": "https://slsp-ubs.primo.exlibrisgroup.com/discovery/fulldisplay?docid=alma{}&vid=41SLSP_UBS:live".format(
                    alma_id),
                "label": "siwsscovery Basel"})
            snippet["link"].extend(
                [{"url": v.get("link"), "label": v.get("label")} for v in self.page.get("pdfs", [])])
        return snippet





class SWAPDFView(SWAView):
    file = "{}/fields_def_SWAPDFView.xlsx".format(RDVView.config_folder)
    fields_def_lookup = {}
    db_class = SWAPDFDatabase
    snippet_fields_port = ["page_sysid"]

    def get_inst_kuerzel(self):
        return "swadok_pdfs"

    def get_obj_type(self, lang="de"):
        obj_type = self.page.get("type") or "PDF"
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        return obj_type

    def get_title(self):
        parent_accordion = str(self.page.get("parent_accordion", "") or "")
        link_text = str(self.page.get("link_text", "") or "")
        filename = self.page.get("filename", "")
        title = " | ".join([t for t in [filename, parent_accordion, link_text] if t])
        if not title:
            title = self.page.get("filename", "")
        return title

    def build_snippet(self):
        snippet = {}
        snippet[SNIPPET_ID] = self.page_id
        snippet[SNIPPET_TITLE] = self.get_title()
        snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type()
        # js -es int problem
        snippet[SNIPPET_SEARCH_AFTER] = json.dumps(self.page.get(SNIPPET_SEARCH_AFTER))
        snippet[SNIPPET_IIIF_PREVIEW] = self.get_preview_image()
        if self.page.get(SNIPPET_FULLTEXT):
            snippet[SNIPPET_FULLTEXT] = " ...</br>... ".join(self.page.get(SNIPPET_FULLTEXT, []))

        snippet["i18n"] = {}
        for lang in ["de", "en"]:
            snippet_values = {}
            snippet["i18n"][lang] = {}
            lang_snippet = snippet["i18n"][lang]
            port_fields = self.snippet_fields_port
            for es_field in port_fields:
                label = {"author": "Autor", "year": "Erscheinungsjahr"}.get(es_field, es_field)
                if isinstance(self.page.get(es_field, []), list):
                    values = self.extract_values(self.page.get(es_field, []))
                    value = ", ".join(values)
                else:
                    value = self.page[es_field]
                    value = value.strip() if isinstance(value, str) else value
                if value:
                    snippet_values[es_field] = "{}: {}".format(label, value)

            lang_snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type(lang)
            lang_snippet[SNIPPET_LINE1] = ", ".join(
                [snippet_values[f] for f in self.snippet_fields_port if f in snippet_values])
        snippet[SNIPPET_LINE1] = lang_snippet[SNIPPET_LINE1]
        # signatur = ", ".join(filter(lambda x: x.startswith("SWA") or x.startswith("UBW"), self.page.get("signatur")))
        snippet["link"] = []
        return snippet

class RDV2ES4SWASACHDOK(RDV2ES4SWA):
    index = "swa_kleinschriften"
    view_class = SWAPDFView

class RDV2ES4SWAPDF(RDV2ES4SWA):
    index = "swadok_pdfs"
    view_class = SWAPDFView
    form_class = SWAPDFForm
