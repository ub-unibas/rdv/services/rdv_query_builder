import json
from rdv_query_builder import RDV2ES, RDVView
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import SNIPPET_SEARCH_AFTER, SNIPPET_TITLE, SNIPPET_OBJ_TYPE, SNIPPET_LINE1, SNIPPET_FULLTEXT, SNIPPET_IIIF_PREVIEW, SNIPPET_ID, \
    RDV_SORT

IIIF_IMAGE_SERVER = "https://ub-sipi.ub.unibas.ch"

class PortraetView(RDVView):
    fields_def_lookup = {}
    file = "{}/fields_def_PortraetView.xlsx".format(RDVView.config_folder)
    snippet_fields_port =["600", "100_1_a", "264__1_c",]

    def get_obj_type(self, lang="de"):
        obj_type = self.page.get("Objekttyp2") or "Porträt"
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        return obj_type

    def get_title(self):
        title = self.page.get("245_ab")
        if isinstance(title, list):
            title = ", ".join([t for t in title if t])
        return title

    def get_manifest_id(self):
        manif_id = "/".join(["portraets", self.page.get("bsiz_id"), "manifest"])
        return manif_id

    def get_preview_image(self):
        id_jp2 = self.page.get("jp2_files")
        if id_jp2:
            link = "{}/{}/{}".format(IIIF_IMAGE_SERVER, "portraets", id_jp2[0])
            return link
        else:
            return ""

    def get_viewer(self):
        return {"viewer": ["MiradorSinglePage"]}

    def build_snippet(self):
        snippet = {}
        snippet[SNIPPET_ID] = self.page_id
        snippet[SNIPPET_TITLE] = self.get_title()
        snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type()
        # js -es int problem
        snippet[SNIPPET_SEARCH_AFTER] = json.dumps(self.page.get(SNIPPET_SEARCH_AFTER))
        snippet[SNIPPET_IIIF_PREVIEW] = self.get_preview_image()
        if self.page.get(SNIPPET_FULLTEXT):
            snippet[SNIPPET_FULLTEXT] = " ...</br>... ".join(self.page.get(SNIPPET_FULLTEXT, []))

        snippet["i18n"] = {}
        for lang in ["de", "en"]:
            snippet_values = {}
            snippet["i18n"][lang] = {}
            lang_snippet = snippet["i18n"][lang]
            port_fields = self.snippet_fields_port

            for es_field in port_fields:
                label =self.get_lang_label(self.fields_def.get(es_field), lang) or "Zeitraum"
                if isinstance(self.page.get(es_field,[]), list):
                    values = self.extract_values(self.page.get(es_field,[]))
                    value = ", ".join(values)
                else:
                    value = self.page[es_field]
                    value = value.strip() if isinstance(value, str) else value
                if value:
                    snippet_values[es_field] = "<b>{}</b>: {}".format(label, value)

            lang_snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type(lang)
            lang_snippet[SNIPPET_LINE1] = "</br> ".join([snippet_values[f] for f in self.snippet_fields_port if f in snippet_values])
        snippet[SNIPPET_LINE1] = lang_snippet[SNIPPET_LINE1]

        return snippet

class RDV2ES4PORT(RDV2ES):
    index= "portraets2"
    max_ids_dyn_manif = 200
    dyn_manif= "portraets"
    view_class = PortraetView

    def __init__(self, request_data, es_client, index, debug=False, iiif_host="https://ub-test-iiifpresentation.ub.unibas.ch", es_host="", autocomplete=False):
        super().__init__(request_data, es_client, index, debug=debug, iiif_host=iiif_host, es_host=es_host, autocomplete=autocomplete)
        self.source_fields = []


class MF226View(PortraetView):
    fields_def_lookup = {}
    file = "{}/fields_def_MF226View.xlsx".format(RDVView.config_folder)
    snippet_fields_port =["orig_sign", "sru_sign"]

    def get_manifest_id(self):
        manif_id = self.page.get("sru_iiif_manif","")
        if not manif_id and self.page.get("jp2_files"):
            manif_id = "/".join(["mf226", self.page.get("bsiz_id"), "manifest"])
        return manif_id

    def get_preview_image(self):
        id_jp2 = self.page.get("sru_iiif_preview")
        if id_jp2:
            return id_jp2
        elif self.page.get("jp2_files"):
            link = "{}/{}/{}".format(IIIF_IMAGE_SERVER, "mf226", self.page.get("jp2_files")[0])
            return link
        else:
            return ""

    def get_obj_type(self, lang="de"):
        obj_type = self.page.get("520_a") or "Musik-Mikrofilm"
        if isinstance(obj_type, list):
            obj_type = ", ".join([t.get("label") for t in obj_type if t])
        return obj_type

class RDV2ES4MF226(RDV2ES4PORT):
    index= "mf226"
    max_ids_dyn_manif = 200
    dyn_manif= "mf226"
    view_class = MF226View

from rdv_query_builder import RDVViewHier

class BurckhardtView(PortraetView, RDVViewHier):
    fields_def_lookup = {}
    file = "{}/fields_def_BurckhardtView.xlsx".format(RDVView.config_folder)
    snippet_fields_port = ["830_a"]
    hierarchy_index = "han_hier2"
    hier_level4links = 3

    def get_title(self):
        title = self.page.get("245_ab")
        hier_pos_label = self.page.get("signatur", [""])[0]
        if isinstance(title, list):
            title = ", ".join([t for t in title if t])
        return hier_pos_label + ": " + title

    def get_obj_type(self, lang="de"):
        obj_type = self.page.get("Objekttyp2") or "Mappentitel"
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        return obj_type

    def get_manifest_id(self):
        manif_id = self.page.get("mets_iiifmanifest","")
        return manif_id

    def get_preview_image(self):
        iiif_images = self.page.get("iiif_imgs", [])
        if iiif_images:
            pos = len(iiif_images) - 1 if len(iiif_images) < 3 else 1
            iiif_image = iiif_images[pos].get("url")
            if iiif_image:
                return iiif_image
            else:
                return ""

    def enrich_page(self):
        self.get_hier_entries(hier_field="hierarchy_filter_han2")
        #self.get_hiersub_entries(hier_field="hierarchy_filter_han2")
        if self.page.get("digitalisiert") != "Nein":
            self.page["rechtestatus"] = [{"link": "https://creativecommons.org/publicdomain/mark/1.0/deed", "label": "Public Domain, Bilder bereitgestellt von e-manuscripta.ch IIIF Infrastructure"}]

    def build_simple_hierarchy_search_body(self, hier_pos, hier_pos_field="hierarchy_filter_han2"):

        must_filters = [
            {"prefix": {"{}.keyword".format(hier_pos_field): {"value": hier_pos}}}
        ]
        count_body = {"query":{"bool":{"filter": must_filters}}}

        return count_body

    def get_idlab_select(self, v, es_field):
        if es_field == "hier_path":
            values = []
            for e in self.page["hier_path"]:

                if e.get("hierarchy_filter")[0] in v.get("hierarchy_filter")[0]:
                    values.append({'value': {'id': e.get("id"), "pos": e.get("hierarchy_filter")[0]},
                                   'label': e.get("label"), 'operator': 'AND'})
            selection = {'hierarchy_filter_han2.keyword': {'values': values}}
            return selection

from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import HIER_ES_LABEL
class RDV2ES4Burckhardt(RDV2ES4PORT):

    index= "bild_burckhardt"
    max_ids_dyn_manif = 50
    dyn_manif= "burckhardt"
    view_class = BurckhardtView
    hierarchy_index = "han_hier2"
    #{"830_a.keyword": {"order": "asc", "unmapped_type": "text"}},
    default_sort = [{"sort_sign.keyword": {"order": "asc", "unmapped_type": "text"}}]

    def __init__(self, request_data, es_client, index, debug=False, iiif_host="https://ub-test-iiifpresentation.ub.unibas.ch", es_host="", autocomplete=False):
        super().__init__(request_data, es_client, index, debug=debug, iiif_host=iiif_host, es_host=es_host, autocomplete=autocomplete)
        self.source_fields = []
        self.hierarchy_index = {
            "hierarchy_filter_han2": {
                "index_name": "han_hier2",
                "top_query": {"size": 500, "query": {"bool": {"must": {"ids": {
            "values": ["han_hier2_9972407385605504","han_hier2_9972407388605504","han_hier2_9972407388105504","han_hier2_9972407386705504"]}}}}},
                #todo: sollte ausgelesen und nicht statisch gesetz werden
                "top_id": "han_hier2_553.14.23.2."
            }
        }
        self.hierarchy_facet_sort = self.sort_sign
        #
        #self.hier_top_id = "han_hier2_553.14.23.2." # "han_hier_9972407387805504" #
        #self.top_hier_query = {"size": 500, "query": {"bool": {"must": {"ids": {"values": ["han_hier2_9972407385605504","han_hier2_9972407388605504","han_hier2_9972407388105504","han_hier2_9972407386705504"]}}}}}
        #self.hierarchy_facet_sort = self.sort_han
        #self.hierarchy_index = "han_hier2"

    def get_pref_label_hier(self, es_rec, hier_pos_field):
        label = es_rec["_source"][HIER_ES_LABEL][0] if isinstance(es_rec["_source"][HIER_ES_LABEL], list) \
            else es_rec["_source"][HIER_ES_LABEL]
        position = es_rec["_source"]["position_label"][0] if isinstance(es_rec["_source"]["position_label"], list) and es_rec["_source"]["position_label"] \
            else es_rec["_source"]["position_label"]
        return ": ".join([p for p in [position, label] if p])

    def sort_han(self, x):
        return x.get(RDV_SORT, "")

class BernoulliView(PortraetView, RDVViewHier):
    fields_def_lookup = {}
    file = "{}/fields_def_BernoulliView.xlsx".format(RDVView.config_folder)
    snippet_fields_port = ["264__1_ac"]
    hierarchy_index = "han_hier"
    hier_level4links = 3

    def get_obj_type(self, lang="de"):
        obj_type = self.page.get("Objekttyp2") or "Brief"
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        return obj_type

    def get_manifest_id(self):
        manif_id = self.page.get("mets_iiifmanifest","")
        return manif_id

    def get_preview_image(self):
        iiif_images = self.page.get("iiif_imgs", [])
        if iiif_images:
            pos = 0
            iiif_image = iiif_images[pos].get("url")
            if iiif_image:
                return iiif_image
            else:
                return ""

    def enrich_page(self):
        self.get_hier_entries(hier_field="hierarchy_filter")
        self.get_hiersub_entries(hier_field="hierarchy_filter")

    def build_simple_hierarchy_search_body(self, hier_pos, hier_pos_field="hierarchy_filter"):

        must_filters = [
            {"prefix": {"{}.keyword".format(hier_pos_field): {"value": hier_pos}}}
        ]
        count_body = {"query":{"bool":{"filter": must_filters}}}

        return count_body

    def get_idlab_select(self, v, es_field):
        if es_field == "hier_path":
            values = []
            for e in self.page["hier_path"]:
                if e.get("hierarchy_filter")[0] in v.get("hierarchy_filter")[0]:
                    values.append({'value': {'id': e.get("id"), "pos": e.get("hierarchy_filter")[0]},
                                   'label': e.get("label"), 'operator': 'AND'})
            selection = {'hierarchy_filter.keyword': {'values': values}}
        else:
            selection = {'{}.id.keyword'.format(es_field): {'values': [
                {'value': {'id': v.get("id")},
                 'label': v.get("label")}], 'operator': 'OR'}}
        return selection


class RDV2ES4Bernoulli(RDV2ES4PORT):
    index= "bernoulli"
    max_ids_dyn_manif = 200
    dyn_manif= "bernoulli"
    view_class = BernoulliView
    hierarchy_index = "han_hier"
    #default_sort = [{"hierarchy_filter.keyword": {"order": "asc", "unmapped_type": "text"}}]

    def __init__(self, request_data, es_client, index, debug=False, iiif_host="https://ub-test-iiifpresentation.ub.unibas.ch", es_host="", autocomplete=False):
        super().__init__(request_data, es_client, index, debug=debug, iiif_host=iiif_host, es_host=es_host, autocomplete=autocomplete)
        self.source_fields = []
        # todo: sollte ausgelesen und nicht statisch gesetz werden
        #self.hier_top_id = "han_hier_553.14.23.2." # "han_hier_9972407387805504" #
        #self.top_hier_query = {"size": 500, "query": {"bool": {"must": {"ids": {"values": ["han_hier_9972407385605504","han_hier_9972407388605504","han_hier_9972407388105504","han_hier_9972407386705504"]}}}}}
        #self.hierarchy_facet_sort = self.sort_han
        self.hierarchy_index = "han_hier"

    def sort_han(self, x):
        return x.get(RDV_SORT, "")