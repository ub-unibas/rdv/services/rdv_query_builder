import json

from rdv_query_builder import RDV2ES, RDVView
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import SNIPPET_SEARCH_AFTER, SNIPPET_TITLE, SNIPPET_OBJ_TYPE, SNIPPET_LINE1, SNIPPET_FULLTEXT, SNIPPET_IIIF_PREVIEW, SNIPPET_ID, \
    RQ_GTE, RQ_LTE


# Fehler bei Autor

class ViseView(RDVView):
    fields_def_lookup = {}
    file = ""
    snippet_fields_port =["attach_note", "lehrveranstaltung", "prio"]

    def get_obj_type(self, lang="de"):
        obj_type = ""#self.page.get("itemType") or ""
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        return obj_type

    def get_title(self):
        title = self.page.get("bib")
        if isinstance(title, list):
            title = ", ".join([t for t in title if t])
        return title

    def get_manifest_id(self):
        manif_id = ""
        return manif_id

    def get_preview_image(self):
        return self.page.get("thumbnail")

    def get_viewer(self):
        return {[None]}

    def build_snippet(self):

        # multiple ids abfangen
        # besser für jeden Pfad
        if 0:
            # hierarchy pfad auslesen
            hierarchy_filters = self.page.get("hierarchy_filter")
            ids = []
            for h_filters in hierarchy_filters:
                for h_filter in h_filters:
                    len_ids = len(h_filter.split("."))
                    id_parts = h_filter.split(".")
                    while len_ids > 0:
                        ids.append(".".join(id_parts[0:len_ids]))
                        len_ids -= 1
            ids = sorted(ids)
            from elasticsearch import Elasticsearch
            es = Elasticsearch("localhost:9209")
            entries = []
            for entry in es.search(index="vise_hier", body={"query":{"terms":{"hierarchy_filter": ids}}}).get("hits", {}).get("hits", []):
                entries.append({
                    "hierarchy_filter": entry.get("_source",{}).get("hierarchy_filter"),
                    "label" : entry.get("_source",{}).get("prefLabel")[0],
                    "id": entry.get("_id")
                    })
            entries = sorted(entries, key=lambda x : sorted(x.get("hierarchy_filter", [])))
            path = " - ".join([e["label"] for e in entries])
            self.page["lehrveranstaltung"] = path

        snippet = {}
        snippet[SNIPPET_ID] = self.page_id
        snippet[SNIPPET_TITLE] = self.get_title()
        snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type()
        # js -es int problem
        snippet[SNIPPET_SEARCH_AFTER] = json.dumps(self.page.get(SNIPPET_SEARCH_AFTER))
        snippet[SNIPPET_IIIF_PREVIEW] = self.get_preview_image()
        if self.page.get(SNIPPET_FULLTEXT):
            snippet[SNIPPET_FULLTEXT] = " ...</br>... ".join(self.page.get(SNIPPET_FULLTEXT, []))

        snippet["i18n"] = {}
        for lang in ["de", "en"]:
            snippet_values = {}
            snippet["i18n"][lang] = {}
            lang_snippet = snippet["i18n"][lang]
            port_fields = self.snippet_fields_port
            for es_field in port_fields:
                #label =self.get_lang_label(self.fields_def.get(es_field), lang) \
                label = {"lehrveranstaltung": "Lehrveranstaltung", "attach_note": "Dozierendenkommentar", "prio": "Relevanz"}.get(es_field, es_field)
                if isinstance(self.page.get(es_field,[]), list):
                    values = self.extract_values(self.page.get(es_field,[]))
                    value = ", ".join(values)
                else:
                    value = self.page[es_field]
                    value = value.strip() if isinstance(value, str) else value
                # damit Dozierendenkommentar nicht angezeigt wird
                if es_field == "attach_note":
                    snippet_values[es_field] = "{}".format(value)
                elif value:
                    snippet_values[es_field] = "{}: {}".format(label, value)

            lang_snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type(lang)
            lang_snippet[SNIPPET_LINE1] = "</br>".join([snippet_values[f] for f in self.snippet_fields_port if f in snippet_values])
        snippet[SNIPPET_LINE1] = lang_snippet[SNIPPET_LINE1]
        snippet["link"] = []
        for url in self.page.get("links", []):
            snippet["link"].append({"url": url.get("url"), "label": url.get("label")})

        return snippet

class RDV2ES4Vise(RDV2ES):
    index= "vise"
    iiif = False
    max_ids_dyn_manif = 0
    dyn_manif= ""
    view_class = ViseView
    highlight_field = "bib"
    default_sort = {}
    #default_sort = {"date": {"order": "desc"}}


    def __init__(self, request_data, es_client, index, debug=False, iiif_host="https://ub-test-iiifpresentation.ub.unibas.ch", es_host="", autocomplete=False):
        super().__init__(request_data, es_client, index, debug=debug, iiif_host=iiif_host, es_host=es_host, autocomplete=autocomplete)
        self.source_fields = []
        self.index = index
        self.hierarchy_index = "vise-hier"
        self.simple_query_fields = ["bib", "links.label", "attach_note", "lehrveranstaltung.*", "author.label"]
        self.default_int_range = {RQ_GTE: 0, RQ_LTE: 1000}
        self.hierarchy_facet_sort = self.sort_semester

    def sort_semester(self, hier_entry):
        sort_value = hier_entry.get("label", "")
        sort_value = sort_value.replace(" HS", " AA")
        sort_value = sort_value.replace(" FS", " ZZ")
        return sort_value

    def get_date_agg_interval(self, values, operator, max_days_aggr= 367, field=None):
        return self._get_date_agg_interval(values, operator, max_days_aggr,min_date_aggr=1)