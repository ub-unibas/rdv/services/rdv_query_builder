import json
from rdv_query_builder import RDV2ES, RDVView
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import SNIPPET_SEARCH_AFTER, SNIPPET_TITLE, SNIPPET_OBJ_TYPE, SNIPPET_LINE1, SNIPPET_FULLTEXT, SNIPPET_IIIF_PREVIEW, SNIPPET_ID


class NewspaperView(RDVView):
    fields_def_lookup = {}

    file = ""
    snippet_fields_clipping = ["Quelldatum", "Quellname"]
    snippet_fields_collection = ["min_date", "max_date", "articles"]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def get_preview_image(self):
        preview_link = ""
        if self.page.get("newspaper") in ["arbeitgeber", "wirtsozpol", "handelsztg"]:
            preview_link = "https://ub-sipi.ub.unibas.ch/impresso/{}".format(
                self.page["pages"][0].get("filename").split(".")[0])
        elif self.page.get("newspaper") in ["chrivoaub", "wandindes"]:
            preview_link = "https://ub-digiworker.ub.unibas.ch/volksbote/{}".format(
                self.page["pages"][0].get("filename"))
        elif self.page.get("newspaper") in ["wochnaaud", "avis", "avisba", "hochprdo"]:
            preview_link = "https://ub-digiworker.ub.unibas.ch/avis-blatt/{}".format(
                self.page["pages"][0].get("filename"))
        return preview_link

    def get_obj_type(self, lang="de"):
        obj_type = self.page["pages"][0].get("issue_type")
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        return obj_type

    def get_title(self):
        title = self.page.get("date_str")
        if isinstance(title, list):
            title = ", ".join([t for t in title if t])
        ausgabe = self.page.get("issue_number")

        if isinstance(ausgabe, list):
            ausgabe_str = "Nr. " + ", ".join([t for t in ausgabe if t])
        elif ausgabe:
            ausgabe_str = "Nr. " + ausgabe
        else:
            ausgabe_str = ""
        return " ".join([title, ausgabe_str])

    def get_viewer(self):
        return {"viewer": ["MiradorSinglePage", "UV"]}


    def get_manifest_id(self):
        # todo: exclude too big manifests?
        manif_id = "/".join(["ub-newspapers", self.page.get("manifest_id"), "manifest"])
        return manif_id

    def build_snippet(self):
        snippet = {}
        snippet[SNIPPET_ID] = self.page_id
        snippet[SNIPPET_TITLE] = self.get_title()
        snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type()
        # js -es int problem
        snippet[SNIPPET_SEARCH_AFTER] = json.dumps(self.page.get(SNIPPET_SEARCH_AFTER))
        if self.get_preview_image():
            snippet[SNIPPET_IIIF_PREVIEW] = self.get_preview_image()
        if self.page.get(SNIPPET_FULLTEXT):
            snippet[SNIPPET_FULLTEXT] = " ...</br>... ".join(self.page.get(SNIPPET_FULLTEXT, []))

        snippet[SNIPPET_LINE1] = "Zeitung: {}".format(self.page.get("newspaper"))

        return snippet

    def get_fields_def(self, proj):
        return {}

class RDV2ES4Newspaper(RDV2ES):
    index= "ub-newspapers"
    dyn_manif= "ub-newspapers"
    max_ids_dyn_manif = 200
    view_class = NewspaperView

    default_sort = {}
    # default_sort = {"pages.date": {"order": "desc", "unmapped_type": "date"}}
    #volltext facette
    #ausgbae facette
    #import handelszeitung
    #auswahl zeitung / zeitschrift
    #metadaten
    # manifeste richtig bauen

    def __init__(self, request_data, es_client, index, debug=False, iiif_host="https://ub-test-iiifpresentation.ub.unibas.ch", es_host="", autocomplete=False):
        super().__init__(request_data, es_client, index, iiif_host=iiif_host, es_host=es_host, autocomplete=autocomplete)
        self.source_fields = ["date", "filename", "newspaper", "issue_type", "issue_number", "page", "date_str", "manifest_id", "pages.filename", "pages.issue_type"]

    @staticmethod
    def _es2dict(hit):
        doc = {SNIPPET_ID: hit["_id"], SNIPPET_SEARCH_AFTER: hit["sort"],
               SNIPPET_FULLTEXT: hit.get("highlight", {}).get("fulltext")}
        doc.update(hit["_source"])
        return doc
