import json

from rdv_query_builder import RDV2ES, RDVView
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import SNIPPET_SEARCH_AFTER, SNIPPET_TITLE, SNIPPET_OBJ_TYPE, SNIPPET_LINE1, SNIPPET_FULLTEXT, SNIPPET_IIIF_PREVIEW, SNIPPET_ID, \
    RQ_GTE, RQ_LTE


# todo: absätze in snippets, ausklappbare Informationen, link als link, datum und stunden als facetten
# editierbar machen
#

class FortbildView(RDVView):
    fields_def_lookup = {}
    file = ""
    snippet_fields_port =["Name", "Kommentar", "Datum_von", "Datum_bis", "Stunden_total"]

    def get_obj_type(self, lang="de"):
        obj_type = self.page.get("Kategorie")
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        return obj_type

    def get_title(self):
        title = self.page.get("Bezeichnung")
        if isinstance(title, list):
            title = ", ".join([t["label"] for t in title if t])
        return title

    def get_manifest_id(self):
        manif_id = ""
        return manif_id

    def get_preview_image(self):
        return ""

    def get_viewer(self):
        return {[None]}

    def build_snippet(self):

        snippet = {}
        snippet[SNIPPET_ID] = self.page_id
        snippet[SNIPPET_TITLE] = self.get_title()
        snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type()
        # js -es int problem
        snippet[SNIPPET_SEARCH_AFTER] = json.dumps(self.page.get(SNIPPET_SEARCH_AFTER))
        snippet[SNIPPET_IIIF_PREVIEW] = self.get_preview_image()
        if self.page.get(SNIPPET_FULLTEXT):
            snippet[SNIPPET_FULLTEXT] = " ...</br>... ".join(self.page.get(SNIPPET_FULLTEXT, []))

        snippet["i18n"] = {}
        for lang in ["de", "en"]:
            snippet_values = {}
            snippet["i18n"][lang] = {}
            lang_snippet = snippet["i18n"][lang]
            port_fields = self.snippet_fields_port
            for es_field in port_fields:

                if es_field in ["gg_label", "author", "year"] and self.get_obj_type() == "Griechischer Geist":
                    continue
                label = es_field
                if isinstance(self.page.get(es_field, []), list):
                    values = self.extract_values(self.page.get(es_field, []))
                    value = "<br/>".join(values)
                else:
                    value = self.page[es_field]
                    value = value.strip() if isinstance(value, str) else value

                if value and es_field == "gg_titles":
                    snippet_values[es_field] = "{}:<br/>{}".format(label, value)
                elif value:
                    snippet_values[es_field] = "{}:{}".format(label, value).strip(":")

            lang_snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type(lang)
            lang_snippet[SNIPPET_LINE1] = "<br/>".join(
                [snippet_values[f] for f in self.snippet_fields_port if f in snippet_values])
        snippet[SNIPPET_LINE1] = lang_snippet[SNIPPET_LINE1]
        snippet["link"] = []
        if self.page.get("sys_id", "") and 0:
            dsv, sys = self.page.get("sys_id", "_").split("_")
            snippet["link"].append({
                                       "url": "https://aleph.unibas.ch/F/?local_base=DSV0{}&con_lng=GER&func=find-b&find_code=SYS&request={}".format(
                                           dsv, sys), "label": "Aleph-Link"})
        return snippet

class RDV2ES4Fortbild(RDV2ES):
    index= "fortbild"
    iiif = False
    max_ids_dyn_manif = 0
    dyn_manif= ""
    view_class = FortbildView
    highlight_field = ""


    def __init__(self, request_data, es_client, index, debug=False, iiif_host="https://ub-test-iiifpresentation.ub.unibas.ch", es_host="", autocomplete=False):
        super().__init__(request_data, es_client, index, debug=debug, iiif_host=iiif_host, es_host=es_host, autocomplete=autocomplete)
        self.source_fields = []
        self.index = index
        self.simple_query_fields = []
        self.default_int_range = {RQ_GTE: 0, RQ_LTE: 1000}
