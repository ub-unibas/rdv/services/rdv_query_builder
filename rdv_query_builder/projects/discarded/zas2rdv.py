import os
import re
from copy import deepcopy
import json
import socket
import datetime
import html
import requests

from rdv_query_builder import RDV2ES, RDVView, RDVForm, RDVDatabase
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import SNIPPET_SEARCH_AFTER, SNIPPET_TITLE, SNIPPET_OBJ_TYPE, SNIPPET_LINE1, SNIPPET_FULLTEXT, \
    SNIPPET_IIIF_PREVIEW, SNIPPET_ID, \
    RQ_GTE, RQ_LTE, RDV_COUNT, RDV_LABEL, RQ_FIELD





IIIF_IMAGE_SERVER = "https://ub-sipi.ub.unibas.ch"
IIIF_IMAGE_AUTH_SERVER = "https://ub-sipi-auth.ub.unibas.ch"

class ZasDatabase(RDVDatabase):
    db_name = "zas"
    # needs to be defined in every subclass, so it is not inherited
    initialized = {}

    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class ZasTestDatabase(ZasDatabase):
    db_name = "zas_test"
    # needs to be defined in every subclass, so it is not inherited
    initialized = {}

class ZasLocalTestDatabase(ZasDatabase):
    db_name = "zas_test"
    # needs to be defined in every subclass, so it is not inherited
    initialized = {}



class ZasView(RDVView):
    # if only defined in super class not "safe" against ZasIntView-Def
    fields_def_lookup = {}
    # todo : anpassen
    file = "{}/zas_portal.xlsx".format(RDVView.config_folder)
    snippet_fields_clipping = ["Quelldatum", "Quellname"]
    snippet_fields_collection = ["min_date", "max_date", "articles"]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def get_rdv_view(self, edit=False):
        view_format = super().get_rdv_view(edit=edit)
        sysnum = self.page.get("Systemnummer")
        if sysnum:
            descr_aleph_link = {"value": {"de": [{"label": "Aleph",
                                                  "link": "https://aleph.unibas.ch/F/?local_base=DSV01&con_lng=GER&func=find-b&find_code=SYS&request={}".format(sysnum)}]},
                                "label": {"de": ["Katalog-Link"]}}
            # add empty service_info for manif
            edit_info = self.get_edit_info()
            descr_aleph_link.update(edit_info)
            view_format["desc_metadata"].append(descr_aleph_link)
        return view_format

    @property
    def zas_type(self):
        return self.new_obj_type

    def get_inst_kuerzel(self):
        proj = self.page.get("proj_id")
        return proj

    def get_obj_type(self, lang="de"):
        if self.zas_type in ["dizas", "ezas", "swissdox"]:
            return "Zeitungsausschnitt" if lang == "de" else "Clipping"
        else:
            return "Dokumentensammlung" if lang == "de" else  "Document collection"

    def get_title(self):
        title = self.page.get("Titel") or self.page.get("title") or self.page.get("descr_all")
        if isinstance(title, list):
            values = self.extract_values(title)
            title = ", ".join(values)
        return title

    def get_manifest_id(self):
        # exclude too big manifests
        if self.zas_type not in ["dizas", "ezas", "swissdox"] and self.page.get("articles", 0) > 300:
            return ""
        else:
            test = "" if not self.test else "test"
            zas_id = self.page.get("zas_id")
            manif_id = "/".join([test + self.zas_type, zas_id, "manifest"])
            return manif_id

    def get_preview_image(self):
        id_jp2 = (self.page.get("jp2_files") or [""])[0]
        if id_jp2:
            return "{}/{}/{}".format(IIIF_IMAGE_SERVER, self.zas_type, id_jp2)
        else:
            return ""

    def get_viewer(self):
        if self.zas_type in ["dizas", "ezas", "swissdox"]:
            return {"viewer": ["MiradorSinglePage", "UV"]}
        else:
            return {"viewer": ["UV"]}

    def build_snippets(self):
        lang_snippets = {}
        for lang in ["de", "en"]:
            lang_snippets[lang] = self.build_snippet(lang)
        return lang_snippets


    def build_snippet(self):
        snippet = {}
        snippet[SNIPPET_ID] = self.page_id
        snippet[SNIPPET_TITLE] = self.get_title()
        snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type()
        # js -es int problem
        snippet[SNIPPET_SEARCH_AFTER] = json.dumps(self.page.get(SNIPPET_SEARCH_AFTER))
        if self.zas_type in ["dizas", "ezas", "swissdox"]:
            snippet[SNIPPET_IIIF_PREVIEW] = self.get_preview_image()
            if self.page.get(SNIPPET_FULLTEXT):
                snippet[SNIPPET_FULLTEXT] = " ...</br>... ".join(self.page.get(SNIPPET_FULLTEXT, []))

        snippet["i18n"] = {}
        for lang in ["de", "en"]:
            snippet_values = {}
            snippet["i18n"][lang] = {}
            lang_snippet = snippet["i18n"][lang]
            zas_fields = self.snippet_fields_clipping + self.snippet_fields_collection
            for es_field in zas_fields:
                label =self.get_lang_label(self.fields_def.get(es_field), lang)
                if isinstance(self.page.get(es_field,[]), list):
                    values = self.extract_values(self.page.get(es_field,[]))
                    value = ", ".join(values)
                else:
                    value = self.page[es_field]
                    value = value.strip() if isinstance(value, str) else value

                if value:
                    # todo:
                    if es_field == "Quelldatum" :
                        date_value = datetime.datetime.strptime(value, "%Y-%m-%d")
                        if lang == "de":
                            value = date_value.strftime("%d.%m.%Y")
                        elif lang == "en":
                            value = date_value.strftime("%m/%d/%Y")
                    snippet_values[es_field] = "{}: {}".format(label, value)

            lang_snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type(lang)
            if self.zas_type in ["dizas", "ezas", "swissdox"]:
                lang_snippet[SNIPPET_LINE1] = ", ".join([snippet_values[f] for f in self.snippet_fields_clipping if f in snippet_values])
            else:
                lang_snippet[SNIPPET_LINE1] = ", ".join(
                    [snippet_values[f] for f in self.snippet_fields_collection if f in snippet_values])

        return snippet

class ZasAuthView(ZasView):

    def get_manifest_id(self):
        # exclude too big manifests
        if self.zas_type not in ["dizas", "ezas", "swissdox"] and self.page.get("articles", 0) > 300:
            return ""
        else:
            test = "" if not self.test else "test"
            zas_id = self.page.get("zas_id")
            manif_id = "/".join(["auth" + self.zas_type, zas_id, "manifest"])
            return manif_id

    def get_preview_image(self):
        id_jp2 = (self.page.get("jp2_files") or [""])[0]
        if id_jp2:
            return "{}/{}/{}".format(IIIF_IMAGE_AUTH_SERVER, self.zas_type, id_jp2)
        else:
            return ""

class ZasIntView(ZasView):
    # if only defined in super class not "safe" against ZasView-Def
    fields_def_lookup = {}
    file = "{}/zas_int.xlsx".format(RDVView.config_folder)
    # longer snippets for intern view
    snippet_fields_clipping = ZasView.snippet_fields_clipping + ["descr_fuv", "descr_person", "stw_ids"]
    db_class = ZasDatabase

class ZASForm(RDVForm):
    facet_search_endpoint = "{}v1/rdv_query/simple_facet_search/zas_int/"
    lookup_file = "{}/ZAS Vocab.xlsx".format(RDVView.config_folder)

    def __init__(self, host="http://127.0.0.1:5000", es_host=None, index=None):
        super().__init__(host= host, es_host=es_host, index=index)
        # todo: lookup für titel mit Systemnummer
        # todo: um place erweitern
        self.field_service = {
            "descr_fuv": self.search_fuv_gnd,
            "descr_person": self.search_person_gnd,
            "descr_place": self.search_place_gnd,
            "stw_ids": self.search_stw,
            "Quellname": self.search_newspaper,
            "Farbcode": self.search_code,
            "Sprache": self.search_language,
            "Autor": self.search_author,
            "title": self.search_cat_collection
        }

    def search_fuv_gnd(self, query=""):
        params = {"q": "type:CorporateBody AND preferredName:{}".format(query),
                  "format": "json:preferredName",
                  "size": self.size}
        field = "descr_fuv.id.keyword"
        suggest = {"format": "json:suggest"}
        return self.search_gnd(query, params, field, suggest)

    def search_person_gnd(self, query=""):
        params = {"q": query,
                  "format": "json:preferredName",
                  "filter": "+(type:DifferentiatedPerson) OR (type:Family)",
                  "size": self.size}
        field = "descr_person.id.keyword"
        suggest = {"format": "json:suggest"}
        return self.search_gnd(query, params, field, suggest)

    def search_place_gnd(self, query=""):
        results_ki = self.get_ki_proposals(field="ki_descr_place")
        params = {"q": query,
                  "format": "json:preferredName",
                  "filter": "+(type:PlaceOrGeographicName)",
                  "size": self.size}
        field = "descr_place.id.keyword"
        suggest = {"format": "json:suggest"}
        results = self.search_gnd(query, params, field, suggest)
        return results_ki + results

    def search_person(self, query=""):
        results = self.query_facets(query, field="descr_person.id.keyword")
        return results

    def search_cat_collection(self, query=""):
        results = self.query_facets(query, field="title.id.keyword")
        return results

    def search_newspaper(self, query=""):
        return self.search_lookup_value(field="Quellname.keyword", sheet="Zeitungen", key="Zeitung", query=query)

    def search_code(self, query=""):
        return self.search_lookup_value(field="Farbcode.keyword", sheet="interne Farbcodes", key="interne Farbcodes", query=query)

    def search_language(self, query=""):
        return self.search_lookup_value(field="Sprache.keyword", sheet="Sprache", key="Sprache", query=query)

    def get_ki_proposals_api(self, field):
        # todo: deactived to avoid dependencies
        try:
            fulltext = self.get_obj_entries(field="fulltext")
            ocr_lang = self.get_obj_entries(field="ocr_lang")
            if fulltext:
                fulltext = fulltext[0]
                lang = "FRENCH" if ocr_lang in ["OldFrench"] else "GERMAN"
                xml_temp = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n<RecordList>\r\n  <Record>\r\n  <Field name=\"Sprache\">{}</Field>" \
                           "\r\n       <Field name=\"Volltext\">{}</Field>\r\n </Record>\r\n</RecordList>\r\n".format(
                    lang, html.escape(fulltext))
                ki_values = self.get_deeptext_results(xml_temp).get(field, [])
        except Exception as e:
            ki_values = [{"value": {"id": "error"}, "service_label": "KI-Error {}, bitte UB-IT (mre) informieren".format(e), "group": {"de": "ZAS KI"}}]
            import traceback
            traceback.print_exc()
            return ki_values
        for ki_prop in ki_values:
            ki_prop.setdefault("value",{})["id"] = ki_prop["id"]
            del ki_prop["id"]
            ki_prop["service_label"] = "KI Vorschlag: {}".format(ki_prop["label"])
            ki_prop["group"] = {"de": "ZAS KI"}
        return ki_values

    def get_deeptext_results(self, doc):
        machine_values = {}
        threshold_level = 1
        from iiif_index_utils.enrichment.stw2es import DataThesaurusZas
        zhaw_ki_api = "http://172.17.0.2:9999/api/categories/" if os.environ.get('RDV_DEBUG') else "http://172.17.0.2:9999/api/categories/"
        thesaurus_zas = DataThesaurusZas(None)
        stw_concordance = thesaurus_zas.get_stw_concordance()
        payload = json.dumps({"abbyyOutput": doc, "fullpath": "true"})
        response = requests.post(zhaw_ki_api, data=payload).json()


        from iiif_index import DataZas
        for descriptor, accurancy in response.items():
            # level damit nicht gleichtlautende Werte von höheren Ebenen berücksichtigt werden
            level_max = max([item[0][1] for item in accurancy[2:]]) + 1
            new_t = stw_concordance.get(descriptor)
            if accurancy[0] > threshold_level and level_max == 4 and new_t:
                if new_t.get("successors"):
                    machine_values.setdefault("ki_" + DataZas.fieldname_stw_ids, []).extend(new_t["successors"])
                if new_t.get("places"):
                    machine_values.setdefault("ki_" + DataZas.fieldname_descr_place, []).extend(new_t["places"])
        return machine_values


    def get_ki_proposals(self, field):
        field_entries = self.get_obj_entries(field=field)
        for ki_prop in field_entries:
            ki_prop.setdefault("value",{})["id"] = ki_prop["id"]
            del ki_prop["id"]
            ki_prop["service_label"] = "KI Vorschlag: {}".format(ki_prop["label"])
            ki_prop["group"] = {"de": "ZAS KI"}
        return field_entries

    def search_stw(self, query=""):
        # todo später erweitern um echte STW-IDs
        # todo ergänzen um KI
        #results_ki = self.query_facets("", field="ki_stw_ids.id.keyword")
        #results_ki = self.get_ki_proposals(field="ki_stw_ids")
        results_ki = self.get_ki_proposals(field="ki_stw_ids")
        # to filter old not stw entries, those have no id -> id == label
        filter_not_stw = lambda x: x.get("label") != x.get("value",{}).get("id")
        results = list(filter(filter_not_stw, self.query_facets(query, field="stw_ids.id.keyword", remove_content_fields=["ki_stw_ids"])))
        return results_ki + results

    def search_author(self, query=""):
        results = self.query_facets(query, field="Autor.keyword")
        return results



class RDV2ES4ZAS(RDV2ES):
    test = False
    index = "zas_hidden"
    dyn_manif = "zas_flex_manifest"
    max_ids_dyn_manif = 200
    view_class = ZasView
    form_class = ZASForm
    highlight_field = "fulltext"
    # TODO: anpassen
    default_sort = [{"Quelldatum": {"order": "desc", "unmapped_type": "date"}}]

    def __init__(self, request_data, es_client, index, debug=False, iiif_host="https://ub-test-iiifpresentation.ub.unibas.ch", es_host="", autocomplete=False):

        super().__init__(request_data=request_data, es_client=es_client, index=index, iiif_host=iiif_host, debug=debug, autocomplete=autocomplete, es_host=es_host)
        self.source_fields = ["Titel", "Quellname", "Quelldatum", "jp2_files", "max_date", "min_date",
                              "title", "sub_index_name", "proj_id", "articles", "pages", "descr_all"]
        self.index = index
        self.hierarchy_index = "stw"
        self.simple_query_fields = ["fulltext", "descr_fuv.*", "stw_ids.*", "descr_person.*"]

        # field from rdv
        self.hierarchy_facet_sort = self.sort_stw
        self.default_date_range = {RQ_GTE: "1800-01-01", RQ_LTE: "2050-01-01"}
        self.default_int_range = {RQ_GTE: 0, RQ_LTE: 100}

    def build_hier_selection(self, field, id_, operator):
        try:
            # field = "hierarchy_filter"
            field = "hierarchy_filter.keyword"
            s_facets = self.selection["selection"]["facets"]
            label, pos = self.get_hier_descriptor_label_pos(id_)
            s_facets.setdefault(field, {}).setdefault("values",[]).append({"value": {"id":id_, "pos": pos}, "label": label})
            s_facets[field]["operator"] = operator
        except KeyError:
            import traceback
            traceback.print_exc()
            pass

    def get_hier_descriptor_label_pos(self, id_):
        #id_ = "stw_" + id_
        query = {"query": {"ids": {"values": [id_]}}, "_source": {"includes": ["prefLabel", "hierarchy_filter"]}, "size": 1}
        results = self.es.search(index=self.hierarchy_index, body=query)
        if results["hits"]["hits"]:
            label = results["hits"]["hits"][0]["_source"]["prefLabel"][0]
            pos = results["hits"]["hits"][0]["_source"]["hierarchy_filter"][0]
        else:
            label = ""
            pos= ""

        return label, pos


    def sort_stw(self, x):
        stw_re = re.compile("^[ABGNPVW][ \.]")
        if stw_re.search(x[RDV_LABEL]):
            try:
                return (0, x[RDV_LABEL], x.get(RDV_COUNT, ""))
            except IndexError:
                return (0, x[RDV_LABEL])
        else:
            # * -1 to get correct order
            try:
                return (1, x.get(RDV_COUNT, 0) * -1, x[RDV_LABEL])
            except IndexError:
                return (1, x[RDV_LABEL])

    def get_stw_autocomplete_data(self):
        stw_query = deepcopy(self.body)
        aggs = stw_query["aggs"]["all_facets"]["aggs"]
        del_keys = list(aggs.keys())
        del_keys.remove("stw_ids")
        for del_key in del_keys:
            del aggs[del_key]
        query_blob = json.dumps(stw_query)
        query_blob = query_blob.replace("stw_ids.id", "altLabel")
        query_blob = query_blob.replace("stw_ids.label", "altLabel")
        query_blob = query_blob.replace("[\"stw_ids\"]", "[\"altLabel\",\"prefLabel\"]")
        query_blob = query_blob.replace("stw_ids", "altLabel")
        query_blob = query_blob.replace("atomenergie.*", "Atomenergie")
        stw_query = json.loads(query_blob)
        result_stw = self.es.search(index=self.hierarchy_index, body=stw_query)
        stw_entries = result_stw["aggregations"]["all_facets"]["altLabel"]["filtered_altLabel.keyword"]["buckets"]
        add_labels = {}
        for stw_entry in stw_entries:
            key = stw_entry["key"]
            add_labels.setdefault(key, [])
            s = stw_entry["example"]["hits"]["hits"][0]["_source"]
            add_labels[key].extend(s.get("altLabel", []))
            add_labels[key].extend(s.get("prefLabel", []))
        autocomplete_stw_data = [{"label": b, "value": {"id": b}, "group": {"de": "STW Altlabels"},
                                  "service_label": "altLabels {}: {}".format(k, b)}
                                 for k, values in add_labels.items() for b in values]
        return autocomplete_stw_data

    def build_facet_autocomplete(self):
        if self.debug:
            print("query of build_facet_autocomplete (self.body)")
        result = self.es.search(index=self.index, body=self.body)
        try:
            autocomplete_stw_data = self.get_stw_autocomplete_dat()
        except Exception:
            autocomplete_stw_data = []
            # sortierung der Vorschläge überprüfen
        autocomplete_data = [{"label": b.get("label"), "value": {"id": b.get("value",{}).get("id", b.get("label"))}, "group": key,"service_label": b.get("label")  }
                        for key, facet_data in self.basic_facets.items() for b in self.get_buckets(result, key, facet_data[RQ_FIELD])]
        enriched_autocomplete = []

        # enrich with altlabels
        # todo: verbessern auf altlabels umstellen
        for field in autocomplete_data + autocomplete_stw_data:
            enriched_autocomplete.append(field)
            id_ = field.get("value",{}).get("id", field.get("label"))
            label = field.get("label")
            field.get("value", {})["id"] = field.get("label")
            group = field.get("group",{})
            if id_.startswith("http://zbw.eu/stw"):
                id_ = "{}_{}".format(self.hierarchy_index, id_)
                query = {"query":{"ids":{"values":[id_]}}}
                alt_labels = self.es.search(index=self.hierarchy_index, body=query)["hits"]["hits"][0].get("_source",{}).get("altLabel",[])
                for a_label in alt_labels:
                #a_label = ", ".join(alt_labels)
                    enriched_autocomplete.append({"group": group, "label": a_label, "value": {"id": a_label}, "service_label": "{} (Synonym: {})".format(a_label, label)})

        return enriched_autocomplete

class RDV2ES4ZASAUTH(RDV2ES4ZAS):
    dyn_manif = "authzas_flex_manifest"
    view_class = ZasAuthView

class RDV2ES4ZASINT(RDV2ES4ZAS):
    view_class = ZasIntView
    index = "zas"

    def __init__(self, request_data, es_client, index, debug=False, iiif_host="https://ub-test-iiifpresentation.ub.unibas.ch", es_host="", autocomplete=False):

        super().__init__(request_data, es_client, index=index, iiif_host=iiif_host, debug=debug, autocomplete=autocomplete, es_host=es_host)
        self.source_fields = ["Titel", "Quellname", "Quelldatum", "jp2_files", "max_date", "min_date",
                          "title", "sub_index_name", "proj_id", "articles", "pages", "descr_all", "descr_fuv", "descr_person", "stw_ids"]

    def build_subcat_aggs(self, result, rq_key, key):
        """nur eine Ebene, Vokabular wird in getrenntem Index vorgehalten"""

        #rewritten_results = self.rewrite_bucket_values(result["aggregations"]["all_facets"][rq_key]["filtered_" + key]["buckets"], key)
        rewritten_results = result.get("aggregations",{}).get("all_facets",{}).get(rq_key,{}).get("filtered_" + key,{}).get("buckets",[])
        # es_buckets = {b["key"]: {RDV_LABEL: b["key"], RDV_COUNT: b["doc_count"]} for b in rewritten_results}
        es_buckets = {self._bucket_map(bucket, key, type_="id")["label"]: self._bucket_map(bucket, key) for bucket in rewritten_results}

        # todo: Index-Query um top level values und subvalues zu erhalten
        # todo: Dossier noch richtig indexieren
        top_level_lookup = {"object_type" : ["Zeitungsausschnitt", "Dokumentensammlung"]}
        sub_values_lookup = {"Zeitungsausschnitt": ["retrodigitalisierter Zeitungsausschnitt", "elektronischer Zeitungsausschnitt (ab 2013)", "swissdox"],
                             "Dokumentensammlung": ["retrodigitalisierte Dokumentensammlung", "elektronische Dokumentensammlung (ab 2013)"]}
        top_level_values = top_level_lookup.get("object_type")

        es_sub_buckets = self.query_subcats(es_buckets, sub_values_lookup, rq_key, key)
        es_buckets.update(es_sub_buckets)

        # todo: anpassen für subcategories
        buckets = []
        for n, k in enumerate(top_level_values, start=1):
            try:
                if k in es_buckets and es_buckets[k][RDV_COUNT]:
                    top_bucket = self._bucket_subcat_map(es_buckets[k], sub_values_lookup.get(k), es_buckets, pos=1)
                    buckets.append(top_bucket)
            except KeyError:
                pass

        return buckets

class ZasTestView(ZasView):
    test = True

class ZasIntTestView(ZasIntView):
    test = True
    db_class = ZasLocalTestDatabase

class RDV2ES4ZASPWD(RDV2ES4ZAS):
    def get_manifest_id(self):
        # exclude too big manifests
        if self.zas_type not in ["dizas", "ezas", "swissdox"] and self.page.get("articles", 0) > 300:
            return ""
        else:
            test = "" if not self.test else "test"
            zas_id = self.page.get("zas_id")
            manif_id = "/".join([test + self.zas_type + "_pwd", zas_id, "manifest"])
            return manif_id

class RDV2ES4TESTZAS(RDV2ES4ZAS):
    test = True
    view_class = ZasTestView
    index = "zas_test_hidden"

class RDV2ES4TESTZASINT(RDV2ES4ZASINT):
    test = True
    view_class = ZasIntTestView
    index = "zas_test"

if __name__ == "__main__":
    ZasView.file = "../" + ZasView.file
    zas = ZasView.get_page(page_id="dizas2_681730", lang="de", index="zas", es_host="localhost:9209",
                           iiif_host="127.0.0.1:5001", rdv_host="127.0.0.1:5000")
    import pprint
    print(json.dumps(zas.return_rdv_view(edit=True)))
    #print(json.dumps(zas.build_snippet()))

if __name__ == "__main__":
    if 0:
        import pprint
        d = ZASForm(host="http://127.0.0.1:5000/")
        d.search_fuv_gnd("Suchard")
        d.search_author("Gm")
        d.search_stw("ba")
        d.search_person("blo")
        pprint.pprint(d.search_person_gnd("kreisky"))
        pprint.pprint(d.contr_vocab_lookup("Zeitungen", "Zeitung"))
        pprint.pprint(d.search_newspaper("Vater"))
        pprint.pprint(d.search_code("bl"))
        pprint.pprint(d.get_gnd_id("120607190"))
        pprint.pprint(d(field="descr_fuv", query="Suchard"))
