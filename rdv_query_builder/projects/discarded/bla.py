import json
from rdv_query_builder import RDV2ES, RDVView, RDVForm
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import SNIPPET_SEARCH_AFTER, SNIPPET_TITLE, SNIPPET_OBJ_TYPE, SNIPPET_LINE1, SNIPPET_FULLTEXT, SNIPPET_IIIF_PREVIEW, SNIPPET_ID




class BlaView(RDVView):
    fields_def_lookup = {}
    file = "{}/fields_def_BlaView.xlsx".format(RDVView.config_folder)
    snippet_fields_port =["typo3_lebensdaten"]

    def get_obj_type(self, lang="de"):
        obj_type = self.page.get("type")
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        return obj_type

    def get_title(self):
        title = self.page.get("title",[{}])[0].get("label") or self.page.get("name",[""])[0]["label"]
        if isinstance(title, list):
            title = ", ".join([t for t in title if t])
        return title

    def get_manifest_id(self):
        manif_id = ""
        return manif_id

    def get_preview_image(self):
        return self.page.get("thumbnail", "")

    def get_viewer(self):
        return ["Mirador"]

    @classmethod
    def get_groups(cls):
        return {"groups": {"leben": {"open": False, "order": 2}, "werk": {"open": True, "order": 1},
                   "name": {"open": True, "order": 1}}}

    def add_descr_data(self):
        if self.get_obj_type() == "AutorIn":
            edit_info = self.get_edit_info()

            autor = self.page["name"][0].get("label")
            primo_link = "https://slsp-ubs.primo.exlibrisgroup.com/discovery/search?query=" \
                         "creator,contains,{},OR&query=sub,contains,{},AND" \
                         "&tab=41SLSP_DN_CI&search_scope=DN_and_CI&vid=41SLSP_UBS:live&mode=advanced&offset=0".format(autor, autor)
            primo_entry = {"value": {"de": [{"label": "Werke von / Dokumente über: {}".format(autor),
                               "link": primo_link}]}
                ,"label": {"de": ["swisscovery"]}}
            primo_entry.update(edit_info)
            add_descr = [primo_entry]
        else:
            add_descr = []
        return add_descr

    def build_snippet(self):
        # todo: falls kein Bla-Eintrag - Link auf GND / Wikipedia
        snippet = {}
        snippet[SNIPPET_ID] = self.page_id
        snippet[SNIPPET_TITLE] = self.get_title()
        snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type()
        # js -es int problem
        snippet[SNIPPET_SEARCH_AFTER] = json.dumps(self.page.get(SNIPPET_SEARCH_AFTER))
        snippet[SNIPPET_IIIF_PREVIEW] = self.get_preview_image()
        if self.page.get(SNIPPET_FULLTEXT):
            snippet[SNIPPET_FULLTEXT] = " ...</br>... ".join(self.page.get(SNIPPET_FULLTEXT, []))

        snippet["i18n"] = {}
        for lang in ["de", "en"]:
            snippet_values = {}
            snippet["i18n"][lang] = {}
            lang_snippet = snippet["i18n"][lang]
            port_fields = self.snippet_fields_port
            for es_field in port_fields:
                label ={"name": "AutorIn", "typo3_lebensdaten": "Lebensdaten", "signatur": "Signatur"}.get(es_field, es_field)
                if isinstance(self.page.get(es_field,[]), list):
                    values = self.extract_values(self.page.get(es_field,[]))
                    value = ", ".join(values)
                else:
                    value = self.page[es_field]
                    value = value.strip() if isinstance(value, str) else value
                if value and self.get_title() != value:
                    snippet_values[es_field] = "{}: {}".format(label, value)

            lang_snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type(lang)
            lang_snippet[SNIPPET_LINE1] = ", ".join([snippet_values[f] for f in self.snippet_fields_port if f in snippet_values])
        snippet[SNIPPET_LINE1] = lang_snippet[SNIPPET_LINE1]
        snippet["link"] = []
        link = self.page.get("link", {})
        if link:
            try:
                id_ = self.page.get("name", [])[0].get("id","").split("/")[-1]
                name = self.page.get("name", [])[0].get("label","").split("/")[-1]

                snippet["link"].append({"url": "{}/de/detail/bla_authors_{}".format("https://ub-bla.ub.unibas.ch", id_),"label": "AutorIn im BLA: {}".format(name)})
            except (IndexError, KeyError):
                pass
            #snippet["link"].append({"url": link.get("url"), "label": "swisscovery Basel: {}".format(self.page.get("signatur")[0])})
            snippet["link"].append({"url": self.page.get("link").get("url"), "label": "swisscovery Basel: {}".format(self.page.get("signatur")[0])})

        if 0 and  not self.page.get("bla_entry") and self.page.get("quellen"):
            snippet["link"] = self.page.get("quellen")

        return snippet

class BlaForm(RDVForm):
    # todo: definition hier fehleranfällig
    facet_search_endpoint = "{}v1/rdv_query/simple_facet_search/bla-test/"
    def __init__(self, host="http://127.0.0.1:5000", es_host=None, index=None):
        super().__init__(host= host, es_host=es_host, index=index)

        if host in ["http://127.0.0.1:5000/"]:
            self.facet_search_endpoint = "{}v1/rdv_query/simple_facet_search/bla-loc/"
        self.field_service = {
        }

    def __call__(self, field, query, object_id=None,selected_entries=[]):
        self.object_id = object_id
        self.selected_entries = selected_entries
        set_ids = self.get_stored_ids(field.split(".")[0])

        results = self.field_service.get(field, self.search_varia)(query, field=field)
        cleansed_results= []
        for r in results:
            if r["value"]["id"] not in set_ids:
                cleansed_results.append(r)
        return cleansed_results[0:self.size]

    def search_varia(self, query="", field=""):
        results = self.query_facets(query, field="{}.keyword".format(field))
        return results

class RDV2ES4Bla(RDV2ES):
    index= "bla"
    max_ids_dyn_manif = 0
    dyn_manif= ""
    view_class = BlaView
    form_class = BlaForm
    highlight_field = ""
    iiif = False
    default_sort = [{"type.keyword": {"order": "asc", "unmapped_type": "keyword"}}, {"signatur.keyword": {"order": "asc", "unmapped_type": "keyword"}}]
    search_after_query = {"bool": {"must": [{"terms": {"type.keyword": ["AutorIn"]}}]}}

    def __init__(self, request_data, es_client, index, debug=False, iiif_host="https://ub-test-iiifpresentation.ub.unibas.ch", es_host="", autocomplete=False):
        super().__init__(request_data, es_client, index, debug=debug, iiif_host=iiif_host, es_host=es_host, autocomplete=autocomplete)
        self.source_fields = []
        self.index = index
        self.simple_query_fields = []

    def factet_query_def(self, facet_data, facet_search, label_field):
        if self.autocomplete:
            return "(({}.*)|(.*[ ,]{}.*))".format(facet_search, facet_search)
        elif label_field == "name.label.keyword":
            return facet_search + ".*"
        else:
            return ".*" + facet_search + ".*" if self.autocomplete else ".*" + facet_search + ".*"

    def facet_query_order(self, label_field):
        if label_field == "name.label.keyword":
            return {"_key": "asc"}
        else:
            return {"_count": "desc"}

