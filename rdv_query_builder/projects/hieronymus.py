import json
from rdv_query_builder import RDV2ES, RDVView
from rdv_data_helpers_ubit.projects.rdv_baseconfig.rdv_baseconfig import SNIPPET_SEARCH_AFTER, SNIPPET_TITLE, SNIPPET_OBJ_TYPE, SNIPPET_LINE1, SNIPPET_FULLTEXT, SNIPPET_IIIF_PREVIEW, SNIPPET_ID

IIIF_IMAGE_SERVER = "https://ub-sipi.ub.unibas.ch"

class HieronymusView(RDVView):
    fields_def_lookup = {}
    file = "{}/fields_def_HieronymusView.xlsx".format(RDVView.config_folder)
    snippet_fields_port =["gg_titles", "gg_label", "author", "year"]

    def get_inst_kuerzel(self):
        proj = self.page.get("proj_id")
        if isinstance(proj, list):
            return proj[0]
        else:
            return proj

    def get_obj_type(self, lang="de"):
        obj_type = self.page.get("type")
        if isinstance(obj_type, list):
            obj_type = ", ".join([t for t in obj_type if t])
        return obj_type

    def get_title(self):
        type_ = self.get_obj_type()
        title = self.page.get("title", [None, None])
        if isinstance(title, list):
            if isinstance(title[0], dict):
                title = ", ".join([t["label"] for t in title if t])
            else:
                title = title
        if self.page.get("type") and "Illustration" in self.page.get("type"):
            title = self.page.get("title")
        elif self.page.get("type") and "Griechischer Geist" in self.page.get("type"):
            title = self.page.get("gg_label")
        return title

    def get_manifest_id(self):
        if self.page.get("type") and "Exemplare UB Basel" in self.page.get("type"):
            manif_id = self.page.get("mets_iiifmanifest","")
            return manif_id
        elif self.page.get("type") and "Illustration" in self.page.get("type"):
            manif_id = self.page.get("img_file",{})[0]
            if manif_id:
                return "gg_img/{}/manifest".format(manif_id)
            else:
                return ""
        else:
            manif_id = self.page.get("gg_id",{})[0].get("id")
            if manif_id:
                return "gg/{}/manifest".format(manif_id)
            else:
                return ""

    def get_preview_image(self):
        if self.page.get("type") and ("Illustration" in self.page.get("type") or "Griechischer Geist" in self.page.get("type")):
            spez_images = self.page.get("img_file", []) or self.page.get("img_files", [])
            if spez_images:
                iiif_image = "https://ub-sipi.ub.unibas.ch/gg/{}".format(spez_images[0])
                return  iiif_image
            else:
                return ""
        else:
            iiif_images = self.page.get("mets_iiifpages", [])  or self.page.get("thumbnails", [{}])
            pos = len(iiif_images) - 1 if len(iiif_images) < 3 else 0
            iiif_image = iiif_images[pos].get("url")
            if iiif_image:
                return iiif_image
            else:
                return ""

    def get_viewer(self):
        if not self.page.get("mets_iiifmanifest",""):
            return {"viewer": []}
        else:
            return {}

    def get_rdv_view(self, edit=False):
        view_format = super().get_rdv_view(edit=edit)
        cat_link = self.page.get("cat_link")
        if cat_link:
            descr_aleph_link = {"value": {"de": [{"label": "Primo",
                                                  "link": cat_link}]},
                                "label": {"de": ["Katalog-Link"]}}
            # add empty service_info for manif
            edit_info = self.get_edit_info()
            descr_aleph_link.update(edit_info)
            view_format["desc_metadata"].append(descr_aleph_link)
        return view_format

    def build_snippet(self):
        snippet = {}
        snippet[SNIPPET_ID] = self.page_id
        snippet[SNIPPET_TITLE] = self.get_title()
        snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type()
        # js -es int problem
        snippet[SNIPPET_SEARCH_AFTER] = json.dumps(self.page.get(SNIPPET_SEARCH_AFTER))
        snippet[SNIPPET_IIIF_PREVIEW] = self.get_preview_image()
        if self.page.get(SNIPPET_FULLTEXT):
            snippet[SNIPPET_FULLTEXT] = " ...</br>... ".join(self.page.get(SNIPPET_FULLTEXT, []))

        snippet["i18n"] = {}
        for lang in ["de", "en"]:
            snippet_values = {}
            snippet["i18n"][lang] = {}
            lang_snippet = snippet["i18n"][lang]
            port_fields = self.snippet_fields_port
            for es_field in port_fields:

                if es_field in ["gg_label", "author", "year"] and self.get_obj_type() == "Griechischer Geist":
                    continue
                label = {"gg_titles": "<b>Enthaltene Werke an der UB Basel</b>", "gg_label": "", "author": "", "year": ""}.get(es_field)
                if isinstance(self.page.get(es_field,[]), list):
                    values = self.extract_values(self.page.get(es_field,[]))
                    value = "<br/>".join(values)
                else:
                    value = self.page[es_field]
                    value = value.strip() if isinstance(value, str) else value

                if value and es_field == "gg_titles":
                    snippet_values[es_field] = "{}:<br/>{}".format(label, value)
                elif value:
                    snippet_values[es_field] = "{}:{}".format(label, value).strip(":")

            lang_snippet[SNIPPET_OBJ_TYPE] = self.get_obj_type(lang)
            lang_snippet[SNIPPET_LINE1] = "<br/>".join([snippet_values[f] for f in self.snippet_fields_port if f in snippet_values])
        snippet[SNIPPET_LINE1] = lang_snippet[SNIPPET_LINE1]
        snippet["link"] = []
        if self.page.get("sys_id", "") and 0:
            dsv, sys = self.page.get("sys_id", "_").split("_")
            snippet["link"].append({"url": "https://aleph.unibas.ch/F/?local_base=DSV0{}&con_lng=GER&func=find-b&find_code=SYS&request={}".format(dsv, sys), "label": "Aleph-Link"})
        return snippet


class RDV2ES4Hieronymus(RDV2ES):
    index= "hieronymus"
    max_ids_dyn_manif = 200
    dyn_manif= "gg_flex"
    view_class = HieronymusView
    highlight_field = "gg_text"
    iiif = False
    #default_sort = [{"sort_obj": {"order": "desc", "unmapped_type": "text"}}]

    def __init__(self, request_data, es_client, index, debug=False, iiif_host="https://ub-test-iiifpresentation.ub.unibas.ch", autocomplete=False, es_host=""):
        super().__init__(request_data, es_client, index, debug= debug, iiif_host=iiif_host, es_host=es_host, autocomplete=autocomplete)
        self.source_fields = []
        self.simple_query_fields = ["gg_text", "title", "author"]
        self.hierarchy_index = "ggkapitel"
