import json

from rdv_query_builder import RDVView, RDVDatabase, RDVViewHier

# obligatory: get_title, get_obj_type

class ProjView(RDVView):
    """class to define snippet and view fields and settings"""
    # to define whether it is a test class, functionality needs to be defined in subclasses, e.g. adapt path, ...
    test = False
    # lookup file for form definition (same as view definition), needs to be defined, including config folder, e.g.: "{}/ZAS Vocab.xlsx".format(RDVView.config_folder)
    file = ""
    # needs to be defined in every subclass, so it is not inherited, stores data from form/view definition, do not overwrite
    fields_def_lookup = {}
    # which fields should be included in the snippet, order defined by order
    snippet_fields = []
    # class to write form data to database, subclass from RDVDatabase
    db_class = RDVDatabase
    # whether versions stored in mongodb for an object shall be accessible while editing
    show_versions = False

    ## if also inherits from RDVViewHier class
    # needs to be defined here to next to RDV2ES class
    hierarchy_index = ""
    # at which level links at detail page shall be provided
    hier_level4links = 0

    def get_inst_kuerzel(self) -> str:
        """returns object type to control code flow (e.g. based on index)"""
        proj = self.page.get("proj_field")
        return proj

    def get_obj_type(self, lang="de") -> str:
        """returns obj type for presentation, pass Es field for "obj_field" """
        obj_type = self.page.get("obj_field")
        return obj_type

    def get_title(self) -> str:
        """returns title for presentation, pass Es field for "title_field" """
        title = self.page.get("title_field")
        return title

    def get_preview_image(self) -> str:
        """returns url for preview image in snippet view"""
        link2img = self.page.get("imgurl_field")
        return link2img

    def get_manifest_id(self) -> str:
        """return path to manifest without http-Domain,
        e.g. "/".join(["dizas", zas_id, "manifest"])"""
        manif_id = self.page.get("manifid_field")
        return manif_id

    def get_viewer(self) -> dict:
        """define which viewer shall be shown in detail page, refers to key in documentViewer Frontend Configuration
        use case: for AV material only use Viewer x, for Images only y, ...
        e.g. {"viewer": ["MiradorSinglePage", "UV"]}"""
        return {"viewer":[None]}

    def add_snippet_link(self) -> dict:
        """add link to snippet (instead of detail page), e.g. swisscovery link
        format: {"url": "", "label": ""}
        """
        link = self.page.get("link_field")
        return link

    def add_snippet_fulltext(self) -> str:
        """add expandabel fulltext to snippet (instead of detail page), e.g. abstract
        """
        fulltext = self.page.get("fulltext_field")
        return fulltext