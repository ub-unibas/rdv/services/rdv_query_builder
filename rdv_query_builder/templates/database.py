from rdv_query_builder import RDVDatabase

class ProjDatabase(RDVDatabase):
    """class to define database access/storage"""
    # name of the mongo db database
    db_name = "mongo_dbname"
    # needs to be defined in every subclass, so it is not inherited
    initialized = {}

    def __init__(self, **kwargs):
        """
        :param obj:
        :param obj_id:
        :param obj_type:
        :param old_obj:
        :param db_host:
        """
        super().__init__(**kwargs)

    def get_obj_type(self, lang="de") -> str:
        """returns obj type for presentation, pass Es field for "obj_field"
         see also RDVView class
         """
        obj_type = self.page.get("obj_field")
        return obj_type