from rdv_query_builder import RDVForm, RDVView

class ProjForm(RDVForm):
    """class to define lookup functions for forms"""
    # facet search endpoint, needs to be defined as format template,
    # e.g. "{}v1/rdv_query/simple_facet_search/zas_int/"
    facet_search_endpoint = ""
    # lookup file for form definition (same as view definition), needs to be defined, including config folder,
    # e.g.: "{}/ZAS Vocab.xlsx".format(RDVView.config_folder)
    lookup_file = ""

    def __init__(self, **kwargs):
        """
        :param host:
        :param es_host:
        :param index:
        """
        super().__init__(**kwargs)
        # field name and lookup function for each field is defined
        self.field_service = {
            "fuv": self.search_fuv_gnd,
        }

    def default_lookup_func(self):
        """lookup function if no other function is defined"""
        return self.search_varia

    def search_varia(self, query="", field=""):
        results = self.query_facets(query, field="{}.keyword".format(field))
        return results

    def search_fuv_gnd(self, query=""):
        """
        example how to narrow down lobid gnd search
        :param query:
        :return:
        """
        params = {"q": "type:CorporateBody AND preferredName:{}".format(query),
                  "format": "json:preferredName",
                  "size": self.size}
        field = "descr_fuv.id.keyword"
        suggest = {"format": "json:suggest"}
        return self.search_gnd(query, params, field, suggest)