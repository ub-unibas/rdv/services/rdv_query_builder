FROM index.docker.io/library/python:3.8

ARG K8S_VERSION=v1.20.4
ARG HELM_VERSION=v3.6.3

ENV FLASK_APP rdv_query_builder
ENV MODE k8s

WORKDIR /
RUN mkdir configs
ADD setup.py /
ADD uwsgi_rdv.py /
ADD requirements.txt /
ADD helm-charts/configmap/test/gunicorn.conf.py /configs/

ADD rdv_query_builder /rdv_query_builder
ADD rdv_cfg /rdv_cfg
ADD rdv_apis /rdv_apis
ADD rdv_view_configs /rdv_view_configs
ADD test_rdv /test_rdv

RUN python -m pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

WORKDIR /

EXPOSE 5000
#ENTRYPOINT ["bash"]
ENTRYPOINT ["gunicorn"]
CMD ["uwsgi_rdv:rdv_query_app", "--config", "/configs/gunicorn.conf.py"]
