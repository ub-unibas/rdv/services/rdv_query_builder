FROM python:3.8

ARG K8S_VERSION=v1.21.5
ARG HELM_VERSION=v3.7.0

ENV FLASK_APP rdv_query_builder
ENV MODE k8s

WORKDIR /
RUN mkdir configs
RUN mkdir app
ADD setup.py /app
ADD requirements.txt /app

WORKDIR /app


RUN python -m pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

WORKDIR /

ADD helm-charts/configmap/test/gunicorn.conf.py /configs/

ADD uwsgi_rdv.py /app
ADD rdv_query_builder app/rdv_query_builder
ADD rdv_apis app/rdv_apis
ADD test_rdv app/test_rdv



WORKDIR /app

EXPOSE 5000
ENTRYPOINT ["gunicorn"]
CMD ["uwsgi_rdv:rdv_query_app", "--config", "/configs/gunicorn.conf.py"]
